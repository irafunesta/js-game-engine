MG = {
	mapW: 640,
	mapH: 480,
	tile_w: 32,
	tile_h: 32,
	maps: [],
	current_level : 0,
	current_level_node: null,
	current_tile: 0,
	isTestMode: false
};

const EntityNamesMap = {
	"Player":0,
	"Wall":1,
	"Box":2,
	"Target_Player":3,
	"Target_Box":4,
	"Gate":5,
	"Key":6 
}

//Mapping of id to Entity or sprite
const MapEntity = {
	[EntityNamesMap.Player]: (map_data, father) => { //Player
		//Create the player
		player.ref = Scene.AddSprite({
			texture: "pl_right",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:EntityNamesMap.Player
		}, father.get_path());
	},
	[EntityNamesMap.Wall]: (map_data, father) => { //Walls
		//Create the wall
		walls.push(Scene.AddRect({
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			color: "grey",
			tile_id:EntityNamesMap.Wall
		}, father.get_path()));
	},
	[EntityNamesMap.Box]: (map_data, father) => { //Boxes
		//Create the wall
		boxes.push(Scene.AddRect({
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			color: "green",
			tile_id:EntityNamesMap.Box
		}, father.get_path()));
	},
	[EntityNamesMap.Target_Player]: (map_data, father) => { //Player target
		//Create the player target
		player_target = Scene.AddSprite({
			texture: "player_target",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:EntityNamesMap.Target_Player,
		}, father.get_path());

		player_target.order = -1;
	},
	[EntityNamesMap.Target_Box]: (map_data, father) => { //Boxes target
		//Create the box target
		let tmp = Scene.AddSprite({
			texture: "box_target",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:EntityNamesMap.Target_Box
		}, father.get_path());
		tmp.order = -1;
		box_targets.push(tmp);
	},
	[EntityNamesMap.Gate]: (map_data, father) => { //Gate
		//Create a gate
		let tmp = Scene.AddSprite({
			texture: "gate",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:EntityNamesMap.Gate
		}, father.get_path());
		gates.push(tmp);
	},
	[EntityNamesMap.Key]: (map_data, father) => { //Keys
		//Create a key
		let tmp = Scene.AddSprite({
			texture: "key",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:EntityNamesMap.Key
		}, father.get_path());
		keys.push(tmp);
	},
}

const MapEditorAdd = {
	[EntityNamesMap.Player]: "Player",
	[EntityNamesMap.Wall]: "Wall",
	[EntityNamesMap.Box]: "Box",
	[EntityNamesMap.Target_Player]: "Target_Player",
	[EntityNamesMap.Target_Box]: "Target_Box",
	[EntityNamesMap.Gate]: "Gate",
	[EntityNamesMap.Key]: "Key"
}

const MapEditorRemove = {
	[EntityNamesMap.Player]: () => {},
	[EntityNamesMap.Wall]: (x, y) => { //Wall
		let id = walls.findIndex((wall) => {
			return wall.data.x === x && wall.data.y === y;
		})

		if(id >= 0) {
			walls.splice(id, 1);
		}
	},
	[EntityNamesMap.Box]: (x, y) => { //Boxes
		let id = boxes.findIndex((box) => {
			return box.data.x === x && box.data.y === y;
		})

		if(id >= 0) {
			boxes.splice(id, 1);
		}
	},
	[EntityNamesMap.Target_Player]: () => {},
	[EntityNamesMap.Target_Box]: (x, y) => { //Targets_boxes
		let id = box_targets.findIndex((target) => {
			return target.data.x === x && target.data.y === y;
		})

		if(id >= 0) {
			box_targets.splice(id, 1);
		}
	},
	[EntityNamesMap.Gate]: (x, y) => { //Gates
		let id = gates.findIndex((gate) => {
			return gate.data.x === x && gate.data.y === y;
		})

		if(id >= 0) {
			gates.splice(id, 1);
		}
	},
	[EntityNamesMap.Key]: (x, y) => { // Keys
		let id = keys.findIndex((key) => {
			return key.data.x === x && key.data.y === y;
		})

		if(id >= 0) {
			keys.splice(id, 1);
		}
	},
}

const MapEditorDraw = {
	[EntityNamesMap.Player]: (x, y) => { //player
		Scene.DrawSprite({
			texture: "pl_right",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	[EntityNamesMap.Wall]: (x, y) => { //"Wall",
		Scene.DrawRect({
			color: "grey",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	[EntityNamesMap.Box]:  (x, y, w, h) => { //"Box",
		Scene.DrawRect({
			color: "green",
			x: x,
			y: y,
			w: w || MG.tile_w,
			h: h || MG.tile_h
		})
	},
	[EntityNamesMap.Target_Player]: (x, y) => { //"Target Player",
		Scene.DrawSprite({
			texture: "player_target",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	}, 
	[EntityNamesMap.Target_Box]: (x, y) => { //"Target Box",
		Scene.DrawSprite({
			texture: "box_target",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	[EntityNamesMap.Gate]: (x, y) => { //"Gate",
		Scene.DrawSprite({
			texture: "gate",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	[EntityNamesMap.Key]: (x, y, w, h) => { //"Key",
		Scene.DrawSprite({
			texture: "key",
			x: x,
			y: y,
			w: w || MG.tile_w,
			h: h || MG.tile_h
		})
	},
}

//Alias
const Scene = GSE.Scene;

let player = {
	ref: undefined,
	dir: { x: 1, y: 0 },
	can_shoot: true,
	box_held: undefined
}

let walls = [];
let boxes = [];
let box_targets = [];
let gates = [];
let player_target = null;

let bullet = {
	ref: undefined,
	speed: 450,
	dir: { x: 1, y: 0 }
};

; (function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();

function InitGame(ctx) {
	Scene.useRoot(true);
	// Scene.debug_draw = true;

	GSE.Scene.LoadTexture("pl_left", "./images/pl_left.png");
	GSE.Scene.LoadTexture("pl_right", "./images/pl_right.png");
	GSE.Scene.LoadTexture("pl_up", "./images/pl_up.png");
	GSE.Scene.LoadTexture("pl_down", "./images/pl_down.png");
	GSE.Scene.LoadTexture("box_target", "./images/box_target.png");
	GSE.Scene.LoadTexture("player_target", "./images/player_target.png");
	GSE.Scene.LoadTexture("gate", "./images/gate.png");
	GSE.Scene.LoadTexture("key", "./images/key.png");

	//TODO manage states
	GSE.StateManager.createState("Game", UpdateGame, RenderGame);
	GSE.StateManager.createState("Editor", UpdateEditor, RenderEditor);

	GSE.StateManager.setCurrentState("Game");

	//Fills maps
	MG.maps = Object.keys(levels);

	//Create level from map
	LoadLevel(MG.current_level);

	bullet.ref = Scene.AddRect({
		x: (32 * 6), y: (32 * 7), w: 8, h: 8, color: "red",
		hidden: true
	})

	console.log("player displace x:", ((bullet.dir.x * -1) * 32));
}

function LoadLevel(id, current_level_node)
{
	let map_to_load = MG.maps[id];

	console.log("Load map:", map_to_load);

	//Clear all arrays
	walls = [];
	boxes = [];
	box_targets = [];
	gates = [];
	keys = [];

	player.dir = {x:1, y:0};

	if(current_level_node) {
		//clean current level
		Scene.RemoveNode(current_level_node.get_path());
	}

	MG.current_level_node = Scene.CreateNode({ id: map_to_load });
	MG.current_level = id;
	Scene.root.add_child(MG.current_level_node);

	let level = levels[map_to_load];
	if(level)
	{
		for (let entity of level.entities) {
			//call the correct creation fun
			if (MapEntity[entity.id]) {
				MapEntity[entity.id](entity, MG.current_level_node);
			}
		}
	}
}

function Update(lastTick, delta) {
	
}

function Render(delta) {
	
}

function isPlayerOnTarget()
{
	if(!player_target) {
		return;
	}
	return GSE.Collisions.CheckCollision(player.ref.data, player_target.data);
}

function areBoxesOnTarget()
{
	let all_target_full = true;
	for (let box of boxes)
	{
		let target = box_targets.find((target) => {
			return target.data.x == box.data.x && target.data.y === box.data.y;
		});

		if(!target)
		{
			all_target_full = false;
		}
	}

	return all_target_full;
}

function UpdateGame(lastTick, delta)
{
	//Win condition
	// console.log("isPlayerOnTarget()", isPlayerOnTarget());
	// console.log("areBoxesOnTarget()", areBoxesOnTarget());
	if(isPlayerOnTarget() && areBoxesOnTarget())
	{
		Scene.DrawText({
			x: MG.mapW / 2, 
			y: MG.mapH / 2,
			color:"white",
			align:"center",
			font: "Serif",
			size: "20",
			text: "LEVEL COMPLETE",
			id:"win_text"
		});

		//Load next level
		console.log("Load next level");
		if(!MG.isTestMode) {
			MG.current_level += 1;
			MG.current_level %= MG.maps.length;
		}

		console.log("MG.current_level:", MG.current_level);
		console.log("level name:", MG.maps[MG.current_level]);

		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//move bullet
	//TODO make shorter way to reach x,y
	bullet.ref.data.x += bullet.dir.x * bullet.speed * delta;
	bullet.ref.data.y += bullet.dir.y * bullet.speed * delta;

	//Walls stops bullets
	for (let wall of walls) {
		if (!bullet.ref.data.hidden && GSE.Collisions.CheckCollision(bullet.ref.data, wall.data)) {
			
			console.log("Hit a wall");

			let new_space = {
				x: wall.data.x + ((bullet.dir.x * -1) * 32),
				y: wall.data.y + ((bullet.dir.y * -1) * 32),
				w: MG.tile_w,
				h: MG.tile_h
			};

			//is free ?
			if (!GSE.Collisions.CheckCollision(player.ref.data, new_space)) {

				if (player.box_held) {
					//place the held box
					//is space is free
					console.log("place the box");
					console.log("place the box:", player.box_held.data.tile_id);
					player.box_held.data.hidden = false;
					player.box_held.data.x = new_space.x;
					player.box_held.data.y = new_space.y;
					player.box_held = undefined;

				}
				else {
					//move the player here (wall position 1 square before)
					console.log("move the player");

					player.ref.data.x = new_space.x;
					player.ref.data.y = new_space.y;
				}
			}

			//Disable the bullet
			DisableBullet(bullet);
			player.can_shoot = true;
		}
	}

	//Boxes get held by the player if bullets hits them
	for (let box of boxes) {
		if (!bullet.ref.data.hidden && GSE.Collisions.CheckCollision(bullet.ref.data, box.data)) {

			console.log("Hit a box");
			//Disable the bullet
			DisableBullet(bullet);
			player.can_shoot = true;

			if (player.box_held) {
				//Place the box
				console.log("place the box")
				player.box_held.data.hidden = false;
				player.box_held.data.x = box.data.x + ((bullet.dir.x * -1) * 32)
				player.box_held.data.y = box.data.y + ((bullet.dir.y * -1) * 32)
				player.box_held = undefined;
			}
			else {
				//Hold the box
				console.log("hold the box")
				box.data.hidden = true;
				box.data.x = 0;
				box.data.y = 0;

				player.box_held = box;
			}
		}
	}

	//Gates are like walls except if the player has a key
	for (let gate of gates) {
		if (!bullet.ref.data.hidden && GSE.Collisions.CheckCollision(bullet.ref.data, gate.data)) {
			// let bounce = BounceFromWall(player.ref, gate);
			console.log("Hit a gate");
			let new_space = {
				x: gate.data.x + ((bullet.dir.x * -1) * 32),
				y: gate.data.y + ((bullet.dir.y * -1) * 32),
				w: MG.tile_w,
				h: MG.tile_h
			};

			//is free ? Prevent player from placing a box on it self
			//But ignore if it is a gate, and has the key
			if (player.box_held) 
			{
				//place the held box
				//is space is free
				console.log("place the box");
				console.log("place the box:", player.box_held.data.tile_id);
				if(player.box_held.data.tile_id === EntityNamesMap.Box) //Box
				{
					if (!GSE.Collisions.CheckCollision(player.ref.data, new_space)) {
						player.box_held.data.hidden = false;
						player.box_held.data.x = new_space.x;
						player.box_held.data.y = new_space.y;
						player.box_held = undefined;
					}
				}

				//But if the player hold a key disable the gate
				if(player.box_held.data.tile_id === EntityNamesMap.Key) //Key
				{
					// player.box_held.data.hidden = false;

					//delete the gate from gates
					MapEditorRemove[gate.data.tile_id](gate.data.x, gate.data.y);
					//remove the gate from the scene
					Scene.RemoveNode(gate.get_path());

					player.box_held = undefined;
				}

			}
			else {
				//move the player here (wall position 1 square before)
				console.log("move the player");

				player.ref.data.x = new_space.x;
				player.ref.data.y = new_space.y;
			}
			

			//Disable the bullet
			DisableBullet(bullet);
			player.can_shoot = true;
		}
	}

	//Keys are like boxes
	for (let key of keys) {
		if (!bullet.ref.data.hidden && GSE.Collisions.CheckCollision(bullet.ref.data, key.data)) {

			console.log("Hit a key");
			//Disable the bullet
			DisableBullet(bullet);
			player.can_shoot = true;

			if (player.box_held) {
				//Place the box
				console.log("place the box")
				player.box_held.data.hidden = false;
				player.box_held.data.x = key.data.x + ((bullet.dir.x * -1) * 32)
				player.box_held.data.y = key.data.y + ((bullet.dir.y * -1) * 32)
				player.box_held = undefined;
			}
			else {
				//Hold the key
				console.log("hold the box")
				key.data.hidden = true;
				key.data.x = 0;
				key.data.y = 0;

				player.box_held = key;
			}
		}
	}


	//Bullet out of map
	if ((bullet.ref.data.x < 0 || bullet.ref.data.x > MG.mapW) ||
		(bullet.ref.data.y < 0 || bullet.ref.data.y > MG.mapH)) {
		DisableBullet(bullet);
		player.can_shoot = true;
	}

	//Shoot
	if (GSE.Input.IsKeyPressed("space")) {
		if (player.can_shoot) {
			//Shoot bullet
			ShootBullet(bullet, player);
			player.can_shoot = false;
		}
	}

	//Change player dir
	if (GSE.Input.IsKeyPressed("d") || GSE.Input.IsKeyPressed("right")) {
		player.dir = { x: 1, y: 0 };
		player.ref.data.texture = "pl_right";
	}
	else if (GSE.Input.IsKeyPressed("a") || GSE.Input.IsKeyPressed("left")) {
		player.dir = { x: -1, y: 0 };
		player.ref.data.texture = "pl_left";
	}
	else if (GSE.Input.IsKeyPressed("w") || GSE.Input.IsKeyPressed("up")) {
		player.dir = { x: 0, y: -1 };
		player.ref.data.texture = "pl_up";
	}
	else if (GSE.Input.IsKeyPressed("s") || GSE.Input.IsKeyPressed("down")) {
		player.dir = { x: 0, y: 1 };
		player.ref.data.texture = "pl_down";
	}

	//Switch to editor
	if (GSE.Input.IsKeyPressed("p")) {
		console.log("Change state to Editor")
		GSE.StateManager.setCurrentState("Editor");
	}

	//Reload the map
	if(GSE.Input.IsKeyPressed("r"))
	{
		console.log("reload map");
		console.log("MG.current_level:", MG.current_level);
		
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Toggle test mode
	if(GSE.Input.IsKeyPressed("t"))
	{
		console.log("Toggle test mode");
		MG.isTestMode = !MG.isTestMode;
	}
}

function RenderGame(delta)
{
	Scene.DrawText({
		x:16, 
		y:20,
		color:"violet",
		align:"left",
		font: "Serif",
		size: "20",
		text: "Comandi:"
	});

	for(let i =0; i< game_commands.length; i++) {
		Scene.DrawText({
			x:16, 
			y:40 + ((16 + 8) *i),
			color:"violet",
			align:"left",
			font: "Serif",
			size: "20",
			text: game_commands[i] 
		});
	}

	if(MG.isTestMode) {
		//Draw level name
		Scene.DrawText({
			x:MG.mapW / 2, 
			y:32,
			color:"white",
			align:"center",
			font: "Serif",
			size: "16",
			text: "TEST MODE"
		});
	}

	if(player.box_held) {
		MapEditorDraw[player.box_held.data.tile_id](player.ref.data.x, player.ref.data.y, MG.tile_w / 2, MG.tile_h / 2);
	}
}

function UpdateEditor(delta)
{
	//Switch to game
	if (GSE.Input.IsKeyPressed("p")) {
		console.log("Change state to Game");
		//Reload the level to fix the walls and boxes array
		// LoadLevel(MG.current_level, MG.current_level_node);
		GSE.StateManager.setCurrentState("Game");
	}

	//Enter test mode from the editor
	if (GSE.Input.IsKeyPressed("t")) {
		console.log("Change state to Game and enter test mode");
		//Reload the level to fix the walls and boxes array
		// LoadLevel(MG.current_level, MG.current_level_node);
		MG.isTestMode = true;
		GSE.StateManager.setCurrentState("Game");
	}
	

	if (GSE.Input.IsKeyPressed("n")) {
		console.log("Load next level");
		MG.current_level += 1;
		MG.current_level %= MG.maps.length;

		console.log("MG.current_level:", MG.current_level);
		console.log("level name:", MG.maps[MG.current_level]);
		
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	if(GSE.Input.IsMouseLeftPressed())
	{
		let x, y;
		x = Math.floor(GSE.Input.mouse.clientX / MG.tile_w);
		y = Math.floor(GSE.Input.mouse.clientY / MG.tile_h);

		if(x >= 0 && x <= MG.mapW / MG.tile_w && y >= 0 && y <= MG.mapH / MG.tile_h )
		{
			
			if(!MG.open_palette)
			{
				//Place the selected item
				let is_cell_full = levels[MG.maps[MG.current_level]].entities.findIndex((ent) => {
					return x === ent.pos.x && y === ent.pos.y;
				})

				console.log("is_cell_full:", is_cell_full);

				if(is_cell_full < 0) {
					console.log("place a box")

					console.log("MG.current_tile:", typeof MG.current_tile);

					//Check if there are more than 1 player
					if(MG.current_tile == EntityNamesMap.Player) {
						let findPlayer = levels[MG.maps[MG.current_level]].entities.findIndex((ent) => {
							return ent.id == MG.current_tile;
						})

						console.log("findPlayer:", findPlayer);

						if(findPlayer >= 0) {
							console.log("Multiple player on the map");
							alert("Solo un player per map");
							return;
						}
					}

					//Place a Box in the level
					let entity = {
						"id":MG.current_tile,
						"pos":{
							"x":x,
							"y":y
						}
					}
					console.log("entity:", entity);

					

					levels[MG.maps[MG.current_level]].entities.push(entity);
					MapEntity[entity.id](entity, MG.current_level_node);
				}
			}
			else
			{
				//Select the item
				MG.current_tile = MG.palette_sel_tile;
				//Close the palette ?
				MG.open_palette = false;
			}
		}
	}

	if(GSE.Input.IsKeyPressed("q"))
	{
		MG.open_palette = !MG.open_palette;
	}

	if(GSE.Input.IsKeyPressed("d"))
	{
		let x, y;
		x = Math.floor(GSE.Input.mouse.clientX / MG.tile_w);
		y = Math.floor(GSE.Input.mouse.clientY / MG.tile_h);

		if(x >= 0 && x <= MG.mapW / MG.tile_w && y >= 0 && y <= MG.mapH / MG.tile_h )
		{
			console.log("remove a tile")
			console.log("MG.current_level_node:", MG.current_level_node);

			//Find the node
			let node_selected = MG.current_level_node.child.find((node) => {

				let full_x = x*32, full_y = y * 32;

				return full_x === node.data.x && full_y === node.data.y;
			});

			if(node_selected) 
			{
				console.log("Tile found:", node_selected.data);
				let index = levels[MG.maps[MG.current_level]].entities.findIndex((ent) => {

					return x === ent.pos.x && y === ent.pos.y;
				});

				console.log("index found:", index);	

				//remove node on level
				if(index >= 0) {

					//If it is a box, a wall or a box_target also remove it from the relative list
					
					MapEditorRemove[node_selected.data.tile_id](node_selected.data.x, node_selected.data.y);
					levels[MG.maps[MG.current_level]].entities.splice(index, 1);
					Scene.RemoveNode(node_selected.get_path());
				}
			}
		}
	}

	//Create a new level
	if(GSE.Input.IsKeyPressed("u"))
	{
		let new_name = prompt("Nome del nuovo livello ?", "nuovo");
		let new_id = MG.maps.push(new_name);
		
		levels[new_name] = {
			"entities":[]
		}
		//TODO this doesn't load the new level
		LoadLevel(new_id-1, MG.current_level_node);
	}

	//Save the map
	if(GSE.Input.IsKeyPressed("s"))
	{
		console.log("save map ?");
		let mapData = ["const levels =" + JSON.stringify(levels) + ";"];
		GSE.SaveMap("levels.js", mapData);
	}

	//Reload the map
	if(GSE.Input.IsKeyPressed("r"))
	{
		console.log("reload map");
		console.log("MG.current_level:", MG.current_level);
		
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Slide all the map up
	if(GSE.Input.IsKeyPressed("up"))
	{
		console.log("transalte up");
		console.log("MG.current_level:", MG.current_level);

		SlideLevel({x:0, y:-1});
		LoadLevel(MG.current_level, MG.current_level_node);
	}
	
	//Slide all the map right
	if(GSE.Input.IsKeyPressed("right"))
	{
		console.log("transalte right");

		SlideLevel({x:1, y:0});
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Slide all the map left
	if(GSE.Input.IsKeyPressed("left"))
	{
		console.log("transalte left");

		SlideLevel({x:-1, y:0});
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Slide all the map down
	if(GSE.Input.IsKeyPressed("down"))
	{
		SlideLevel({x:0, y:1});	
		LoadLevel(MG.current_level, MG.current_level_node);
	}
}

const editor_commands = [
	"p:Exit",
	"u:New level",
	"n:cycle level",
	"q:Palette",
	"r:reset",
	"s:save all"
]

const game_commands = [
	"Frecce/WASD = Routa personaggio",
	"Spazio = Spara",
	"R = Ricarica livello"
]

function RenderEditor(delta)
{
	GSE.drawGrid(MG.tile_w, MG.tile_h, "green", false)

	let x, y;
		x = Math.floor(GSE.Input.mouse.clientX / MG.tile_w);
		y = Math.floor(GSE.Input.mouse.clientY / MG.tile_h);
	let string = x + ", " + y;
	let string2 = GSE.Input.mouse.clientX + ", " + GSE.Input.mouse.clientY;

	let mouse_rect = Rect(GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, MG.tile_w, MG.tile_h);

	Scene.DrawText({
        x:GSE.Input.mouse.clientX, 
        y:GSE.Input.mouse.clientY,
        color:"white",
        align:"left",
        font: "Serif",
        size: "12",
		text: string,
		id:"Coords"
    });

	//Draw level name
	Scene.DrawText({
		x:MG.mapW / 2, 
		y:32,
		color:"white",
		align:"center",
		font: "Serif",
		size: "16",
		text: MG.maps[MG.current_level]
	});

	Scene.DrawText({
		x:16, 
		y:16,
		color:"white",
		align:"left",
		font: "Serif",
		size: "16",
		text: "Commands:"
	});

	for(let i =0; i< editor_commands.length; i++) {
		Scene.DrawText({
			x:16, 
			y:32 + (16*i),
			color:"white",
			align:"left",
			font: "Serif",
			size: "16",
			text: editor_commands[i] 
		});
	}
	
	if(MG.open_palette) 
	{
		let start_x = 64 + 32, start_y = 320, i=0;

		//Palette background
		Scene.DrawRect({
			color: "Black",
			x: start_x - 32,
			y: start_y - 32,
			w: MG.tile_w * 15,
			h: MG.tile_h * 3
		})

		for(let key of Object.keys(MapEditorAdd)) 
		{
			let object_rect = Rect(start_x + (i*64), start_y, MG.tile_w, MG.tile_h);
			if(GSE.Collisions.CheckCollision(mouse_rect, object_rect))
			{
				Scene.DrawRect({
					color: "yellow",
					x: (start_x + (i*64)) - 4,
					y: start_y - 4,
					w: MG.tile_w + 8,
					h: MG.tile_h + 8
				})

				MG.palette_sel_tile = Number(key); //Why do we need a number ? don't know but works
			}

			MapEditorDraw[key](start_x + (i*64),start_y);
			i++;
		}
	}

	//Draw Button new level
	Scene.DrawRect({
		color: "Black",
		x: MG.tile_w * 4,
		y: MG.tile_h * 0,
		w: MG.tile_w * 3,
		h: MG.tile_h * 1
	});

	Scene.DrawText({
        x:MG.tile_w * 4, 
        y:MG.tile_h * 1,
        color:"white",
        align:"left",
        font: "Serif",
        size: "16",
		text: "New level : U",
		id:"Coords"
    });
}

function Rect(x, y, w, h)
{
	return {
		x:x, y:y, w:w, h:h}
}

function BounceFromWall(player, wall, multiplier = 1) {
	x_diff = 0;
	y_diff = 0;

	//fix X 
	//Case 1 if Px < Wx
	if (player.data.x < wall.data.x) {
		x_diff = (player.data.x + player.data.w) - wall.data.x;
		x_diff *= -1;
	}
	else if (player.data.x > wall.data.x) {
		x_diff = (wall.data.x + wall.data.w) - player.data.x;
	}
	//fix Y
	//Player over
	if (player.data.y < wall.data.y) {
		y_diff = (player.data.y + player.data.w) - wall.data.y;
		y_diff *= -1;
	}
	else if (player.data.y > wall.data.y) {
		y_diff = (wall.data.y + wall.data.w) - player.data.y;
	}

	let x_diff_abs = Math.abs(x_diff);
	let y_diff_abs = Math.abs(y_diff);

	let apply_x = 0;
	let apply_y = 0;

	let amount = 0;

	//extract x_diff
	if (x_diff_abs > 0 && y_diff_abs > 0) { //Both not 0 , use the lower
		if (x_diff_abs < y_diff_abs) {
			apply_x = 1;
			amount = x_diff;
		}
		else {
			apply_y = 1;
			amount = y_diff;
		}
	}
	else if (x_diff_abs > 0 && y_diff_abs == 0) { //Apply X
		apply_x = 1;
		amount = x_diff;
	}
	else if (y_diff_abs > 0 && x_diff_abs == 0) { //Apply Y
		apply_y = 1;
		amount = y_diff;
	}

	console.log("collision amount:", amount);
	console.log("collision multiplier:", multiplier);

	//I want a consistent bounce 
	// The overlap need to be different, 

	//Align to rect
	// player.data.x += amount * apply_x
	// player.data.y += amount * apply_y

	//Bounce the amount of pixel
	// if(multiplier > 1) {
	// 	player.data.x += multiplier * Math.sign(amount) * apply_x;
	// 	player.data.y += multiplier * Math.sign(amount) * apply_y;
	// }

	let x_val = amount * apply_x * multiplier;
	let y_val = amount * apply_y * multiplier;

	if (multiplier > 1) {
		x_val += multiplier * Math.sign(amount) * apply_x
		y_val += multiplier * Math.sign(amount) * apply_y
	}

	return {
		x: x_val,
		y: y_val
	}
}

function DisableBullet(bullet) {
	bullet.ref.data.hidden = true;
	bullet.dir = { x: 0, y: 0 };
	bullet.ref.data.x = 0;
	bullet.ref.data.y = 0;
}

function ShootBullet(bullet, player) {
	bullet.ref.data.hidden = false;
	bullet.ref.data.x = player.ref.data.x + player.ref.data.w / 2;
	bullet.ref.data.y = player.ref.data.y + player.ref.data.h / 2;
	bullet.dir = player.dir;
}

function TestDeleteNode()
{
	let path = "/to_be_deleted";
	Scene.RemoveNode(path);
}

function SlideLevel(dir) {
	let map_to_load = MG.maps[MG.current_level];

	console.log("Load map:", map_to_load);

	let level = levels[map_to_load];
	for (let entity of level.entities) {
		//Move everything up
		entity.pos.x += dir.x;
		entity.pos.y += dir.y;
	}
}
