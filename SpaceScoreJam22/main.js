MG = {
	mapW: 480 * 1.5,
	mapH: 640 * 1.5,
	tile_w: 32 * 1.5,
	tile_h: 32 * 1.5,
	paused: false
};

//Alias
const Scene = GSE.Scene;

let player = {
	ref: null,
	lives : 4,
	attack_time: 0.5,
	state : "move",
	last_direction : {x:0, y:1},
	anim :{
		frames : 2,
		current_frame : 0,
		frame_speed : 0.3,
		frame_time : 0,
		anim_run : false
	}
}

let player_speed = 128;
let player_lives = 4;

let something = 100;

let walls = [];
let pickups = [];
let enemies = [];
let fast_enemies = [];
let highscores = [];
let max_saved_scores = 3;
let max_enemies = 10;
let max_pickups = 10;

let pickup_timer;

let enemy_timer;
let fast_enemy_timer;

let score = 0;

let over_stop = 0;
let overstop_threshold = 2;

let border = {
	x: MG.tile_w,
	y: MG.tile_h,
	w: MG.mapW - MG.tile_w * 2,
	h: MG.mapH - MG.tile_h * 2
}

let sea = {}

let corners = [
	{x: border.x, y: border.y}, //top left
	{x: border.w , y: border.y}, //top right
	{x: border.x, y: border.h}, //bottom left
	{x: border.w, y: border.h} //bottom right
]

let start_button_rect = {
	x: MG.mapW / 2 - MG.tile_w * 2 + 16,
	y: sea.y + MG.tile_h * 3,
	w: 158 ,
	h: 60
}

let restart_button_rect = {
	x: MG.mapW / 2 - MG.tile_w * 2 + 16,
	y: sea.y + MG.tile_h * 3,
	w: 158 ,
	h: 60
}

let fix_game_over_x = 0;

let credits = [
	"Programming by Irafunesta",
	"Art by y_benjamin",
	"\"Splash, Jumping, D.wav\" by InspectorJ (www.jshaw.co.uk) of Freesound.org",
	"UX by x",
]

let fonts = {
	'scores':'Sunflower-Light',
	'scores_bold':'Sunflower-Bold',
	'first-snow':'first-snow',
	'frostbitten-wanker':'frostbitten-wanker',
	'ice-and-snow':'ice-and-snow',
	'polar-vortex':'polar-vortex'
}

let isMobile = false;

function PickUp(sprite, amount)
{
	return {
		'ref': sprite,
		'amount': amount,
		'active':false
	}
}

let color_palete = {
	'white': "#F2F2F2",
	'light_grey': "#808080",
	'dark_grey': "#424242",
	'ice': "#4cf1e7",
	'light_blue': "#cce9f6",
	'black':"black",
	
}


//simulate an ipulse, 
/*
	Always calculate the target dir, 
	after a timer, 
	the speed goes high, direction is locked
	and slowly falls to zero, 
	then restart
*/
function Enemy(sprite, life, speed, tL, tR)
{
	return {
		'ref': sprite,
		'texture_l': tL,
		'texture_r': tR,
		'life': life,
		'speed': speed || 64,
		'max_speed': speed,
		'active': false,
		'move_timer': 0,
		'move_time' : 5,
		'moving':false,
		'speed_reduction': 96,
		'target_dir': {x:0, y:0},
		'anim' :{
			frames : 2,
			current_frame : 0,
			frame_speed : 0.3,
			frame_time : 0,
			anim_run : false
		}
	}
}

;(function () 
{
	//change screen size based on mobile or not
	isMobile = ('ontouchstart' in document.documentElement && navigator.userAgent.match(/Mobi/));

	let fix_button_y = 0;
	let fix_button_y_restart = 0;

	if(isMobile === false)
	{
		console.log("On desktop")
		MG.mapW = MG.tile_w * 21
		MG.mapH = MG.tile_h * 16

		fix_button_y = MG.tile_h * 3;
		fix_button_y_restart = MG.tile_h * 5.2
	}
	else
	{
		MG.mapW = MG.tile_w * 16
		MG.mapH = MG.tile_h * 21

		fix_button_y = MG.tile_h * 4;
		fix_button_y_restart = MG.tile_h * 5.5
		fix_game_over_x = -MG.tile_h * 2.5
	}

	border = {
		x: MG.tile_w * 2,
		y: MG.tile_h * 2,
		w: MG.mapW - MG.tile_w * 4,
		h: MG.mapH - MG.tile_h * 4
	}

	sea = {
		x: MG.tile_w,
		y: MG.tile_h,
		w: MG.mapW - MG.tile_w * 2,
		h: MG.mapH - MG.tile_h * 2
	}

	corners = [
		{x: border.x, y: border.y}, //top left
		{x: border.w , y: border.y}, //top right
		{x: border.x, y: border.h}, //bottom left
		{x: border.w, y: border.h} //bottom right
	]

	start_button_rect = {
		x: MG.mapW / 2 - MG.tile_w * 2 + 16,
		y: sea.y + fix_button_y,
		w: 158 ,
		h: 60
	}

	restart_button_rect = {
		x: MG.mapW / 2 - MG.tile_w * 2 + 16,
		y: sea.y + fix_button_y_restart,
		w: 158 ,
		h: 60
	}
	
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, color_palete.light_blue);
})();

function InitGame(ctx)
{
	Scene.useRoot(true);

	GSE.Scene.LoadTexture("player_walk", "./img/player.png");
	GSE.Scene.LoadTexture("player_right", "./img/player_right.png");

	GSE.Scene.LoadTexture("seal_right", "./img/seal_right.png");
	GSE.Scene.LoadTexture("seal_left", "./img/seal_left.png");
	
	GSE.Scene.LoadTexture("penguin", "./img/penguin.png");

	GSE.Scene.LoadTexture("objects", "./img/collectiblaes_sheet.png");
	GSE.Scene.LoadTexture("logo", "./img/logo.png");
	GSE.Scene.LoadTexture("logo_dark", "./img/logo_dark.png");


	GSE.Scene.LoadTexture("start_mobile", "./img/start_mobile.png");
	GSE.Scene.LoadTexture("start_desktop", "./img/start_desktop.png");

	GSE.Scene.LoadTexture("button", "./img/button.png");


	GSE.Scene.CreateSound("splash", "./sounds/splash.wav");

	GSE.StateManager.createState("Game", UpdateGame, RenderGame);
	GSE.StateManager.createState("Menu", UpdateMenu, RenderMenu);
	GSE.StateManager.createState("GameOver", UpdateGameOver, RenderGameOver);

	GSE.StateManager.createState("Pause", UpdatePause, RenderPause);

	GSE.StateManager.setCurrentState("Menu");

	player.ref = Scene.AddSpriteExt({
		texture: "player_walk",
		x: MG.tile_w * 10,
		y: MG.tile_h * 5,
		w: MG.tile_w,
		h: MG.tile_h,
		sx:0, sy:0, sw:64, sh:64,
		tile_id:0,
		id:"player"
	});

	let saved_score = localStorage.getItem("PDshs") || "";
	
	if(saved_score && saved_score !== "")
	{	
		let arr = saved_score.split("|");
		for(score of arr)
		{
			let num = score.split(";");

			if(num && num.length > 0)
			{
				let score_obj = {
					name: num[0],
					score: Number(num[1])
				}

				highscores.push(score_obj);
			}
		}
	}

	for(let i=0;i<max_pickups;i++)
	{
		let image = Math.floor(GSE.randomRange(0,3));
		let cols = 4
		let icon_hor_space, icon_vert_space;
		icon_hor_space = 0;
		icon_vert_space = 0;

		//Use the tile size of the image istead fo the map
		let sx = Math.floor(image%cols) * (64 + icon_hor_space);
        let sy = Math.floor(image/cols) * (64 + icon_vert_space);

		let pos_x = GSE.randomRange(border.x, border.x + border.w);

		let pos_y = GSE.randomRange(border.y, border.y + border.h);

		let sprite = Scene.AddSpriteExt({
			texture: "objects",
			x: pos_x, //pos
			y: pos_y,
			w: MG.tile_w,
			h: MG.tile_h,
			sx:sx, sy:sy, sw:64, sh:64,
			tile_id:0,
			id:"other",
			hidden : true
		});
		pickups.push(PickUp(sprite, 20))
	}

	for(let i=0; i<max_enemies; i++)
	{
		let sprite = Scene.AddSpriteExt({
			texture: "penguin",
			x: -64, //pos
			y: -64,
			sx:0, sy:0, sw:64, sh:64,
			w: MG.tile_w,
			h: MG.tile_h,
			color: "green"
		});
		enemies.push(Enemy(sprite, 20, 256))
	}

	for(let i=0; i<max_enemies; i++)
	{
		let sprite = Scene.AddSpriteExt({
			texture: "seal_right",
			x: -64, //pos
			y: -64,
			sx:0, sy:0, sw:64, sh:64,
			w: MG.tile_w,
			h: MG.tile_h,
			color: "red"
		});
		fast_enemies.push(Enemy(sprite, 20, 400, "seal_left", "seal_right"))
	}

	score = 0;

	pickup_timer = new GSE.Timer(6, SpawnNewPickup);
	fast_enemy_timer = new GSE.Timer(7, SpawnNewFastEnemy);
	enemy_timer = new GSE.Timer(5, SpawnNewEnemy)
}

function UpdateGame(lastTick, delta)
{
	if(GSE.Input.IsKeyPressed("p")) 
	{
		MG.paused = !MG.paused;
		return;
	}

	if(MG.paused == true)
	{
		return;
	}
	//Get harder when scores goes up
	if(score > 500)
	{
		enemy_threshold = 3;
	}	

	//Freeze to death
	if(something <= 0)
	{
		GameOver();
		return
	}
	something -= 1 * delta;

	// if(GSE.Input.IsKeyDown("p")) 
	// {
	// 	GSE.StateManager.setCurrentState("Pause");
	// 	return;
	// }

	PlayerInput(delta)

	//
	for(let wall of walls) {
		if(GSE.Collisions.CheckCollision(player.ref.data, wall.data)) {
			let bounce = BounceFromWall(player.ref, wall);

			player.ref.data.x += bounce.x;
			player.ref.data.y += bounce.y;
		}
	}

	for(let pickup of pickups) {

		if(pickup.active == false)
		{
			continue;
		}

		if(GSE.Collisions.CheckCollision(player.ref.data, pickup.ref.data)) {
			let bounce = BounceFromWall(player.ref, pickup.ref);

			pickup.ref.data.hidden = true; //Not visible, move to -1, -1
			pickup.ref.data.x = - MG.tile_w;
			pickup.ref.data.y = - MG.tile_h;

			
			//TODO set max amount of something
			something += Math.min(pickup.amount, 100 - something);
			score += pickup.amount
		}
	}

	// Move them, but add a delay , also only active ones
	for(let enemy of enemies)
	{
		if (enemy.active == false)
		{
			continue;
		}

		MoveEnemy(enemy, delta)
	}

	// Move them, but add a delay , also only active ones
	for(let enemy of fast_enemies)
	{
		if (enemy.active == false)
		{
			continue;
		}
		
		AnimateEnemy(enemy, delta)

		MoveEnemy(enemy, delta)
	}

	let all_enemies = enemies.concat(fast_enemies)

	//Collide player enemies
	for(let enemy of all_enemies) {

		if (enemy.active == false)
		{
			continue;
		}

		if(GSE.Collisions.CheckCollision(player.ref.data, enemy.ref.data)) {
			let bounce = BounceFromWall(player.ref, enemy.ref, 2);

			player.ref.data.x += bounce.x;
			player.ref.data.y += bounce.y;
		}
	}

	for(let enemy of fast_enemies) {

		if (enemy.active == false)
		{
			continue;
		}

		if(GSE.Collisions.CheckCollision(player.ref.data, enemy.ref.data)) {
			let bounce = BounceFromWall(player.ref, enemy.ref, 2);

			player.ref.data.x += bounce.x;
			player.ref.data.y += bounce.y;
		}
	}

	

	//collide between them
	for(let enemy of all_enemies) {

		if (enemy.active == false)
		{
			continue;
		}

		for(let other_enemy of all_enemies) 
		{
			if (other_enemy.active == false)
			{
				continue;
			}

			if(GSE.Collisions.CheckCollision(other_enemy.ref.data, enemy.ref.data)) {
				let bounce = BounceFromWall(other_enemy.ref, enemy.ref);

				other_enemy.ref.data.x += bounce.x;
				other_enemy.ref.data.y += bounce.y;
			}
		}
	}

	//Spawn new enemies
	enemy_timer.tick(delta);

	// //Spawn new fast enemies
	fast_enemy_timer.tick(delta);
	
	//Spawn new pickups
	pickup_timer.tick(delta);	
}

function PlayerInput(delta)
{
	if(player.anim.anim_run) {
		if(player.anim.frame_time > player.anim.frame_speed)
		{
			player.anim.current_frame += 1;
			player.anim.current_frame %= player.anim.frames;
			player.anim.frame_time = 0;
		}

		player.anim.frame_time += 1*delta;

		//Need to use the tile fo the sprite not the map
		player.ref.data.sx = player.anim.current_frame * 64;
	}
	else
	{
		player.ref.data.sx = 0;
	}

	let pl_direction = {x:0, y:0};

	if(GSE.Input.IsKeyDown("d") || GSE.Input.IsKeyDown("right")) {
		pl_direction.x = 1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
		player.ref.data.texture = "player_right"
	}
	else if(GSE.Input.IsKeyDown("a") || GSE.Input.IsKeyDown("left")) {
		pl_direction.x = -1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
		player.ref.data.texture = "player_walk"
	}
	else if(GSE.Input.IsKeyDown("w") || GSE.Input.IsKeyDown("up")) {
		pl_direction.y = -1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
	}
	else if(GSE.Input.IsKeyDown("s") || GSE.Input.IsKeyDown("down")) {
		pl_direction.y = 1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
	}
	else
	{
		player.anim.anim_run = false;
	}

	//Click movements
	if(GSE.Input.IsMouseLeftDown())
	{
		player.state = "move"
		player.anim.anim_run = true;

		let deltaX = GSE.Input.mouse.clientX - player.ref.data.x;
		let deltaY = GSE.Input.mouse.clientY - player.ref.data.y;

		angle = Math.atan2(deltaY,deltaX);
		pl_direction.x = Math.cos(angle);
		pl_direction.y = Math.sin(angle);

		if(pl_direction.x > 0 )
		{
			player.ref.data.texture = "player_right"
		}
		if(pl_direction.x < 0)
		{
			player.ref.data.texture = "player_walk"
		}
	}

	//Touch test
	if(GSE.Input.IsTouchDown())
	{
		player.state = "move"
		player.anim.anim_run = true;

		let deltaX = GSE.Input.touch.clientX - player.ref.data.x;
		let deltaY = GSE.Input.touch.clientY - player.ref.data.y;

		angle = Math.atan2(deltaY,deltaX);
		pl_direction.x = Math.cos(angle);
		pl_direction.y = Math.sin(angle);

		if(pl_direction.x > 0 )
		{
			player.ref.data.texture = "player_right"
		}
		if(pl_direction.x < 0)
		{
			player.ref.data.texture = "player_walk"
		}
	}

	// if(GSE.Input.IsKeyDown("enter") || GSE.Input.IsKeyDown("space")) 
	// {
	// 	player.state = "digging"
	// }
	// else
	// {
	// 	player.state = "move"
	// }

	if (player.state === "move") 
	{
		player.ref.data.x += pl_direction.x * player_speed * delta;
		player.ref.data.y += pl_direction.y * player_speed * delta;

		//kill the player
		if(IsOutOfBoundsLoose(player.ref.data, sea))
		{
			GameOver()
			return;
		}
	}
}

function AnimateEnemy(enemy, delta)
{
	if(enemy.anim.anim_run) {
		if(enemy.anim.frame_time > enemy.anim.frame_speed)
		{
			enemy.anim.current_frame += 1;
			enemy.anim.current_frame %= enemy.anim.frames;
			enemy.anim.frame_time = 0;
		}

		enemy.anim.frame_time += 1*delta;

		//Need to use the tile fo the sprite not the map
		enemy.ref.data.sx = enemy.anim.current_frame * 64;
	}
	else
	{
		enemy.ref.data.sx = 0;
	}
}

function MoveEnemy(enemy, delta)
{
	if(enemy.moving == true)
	{
		enemy.anim.anim_run = true;
		//Reduce speed and move in a direction
		enemy.ref.data.x += enemy.target_dir.x * enemy.speed * delta;
		enemy.ref.data.y += enemy.target_dir.y * enemy.speed * delta;

		enemy.speed -= enemy.speed_reduction * delta;

		if(enemy.speed <= 0)
		{
			enemy.moving = false;
			enemy.move_time = 0;
		}
	}
	else
	{
		//Calculate direction and timer

		let enemy_dir = {x:0, y:0}

		let deltaX = player.ref.data.x - enemy.ref.data.x;
		let deltaY = player.ref.data.y - enemy.ref.data.y

		angle = Math.atan2(deltaY,deltaX);
		enemy_dir.x = Math.cos(angle);
		enemy_dir.y = Math.sin(angle);

		enemy.target_dir = enemy_dir;

		if(enemy_dir.x > 0 && enemy.texture_r) 
		{
			enemy.ref.data.texture = enemy.texture_r
		}
		if(enemy_dir.x < 0 && enemy.texture_l) 
		{
			enemy.ref.data.texture = enemy.texture_l
		}

		//TODO animation

		if(IsOutOfBoundsLoose(enemy.ref.data, sea))
		{
			//Despawn enemy
			GSE.Scene.PlaySound("splash", 0.5);
			enemy.active = false;
			score += 50;
		}

		if(enemy.move_time >= enemy.move_timer)
		{
			//Move until speed is zero
			enemy.moving = true;
			enemy.speed = enemy.max_speed;
			enemy.move_time = 0;
		}
		else
		{
			enemy.move_time += 1 * delta
		}
	}
}

function SpawnNewEnemy()
{
	for(let i=0; i<max_enemies; i++)
	{
		let enemy = enemies[i];
		if(enemy.active == false)
		{
			console.log("Activate enemy:", i)
			//TODO Pick a random corner
			enemy.speed = 64

			let corner = Math.floor(GSE.randomRange(0, 3))

			enemy.ref.data.x = corners[corner].x;
			enemy.ref.data.y = corners[corner].y;
			enemy.ref.data.hidden = false;
			enemy.active = true;
			enemy.target_dir = {x:0, y:0}
			break;
		}
	}
}

function SpawnNewFastEnemy()
{
	for(let i=0; i<max_enemies; i++)
	{
		let enemy = fast_enemies[i];
		if(enemy.active == false)
		{
			console.log("Activate enemy:", i)
			enemy.speed = 128

			let corner = Math.floor(GSE.randomRange(0, 3))

			enemy.ref.data.x = corners[corner].x;
			enemy.ref.data.y = corners[corner].y;
			enemy.ref.data.hidden = false;
			enemy.active = true;
			enemy.target_dir = {x:0, y:0}
			break;
		}
	}
}

function SpawnNewPickup()
{
	for(let i=0; i<max_pickups; i++)
	{
		let pickup = pickups[i];
		if(pickup.active == false)
		{
			console.log("Activate pickup:", i)

			let image = Math.floor(GSE.randomRange(0,4));
			let cols = 3
			let icon_hor_space, icon_vert_space;
			icon_hor_space = 0;
			icon_vert_space = 0;

			let sx = Math.floor(image%cols) * (64 + icon_hor_space);
			let sy = Math.floor(image/cols) * (64 + icon_vert_space);
			
			let pos_x = GSE.randomRange(border.x, border.x + border.w);

			let pos_y = GSE.randomRange(border.y, border.y + border.h);

			pickup.ref.data.sx = sx;
			pickup.ref.data.sy = sy;

			pickup.ref.data.x = pos_x;
			pickup.ref.data.y = pos_y;
			pickup.ref.data.hidden = false;
			pickup.active = true;
			break;
		}
	}
}

function RenderGame(delta)
{
	Scene.DrawRect({
        x:sea.x,
        y:sea.y, 
        w:sea.w, 
        h:sea.h,
        color:color_palete.white,
		order:-5
	});

	Scene.DrawRect({
        x:border.x, 
        y:border.y, 
        w:border.w, 
        h:border.h,
        color:color_palete.white,
		order:-5
	});

	let logo = {
		x: 6,
		y : 32
	}

	Scene.DrawSprite({
		texture: "logo_dark",
		x: sea.x,
		y: sea.y - MG.tile_h + 12,
		w: 295 ,
		h: 32
	})

	let high = 0;
	if(highscores.length > 0 )
	{
		high = highscores[0].score.toString()
	}

	Scene.DrawText({ //Highscore
        x:MG.mapW - MG.tile_w * 7, 
		y:sea.y-4,
		color:color_palete.dark_grey,
		align:"left",
		font: fonts.scores_bold,
		size: "30",
		text: "HIGHSCORE  " + high || "0"
	});

	let bar = {
		x: MG.mapW / 2 - 95,
		y: 45,
		w: 200,
		h: 25
	}

	Scene.DrawRect({
        x:bar.x,
        y:bar.y, 
        w:bar.w, 
        h:bar.h,
        color:color_palete.dark_grey
	});

	Scene.DrawRect({
        x:bar.x,
        y:bar.y,
        w: bar.w * something / 100, 
        h:bar.h,
        color:color_palete.ice
	});

	// Scene.DrawText({
	// 	x:16, 
	// 	y:20,
	// 	color:"black",
	// 	align:"left",
	// 	font: "Serif",
	// 	size: "20",
	// 	text: something.toString()
	// });

	Scene.DrawText({
		x:MG.mapW / 2, 
		y:MG.mapH - 16,
		color:color_palete.dark_grey,
		align:"center",
		font: fonts.scores_bold,
		size: "30",
		text: "SCORE  " + score.toString()
	});
}

function GameOver()
{
	//Hide enemies
	let all_enemies = enemies.concat(fast_enemies)

	//Collide player enemies
	for(let enemy of all_enemies) 
	{
		enemy.ref.data.x = -120;
		enemy.ref.data.y = -120;
	}

	for(let pickup of pickups) 
	{
		pickup.ref.data.x = -64;
		pickup.ref.data.y = -64;
		pickup.active = false;
		pickup.ref.data.hidden = true;
	}

	player.ref.data.hidden = true;

	over_stop = 0;
	SaveHighscore(score)
	GSE.StateManager.setCurrentState("GameOver");
}

function Restart()
{
	//TODO restart
	something = 100;
	enemy_threshold = 5;
	score = 0;
	
	//player in the center
	player.ref.data.x = MG.mapW / 2;
	player.ref.data.y = MG.mapH / 2;
	player.ref.data.hidden = false;

	//enemies on border ?
	//Polling
	for(let enemy of enemies) 
	{
		enemy.speed = 0
		enemy.ref.data.x = -64;
		enemy.ref.data.y = -64;
		enemy.active = false;
		enemy.moving = false;
		enemy.move_timer = 0;
	}	
	
	for(let pickup of pickups) 
	{
		pickup.ref.data.x = -64;
		pickup.ref.data.y = -64;
		pickup.active = false;
		pickup.ref.data.hidden = true;
	}	

	GSE.StateManager.setCurrentState("Game");
}

function UpdateGameOver(lastTick, delta)
{
	if(GSE.Input.IsKeyPressed("space"))
	{
		Restart();
	}

	if(GSE.Input.IsMouseLeftPressed())
	{
		let mouse_rect = {
			x:GSE.Input.mouse.clientX, 
			y:GSE.Input.mouse.clientY, w:2, h:2
		}
		if(GSE.Collisions.CheckCollision(mouse_rect, restart_button_rect))
		{
			console.log("restart")
			Restart();
		}
	}

	if(over_stop > overstop_threshold)
	{
		//Restart with a touch on a button
		if(GSE.Input.IsTouchDown())
		{		
			let mouse_rect = {
				x:GSE.Input.touch.clientX, 
				y:GSE.Input.touch.clientY, w:2, h:2
			}

			if(GSE.Collisions.CheckCollision(mouse_rect, restart_button_rect))
			{
				console.log("restart")
				Restart();
			}
		}
	}
	else
	{
		over_stop += 1 * delta;
	}
}

function RenderGameOver(delta)
{
	let startButton = isMobile === false ? "Press SPACE to Start" : "TAP to Start"

	Scene.DrawRect({
        x:sea.x, 
        y:sea.y, 
        w:sea.w, 
        h:sea.h,
        color:color_palete.white,
		order:-5
	});

	Scene.DrawRect({
        x:sea.x + MG.tile_w * 3,
        y:sea.y + MG.tile_w * 2, 
        w:sea.w - MG.tile_w * 6, 
        h:sea.h - MG.tile_w * 4,
        color:color_palete.dark_grey,
		order:-5
	});

	Scene.DrawText({ //Highscore
		x:sea.x + MG.tile_w * 6.5 + fix_game_over_x,
        y:sea.y + MG.tile_w * 3.5,
		color:"white",
		align:"left",
		font: fonts["frostbitten-wanker"],
		size: "50",
		text: "GAME OVER"
	});

	//Play button
	Scene.DrawSprite({
		texture: "button",
		x: restart_button_rect.x,
		y: restart_button_rect.y,
		w: restart_button_rect.w,
		h: restart_button_rect.h
	})

	Scene.DrawText({ 
        x:restart_button_rect.x + 48,
		y:restart_button_rect.y + 34,
		color:color_palete.dark_grey,
		align:"left",
		font: fonts.scores_bold,
		size: "30",
		text: "Start"
	});

	let logo = {
		x: 6,
		y : 32
	}

	Scene.DrawSprite({
		texture: "logo_dark",
		x: sea.x,
		y: sea.y - MG.tile_h + 12,
		w: 295 ,
		h: 32
	})

	let high = 0;
	if(highscores.length > 0 )
	{
		high = highscores[0].score.toString()
	}

	Scene.DrawText({ //Highscore
        x:MG.mapW - MG.tile_w * 7, 
		y:sea.y-4,
		color:color_palete.dark_grey,
		align:"left",
		font: fonts.scores_bold,
		size: "30",
		text: "HIGHSCORE  " + high || "0"
	});

	let highscores_y = restart_button_rect.y + MG.tile_w * 3

	Scene.DrawText({ //Highscore
        x:restart_button_rect.x - 10, 
		y:highscores_y,
		color:"white",
		align:"left",
		font: fonts.scores_bold,
		size: "30",
		text: "HIGHSCORES"
	});

	let startPosX = MG.mapW / 2
	let startPosY = highscores_y + MG.tile_h * 1

	for(let i=0; i<3; i++)
	{
		let score = highscores[i];

		if(score == null)
		{
			continue;
		}

		Scene.DrawText({
			x:startPosX, 
			y:startPosY + + (i * MG.tile_h),
			color:color_palete.white,
			align:"center",
			font: fonts.scores_bold,
			size: "25",
			text: (i + 1).toString() +". "+ score.name + " - " + score.score.toString()
		});
	}

}

function UpdatePause(lastTick, delta)
{
	if(GSE.Input.IsKeyPressed("space")) 
	{
		GSE.StateManager.setCurrentState("Game");
	}
}

function RenderPause(delta)
{
	Scene.DrawRect({
        x:MG.mapW / 2 - 50,
        y:20, 
        w:100, 
        h:20,
        color:color_palete.dark_grey
	});

	//TODO
	Scene.DrawRect({
        x:MG.mapW / 2 - 50,
        y:20,
        w:something, 
        h:20,
        color:color_palete.ice
	});
}

function Update(lastTick, delta)
{
	
}

function Render(delta)
{
	
}

function UpdateMenu(lastTick, delta)
{
	if(GSE.Input.IsKeyPressed("space"))
	{
		GSE.StateManager.setCurrentState("Game");
	}

	if(GSE.Input.IsMouseLeftPressed())
	{
		let mouse_rect = {
			x:GSE.Input.mouse.clientX, 
			y:GSE.Input.mouse.clientY, w:2, h:2
		}
		if(GSE.Collisions.CheckCollision(mouse_rect, start_button_rect))
		{
			console.log("Start game")
			GSE.StateManager.setCurrentState("Game");
		}
	}

	if(isMobile !== false)
	{
		if(GSE.Input.IsTouchDown())
		{
			let mouse_rect = {
				x:GSE.Input.touch.clientX, 
				y:GSE.Input.touch.clientY, w:2, h:2
			}

			if(GSE.Collisions.CheckCollision(mouse_rect, start_button_rect))
			{
				console.log("Start game")
				GSE.StateManager.setCurrentState("Game");
			}
		}
	}
}

function RenderMenu(delta)
{
	Scene.DrawRect({
        x:sea.x, 
        y:sea.y, 
        w:sea.w, 
        h:sea.h,
        color:color_palete.white,
		order:5
	});

	Scene.DrawSprite({
		texture: "logo_dark",
		x: sea.x,
		y: sea.y - MG.tile_h + 12,
		w: 295 ,
		h: 32
	})

	let high = 0;
	if(highscores.length > 0 )
	{
		high = highscores[0].score.toString()
	}

	Scene.DrawText({ //Highscore
        x:MG.mapW - MG.tile_w * 7, 
		y:sea.y-4,
		color:color_palete.dark_grey,
		align:"left",
		font: fonts.scores_bold,
		size: "30",
		text: "HIGHSCORE  " + high
	});

	//Show basic commands and credits
	//Press space to start
	// Move with WASD or Arrows or by Left Mouse Button 
	//credit
	let start = {x: MG.mapW / 2, y: MG.mapH / 2 - 10}
	let space = {x: start.x, y: start.y + 50}
	let instrut = {x: start.x, y: space.y + 40}
	let credits_p = {
		x: start.x, 
		y: instrut.y + 40}

	let start_texture = "start_desktop"

	if(isMobile === false)
	{
		Scene.DrawSprite({
			texture: "start_desktop",
			x: MG.tile_w * 4,
			y: MG.tile_h * 2,
			w: 613 ,
			h: 635 - MG.tile_h - 16
		})
	}
	else
	{
		Scene.DrawSprite({
			texture: "start_mobile",
			x: MG.tile_w * 2,
			y: MG.tile_h * 2,
			w: 613 - MG.tile_w,
			h: 728
		})
	}

	//Play button
	Scene.DrawSprite({
		texture: "button",
		x: start_button_rect.x,
		y: start_button_rect.y,
		w: start_button_rect.w,
		h: start_button_rect.h
	})

	Scene.DrawText({ //Highscore
        x:start_button_rect.x + 48,
		y:start_button_rect.y + 34,
		color:color_palete.dark_grey,
		align:"left",
		font: fonts.scores_bold,
		size: "30",
		text: "Play"
	});

	let startInstruction = isMobile === false ? "Move with WASD or Arrows or by Left Mouse Button" : "Tap to move"

	let startButton = isMobile === false ? "Press SPACE to Start" : "TAP to Start"

	// Scene.DrawText({
	// 	x:start.x, 
	// 	y:start.y,
	// 	color:"black",
	// 	align:"center",
	// 	font: "Serif",
	// 	size: "30",
	// 	text: startInstruction
	// });

	// Scene.DrawText({
	// 	x:space.x, 
	// 	y:space.y,
	// 	color:"black",
	// 	align:"center",
	// 	font: "Serif",
	// 	size: "40",
	// 	text: startButton
	// });
	
	// Scene.DrawText({
	// 	x:instrut.x, 
	// 	y:instrut.y,
	// 	color:"black",
	// 	align:"center",
	// 	font: "Serif",
	// 	size: "30",
	// 	text: "Credits:"
	// });

	// for(let i=0; i<credits.length; i++)
	// {
	// 	Scene.DrawText({
	// 		x:credits_p.x, 
	// 		y:credits_p.y + (20 * i),
	// 		color:"black",
	// 		align:"center",
	// 		font: "Serif",
	// 		size: "20",
	// 		text: credits[i]
	// 	});
	// }
}

function IsOutOfBoundsLoose(rect1, bounds)
{
	let exit = ''
	if(rect1.x > bounds.w + bounds.x)
	{
		exit = 'r';
	}
	else if(rect1.x + rect1.w < bounds.x) //exit l
	{
		exit = 'l';
	}
	else if(rect1.y > bounds.y + bounds.h)
	{
		exit = 'd';
	}
	else if(rect1.y + rect1.h < bounds.y)
	{
		exit = 'u';
	}
	return exit;
}

function IsOutOfBoundsJust(rect1, bounds)
{
	let exit = ''
	if(rect1.x + rect1.w > bounds.w + bounds.x)
	{
		exit = 'r';
	}
	else if(rect1.x < bounds.x) //exit l
	{
		exit = 'l';
	}
	else if(rect1.y + rect1.h > bounds.h + bounds.y)
	{
		exit = 'd';
	}
	else if(rect1.y < bounds.y)
	{
		exit = 'u';
	}
	return exit;
}

function BounceFromWall(player, wall, multiplier = 1)
{
	x_diff = 0;
	y_diff = 0;

	//fix X 
	//Case 1 if Px < Wx
	if(player.data.x < wall.data.x) {
		x_diff = (player.data.x + player.data.w) - wall.data.x;
		x_diff *= -1;
	}
	else if(player.data.x > wall.data.x) {
		x_diff = (wall.data.x + wall.data.w) - player.data.x;
	}
	//fix Y
	//Player over
	if(player.data.y < wall.data.y) {
		y_diff = (player.data.y + player.data.w) - wall.data.y;
		y_diff *= -1;		
	}
	else if(player.data.y > wall.data.y) {
		y_diff = (wall.data.y + wall.data.w) - player.data.y;		
	}

	let x_diff_abs = Math.abs(x_diff);
	let y_diff_abs = Math.abs(y_diff);

	let apply_x = 0;
	let apply_y = 0;

	let amount = 0;

	//extract x_diff
	if(x_diff_abs > 0 && y_diff_abs > 0) { //Both not 0 , use the lower
		if( x_diff_abs < y_diff_abs) {
			apply_x = 1;
			amount = x_diff;
		}
		else {
			apply_y = 1;
			amount = y_diff;
		}
	}
	else if(x_diff_abs > 0 && y_diff_abs == 0) { //Apply X
		apply_x = 1;
		amount = x_diff;
	}
	else if(y_diff_abs > 0 && x_diff_abs == 0) { //Apply Y
		apply_y = 1;
		amount = y_diff;
	}

	// console.log("collision amount:", amount);
	// console.log("collision multiplier:", multiplier);

	//I want a consistent bounce 
	// The overlap need to be different, 

	//Align to rect
	// player.data.x += amount * apply_x
	// player.data.y += amount * apply_y

	//Bounce the amount of pixel
	// if(multiplier > 1) {
	// 	player.data.x += multiplier * Math.sign(amount) * apply_x;
	// 	player.data.y += multiplier * Math.sign(amount) * apply_y;
	// }

	let x_val = amount * apply_x * multiplier;
	let y_val = amount * apply_y * multiplier;

	if(multiplier > 1) {
		x_val += multiplier * Math.sign(amount) * apply_x
		y_val += multiplier * Math.sign(amount) * apply_y
	}

	return {
		x: x_val,
		y: y_val
	}
}

function SaveHighscore(final_score)
{
	console.log("final_score:", final_score);
	let name = "";

	console.log("highscores.length", highscores.length);
	if(highscores.length == 0 || highscores.length < max_saved_scores)
	{
		name = prompt("Insert name (max 5 char)");
		name = name.substring(0, 5);
	}
	else
	{
		//If score is an highscore, ask for a name
		for(score_obj of highscores)
		{
			if(final_score > score_obj.score)
			{
				name = prompt("New Highscore (max 5 char)");
				name = name.substring(0, 5);
				break;
			}
		}
	}

	//Add score
	let score_to_add = {
		name : name,
		score: final_score
	}
	
	highscores.push(score_to_add);
	highscores.sort(compare);
	highscores = highscores.splice(0,10);

	let save_scores = [];
	for(score_obj of highscores)
	{
		save_scores.push(score_obj.name + ";" + score_obj.score.toString());
	}
	localStorage.setItem("PDshs", save_scores.join("|"));
}

function compare(a, b) 
{
	let numA = a.score;
	let numB = b.score; 

	if (numA < numB) {
		return 1;
	}
	if (numA > numB) {
		return -1;
	}
	// a must be equal to b
	return 0;
}