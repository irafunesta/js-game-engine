MG = {
	mapW: 320 + 64, //12
	mapH: 320 + 64,
	tile_w: 32,
	tile_h: 32,
	number_tiles_width: 0,
	number_tiles_height: 0
};

//Alias
const Scene = GSE.Scene;
const Input = GSE.Input

let tileset1 = {
	number_tiles_width: 23,
	number_tiles_height: 21
}

let player_states = {
	idle : "idle",
	moving : "moving",
}

let player = {
	ref : null,
	speed : 120,
	x_moved : 0,
	y_moved : 0,
	world_x : 0,
	world_y : 0,
	state: "idle",
	dir:{
		x:0, y:0
	},
	hp : 100,
	combat_state : "out_of_combat", //ofc, auto-attack, 
	attack_timer : null
}

//rappresent the view on the full map
let camera = {
	ref : null,
	view : [],
	grid : [],
	x: 0,
	y: 0,
	width_tile : 10,
	heigth_tile : 10
}

//A grid of data that is the size of the view + the offset for loading
//And it start at 0 - offset
let vram = {
	ref : null,
	grid : [],
	x: 0,
	y: 0,
	width_tile : 11,
	heigth_tile : 11,
	offset_x : 32,
	offset_y : 32
}

let current_map = null;

let grid_row_wrapped = 0;

let chunk_size = 4;

//Use this to determine if the player got over a chunk
let player_moved = {
	x: 0,
	y: 0
}

let square_trigger = 2;
let view_trigger = (square_trigger) * MG.tile_w;

let chunk_map = {
	x: -view_trigger, 
	y: -view_trigger,
	map : {}, //Use the stupid hash function for x+y,
	ref : {}
}

function hash(x, y)
{
	return x + "-" + y;
}

function Chunk(size, data)
{
	return {
		size: size,  //for now 4
		data: data,
		x : 0,
		y : 0
	}
}

//Area
let area_coords = {
	x:0,
	y:0,
	w:12 + (square_trigger * 2),
	h:12 + (square_trigger * 2)
}

let entity = {
	x: 4, 
	y: 7, 
	type: "wall",
	ref : {}
}

//If i store the entities in a hashmap of x-y we should be good
let entities_map = {}; 
let entities = [];

//Padding for the window, this is in the css, but it still is part of the cavas so the actual screen start at 10, 10
let padding = 10;

let editor = false;

let player_combat_message = "";
let enemy_combat_message = "";
let current_target = {
	x:0,
	y:0
}
let hide_player_message_timer;
let hide_enemy_message_timer;

let max_combat_messages = 10;
let combat_messages_pool = [];

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();

function IdToMapCoord(id, width, height) {
	return {
		x : Math.floor(id % width),
		y : Math.floor(id / height)
	}
}

//Formula to move an area over an array
function AreaFormula(data, area_coords, map_width) 
{
	let area_to_draw = [];
	let index = [];
	
	for(let row = 0; row < area_coords.w; row++)
	{
		for(let col = 0; col < area_coords.h; col++)
		{
			let f2 = (col + area_coords.x) + ((row + area_coords.y) * map_width)
			index.push(f2);
			area_to_draw.push(data[f2]);
		}
	}

	// console.log("indecies:", index);
	return (area_to_draw);
}

function InitGame(ctx)
{
	GSE.Scene.LoadTexture("player", "./imgs/player_walk.png");
	GSE.Scene.LoadTexture("monster", "./imgs/alien2.png");
	GSE.Scene.LoadTexture("tileset", "./tilesets/tileSetTest.png");

	GSE.Scene.CreateSound("loot_pickup", "./sound/menusel.wav");

	current_map = map1;

	MG.number_tiles_width = MG.mapW / MG.tile_w;
	MG.number_tiles_height = MG.mapH / MG.tile_h;

	chunk_map.ref = Scene.CreateNode({
        id:"chunk_map",
        data:{
            x:chunk_map.x,
            y:chunk_map.y,
            hidden:false
        },
        render_func: () => undefined
    })

	// Scene.root.add_child(camera.ref);
	Scene.root.add_child(chunk_map.ref);

	//Fill the vram, with data from the map, keeping in mind the offset and the camera area

	let starting_position = {x:0, y:0};

	//cycle all the chunks
	//UPDATE only cycle the chunks on the area
	// CreateChunkMap();
	CreateTileMap()

	player.ref = Scene.AddSpriteExt(
		{
			texture: "player",
			x: MG.tile_w * 5,
			y: MG.tile_h * 5,
			w: MG.tile_w,
			h: MG.tile_h,
			sx:0, sy:0, sw:32, sh:32,
			tile_id:0,
			id:"player"
		}
	)

	entity.ref = Scene.AddSpriteExt(
		{
			texture: "player",
			x: entity.x,
			y: entity.y,
			w: MG.tile_w,
			h: MG.tile_h,
			sx:0, sy:0, sw:32, sh:32,
			tile_id:0
		}
	)

	let loot = {
		x: 6,
		y: 7,
		type: "loot",
		ref : Scene.AddSpriteExt(
			{
				texture: "player",
				x: entity.x,
				y: entity.y,
				w: MG.tile_w,
				h: MG.tile_h,
				sx:32, sy:0, sw:32, sh:32,
				tile_id:0
			}
		)
	}

	let monster = {
		x: 5,
		y:10,
		hp: 100 + Math.floor(GSE.randomRange(0, 100)),
		type: "monster",
		attack_timer: new GSE.Timer(2, () => 
		{
			let player_pos = FromCanvasToMap(player.ref.data.x, player.ref.data.y);
			let message_pos = {x: player.ref.data.x, y: player.ref.data.y}
			if(RangeCheck(player_pos, current_target, 1))
			{
				enemy_combat_message = ShowCombatMessage(message_pos,"OUT OF RANGE");
				return;
			}

			let monster_attack = Math.floor(GSE.randomRange(0,20));
			console.log("monster_attack : ", monster_attack);

			player_combat_message = ShowCombatMessage(message_pos,monster_attack);
			player.hp -= monster_attack;
		}),
		combat_state: "idle", //idle, chase, combat ??
		ref : Scene.AddSpriteExt(
			{
				texture: "monster",
				x: entity.x,
				y: entity.y,
				w: MG.tile_w,
				h: MG.tile_h,
				sx:0, sy:0, sw:32, sh:32,
				tile_id:0,
			}
		)
	}

	// console.log("todraw:", AreaFormula(current_map.data, area_coords, current_map.width))
	// entities.push(entity);
	// entities.push(loot);
	// entities.push(monster);

	entities_map[hash(entity.x, entity.y)] = entity;
	entities_map[hash(loot.x, loot.y)] = loot;
	entities_map[hash(monster.x, monster.y)] = monster;

	hide_player_message_timer = new GSE.Timer(1, () => {
		// console.log("Hide message");
		HideCombatMessage(player_combat_message);
	})

	hide_enemy_message_timer = new GSE.Timer(1, () => {
		// console.log("Hide message");
		HideCombatMessage(enemy_combat_message);
	})

	player.attack_timer = new GSE.Timer(1.5, () => {

		let player_pos = FromCanvasToMap(player.ref.data.x, player.ref.data.y);
		let message_pos = {x: player.ref.data.x, y: player.ref.data.y};
		let target_message_pos = {x:current_target.ref.data.x, y:current_target.ref.data.y};
		let msg = "";
		if(RangeCheck(player_pos, current_target, 1))
		{
			msg = "OUT OF RANGE";
			player_combat_message = ShowCombatMessage(message_pos, msg);
			return;
		}

		let attack = Math.floor(GSE.randomRange(0,20));
		console.log("Attack : ", attack);
		enemy_combat_message = ShowCombatMessage(target_message_pos, attack);
		current_target.hp -= attack;
		hide_enemy_message_timer.restart();
	})

	for(let i=0; i< max_combat_messages; i++)
	{		
		let message = {
			text: "5",
			ref: Scene.AddRect({
				x: i === 0 ? 160 : 0,
				y: i === 0 ? 32 : 0,
				w: 16,
				h: 16,
				color: "red",
				hidden : true,
				id: "message_bg_" + i 
			})
		}

		Scene.AddText({
			text:message.text,
			x : 10,
			y : 11,
			color : "black",
			align:"center",
			id: "message_txt1"
		}, message.ref.get_path())
		
		Scene.AddText({
			text:message.text,
			x : 8,
			y : 10,
			color : "white",
			align:"center",
			id: "message_txt2"
		}, message.ref.get_path())
		

		combat_messages_pool[i] = message;
	}
}

function UpdateCombatMessage(message, text)
{
	if(!message) {
		return;
	}

	message.ref.child[0].data.text = text;
	message.ref.child[1].data.text = text;
}

function ShowCombatMessage(position, text)
{
	let message = undefined;
	for(let i=0; i< max_combat_messages; i++)
	{
		if(combat_messages_pool[i].ref.data.hidden)
		{
			message = combat_messages_pool[i];
			break;
		}
	}

	if(!message) 
	{
		return;
	}
	
	message.ref.data.hidden = false;
	message.ref.data.x = position.x;
	message.ref.data.y = position.y;

	UpdateCombatMessage(message, text);

	return message;	
}

function HideCombatMessage(message)
{
	if(!message) {
		return;
	}
	console.log(message);
	message.ref.data.hidden = true;
}

function CreateChunkMap()
{
	let mapArea = AreaFormula(current_map.chunk_map, area_coords, current_map.chunk_map_width);
	for(let chunk_num  = 0; chunk_num < mapArea.length; chunk_num++)
	{
		//First create 1 single chunk from map data
		let first_chunk_id = mapArea[chunk_num]
		let first_chunk = new Chunk(chunk_size, current_map.chunks[first_chunk_id]);

		console.log(first_chunk);

		let pos = IdToMapCoord(chunk_num, 3, 3);

		//Add an empty node father for the chunk
		//this will be multiplied by tile_widht ecc
		//and the single tiles inside will start from 0,0
		first_chunk.ref = Scene.CreateNode({
			id:"chunk" + chunk_num,
			data:{
				x:(pos.x * first_chunk.size * MG.tile_w), //Add current col for the relative chunk
				y:(pos.y * first_chunk.size * MG.tile_h), //Add current row for the relative chunk
				hidden:false
			},
			render_func: () => undefined
		})

		//Add it as child of the entire map
		chunk_map.ref.add_child(first_chunk.ref);
		//update the actual map
		chunk_map.map[hash(pos.x, pos.y)] = first_chunk.ref;

		//Add the first chunk to the map
		for(let row = 0; row < first_chunk.size; row++)
		{
			// let row_array = [];
			for(let col = 0; col < first_chunk.size; col++)
			{
				/*
				Il tile che deve andare a prendere deve essere sulla mappa, 
				quindi
				normal :
				camera_x = 0, camera_y = 1 --> (1 * 10) + 0 -> 10, sbagliato
				caring for the tiles :
				camera_x = 0, camera_y = 1 --> (1 * 11) + 0 -> 11
				bisogna usare la dimensione della mappa e non quella del MG

				*/
				
				let position_on_map = (row * first_chunk.size) + col;
				tile_id = first_chunk.data[position_on_map]

				//Cool stuff to draw a grid of squares
				// let x_division = Math.floor(i%MG.number_tiles_width);
				// let y_division = Math.floor(i/MG.number_tiles_height);
				

				let screen_x = (first_chunk.x + col) * MG.tile_w;
				let screen_y = (first_chunk.y + row) * MG.tile_h;

				//Change this to use the number of tiles for the tileset and not the screen
				let tile_x = Math.floor(tile_id % tileset1.number_tiles_width) * MG.tile_w;
				let tile_y = Math.floor(tile_id / tileset1.number_tiles_height) *  MG.tile_h;

				let chunk_father = chunk_map.map[hash(pos.x, pos.y)];

				let tile = Scene.AddSpriteExt(
					{
						texture: "tileset",
						x: screen_x,
						y: screen_y,
						w: MG.tile_w,
						h: MG.tile_h,
						sx:tile_x, 
						sy:tile_y,
						sw:MG.tile_w,
						sh:MG.tile_h,
						tile_id:tile_id,
						id:"map_tile"
					},
					chunk_father.get_path() //add this tile to the Father
				)
			}
		}
	}
}

function GenerateRandomBigMap()
{
	let map = []
	for(let i = 0; i< 1000*1000; i++)
	{
		map.push(Math.round(GSE.randomRange(0,  (tileset1.number_tiles_width * tileset1.number_tiles_height) - 1)));
	}
	return JSON.stringify(map);
}

function CreateTileMap()
{
	let mapArea = AreaFormula(current_map.data, area_coords, current_map.width);
	DrawTileMap(mapArea);
}

function DrawTileMap(mapArea)
{
	for(let i  = 0; i < mapArea.length; i++)
	{
		let pos = IdToMapCoord(i, area_coords.w, area_coords.h);
		tile_id = mapArea[i];

		let current_chunk = chunk_map.map[hash(pos.x, pos.y)]
		if(current_chunk) {
			Scene.RemoveNode(current_chunk.get_path());
		}
		
		let screen_x = pos.x * MG.tile_w;
		let screen_y = pos.y * MG.tile_h;

		let tile_x = Math.floor(tile_id % tileset1.number_tiles_width) * MG.tile_w;
		let tile_y = Math.floor(tile_id / tileset1.number_tiles_height) *  MG.tile_h;

		let tile = Scene.AddSpriteExt(
			{
				texture: "tileset",
				x: screen_x, //This is offset from the start
				y: screen_y,
				w: MG.tile_w + 1,
				h: MG.tile_h + 1,
				sx:tile_x, 
				sy:tile_y,
				sw:MG.tile_w,
				sh:MG.tile_h,
				tile_id:tile_id,
				id:"map_tile"
			},
			chunk_map.ref.get_path() //add this tile to the Father
		)

		chunk_map.map[hash(pos.x, pos.y)] = tile;
	}
}

function UpdateTileMap()
{	
	let mapArea = AreaFormula(current_map.data, area_coords, current_map.width);
	DrawTileMap(mapArea);
	// AlignMapToGrid();
}

function UpdateChunkMap()
{
	//Get the current map
	//get the new area to draw
	//update all the chunks for the new area

	let mapArea = AreaFormula(current_map.chunk_map, area_coords, current_map.chunk_map_width);

	for(let chunk_num  = 0; chunk_num < mapArea.length; chunk_num++)
	{
		//First create 1 single chunk from map data
		let first_chunk_id = mapArea[chunk_num]
		let first_chunk = new Chunk(chunk_size, current_map.chunks[first_chunk_id]);

		console.log("chunk ", chunk_num, ":", first_chunk);

		let pos = IdToMapCoord(chunk_num, 3, 3);

		/*
			I want to reuse chunks, so i need to redraw the
			chunks starting from 0,0 -> 2,2 
			each one will have a new tiles, based on the area

			let's get chunk 0,0, and clear it, i mean we just need to change the source position of the tile

			well for now it's easier to clear it an recreate
		*/
		let current_chunk = chunk_map.map[hash(pos.x, pos.y)];
		Scene.RemoveNode(current_chunk.get_path());

		//Reset the map position, outside of here		

		//Add an empty node father for the chunk
		//this will be multiplied by tile_widht ecc
		//and the single tiles inside will start from 0,0
		first_chunk.ref = Scene.CreateNode({
			id:"chunk" + chunk_num,
			data:{
				x:(pos.x * first_chunk.size * MG.tile_w), //Add current col for the relative chunk
				y:(pos.y * first_chunk.size * MG.tile_h), //Add current row for the relative chunk
				hidden:false
			},
			render_func: () => undefined
		})

		//Add it as child of the entire map
		chunk_map.ref.add_child(first_chunk.ref);
		//update the actual map
		chunk_map.map[hash(pos.x, pos.y)] = first_chunk.ref;

		//Add the first chunk to the map
		for(let row = 0; row < first_chunk.size; row++)
		{
			// let row_array = [];
			for(let col = 0; col < first_chunk.size; col++)
			{
				/*
				Il tile che deve andare a prendere deve essere sulla mappa, 
				quindi
				normal :
				camera_x = 0, camera_y = 1 --> (1 * 10) + 0 -> 10, sbagliato
				caring for the tiles :
				camera_x = 0, camera_y = 1 --> (1 * 11) + 0 -> 11
				bisogna usare la dimensione della mappa e non quella del MG

				*/
				
				let position_on_map = (row * first_chunk.size) + col;
				tile_id = first_chunk.data[position_on_map]

				//Cool stuff to draw a grid of squares
				// let x_division = Math.floor(i%MG.number_tiles_width);
				// let y_division = Math.floor(i/MG.number_tiles_height);
				

				let screen_x = (first_chunk.x + col) * MG.tile_w;
				let screen_y = (first_chunk.y + row) * MG.tile_h;

				//Change this to use the number of tiles for the tileset and not the screen
				let tile_x = Math.floor(tile_id % tileset1.number_tiles_width) * MG.tile_w;
				let tile_y = Math.floor(tile_id / tileset1.number_tiles_height) *  MG.tile_h;

				let chunk_father = chunk_map.map[hash(pos.x, pos.y)];

				let tile = Scene.AddSpriteExt(
					{
						texture: "tileset",
						x: screen_x,
						y: screen_y,
						w: MG.tile_w,
						h: MG.tile_h,
						sx:tile_x, 
						sy:tile_y,
						sw:MG.tile_w,
						sh:MG.tile_h,
						tile_id:tile_id,
						id:"map_tile"
					},
					chunk_father.get_path() //add this tile to the Father
				)
			}
		}
	}
}

function PlaceEntities(grid_x, grid_y)
{
	let new_ent = {
		x : grid_x ,
		y : grid_y ,
		type: "wall",
	}

	new_ent.ref = Scene.AddSpriteExt(
		{
			texture: "player",
			x: new_ent.x,
			y: new_ent.y,
			w: MG.tile_w,
			h: MG.tile_h,
			sx:0, sy:0, sw:32, sh:32,
			tile_id:0
		}
	)

	// entities.push(new_ent);
	entities_map[hash(new_ent.x, new_ent.y)] = new_ent;
}

//Goes from canvas coords (32.5, 123.7) to map coords (4,5), counting also the camera and map scroll
function FromCanvasToMap(x, y)
{
	//Show player position on the map
	let playerX = x + (-chunk_map.ref.data.x) ; //5
	let playerY = y + (-chunk_map.ref.data.y); //5	

	playerX = Math.floor( playerX / MG.tile_w);
	playerY = Math.floor( playerY / MG.tile_h);

	// console.log("mouse:", playerX, playerY);

	playerX = playerX + area_coords.x
	playerY = playerY + area_coords.y

	return {
		x: playerX,
		y: playerY
	}
}

function FromMapToCanvas(x, y)
{
	//multiply by tile size, remove the map difference, add the 
}

function RangeCheck(start, end, range)
{
	console.log("start point",start)
	console.log("end point",end)
	if(!start || !end) 
	{
		return true; //Out of range
	}
	//Range check
	let delta_x = Math.abs(start.x - end.x);
	let delta_y = Math.abs(start.y - end.y);
	console.log("diff ", delta_x, ",", delta_y);
	return delta_x > range || delta_y > range;
}


function Update(lastTick, delta)
{
	//Manually move camera and player
	onlyMoveCamera(delta);

	for (let i in entities_map)
	{
		let entity = entities_map[i];
		if(!entity) 
		{
			continue;
		}
		//Entitiyes are drawned at their position - the camera pos
		entity.ref.data.x = (entity.x - area_coords.x) *MG.tile_w
		entity.ref.data.y = (entity.y - area_coords.y) *MG.tile_h

		//Also apply movemt of map
	
		entity.ref.data.x += chunk_map.ref.data.x
		entity.ref.data.y += chunk_map.ref.data.y

		// let new_map_pos = FromCanvasToMap(entity.ref.data.x, entity.ref.data.y);
		// entity.x = new_map_pos.x;
		// entity.y = new_map_pos.y;

		//Update monster hp
		if(entity.type === "monster")
		{
			if(entity.hp && entity.hp <= 0) 
			{
				Scene.RemoveNode(entity.ref.get_path())
				// entities.splice(i, 1);
				entities_map[i] = undefined; //removing them is a little hard

				//player loses the comabat target
				current_target.combat_state = "idle";
				player.combat_state = "idle";
				current_target = undefined;

				//Leave loot
				let loot = {
					x: entity.x,
					y: entity.y,
					type: "loot",
					ref : Scene.AddSpriteExt(
						{
							texture: "player",
							x: entity.x,
							y: entity.y,
							w: MG.tile_w,
							h: MG.tile_h,
							sx:32, sy:0, sw:32, sh:32,
							tile_id:0
						}
					)
				}

				entities_map[hash(entity.x, entity.y)] = loot;
				continue;
			}

			if(entity.combat_state === "auto-attack")
			{
				//tick the monster timer for attacks
				entity.attack_timer.tick(delta);
			}
		}
	}

	if(GSE.Input.IsMouseLeftPressed())
	{
		let mouseX = GSE.Input.mouse.clientX - padding;
		let mouseY = GSE.Input.mouse.clientY - padding;

		//Round muose position
		mouseX = Math.floor(mouseX / MG.tile_w) * MG.tile_w ;
		mouseY = Math.floor(mouseY / MG.tile_h) * MG.tile_h ;

		// console.log("mouse:", mouseX, mouseY);

		let pos_grid = FromCanvasToMap(mouseX, mouseY);
		
		// console.log("grid2:", pos_grid.x * MG.tile_w, ",", pos_grid.y * MG.tile_h);
		// console.log("grid id2:", pos_grid.x, ",", pos_grid.y );

		//Test add an enitity to the map at the grid mouse pos
		if(editor == true)
		{
			PlaceEntities(pos_grid.x, pos_grid.y);
		}

		//Check clicked map square
		let entity = entities_map[hash(pos_grid.x, pos_grid.y)]
		if(entity)
		{
			console.log(entity.ref.id);
			let player_pos = FromCanvasToMap(player.ref.data.x, player.ref.data.y);
			console.log("player pos", player_pos);
			switch(entity.type)
			{
				case "monster" : {
					
					//Start attack mode for player, with the target
					current_target = entity;
					player.combat_state = "auto-attack";
					//Also enemy in auto attack
					entity.combat_state = "auto-attack";
					break;
				}
			}
		}
	}

	if(player.combat_state === "auto-attack")
	{
		//tick the monster timer for attacks
		player.attack_timer.tick(delta);
	}

	hide_enemy_message_timer.tick(delta);
	hide_player_message_timer.tick(delta);
}

function AlignMapToGrid()
{

	chunk_map.ref.data.x = Math.round(chunk_map.ref.data.x / MG.tile_w) * MG.tile_w ;
	chunk_map.ref.data.y = Math.round(chunk_map.ref.data.y / MG.tile_h) * MG.tile_h ;
}

function stopPlayer()
{
	console.log("Stop player");
	player.dir.x = 0;
	player.dir.y = 0;
	player.state = player_states.idle;

	AlignMapToGrid();
}

function onlyMoveCamera(delta)
{
	let speed = view_trigger * delta;
	let move_speed = player.speed * delta;

	switch(player.state) {
		case player_states.idle : 
		{
			//Init the movement if key pressed

			if(Input.IsKeyDown(Input.keyCodes.KeyD) || Input.IsKeyDown(Input.keyCodes.ArrowRight))
			{
				player.dir.x = -1; //use this for the map, and opposite for the moved
				player.state = player_states.moving;
			}
			else if(Input.IsKeyDown(Input.keyCodes.KeyA) || Input.IsKeyDown(Input.keyCodes.ArrowLeft))
			{
				player.dir.x = 1;
				player.state = player_states.moving;
			}
			else if(Input.IsKeyDown(Input.keyCodes.KeyW) || Input.IsKeyDown(Input.keyCodes.ArrowUp))
			{
				player.dir.y = 1;
				player.state = player_states.moving;
			}
			else if(Input.IsKeyDown(Input.keyCodes.KeyS) || Input.IsKeyDown(Input.keyCodes.ArrowDown))
			{
				player.dir.y = -1;
				player.state = player_states.moving;
			}

			//Check collision before moving, only if u moved
			if(player.state !== player_states.moving)
			{
				break;
			}

			let next_square = {h:MG.tile_h, w:MG.tile_w};

			next_square.x = player.ref.data.x + (-player.dir.x) * MG.tile_w;
			next_square.y = player.ref.data.y + (-player.dir.y) * MG.tile_w;

			// let loot = entities.filter(entity => entity.ref.id === "loot");
			for(let i in entities_map) 
			{				
				let entity = entities_map[i];
				if(!entity) 
				{
					continue;
				}

				if(GSE.Collisions.CheckCollision(next_square, entity.ref.data))
				{
					switch(entity.type) {
						case "wall": {
							stopPlayer();
							break;
						}
						case "monster": {
							stopPlayer();
							break;
						}
						case "loot": {

							Scene.PlaySound("loot_pickup");
							Scene.RemoveNode(entity.ref.get_path())
							// entities.splice(i, 1);
							entities_map[i] = undefined; //removing them is a little hard
							break;
						}
					}
				}
			}
		}
		case player_states.moving : 
		{
			let can_stop = false;
			//Just move
			chunk_map.ref.data.x += (move_speed * player.dir.x);
			chunk_map.ref.data.y += (move_speed * player.dir.y);
			//The player moved in the opposite direction
			player_moved.x += (move_speed * (player.dir.x * -1));
			player_moved.y += (move_speed * (player.dir.y * -1));

			player.x_moved += (move_speed * (player.dir.x * -1));
			player.y_moved += (move_speed * (player.dir.y * -1));

			//Update the map in case
			if(player.dir.x > 0) //A
			{
				if(player.x_moved < -(MG.tile_w))
				{
					player.x_moved = 0;
					stopPlayer();
					
					//Align how much i moved for the map trigger
					player_moved.x = Math.round(player_moved.x / MG.tile_w) * MG.tile_w ;
				}

				if(player_moved.x < -(view_trigger))
				{
					//Move area right
					if(area_coords.x <= 0)
						return;

					player_moved.x = 0; //I need to reset the trigger in any case, 
					area_coords.x -= square_trigger;	//move the area
					UpdateTileMap();					//update the new tile map if area moved
					chunk_map.ref.data.x = chunk_map.x; //reset only if area moved
				}
			}

			if(player.dir.x < 0) //D
			{
				if(player.x_moved >= MG.tile_w)
				{
					player.x_moved = 0;
					stopPlayer();

					player_moved.x = Math.round(player_moved.x / MG.tile_w) * MG.tile_w ;
				}

				if(player_moved.x > view_trigger)
				{
					//Move area right
					if(area_coords.x + area_coords.w >= current_map.width)
						return;

					player_moved.x = 0;
					area_coords.x += square_trigger;
					UpdateTileMap()					
					chunk_map.ref.data.x = chunk_map.x;
				}

				
			}
			if(player.dir.y > 0) //W
			{
				if(player.y_moved < -(MG.tile_h))
				{
					player.y_moved = 0;
					stopPlayer();
					player_moved.y = Math.round(player_moved.y / MG.tile_h) * MG.tile_h ;
				}

				//if the player moved enough, change the area and update the map		
				if(player_moved.y < -(view_trigger))
				{
					//Move area up
					if(area_coords.y <= 0)
						return;

					area_coords.y -= square_trigger;
					UpdateTileMap();
					player_moved.y = 0;
					chunk_map.ref.data.y = chunk_map.y;
				}

				
			}
			if(player.dir.y < 0) //S
			{
				if(player.y_moved >= MG.tile_h) 
				{
					player.y_moved = 0;
					stopPlayer();
					player_moved.y = Math.round(player_moved.y / MG.tile_h) * MG.tile_h ;
				}

				if(player_moved.y >= (view_trigger))
				{
					//Move area down
					if(area_coords.y + area_coords.h >= current_map.height)
						return;

					area_coords.y += square_trigger;
					UpdateTileMap();
					player_moved.y = 0;
					chunk_map.ref.data.y = chunk_map.y;
				}
			}
		}
	}
}

function Render(delta)
{
	Scene.DrawText({
		text:"player_moved.x: " + player_moved.x,
		x : 16,
		y : 16,
		color : "yellow"
	})
	

	Scene.DrawText({
		text:"player_moved.y: " + player_moved.y,
		x : 16,
		y : 32,
		color : "yellow"
	})

	Scene.DrawText({
		text:"camera: (" + area_coords.x + "," + area_coords.y + ")",
		x : 16,
		y : 48,
		color : "yellow"
	})

	Scene.DrawText({
		text:"player_moved: (" + player.x_moved + "," + player.y_moved + ")",
		x : 16,
		y : 68,
		color : "yellow"
	})

	Scene.DrawText({
		text:"chunk_map: (" + chunk_map.ref.data.x  + "," + chunk_map.ref.data.y + ")",
		x : 16,
		y : 96,
		color : "yellow"
	})

	Scene.DrawText({
		text:"player.status: " + player.state,
		x : 16,
		y : 76,
		color : "yellow"
	})


	//Player HP
	Scene.DrawText({
		text:"HP: " + player.hp,
		x : player.ref.data.x,
		y : player.ref.data.y,
		color : "yellow"
	})

	//Show player position on the map
	let player_pos = FromCanvasToMap(player.ref.data.x, player.ref.data.y);

	Scene.DrawText({
		text:"player_pos: (" + player_pos.x  + "," + player_pos.y + ")",
		x : 16,
		y : 132,
		color : "yellow"
	})

	for(let i in entities_map)
	{
		let entity = entities_map[i];
		if(!entity) 
		{
			continue;
		}

		switch(entity.type) {
			case "wall" : color = "grey"; break;
			case "loot" : color = "gold"; break;
			case "monster" :  
			{
				color = "red"; 
				Scene.DrawText({
					text:"HP: " + entity.hp,
					x : entity.ref.data.x,
					y : entity.ref.data.y,
					color : "yellow"
				})
				break;
			}
		}

		GSE.rawDrawRectLine( entity.ref.data.x, entity.ref.data.y , entity.ref.data.w, entity.ref.data.h, color, 6);
	}
}