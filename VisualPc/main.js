MG = {
	mapW: 640,
	mapH: 480
};

//Alias
const Scene = GSE.Scene;
const Input = GSE.Input;

let draggable_rect1 = {
	x: 10, 
	y: 10,
	w: 64,
	h: 64,
	dragging: false,
	color: "#ff9900",
	id:"nodeA"
}

let nodes = []
let connections = [];

let dragging_node_id = 0;
let dragging_node = false;
let connecting_node = false;
let node_ref;
let drag_offset = {
	x: 0,
	y: 0
}

let modes = ["MOVE", "CONNECT"];
let mode = 0
let game_title;

let grid_size = 6;
let grid = [];
let cell_size = 32;

let units = [];
let enemies = [];

let grid_start = {x: cell_size, y: cell_size * 2}

function InitGame(ctx)
{
	Scene.useRoot(true);
	// Scene.debug_draw = true;

	for(y = 0; y< grid_size; y++)
	{
		let row = [];
		for(x = 0; x< grid_size; x++)
		{
			let cell = {
				x: grid_start.x + (x * 32), 
				y: grid_start.y + (y * 32),
				w: cell_size,
				h: cell_size,
				color: "#ff9900",
				borderColor: "white",
				border: 1
			}

			row.push(Scene.AddRect(cell));
		}
		grid.push(row);
	}
		

		// let text_on_rect = {
		// 	x: 10,
		// 	y: 10,
		// 	color:"white",
		// 	align:"left",
		// 	size: "12",
		// 	text: "Test" + i.toString()
		// }

		// let node_ref = Scene.AddRect(draggable_rect)
		// console.log("node_ref path:", node_ref.get_path());
		// let text = Scene.AddText(text_on_rect, node_ref.get_path())
		// // node_ref.add_child(text);

		// nodes.push(node_ref);
	
	game_title = document.getElementById("title");
	
	// console.log("node_ref", node_ref);
}

function snapMouseToGrid() {

	let xt = Math.floor(GSE.Input.mouse.clientX / cell_size );
	let	yt = Math.floor(GSE.Input.mouse.clientY / cell_size );

	return {
		x: Math.floor(xt),
		y: Math.floor(yt)
	}
}

function Update(lastTick, delta)
{
	game_title.text = "Nodes:" + Scene.root.child.length;

	let mouse_rect = 
	{
		x:GSE.Input.mouse.clientX,
		y:GSE.Input.mouse.clientY,
		w:2,
		h:2
	}

	if (GSE.Input.IsKeyPressed("a"))
	{
		let u = snapMouseToGrid();
		
		//Add a unit at x, y
		let ref = Scene.AddRect({
			x: u.x * cell_size, 
			y: u.y * cell_size,
			w: cell_size,
			h: cell_size,
			color: "green",
			borderColor: "white",
			border: 2
		})

		units.push(ref);
	}

	if (GSE.Input.IsKeyPressed("d"))
	{
		let u = snapMouseToGrid();
		
		//Add a unit at x, y
		let ref = Scene.AddRect({
			x: u.x * cell_size, 
			y: u.y * cell_size,
			w: cell_size,
			h: cell_size,
			color: "red",
			borderColor: "black",
			border: 2
		})

		enemies.push(ref);
	}

	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);
	// let mouse_rect = 
	// {
	// 	x:GSE.Input.mouse.clientX,
	// 	y:GSE.Input.mouse.clientY,
	// 	w:2,
	// 	h:2
	// }

	// for(i = 0; i< nodes.length; i++)
	// {
		
	// 	let node_ref = nodes[i];
	// 	if(GSE.checkBoundsRect(node_ref.data, mouse_rect))
	// 	{
	// 		if (GSE.Input.IsMouseLeftDown() && !dragging_node)
	// 		{
	// 			dragging_node = true;
	// 			dragging_node_id = i;
	// 			drag_offset.x = node_ref.data.x - GSE.Input.mouse.clientX
	// 			drag_offset.y = node_ref.data.y - GSE.Input.mouse.clientY
	// 		}

	// 		if (GSE.Input.IsMouseRightDown() && !connecting_node)
	// 		{
	// 			//TODO trace a line between 2 points
	// 			// connecting_node = true;
	// 			// dragging_node_id = i;
	// 			// drag_offset.x = node_ref.data.x - GSE.Input.mouse.clientX
	// 			// drag_offset.y = node_ref.data.y - GSE.Input.mouse.clientY
	// 		}
	// 	}
	// }
	
	// if(!GSE.Input.IsMouseLeftDown())
	// {
	// 	//release node
	// 	dragging_node = false;
	// 	dragging_node_id = -1;
	// }

	// if(dragging_node == true)
	// {
	// 	let node = nodes[dragging_node_id];
	// 	node.data.x = GSE.Input.mouse.clientX + drag_offset.x;
	// 	node.data.y = GSE.Input.mouse.clientY + drag_offset.y;
	// }
}

function Render(delta)
{
	game_title.text = "Nodes:" + Scene.root.child.length;
	let temp_text = {
        x:20,
        y:20,
        color:"white",
        align:"left",
        size: "12",
		text: `x:${GSE.Input.mouse.clientX}, y:${GSE.Input.mouse.clientY}`
    }

	Scene.DrawText(temp_text);

	let mouse_grid = snapMouseToGrid()

	Scene.DrawRect({
		x: mouse_grid.x * cell_size, 
		y: mouse_grid.y * cell_size,
		w: cell_size,
		h: cell_size,
		color: "blue",
		borderColor: "white",
		border: 2,
		alpha: 0.3
	})
	
	// Scene.DrawText(temp_text)
	//Test DrawLine
	// Scene.DrawLine({
	// 	x_start:0,
    //     y_start:0,
    //     x_end:64,
    //     y_end:64,
    //     color:"green",
	// 	size:10
	// })

	// Scene.DrawRect({
	// 	x: 10,
	// 	y: 74,
	// 	w: 64,
	// 	h: 64,
	// 	color: "blue",
	// 	border: 2,
	// 	borderColor: "white",
	// 	id: "Rect render Draw"
	// })

}

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "#5e5948");
})();