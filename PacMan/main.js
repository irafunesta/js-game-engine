MG = {};
MG.cell_size = 16;
MG.col = 28;
MG.row = 36;
MG.mapW = MG.cell_size * MG.col, //4 19
MG.mapH = MG.cell_size * MG.row //3 30
MG.level_editor = false;

MG.STATES = {};
MG.STATES.SCARED = "scared";
MG.STATES.SCATTER = "scutter";
MG.STATES.CHASE = "chase";
MG.STATES.EATEN = "eaten";
MG.STATES.CHASE_TIME = 5;
MG.STATES.SCATTER_TIME = 5;

;(function () {
    GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, null, "black");
})();

function DotOffeset(v)
{
    return v + Math.floor(MG.cell_size / 3) + 1;
}

function SuperDotOffeset(v)
{
    return v + Math.floor(MG.cell_size / 4);
}

function InitGame(ctx)
{
    var grid_size = 10;    
    MG.score = 0;
    MG.inGame = true;    
    MG.super_dot_time = 5000; //msec
    MG.ghosts_scatter_time = MG.STATES.SCATTER_TIME; //msec
    MG.ghosts_chase_time = MG.STATES.CHASE_TIME;
    MG.scare_state = false;
    MG.scare_color = "aqua";
    MG.ghost_color_swap_time = 3;
    MG.current_level = 0;
    MG.levels = [map];

    // GSE.Scene.LoadTexture("gems", "./tilesets/project_utumno_full.png");
    // GSE.Scene.LoadTexture("gems", "./images/gem_set.png");
    GSE.Scene.LoadTexture("tileset", "./images/pacman_set.png");
    console.log("Cellsize:", MG.cell_size);

    MG.squareman_spr = GSE.Scene.CreateSprite(null, "yellow", 100, 100, MG.cell_size, MG.cell_size);
    MG.wall_sprite = GSE.Scene.CreateSprite(null, "green", 100, 100, MG.cell_size, MG.cell_size);
    MG.dot_sprite = GSE.Scene.CreateSprite(null, "white", 100, 100,  Math.floor(MG.cell_size / 4),  Math.floor(MG.cell_size / 4));
    MG.super_dot_spr = GSE.Scene.CreateSprite(null, "white", 100, 100, Math.floor(MG.cell_size / 2), Math.floor(MG.cell_size / 2));
    MG.blue_spr = GSE.Scene.CreateSprite(null, "blue", 0, 0, MG.cell_size, MG.cell_size);
    MG.ghost_spr = GSE.Scene.CreateSprite(null, "red", 0, 0, MG.cell_size, MG.cell_size);

    if(map != undefined && map2 != undefined)
    {
        MG.game_grid = MG.levels[MG.current_level];
    }
    else
    {
        //TODO make empty grid
    }

   
    MG.ghosts_colors = ["pink", "orange", "red", "lime"];
    MG.ghosts_targets = [{x : 0, y : 0},  {x : MG.mapW, y : 0},  {x : 0, y : MG.mapH},  {x : MG.mapW, y : MG.mapH} ];
    MG.ghosts_colors_id = 0;

    for (var r=0; r< MG.row; r++)
    {
        for (var c=0; c< MG.col; c++)
        {
            var tile = MG.game_grid[r][c];
            switch(tile)
            {
                case 1: //wall
                    // var w = GSE.Scene.CreateEntity("wall", MG.wall_sprite, null);
                    // w.x = c * MG.cell_size;
                    // w.y = r * MG.cell_size;
                    // w.visible = false;
                    CreateWall(c, r);
                    break;
                case 2: //dot
                    var d = GSE.Scene.CreateEntity("dot", MG.dot_sprite, null);
                    d.x = c * MG.cell_size;
                    d.x = DotOffeset(d.x);
                    d.y = r * MG.cell_size;
                    d.y = DotOffeset(d.y);
                break;
                case 3: //squareman
                    MG.startingX = c * MG.cell_size;
                    MG.startingY = r * MG.cell_size;
                break;
                case 5: //super dot
                    CreateSuperDot(c, r)
                break;
            }
        }
    }

    for (var r=0; r< MG.row; r++)
    {
        for (var c=0; c< MG.col; c++)
        {
            var tile = MG.game_grid[r][c];
            switch(tile)
            {
                case 4: //ghost
                    CreateGhost(c, r);
                break;
            }
        }
    }

    CreateSquareMan();

    var mapState = GSE.StateManager.createState("Game", function mapUpdate(delta)
    {

    }, function Render(ctx) {
        // GSE.rawDrawText(MG.squareman.move.toString(), GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 60, 'white', '42');

        GSE.rawDrawText("SCORE", 268, 24, 'white', '20', 'left');
        GSE.rawDrawText(MG.score.toString(), 288, 48, 'white', '24', 'left');
        //Draw lives
        for(var i = 0; i < MG.squareman.lives; i++)
        {
            GSE.drawSprite(null, "yellow", 448 + (36*i), 800, MG.cell_size, MG.cell_size);
        }

        if(MG.level_editor) 
        {
            GSE.drawGrid(GSE.Scene.tileWidth,GSE.Scene.tileHeight, 'pink', false);
        }
    });

    GSE.StateManager.createState("GameOver", function mapUpdate(delta)
    {
        if(GSE.Input.IsKeyPressed('space'))
        {
            //Reset game
            PlayerRestart();
            MG.squareman.lives = 3;

            var dots = GSE.Scene.GetEntities("dot");
            for(i in dots)
            {
                var e = dots[i];
                e.setActive(true);
            }

            var ghosts = GSE.Scene.GetEntities("ghost");
            for(var f in ghosts)
            {
                var g = ghosts[f];
                ResetGhost(g);
            }

            resetSuperDots();

            MG.scare_state = false;
            MG.score = 0;
            MG.inGame = true;
            GSE.StateManager.setCurrentState("Game");
        }

    }, function Render(ctx) {
        // GSE.rawDrawText(MG.squareman.move.toString(), GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 60, 'white', '42');

        GSE.rawDrawText("GAME OVER", 224, 320, 'white', '40', 'left');
        GSE.rawDrawText("Space to restart", 224, 350, 'white', '30', 'left');
    });

    GSE.StateManager.setCurrentState("Game");

    GSE.Scene.tileHeight = MG.cell_size;
    GSE.Scene.tileWidth = MG.cell_size;

    GSE.Scene.setTileImage("tileset", MG.cell_size, MG.cell_size, 10, 6);

    GSE.Scene.LoadMap(tileMap, "tileMap");
}

function CreateGhost(c, r)
{
    
    var ghost = GSE.Scene.CreateEntity("ghost", MG.ghost_spr, GhostMoves);
    ghost.x = c * MG.cell_size;
    ghost.y = r * MG.cell_size;
    ghost.start_x = ghost.x;
    ghost.start_y = ghost.y;
    ghost.dir = "u";
    ghost.vx = 0;
    ghost.vy = -1;
    ghost.move = 0;
    ghost.start_move = MG.cell_size;
    ghost.speed = 110;
    // ghost.dir_id = 0;
    ghost.colors = [MG.ghosts_colors[MG.ghosts_colors_id], MG.scare_color];
    // ghost.old_color = ghost.color;
    ghost.scared = false;
    ghost.current_color = 0;
    ghost.color_swap_time = MG.ghost_color_swap_time;

    ghost.target = MG.ghosts_targets[MG.ghosts_colors_id];
    ghost.target_id = MG.ghosts_colors_id;
    ghost.status = "scutter";
    ghost.scatter_time = MG.ghosts_scatter_time;
    ghost.chase_time = MG.STATES.CHASE_TIME;

    MG.ghosts_colors_id++;
    MG.ghosts_colors_id %= MG.ghosts_colors.length;
}

function CreateWall(c, r)
{
    var w = GSE.Scene.CreateEntity("wall", MG.wall_sprite, UpdateWall);
    w.x = c * MG.cell_size;
    w.y = r * MG.cell_size;
    w.visible = false;
}

function UpdateWall(self)
{
    if(MG.level_editor)
    {
        self.visible = true;
    }
    else
    {
        self.visible = false;
    }
}

function CreateSquareMan()
{
    MG.squareman = GSE.Scene.CreateEntity("squareman", MG.squareman_spr, PlayerMovements);
    MG.squareman.x = MG.startingX;
    MG.squareman.y = MG.startingY;
    MG.squareman.dir = "r";
    MG.squareman.next_dir = "r";
    MG.squareman.move = MG.cell_size;
    MG.squareman.start_move = MG.cell_size;
    MG.squareman.canMove = true;
    MG.squareman.speed = 120;
    MG.squareman.vx = 1;
    MG.squareman.vy = 0;
    MG.squareman.super_dot_time = MG.super_dot_time; //sec
    MG.squareman.lives = 3;
}

function ResetGhost(ghost)
{
    ghost.x = ghost.start_x;
    ghost.y = ghost.start_y;
    ghost.scared = false;
    ghost.dir = "u";
    ghost.vx = 0;
    ghost.vy = -1;
    ghost.move = 0;
    ghost.current_color = 0;
    ghost.status = MG.STATES.SCATTER;
    MG.ghosts_scatter_time = MG.STATES.SCATTER_TIME; //msec
    MG.ghosts_chase_time = MG.STATES.CHASE_TIME;
}

function AddEntity(c, r, tile)
{
    switch(tile)
    {
        case 1: //wall
            // var w = GSE.Scene.CreateEntity("wall", MG.wall_sprite, null);
            // w.x = c * MG.cell_size;
            // w.y = r * MG.cell_size;
            // w.visible = false;
            CreateWall(c, r);
            break;
        case 2: //dot
            var d = GSE.Scene.CreateEntity("dot", MG.dot_sprite, null);
            d.x = c * MG.cell_size;
            d.x = DotOffeset(d.x);
            d.y = r * MG.cell_size;
            d.y = DotOffeset(d.y);
        break;
        case 3: //squareman
            // MG.squareman = GSE.Scene.CreateEntity("squareman", MG.squareman_spr, PlayerMovements);
            MG.startingX = c * MG.cell_size;
            MG.startingY = r * MG.cell_size;
        break;
        case 4: //ghost
            CreateGhost(c, r);
        break;
        case 5: //super dot
            CreateSuperDot(c, r)
        break;
    }

    CreateSquareMan();
}

function CreateSuperDot(c, r)
{
    var sd = GSE.Scene.CreateEntity("super_dot", MG.super_dot_spr, null);
    sd.x = c * MG.cell_size;
    sd.y = r * MG.cell_size;
    sd.x = SuperDotOffeset(sd.x);
    sd.y = SuperDotOffeset(sd.y);
}

function DeleteEntity(c, r, tile)
{
    var id = "";
    switch(tile)
    {
        case 1: //wall
            id = "wall"
            break;
        case 2: //dot
            id = "dot"
        break;
        case 3:
            id = "squareman"
        break;
        case 4: //ghost
            id = "ghost"
        break;
        case 5: //super_dot
            id = "super_dot"
        break;
    }

    var dotE = GSE.Scene.FindEntity(function(e, i)
    {
        var x = (c * MG.cell_size)
        var y = (r * MG.cell_size)
        if(id == "dot")
        {
            x = DotOffeset(x);
            y = DotOffeset(y);            
        }
        if(id == "super_dot")
        {
            x = SuperDotOffeset(x);
            y = SuperDotOffeset(y);
        }
        return (e.x == x && e.y == y && e.id == id);
    });

    if(dotE)
    {
        console.log("remove ent");
        GSE.Scene.RemoveEntityByUid(dotE.i);
    }
}

function checkBounds(x1,y1, w1, h1, x2,y2,w2,h2) {
    if (x1 < x2 + w2 &&
         x1 + w1 > x2 &&
         y1 <y2 + h2 &&
         h1 + y1 >y2) {
            // collision detected!
        return true;
    }

    return false;
}

function distance(x1, y1, x2, y2) {
    var dx = Math.pow((x2-x1),2);
    var dy = Math.pow((y2-y1),2);

    // console.log("dx:", dx, " dy:", dy);

    var dist = Math.sqrt(dx + dy);
    return Math.round(dist);
}

function resetSuperDots()
{
    GSE.Scene.GetEntities("super_dot").forEach(function (e){
        e.setActive(true);
    });
}

function saveGameGrid(name)
{
    var map_file = JSON.stringify(MG.game_grid);

    try {
        var isFileSaverSupported = !!new Blob;
        var blob = new Blob(["var " + name + "=" + map_file + ";"], {type: "text/plain;charset=utf-8"});
        console.log("blob:", blob);
        saveAs(blob, "game_grid.js");
    } 
    catch (e) 
    {
        console.log("exception:", e);
        console.log("manual save:", map_file);
    }
}

function Update(lastTick, timeSinceTick)
{
    if(GSE.Input.IsKeyPressed('p'))
    {
        MG.inGame = !MG.inGame
    }

    if(MG.level_editor == true)
    {
        if(GSE.Input.IsMouseLeftDown())
        {
            // console.log("clicked");
            var mX = GSE.Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
            var mY = GSE.Input.mouse.clientY;

            // console.log("mx:", mX, "my:", mY);

            
            var mapX = Math.floor(mX / MG.cell_size);
            var mapY = Math.floor(mY / MG.cell_size);
            // console.log("x:", mapX, "y:", mapY);

            if(MG.game_grid[mapY][mapX] == 0)
            {
                MG.game_grid[mapY][mapX] = 1;
                AddEntity(mapX, mapY, 1);
            }
        }

        if(GSE.Input.IsKeyDown('o'))
        {
            // console.log("clicked");
            var mX = GSE.Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
            var mY = GSE.Input.mouse.clientY;

            // console.log("mx:", mX, "my:", mY);

            
            var mapX = Math.floor(mX / MG.cell_size);
            var mapY = Math.floor(mY / MG.cell_size);
            // console.log("x:", mapX, "y:", mapY);

            if(MG.game_grid[mapY][mapX] == 0)
            {
                MG.game_grid[mapY][mapX] = 2;
                AddEntity(mapX, mapY, 2);
            }

        }

        if(GSE.Input.IsKeyDown('l'))
        {
            // console.log("clicked");
            var mX = GSE.Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
            var mY = GSE.Input.mouse.clientY;

            // console.log("mx:", mX, "my:", mY);

            
            var mapX = Math.floor(mX / MG.cell_size);
            var mapY = Math.floor(mY / MG.cell_size);
            // console.log("x:", mapX, "y:", mapY);

            if(MG.game_grid[mapY][mapX] == 0)
            {
                MG.game_grid[mapY][mapX] = 3;
                AddEntity(mapX, mapY, 3);
            }

        }

        if(GSE.Input.IsKeyDown('g'))
        {
            // console.log("clicked");
            var mX = GSE.Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
            var mY = GSE.Input.mouse.clientY;

            // console.log("mx:", mX, "my:", mY);

            
            var mapX = Math.floor(mX / MG.cell_size);
            var mapY = Math.floor(mY / MG.cell_size);
            // console.log("x:", mapX, "y:", mapY);

            if(MG.game_grid[mapY][mapX] == 0)
            {
                MG.game_grid[mapY][mapX] = 4;
                AddEntity(mapX, mapY, 4);
            }
        }

        if(GSE.Input.IsKeyDown('k'))
        {
            // console.log("clicked");
            var mX = GSE.Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
            var mY = GSE.Input.mouse.clientY;


            // console.log("mx:", mX, "my:", mY);
            
            var mapX = Math.floor(mX / MG.cell_size);
            var mapY = Math.floor(mY / MG.cell_size);
            // console.log("x:", mapX, "y:", mapY);

            if(MG.game_grid[mapY][mapX] == 0)
            {
                MG.game_grid[mapY][mapX] = 5;
                AddEntity(mapX, mapY, 5);
            }
        }

        if(GSE.Input.IsKeyDown('d'))
        {
            var mX = GSE.Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
            var mY = GSE.Input.mouse.clientY;

            var mapX = Math.floor(mX / MG.cell_size);
            var mapY = Math.floor(mY / MG.cell_size);

            if(MG.game_grid[mapY][mapX] != 0)
            {
                DeleteEntity(mapX, mapY, MG.game_grid[mapY][mapX]);
            }
            MG.game_grid[mapY][mapX] = 0;
        }

        if(GSE.Input.IsKeyPressed('u'))
        {
            saveGameGrid("map");
        }
    }

    //Disable the game when in Tileset editor mode or Level Editor mode
    if(GSE.editorMode || MG.level_editor)
    {
        MG.inGame = false;
    }
    else
    {
        MG.inGame = true;
    }

    //toggle level editor
    if(GSE.Input.IsKeyPressed('v'))
    {
        MG.level_editor = !MG.level_editor;
    }

    if (GSE.Scene.CountActiveEntitiesById("dot") <= 0 && GSE.editorMode == false)
    {
        NextLevel();

        MG.scare_state = false;
    }

    GSE.Scene.GetEntities("ghost").forEach(function(g)
    {
        if (GSE.Collisions.CheckCollision(g, MG.squareman) == true)
        {
            if(MG.squareman.lives > 0)
            {
                if(MG.scare_state == false)
                {
                    //Loose life
                    MG.squareman.lives--;
                    PlayerRestart();
                    var ghosts = GSE.Scene.GetEntities("ghost");
                    for(var i in ghosts)
                    {
                        var g = ghosts[i];
                        ResetGhost(g);
                    }
                }
                else
                {
                    //Gain score, reset the ghost
                    MG.score += 20;
                    ResetGhost(g);
                }
            }
            else
            {
                //Game over
                MG.inGame = false;
                GSE.StateManager.setCurrentState("GameOver");
            }
        }
    });

    //Check super_dot time end
    if(MG.squareman.super_dot_time <= 0)
    {
        MG.scare_state = false;
        MG.squareman.super_dot_time = MG.super_dot_time;

        var ghosts = GSE.Scene.GetEntities("ghost");
        for(var i in ghosts)
        {
            ghosts[i].scared = false;
            ghosts[i].status = MG.STATES.SCATTER;
            ghosts[i].scatter_time = MG.STATES.SCATTER_TIME;
        }
    }
     //reduce super_dot time
    if(MG.scare_state == true)
    {
        MG.squareman.super_dot_time -= 1 * timeSinceTick;
    }

    //Start super_dot time
    GSE.Scene.GetEntities("super_dot").forEach(function(sd){
        if (GSE.Collisions.CheckCollision(sd, MG.squareman) == true)
        {
            //Gain score, reset the ghost
            MG.score += 15;
            sd.setActive(false);
            MG.scare_state = true;

            var ghosts = GSE.Scene.GetEntities("ghost");
            for(var i in ghosts)
            {
                ghosts[i].scared = true;
                ghosts[i].status = MG.STATES.SCARED;
            }
        }
    });

    //go to the next level
    if(GSE.Input.IsKeyPressed('j'))
    {
        NextLevel();
    }
}

function NextLevel()
{
    MG.current_level++;
    MG.current_level %= MG.levels.length;
    MG.game_grid = MG.levels[MG.current_level];
    //Clear the entities
    // GSE.Scene.entites = [];
    GSE.Scene.RemoveAllEntity("all");

    //Recreate the entities.

    for (var r=0; r< MG.row; r++)
    {
        for (var c=0; c< MG.col; c++)
        {
            var tile = MG.game_grid[r][c];
            switch(tile)
            {
                case 1: //wall
                    // var w = GSE.Scene.CreateEntity("wall", MG.wall_sprite, null);
                    // w.x = c * MG.cell_size;
                    // w.y = r * MG.cell_size;
                    // w.visible = false;
                    CreateWall(c, r);
                    break;
                case 2: //dot
                    var d = GSE.Scene.CreateEntity("dot", MG.dot_sprite, null);
                    d.x = c * MG.cell_size;
                    d.x = DotOffeset(d.x);
                    d.y = r * MG.cell_size;
                    d.y = DotOffeset(d.y);
                break;
                case 3: //squareman
                    MG.startingX = c * MG.cell_size;
                    MG.startingY = r * MG.cell_size;
                break;
                case 5: //super dot
                    CreateSuperDot(c, r)
                break;
            }
        }
    }

    CreateSquareMan();

    for (var r=0; r< MG.row; r++)
    {
        for (var c=0; c< MG.col; c++)
        {
            var tile = MG.game_grid[r][c];
            switch(tile)
            {
                case 4: //ghost
                    CreateGhost(c, r);
                break;
            }
        }
    }
}

function PlayerRestart()
{
    MG.squareman.x = MG.startingX;
    MG.squareman.y = MG.startingY;
    MG.squareman.dir = "r";
    MG.squareman.next_dir = "r";
    MG.squareman.move = MG.cell_size;
    MG.squareman.vx = 1;
    MG.squareman.vy = 0;
}

function PlayerMovements(self, delta)
{    
    if(MG.inGame == false)
    {
        return;
    }

    checkInput(self);
    if(self.move <= 0)
    {
        self.x = getMapXY(self.x) * MG.cell_size;
        self.y = getMapXY(self.y) * MG.cell_size;
        
        if(checkFreeBlock(self, self.next_dir))
        {
            self.dir = self.next_dir;
            self.move = self.start_move;
        }
        else if(checkFreeBlock(self, self.dir))
        {
            self.move = self.start_move;
        }

        //warp
        if(self.x > MG.mapW) //warp right
        {
            self.x = -MG.cell_size;
        }
        else if(self.x <= -MG.cell_size) //warp left
        {
            self.x = MG.mapW + MG.cell_size;
        }
        
        if(self.y > MG.mapH) //warp down
        {
            self.y = -MG.cell_size;
        }
        else if(self.y <= -MG.cell_size) //warp up
        {
            self.y = MG.mapH + MG.cell_size;
        }

        //dot
        var dot = checkDot(self);
        if(dot.check == true)
        {
            //TODO score up
            // console.log("score");
            // MG.game_grid[dot.mapY][dot.mapX] = 0;
            // console.log("map" + MG.game_grid[dot.mapY][dot.mapX]);
            var dotE = GSE.Scene.FindEntity(function(e)
            {
                var realx, realy;
                realx = getMapXY(e.x);
                realy = getMapXY(e.y);
                return dot.mapX == realx && dot.mapY == realy && e.id == "dot" && e.active == true;
            });

            if(dotE)
            {
                console.log("deactivate ent");
                dotE.ent.setActive(false);
                MG.score += 10;
            }
        }
    }
    else
    {
        MoveInDirection(self, delta);
    }
}

var corners = [
    [1,1],
    [17,1],
    [1,28],
    [17,28],
]

// var path = [];

function getDirToTarget(start_x, start_y, target_x, target_y)
{
    debugger;
    var path = [];
    if(start_x == target_x && start_y == target_y)
    {
        //Goal reached add and go back
        path.push([start_x, start_y]);
        return path;
    }
    
    if(MG.game_grid[start_y][start_x] == 1)
    {
        return path;
    }

    if(path.indexOf([start_x, start_y]) != -1)
    {
        return path;
    }

    //check left,  up, right, down
    path.push(getDirToTarget(start_x - 1, start_y, target_x, target_y));
    if(path.indexOf([target_x, target_y]) != -1)
    {
        return path;
    }

    path.push(getDirToTarget(start_x, start_y - 1, target_x, target_y));
    if(path.indexOf([target_x, target_y]) != -1)
    {
        return path;
    }

    path.push(getDirToTarget(start_x + 1, start_y, target_x, target_y));
    if(path.indexOf([target_x, target_y]) != -1)
    {
        return path;
    }

    path.push(getDirToTarget(start_x, start_y + 1, target_x, target_y));
    return path;
}

function GetDistance(x1, y1, x2, y2) {
    var dx = Math.pow((x2-x1),2);
    var dy = Math.pow((y2-y1),2);

    // console.log("dx:", dx, " dy:", dy);

    var dist = Math.sqrt(dx + dy);
    return Math.round(dist);
}

function followTarget(self)
{
    var res_dir = "r";
    
    var dirs = ["r", "l", "u", "d"];
    
    var free_dir = [];

    for(dir in dirs)
    {
        if(checkFreeBlock(self, dirs[dir]) == true)
        {
            free_dir.push(dirs[dir]);
        }
    }

    //remove the direction from where the ghost came
    var remove = 0;
    if(free_dir.length > 1)
    {
        switch(self.dir)
        {
            case "l": //remove right
                remove = free_dir.indexOf("r");
                break;
            case "r": //remove left
                remove = free_dir.indexOf("l");
                break;
            case "u": //remove down
                remove = free_dir.indexOf("d");
                break;
            case "d": //remove up
                remove = free_dir.indexOf("u");
                break;
        }

        // console.log("remove", remove);

        if(remove != -1)
        {
            // console.log("splice");
            free_dir.splice(remove, 1);
        }
    }

    //The ghost is chasing the player
    var target_x = getMapXY(self.target.x);
    var target_y = getMapXY(self.target.y);

    var curr_x = getMapXY(self.x);
    var curr_y = getMapXY(self.y);

    //Loop the free direction and pick the closer to the target
    var min_distance = 999;
    var closer_dir = 0;
    var distance = 0;
    for(var i=0; i<free_dir.length; i++)
    {
        var d = free_dir[i];
        switch(d)
        {
            case "l": //one block to the left
                distance = GetDistance(curr_x - 1, curr_y, target_x, target_y);

                if(distance < min_distance)
                {
                    min_distance = distance;
                    closer_dir = i;
                }

                break;
            case "r": //right
                distance = GetDistance(curr_x + 1, curr_y, target_x, target_y);

                if(distance < min_distance)
                {
                    min_distance = distance;
                    closer_dir = i;
                }
                break;
            case "u": //up
                distance = GetDistance(curr_x, curr_y - 1, target_x, target_y);

                if(distance < min_distance)
                {
                    min_distance = distance;
                    closer_dir = i;
                }
                break;
            case "d": //remove up
                distance = GetDistance(curr_x, curr_y + 1, target_x, target_y);

                if(distance < min_distance)
                {
                    min_distance = distance;
                    closer_dir = i;
                }
                break;
        }
    }

    return free_dir[closer_dir];
}

function selectDirection(self)
{    
    var dirs = ["r", "l", "u", "d"];
    
    var free_dir = [];

    for(dir in dirs)
    {
        if(checkFreeBlock(self, dirs[dir]) == true)
        {
            free_dir.push(dirs[dir]);
        }
    }

    // console.log("free_dir TOTAL", free_dir);

    var remove = 0;
    if(free_dir.length > 1)
    {
        switch(self.dir)
        {
            case "l": //remove right
                remove = free_dir.indexOf("r");
                break;
            case "r": //remove left
                remove = free_dir.indexOf("l");
                break;
            case "u": //remove down
                remove = free_dir.indexOf("d");
                break;
            case "d": //remove up
                remove = free_dir.indexOf("u");
                break;
        }

        // console.log("remove", remove);

        if(remove != -1)
        {
            // console.log("splice");
            free_dir.splice(remove, 1);
        }

        if(self.x == MG.squareman.x) // follow vertical
        {
            if(self.y > MG.squareman.y)
            {
                //Up
                if(free_dir.indexOf("u") != -1)
                {
                    self.dir = "u";
                    return;
                }
            }
            else if(free_dir.indexOf("d") != -1)
            {
                self.dir = "d";
                return;
            }
        }
        else if(self.y == MG.squareman.y) // follow horizzontal
        {
            if(self.x > MG.squareman.x)
            {
                //Up
                if(free_dir.indexOf("l") != -1)
                {
                    self.dir = "l";
                    return;
                }
            }
            else if(free_dir.indexOf("r") != -1)
            {
                self.dir = "r";
                return;
            }
        }
    }

    // console.log("free_dir", free_dir);

    var dir_id = Math.round(GSE.randomRange(0, free_dir.length-1));

    // console.log(dir_id);

    // debugger;
    self.dir = free_dir[dir_id];
}

function GhostMoves(self, delta)
{
    // console.log("#GhostMoves start x:", self.x)
    if(MG.inGame == false)
    {
        return;
    }

    //TODO make a correct state machine
    switch(self.status)
    {
        case MG.STATES.SCATTER:
                //follow the base target
                self.target = MG.ghosts_targets[self.target_id];
                if(self.scatter_time > 0)
                {
                    self.scatter_time -= 1 * delta;
                }
                else
                {
                    self.status = MG.STATES.CHASE;
                    self.scatter_time = MG.STATES.SCATTER_TIME;
                }
            break;
        case MG.STATES.CHASE:
                //Follow the player
                self.target = {"x" : MG.squareman.x, "y" : MG.squareman.y};
                
                if(self.chase_time > 0)
                {
                    self.chase_time -= 1 * delta;
                }
                else
                {
                    self.status = MG.STATES.SCATTER;
                    self.chase_time = MG.STATES.CHASE_TIME;
                }           
            break;
        case MG.STATES.SCARED:
                //TODO make the ghost get a random tagert every frame
                self.target = MG.ghosts_targets[self.target_id];
            break;
        case MG.STATES.EATEN:
                self.target = {"x" : 208, "y" : 224};
            break;
        default:
                self.target = MG.ghosts_targets[self.target_id];
            break;
    }

    if(self.scared == true)
    {
        if(MG.squareman.super_dot_time > 2000)
        {
            self.current_color = 1;
        }
        else
        {
            self.blink = true;
        }

        if (self.blink == true)
        {
            if(self.color_swap_time <= 0)
            {
                self.current_color++;
                self.current_color %= self.colors.length;
                self.color_swap_time = MG.ghost_color_swap_time;
            }

            self.color_swap_time -= 10 * delta;
        }
    }
    else
    {
        self.current_color = 0;
        self.blink = false;
        self.color_swap_time = MG.ghost_color_swap_time;
    }

    self.color = self.colors[self.current_color];

    if(self.move <= 0)
    {
        self.x = getMapXY(self.x) * MG.cell_size;
        self.y = getMapXY(self.y) * MG.cell_size;

        //warp
        if(self.x >= MG.mapW && self.dir == "r") //warp right
        {
            self.x = -31;
        }
        else if(self.x <= -MG.cell_size && self.dir == "l") //warp left
        {
            self.x = MG.mapW - 30;
        }
        else if(self.y >= MG.mapH && self.dir == "d") //warp down
        {
            self.y = -31;
        }
        else if(self.y <= -MG.cell_size && self.dir == "u") //warp up
        {
            self.y = MG.mapH + 30;
        }
        else
        {
            // selectDirection(self);
            self.dir = followTarget(self);
        }
        
        if(checkFreeBlock(self, self.dir))
        {
            self.move = self.start_move;
        }

        //Change target if you get to current target
    }
    else
    {
        MoveInDirection(self, delta);
    }

    // console.log("#GhostMoves end x:", self.x);
}

function checkDot(self)
{
    var mapX = getMapXY(self.x);
    var mapY = getMapXY(self.y);

    var res = {
        "check" : false,
        "mapX" : mapX,
        "mapY" : mapY
    }

    if(mapY >=0 && mapY < MG.row && mapX >= 0 && mapX < MG.col)
    {
        res.check = MG.game_grid[mapY][mapX] == 2; //true = dot
    }

    return res;
}

function checkFreeBlock(self, dir)
{
    self.vx = 0;
    self.vy = 0;

    switch(dir)
    {
        case "l": //push left
            self.vx = -1;
            break;
        case "r": //push right
            self.vx = 1;
            break;
        case "u": //push up
            self.vy = -1;
            break;
        case "d": //push down
            self.vy = 1;
            break;
    }


    var mapX = getMapXY(self.x - Math.floor(MG.cell_size / 2)) + self.vx;
    var mapY = getMapXY(self.y - Math.floor(MG.cell_size / 2)) + self.vy;
    
    if(mapY >=0 && mapY < MG.row && mapX >= 0 && mapX < MG.col)
    {
        return (MG.game_grid[mapY][mapX] != 1); //true = free
    }
    else //out of map keep going
    {
        return true;
    }
}

function checkInput(self)
{
    if(GSE.Input.IsKeyDown('right'))
    {
        self.next_dir = "r";
    }

    if(GSE.Input.IsKeyDown('left'))
    {
        self.next_dir = "l";
    }

    if(GSE.Input.IsKeyDown('up'))
    {
        self.next_dir = "u";
    }

    if(GSE.Input.IsKeyDown('down'))
    {
        self.next_dir = "d";
    }

}

function getMapXY(x)
{
    return Math.round( Math.round(x) / MG.cell_size);
}

function MoveInDirection(self, delta)
{
    self.x += (self.speed * self.vx) * delta;
    self.y += (self.speed * self.vy) * delta;
    self.move -= self.speed * delta;
}
