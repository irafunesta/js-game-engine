MG = {};

;(function () {
	GSE.Init("myCanvas", 800, 600, 800, 600, false, InitGame, Update);

	MG.socket = socketCluster.connect({
		codecEngine: scCodecMinBin
	});

	MG.socket.on('error', function (err) {
	  throw 'Socket error - ' + err;
	});

	MG.socket.on('connect', function () {
	  console.log('CONNECTED');
	});

	MG.socket.on('rand', function (data) {
	  console.log('RANDOM STREAM: ' + data.rand);
	});

	MG.socket.on("updateGame", function(data) {
		UpdateFromServer(data);
	});

	// var updateGameChannel = MG.socket.subscribe('updateGame');
	var disconnectChannel = MG.socket.subscribe('p_disconnect');

	// updateGameChannel.on('subscribeFail', function (err) {
	//   console.log('Failed to subscribe to the updateGame channel due to error: ' + err);
	// });

	disconnectChannel.on('subscribeFail', function (err) {
	  console.log('Failed to subscribe to the p_disconnect channel due to error: ' + err);
	});

	// updateGameChannel.watch(UpdateFromServer);
	disconnectChannel.watch(PlayerDisconnected);
	// MG.socket.on("playerUp", UpdateFromServer);
	// MG.socket.on("p_disconnect", PlayerDisconnected);
})();

function SendPlayerPos(pos)
{
	if(MG.socket)
	{
		MG.socket.emit("move", pos);
	}
}

function UpdateStatus(pos)
{
	console.log("AskUpdate");
	if(MG.socket)
	{
		MG.socket.emit("askUpdate", pos);
	}
}

function handleUpdate(self)
{
	var pos = {"x": self.x, "y": self.y};
	var speed = 8;

	if(GSE.Input.IsKeyDown('d'))
	{
		pos.x += speed
	}
	if(GSE.Input.IsKeyDown('a'))
	{
		pos.x -= speed
	}
	if(GSE.Input.IsKeyDown('w'))
	{
		pos.y -= speed
	}
	if(GSE.Input.IsKeyDown('s'))
	{
		pos.y += speed
	}

	SendPlayerPos(pos);
	// self.x = pos.x;
	// self.y = pos.y;
}

function PlayerDisconnected(id)
{
	console.log(id, " disconnected");
	var p = GSE.Scene.GetEntity(id);
	if(p.tag != null | p.tag != undefined)
	{
		GSE.Scene.UI.RemoveEntity(p.id);
	}
	GSE.Scene.RemoveEntity(id);
}

function MPoint(x, y)
{
	return {"x":x, "y":y};
}

function MidPoint(p1, p2)
{
	var mid = {};

	mid.x = Math.round((p1.x + p2.x) / 2);
	mid.y = Math.round((p1.y + p2.y) / 2);

	return mid;
}

function UpdateFromServer(data)
{
	// debugger;
	data.players.forEach(function(player)
	{
		if(player.id == MG.socket.id) {

			var mid_x = (player.pos.x + MG.playerEnt.x) / 2;
			var mid_y = (player.pos.y + MG.playerEnt.y) / 2;

			var mid = MidPoint(player.pos, MG.playerEnt);
			//This is my player
			MG.playerEnt.x = mid.x;
			MG.playerEnt.y = mid.y;
			MG.playerTag.x = mid.x;
			MG.playerTag.y = mid.y;
			MG.playerTag.text = player.id;
		}
		else {
			var otherPg = GSE.Scene.GetEntity(player.id);
			if(otherPg != null)
			{
				var mid_x = (player.pos.x + otherPg.x) / 2;
				var mid_y = (player.pos.y + otherPg.y) / 2;

				var mid = MidPoint(player.pos, otherPg);

				//Update the entity
				otherPg.x = mid.x;
				otherPg.y = mid.y;
				otherPg.tag.x = mid.x;
				otherPg.tag.y = mid.y;
				otherPg.tag.text = player.id;
			}
			else {
				var spr = GSE.Scene.CreateSprite("/img/alien1.png", '', 20, GSE.Window.screenHeight/2, 32, 32);

				var pg = GSE.Scene.CreateEntity(player.id, spr, null);
				pg.x = player.pos.x;
				pg.y = player.pos.y;
				pg.tag = GSE.Scene.UI.DrawText(player.id, player.pos.x, player.pos.y, "white", "12");
			}
		}
	});

	// MG.playerEnt.tag = data.tag;
	//console.log("Server status ", JSON.stringify(data));

}

function InitGame(ctx)
{
	// MG.context = ctx;
	var image = "";

	//A way to create the Scene, and add object to it for drawing on the window
	var id = "Player";


	// var enemySpr = GSE.Scene.CreateSprite('./sprites/goblin.png', 'red', 235,98,128,128);
	var playerSpr2 = GSE.Scene.CreateSprite("/img/alien1.png", 'yellow', 20, GSE.Window.screenHeight/2, 32, 32);

	MG.playerEnt = GSE.Scene.CreateEntity('player', playerSpr2, handleUpdate);
	MG.playerEnt.tag = "guest";
	MG.playerTag = GSE.Scene.UI.DrawText(MG.playerEnt.tag, MG.playerEnt.x, MG.playerEnt.y, "white", "12");

	var mapState = GSE.StateManager.createState("Game", function mapUpdate() {

	}, function Render(ctx) {
		GSE.rawDrawText(MG.playerEnt.x.toString(), 20, 20, 'white', '24', 'left', true);
		GSE.rawDrawText(MG.playerEnt.y.toString(), 20, 40, 'white', '24', 'left', true);
		if(GSE.Window.fps)
		{
			GSE.rawDrawText(GSE.Window.fps.toString(), 20, 60, 'white', '24', 'left', true);
		}
		// GSE.rawDrawText(MG.highscore.toString(), GSE.Window.screenWidth / 2 + 128 + 64, 60, 'white', '20');
	});

	GSE.StateManager.setCurrentState("Game");
	// var handleUpdatePlayer =

	//MG.cellSpr =  GSE.Scene.CreateSprite("", 'blue', 0, 0, 2, 2);
	// MG.cellGrid = [GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2];

	// for(i = 0; i < GSE.Window.screenWidth / 2; i++) //y
	// {
	// 	for(j = 0; j < GSE.Window.screenHeight / 2; j++) //x
	// 	{
	// 		var id = i.toString() + j.toString();
	// 		MG.cellSpr.x = j * 2;
	// 		MG.cellSpr.y = i * 2;
	// 		var cell = GSE.Scene.CreateEntity(id, MG.cellSpr, cellUpdate);
	// 		cell.food = GSE.randomRange(100, 1000);
	// 		// MG.cellGrid[i, j] = cell;
	// 	}
	// }

	// var enemy = GSE.Scene.CreateEntity('Enemy', enemySpr, updateEnemy, false, false);

	// GSE.Scene.setTileImage("./tilesets/tileSetTest.png");
	//
	// GSE.Scene.map = [
	// 	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,
	// 	0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,1,0,0,0,0,0,1,9,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
	// ];

	// player.speed = 4;
	// player.stepThreshold = 10;
	// player.steps = 0
	// player.canMove = true;
	// player.hp = 1;
	// player.maxHp = 10;
	//
	// enemy.hp = 3;
	// enemy.maxHp = 3;
	// enemy.canAttack = true;

	// player.checkSteps = function(self) {
	// 	if(self.steps < self.stepThreshold)
	// 	{
	// 		self.steps++;
	// 	}
	// 	else {
	// 		self.steps = 0;
	// 		MG.EnterCombat();
	// 	}
	// }
	// if(MG.socket)
	// {
	// 	MG.socket.on("playerUp", UpdateFromServer);
	// }

	var combatOptions = ["Attack", "Item", "Flee", "Pass"];
	var currentOption = 0;
	MG.combatTurn = -1; //0 player, 1 Enemy
	MG.start = false;
	MG.halfCount = 0;
}

function Update(lastTick, timeSinceTick)
{
	// if(MG.halfCount % 3 == 0)
	// {
	// 	// SendPlayerPos({"x":MG.playerEnt.x,"y":MG.playerEnt.y});
	// 	MG.socket.emit("askUpdate");
	// }
	//SendPlayerPos({"x":MG.playerEnt.x,"y":MG.playerEnt.y});
	// MG.halfCount += 1;
	// MG.halfCount %= 2;
	// if(MG.inGame == true)
	// {
	// 	MG.scoreText.text = MG.score;
	// 	MG.score += 1;
	// }
	// var player = GSE.Scene.GetEntity('Player');
	// MG.nextPos.x = player.x + 4;
	// MG.nextPos.y = player.y + 4;

	if(GSE.Input.IsKeyPressed('r'))
	{
		console.log("r");
		// GSE.StateManager.nextState("Map");

			GSE.Scene.entities[160001].food = 0;
	}
	if(GSE.Input.IsKeyPressed('q'))
	{
		MG.start = !MG.start;
	}
}
