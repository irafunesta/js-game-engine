var SCWorker = require('socketcluster/scworker');
var express = require('express');
var serveStatic = require('serve-static');
var path = require('path');
var morgan = require('morgan');
var healthChecker = require('sc-framework-health-check');
var scCodecMinBin = require('sc-codec-min-bin');

class Worker extends SCWorker {
  run() {
    console.log('   >> Worker PID:', process.pid);
    var environment = this.options.environment;

    var app = express();

    var httpServer = this.httpServer;
    var scServer = this.scServer;

		this.scServer.setCodecEngine(scCodecMinBin);

    if (environment === 'dev') {
      // Log every HTTP request. See https://github.com/expressjs/morgan for other
      // available formats.
      app.use(morgan('dev'));
    }
    app.use(serveStatic(path.resolve(__dirname, 'public')));

    // Add GET /health-check express route
    healthChecker.attach(this, app);

    httpServer.on('request', app);

    var count = 0;
		var players = [];
		var roomW = 800;
		var roomH = 600;
		var index_p = {};
    /*
      In here we handle our incoming realtime connections and listen for events.
	  // Some sample logic to show how to handle client events,
	  // replace this with your own logic
    */
    scServer.on('connection', function (socket) {
			let p_id = players.push ({
				"id":socket.id,
				"pos": {
					x: 200,
					y: 200
				}
			});

			p_id -= 1;
			index_p[socket.id] = p_id;

			console.log("a user connected ", socket.id);

			socket.on("askUpdate", function () {
				// socket.emit("playerUp", {"players":players,"current":socket.id});
				socket.emit('updateGame', {"players":players,"current":socket.id});
			}) ;

			socket.on("move", function(pos) {
			  //console.log("move: " + pos, socket.id);
			  var currentPlayer = players.find(function(p)
			  {
				  return p.id == socket.id
			  });

			  var data = {};
			  if(pos.x > 0 && pos.x < roomW - 32 && pos.y > 0 && pos.y < roomH - 32)
			  {
					currentPlayer.pos = pos;
			  }

			  // currentPlayer.id;

			  //socket.emit("playerUp", {"players":players,"current":socket.id});
			});

		// socket.on('askUpdate', function (data) {
		// 	count++;
		// 	console.log('Handled sampleClientEvent', data);
		// 	scServer.exchange.publish('sample', count);
		// });
		//
			var interval = setInterval(function () {
				socket.emit('updateGame', {"players":players,"current":socket.id});
			}, 1000/30);

			socket.on('disconnect', function () {
				// clearInterval(interval);
				console.log("player disconnect", socket.id);
				var pid = 0
				var currentPlayer = players.find(function(p,i)
				{
					pid = i;
					return p.id == socket.id
				});
				players.splice(pid, 1);
				//io.emit("playerUp", {"players":players,"current":socket.id});
				socket.exchange.publish('p_disconnect', socket.id);
				clearInterval(interval);
			});
    });
  }
}

new Worker();
