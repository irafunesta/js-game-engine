MG = {};

;(function () {
	GSE.Init("myCanvas", 400, 400, false, InitGame, Update);
	MG.socket = io("http://109.235.104.115:3001/");
	MG.socket.on("playerUp", UpdateFromServer);
	MG.socket.on("p_disconnect", PlayerDisconnected);
})();

function cellUpdate(self)
{
	if(MG.start == true)
	{
		self.food -= 1
	}

	var food = self.food;

	if(food < 1)
	{
		self.color = 'black';
		// self.food = GSE.randomRange(50, 90);
	}
	else if(food > 1 && food < 10)
	{
		self.color = 'red';
	}
	else if(food > 10 && food < 50)
	{
		self.color = 'yellow';
		self.food = GSE.randomRange(50, 1000);
	}
	else if(food > 50 && food < 90)
	{
		self.color = 'green';
	}
	else
	{
		self.color = 'blue';
	}
}
function SendPlayerPos(pos)
{
	if(MG.socket)
	{
		MG.socket.emit("move", {"x":pos.x,"y":pos.y});
	}
}

function UpdateStatus(pos)
{
	console.log("AskUpdate");
	if(MG.socket)
	{
		MG.socket.emit("askUpdate", pos);
	}
}

function handleUpdate(self)
{
	if(GSE.Input.IsKeyDown('d'))
	{
		self.x += 4
		SendPlayerPos({"x":self.x,"y":self.y});
	}
	if(GSE.Input.IsKeyDown('a'))
	{
		self.x -= 4
		SendPlayerPos({"x":self.x,"y":self.y});
	}
	if(GSE.Input.IsKeyDown('w'))
	{
		self.y -= 4
		SendPlayerPos({"x":self.x,"y":self.y});
	}
	if(GSE.Input.IsKeyDown('s'))
	{
		self.y += 4
		SendPlayerPos({"x":self.x,"y":self.y});
	}
}

function PlayerDisconnected(id)
{
	console.log(id, " disconnected");
	var p = GSE.Scene.GetEntity(id);
	if(p.tag != null | p.tag != undefined)
	{
		GSE.Scene.UI.RemoveEntity(p.id);
	}
	GSE.Scene.RemoveEntity(id);
}

function UpdateFromServer(data)
{
	data.players.forEach(function(player)
	{
		if(player.id == MG.socket.id) {
			//This is my player
			MG.playerEnt.x = player.pos.x;
			MG.playerEnt.y = player.pos.y;
			MG.playerTag.x = player.pos.x;
			MG.playerTag.y = player.pos.y;
			MG.playerTag.text = player.id;
		}
		else {
			var otherPg = GSE.Scene.GetEntity(player.id);
			if(otherPg != null)
			{
				//Update the entity
				otherPg.x = player.pos.x;
				otherPg.y = player.pos.y;
				otherPg.tag.x = player.pos.x;
				otherPg.tag.y = player.pos.y;
				otherPg.tag.text = player.id;
			}
			else {
				var spr = GSE.Scene.CreateSprite("", 'yellow', 20, GSE.Window.screenHeight/2, 32, 32);

				var pg = GSE.Scene.CreateEntity(player.id, spr, null);
				pg.x = player.pos.x;
				pg.y = player.pos.y;
				pg.tag = GSE.Scene.UI.DrawText(player.id, player.pos.x, player.pos.y, "white", "12");
			}
		}
	});

	// MG.playerEnt.tag = data.tag;
	//console.log("Server status ", JSON.stringify(data));

}

function InitGame(ctx)
{
	// MG.context = ctx;
	var image = "";

	//A way to create the Scene, and add object to it for drawing on the window
	var id = "Player";


	// var enemySpr = GSE.Scene.CreateSprite('./sprites/goblin.png', 'red', 235,98,128,128);
	var playerSpr2 = GSE.Scene.CreateSprite(image, 'yellow', 20, GSE.Window.screenHeight/2, 32, 32);

	MG.playerEnt = GSE.Scene.CreateEntity('player', playerSpr2, handleUpdate);
	MG.playerEnt.tag = "guest";
	MG.playerTag = GSE.Scene.UI.DrawText(MG.playerEnt.tag, MG.playerEnt.x, MG.playerEnt.y, "white", "12");
	// var handleUpdatePlayer =

	//MG.cellSpr =  GSE.Scene.CreateSprite("", 'blue', 0, 0, 2, 2);
	// MG.cellGrid = [GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2];

	// for(i = 0; i < GSE.Window.screenWidth / 2; i++) //y
	// {
	// 	for(j = 0; j < GSE.Window.screenHeight / 2; j++) //x
	// 	{
	// 		var id = i.toString() + j.toString();
	// 		MG.cellSpr.x = j * 2;
	// 		MG.cellSpr.y = i * 2;
	// 		var cell = GSE.Scene.CreateEntity(id, MG.cellSpr, cellUpdate);
	// 		cell.food = GSE.randomRange(100, 1000);
	// 		// MG.cellGrid[i, j] = cell;
	// 	}
	// }

	// var enemy = GSE.Scene.CreateEntity('Enemy', enemySpr, updateEnemy, false, false);

	// GSE.Scene.setTileImage("./tilesets/tileSetTest.png");
	//
	// GSE.Scene.map = [
	// 	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,
	// 	0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,1,0,0,0,0,0,1,9,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
	// 	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
	// ];

	// player.speed = 4;
	// player.stepThreshold = 10;
	// player.steps = 0
	// player.canMove = true;
	// player.hp = 1;
	// player.maxHp = 10;
	//
	// enemy.hp = 3;
	// enemy.maxHp = 3;
	// enemy.canAttack = true;

	// player.checkSteps = function(self) {
	// 	if(self.steps < self.stepThreshold)
	// 	{
	// 		self.steps++;
	// 	}
	// 	else {
	// 		self.steps = 0;
	// 		MG.EnterCombat();
	// 	}
	// }
	// if(MG.socket)
	// {
	// 	MG.socket.on("playerUp", UpdateFromServer);
	// }

	var combatOptions = ["Attack", "Item", "Flee", "Pass"];
	var currentOption = 0;
	MG.combatTurn = -1; //0 player, 1 Enemy
	MG.start = false;
	MG.halfCount = 0;
}

function updateEnemy(self) {
	if(self.hp <= 0)
	{
		MG.ExitCombat();
	}
	if(MG.combatTurn == 1)
	{
		//TODO Enemy AI
		var player = GSE.Scene.GetEntity("Player");
		if(self.canAttack == true)
		{
			player.hp--;
			self.canAttack = false;
		}
		// MG.combatTurn = 0;
		// MG.ChangeCombatTurn(-1, 10);
		MG.ChangeCombatTurn(0, 500);
	}
}

function Update(lastTick, timeSinceTick)
{
	if(MG.halfCount % 3 == 0)
	{
		SendPlayerPos({"x":MG.playerEnt.x,"y":MG.playerEnt.y});
	}
	MG.halfCount += 1;
	MG.halfCount %= 2;
	// if(MG.inGame == true)
	// {
	// 	MG.scoreText.text = MG.score;
	// 	MG.score += 1;
	// }
	// var player = GSE.Scene.GetEntity('Player');
	// MG.nextPos.x = player.x + 4;
	// MG.nextPos.y = player.y + 4;

	if(GSE.Input.IsKeyPressed('r'))
	{
		console.log("r");
		// GSE.StateManager.nextState("Map");

			GSE.Scene.entities[160001].food = 0;
	}
	if(GSE.Input.IsKeyPressed('q'))
	{
		MG.start = !MG.start;
	}
}
