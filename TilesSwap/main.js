MG = {
	mapW: 1024,
	mapH: 768
};

//Alias
const Scene = GSE.Scene;

let cell_size = 64;
let half_cell = cell_size / 2;
let image_size = {
	w: 1024,
	h: 1024
}

let grid = [];
let grid_w = (image_size.w / cell_size);
let grid_h = (image_size.h / cell_size);

let view = {
	offset_x : 192,
	offset_y : 32,
	x:0,
	y:0,
	w:10,
	h:10
}

let sel_cell_1 = null;
let sel_cell_2 = null;

let BG_layer = null;
let select_layer = null;
let nums_layer = null;

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();

function InitGame(ctx)
{
	for(let y=0; y < grid_h; y++)
	{
		for(let x=0; x< grid_w; x++)
		{
			grid.push({
				x:(x*cell_size), 
				y:(y*cell_size),
				w: cell_size,
				h: cell_size,
				id: y * grid_w + x
			});
		}
	}

	for(let i = 0; i < 10; i++)
	{
		let cell_1, cell_2;
		cell_1 = Math.floor(GSE.randomRange(0, grid_w * grid_h));
		cell_2 = Math.floor(GSE.randomRange(0, grid_w * grid_h));
		console.log("swap cell1:", cell_1, ",cell2:", cell_2);
		swap(cell_1, cell_2);
	}
	
	GSE.Scene.LoadTexture("image", "./images/red_box.jpg");

	BG_layer = Scene.CreateNode({ id: "background", order:0 });
	select_layer = Scene.CreateNode({ id: "select", order:1 });
	nums_layer = Scene.CreateNode({ id: "nums", order:2 });
	
	Scene.root.add_child(BG_layer);
	Scene.root.add_child(select_layer);
	Scene.root.add_child(nums_layer);
}

let first_off = -1;

function Update(lastTick, delta)
{
	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);
	first_off = grid.findIndex((cell, index) => {
		return cell.id !== index;
	});

	//Change player dir
	if (GSE.Input.IsKeyPressed("d") || GSE.Input.IsKeyPressed("right")) {
		if(view.x < grid_w - view.w) {
			view.x += 1;
		}
	}
	else if (GSE.Input.IsKeyPressed("a") || GSE.Input.IsKeyPressed("left")) {
		if(view.x > 0) {
			view.x -= 1;
		}
	}
	else if (GSE.Input.IsKeyPressed("w") || GSE.Input.IsKeyPressed("up")) {
		
		if(view.y > 0) {
			view.y -= 1;
		}
	}
	else if (GSE.Input.IsKeyPressed("s") || GSE.Input.IsKeyPressed("down")) {
		if(view.y < grid_h - view.h) {
			view.y += 1;
		}
	}

	if(GSE.Input.IsMouseLeftPressed())
	{
		// let mouse_rect = Rect(GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, cell_size, cell_size);
		let x_clicked =Math.floor(((GSE.Input.mouse.clientX - view.offset_x) / (cell_size + 2)));
		let y_clicked =Math.floor((GSE.Input.mouse.clientY - view.offset_y) / (cell_size + 2));

		let final_y = view.y + y_clicked;
		let final_x = view.x + x_clicked;
		let i = (final_y * grid_w) + final_x;
		let cell = grid[i];
		
		console.log("cell_id:", cell.id);

		if(!sel_cell_1)
		{
			sel_cell_1 = i;
		}
		else if(sel_cell_1 && i !== sel_cell_1)
		{
			//Swap
			swap(sel_cell_1, i);
			sel_cell_1 = null;
		}



		// console.log("mouse x:", GSE.Input.mouse.clientX - view.offset);
		// console.log("mouse y:", GSE.Input.mouse.clientY - view.offset);

		// console.log("mouse x/32:", (GSE.Input.mouse.clientX - view.offset) / (cell_size + 2) );
		// console.log("mouse y/32:", (GSE.Input.mouse.clientY - view.offset) / (cell_size + 2) );

		// console.log("x_clicked:", x_clicked);
		// console.log("y_clicked:", y_clicked);
		// console.log("cell_index:", i);
		
	}
}

function swap(id1, id2)
{
	let tmp_cell = grid[id1];
	grid[id1] = grid[id2];
	grid[id2] = tmp_cell;
}

function Render(delta)
{
	let view_pos = "x:" + view.x + ",y:" + view.y;
	Scene.DrawText({
        x:0, 
        y:500,
        color:"white",
        align:"left",
        font: "Serif",
        size: "12",
		text: view_pos,
		id:"Coords"
    });

	Scene.DrawText({
        x:0, 
        y:532,
        color:"green",
        align:"left",
        font: "Serif",
        size: "12",
		text: "first_off: " + first_off.toString(),
		id:"Coords"
    });

	for(let y=0; y < view.h; y++)
	{
		for(let x=0; x< view.w; x++)
		{
			let final_y = view.y + y;
			let final_x = view.x + x;
			let i = (final_y * grid_w) + final_x;

			let cell = grid[i];

			// console.log(cell);
			// debugger;

			if(first_off === cell.id) 
			{
				GSE.Scene.DrawRect({
					color: "green",
					x: view.offset_x + x * cell_size + (1*x),
					y: view.offset_y + y * cell_size + (1*y),
					w: cell_size + (2*x),
					h: cell_size + (2*y),
					path: select_layer.get_path()
				})
			}

			GSE.Scene.DrawSpriteExt(
			{
				texture: "image",
				x: view.offset_x + x * cell_size + (2*x),
				y: view.offset_y + y * cell_size + (2*y),
				w: cell_size,
				h: cell_size,
				sx: cell.x,
				sy: cell.y,
				sw: cell_size,
				sh: cell_size,
				tile_id:0,
				path: BG_layer.get_path()
			})

				
			
			Scene.DrawText({
				x: view.offset_x + half_cell + (x * cell_size) + (2*x), 
				y: view.offset_y + half_cell + (y * cell_size) + (2*y),
				color:"black",
				align:"center",
				font: "Serif",
				size: "20",
				text: cell.id.toString(),
				id:"Coords",
				path: nums_layer.get_path()
			});
		}
	}

	// for(let cell of grid) 
	// {
	// 	if(GSE.Collisions.CheckCollision(view, cell))
	// 	{
	// 		GSE.Scene.DrawSpriteExt(
	// 		{
	// 			texture: "image",
	// 			x: cell.x,
	// 			y: cell.y,
	// 			w: cell_size,
	// 			h: cell_size,
	// 			sx: cell.x, 
	// 			sy: cell.y,
	// 			sw: cell_size,
	// 			sh: cell_size,
	// 			tile_id:0
	// 		})
	// 	}
	// }
}