//Physics engine

var Physics = {}

Physics.Vec2 = function Vec2(x, y) {

    return Object.assign({}, {
        x : x || 0,
        y : y || 0,
        '0': x || 0,
        '1': y || 0
    });
}

Physics.Vec3 = function Vec3(x, y, z) {
    return Object.assign({}, {
        x : x || 0,
        y : y || 0,
        z : z || 0,
        '0': x || 0,
        '1': y || 0,
        '2': z || 0
    });
}

Physics.VecSum = function VecSum(vec1, vec2)
{
    if(vec1.z != undefined) {
        return Object.assign({}, {
            x : vec1.x + vec2.x,
            y : vec1.y + vec2.y,
            z : vec1.z + vec2.z
        });
    }
    else {
        return Object.assign({}, {
            x : vec1.x + vec2.x,
            y : vec1.y + vec2.y
        });
    }
}

Physics.VecDiff = function VecDiff(vec1, vec2)
{
    if(vec1.z != undefined) {
        return Object.assign({}, {
            x : vec1.x - vec2.x,
            y : vec1.y - vec2.y,
            z : vec1.z - vec2.z
        });
    }
    else {
        return Object.assign({}, {
            x : vec1.x - vec2.x,
            y : vec1.y - vec2.y
        });
    }
}

Physics.VecMull = function VecMull(vec1, vec2)
{
    if(vec1.z != undefined) {
        return Object.assign({}, {
            x : vec1.x * vec2.x,
            y : vec1.y * vec2.y,
            z : vec1.z * vec2.z
        });
    }
    else {
        return Object.assign({}, {
            x : vec1.x * vec2.x,
            y : vec1.y * vec2.y
        });
    }
}

Physics.VecScale = function VecScale(vec1, scalar)
{
    if(vec1.z != undefined) {
        return Object.assign({}, {
            x : vec1.x * scalar,
            y : vec1.y * scalar,
            z : vec1.z * scalar
        });
    }
    else {
        return Object.assign({}, {
            x : vec1.x * scalar,
            y : vec1.y * scalar
        });
    }
}

//Correct floating point comparison
Physics.CMP = function CMP(x, y)
{
    return Math.abs(x - y) <= Number.EPSILON * Math.max(1.0, Math.abs(x), Math.abs(y));
}

Physics.Compare = function Compare(vec1, vec2, operand)
{
    if(vec1.z != undefined) {
        switch (operand) {
            case '==':
                return this.CMP(vec1.x, vec2.x) && this.CMP(vec1.y, vec2.y) && this.CMP(vec1.z, vec2.z)
                break;
            case '!=':
                return !Physics.Compare(vec1, vec2);
                break;
            default:

                break;
        }
    }
    else {
        switch (operand) {
            case '==':
                return this.CMP(vec1.x, vec2.x) && this.CMP(vec1.y, vec2.y)
                break;
            case '!=':
                return !Physics.Compare(vec1, vec2);
                break;
            default:

                break;
        }
    }
}

Physics.Dot = function Dot(vec1, vec2)
{
    if(vec1.z != undefined) {
        return vec1.x * vec2.x + vec1.y * vec2.y + vec1.z * vec2.z;
    }
    else {
        return vec1.x * vec2.x + vec1.y * vec2.y;
    }
}

Physics.Magnitude = function Magnitude(vector)
{
    return Math.sqrt(Physics.Dot(vector, vector));
}

Physics.MagnitudeSqr = function MagnitudeSqr(vector)
{
    return Physics.Dot(vector, vector);
}

Physics.Distance = function Distance(vec1, vec2)
{
    var t = Physics.VecDiff(vec1, vec2);
    return Physics.Magnitude(t);
}

Physics.Normalize = function Normalize(v)
{
    //v * (1.0f / Magnitude(v));
    v = Physics.VecScale(v, (1.0 / Physics.Magnitude(v)));
    return v;
}

Physics.Normalized = function Normalize(v)
{
    //v * (1.0f / Magnitude(v));
    return Physics.VecScale(v, (1.0 / Physics.Magnitude(v)));
}

//Return a 90d perpendicolar vector , works only on Vec3
Physics.Vec3Cross = function Vec3Cross(l, r)
{
    if(l.z != undefined && r.z != undefined) {
        var result = Physics.Vec3();
        result.x = l.y * r.z - l.z * r.y;
        result.y = l.z * r.x - l.x * r.z;
        result.z = l.x * r.y - l.y * r.x;
        return result;
    }
}

export {Physics};
