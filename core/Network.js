// var net = require('net');

// var HOST = '127.0.0.1';
// var PORT = 6969;

// var client = new net.Socket();
// client.connect(PORT, HOST, function() {

//     console.log('CONNECTED TO: ' + HOST + ':' + PORT);
//     // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
//     for(i = 0; i < 8; i++)
//     {
//       client.write('I am Chuck Norris!' + i.toString());
//     }
// });

// // Add a 'data' event handler for the client socket
// // data is what the server sent to this socket
// client.on('data', function(data) {

//     console.log('DATA: ' + data);
//     // Close the client socket completely
//     // client.destroy();

// });

// // Add a 'close' event handler for the client socket
// client.on('close', function() {
//     console.log('Connection closed');
// });

var Net =
{
    "serverUrl" : "",
    "req": null,
    "host" : '127.0.0.1',
    "port" : 6969,
    "client" : undefined
}

Net.createRequest = function createRequest(url, method = "GET", body = {})
{
    Net.req = new XMLHttpRequest();
    Net.serverUrl = url;
    Net.method = method;
    Net.req.open(method, url, true);
}

Net.send = function send(resCallBack)
{
    Net.req.open(Net.method, Net.serverUrl, true);
    Net.req.send();
    Net.req.onreadystatechange = resCallBack;
}

// Net.createClient()
// {
// 	var client = new net.Socket();
// 	Net.client = client;
// 	return client;
// }

// Net.Connect(callback)
// {
// 	// Net.client.connect(PORT, HOST, function() {

// 	//     console.log('CONNECTED TO: ' + HOST + ':' + PORT);
// 	//     // Write a message to the socket as soon as the client is connected, the server will receive it as message from the client
// 	//     for(i = 0; i < 8; i++)
// 	//     {
// 	//       client.write('I am Chuck Norris!' + i.toString());
// 	//     }
// 	// });

// 	Net.client.connect(Net.port, Net.host, function()
// 	{
// 		console.log('CONNECTED TO: ' + Net.host + ':' + Net.port);
// 		callback();
// 	});
// }

export {Net};
