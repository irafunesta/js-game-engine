
import {RenderingFunctions} from "./main.js"

var Scene = {
    'entities': [],
    'uiEntities': [],
    'sounds': {},
    'textures_array': [],
    'textures': {},

    //This option may be useless in the future
    'map': undefined,
    'mapid': "",
    'tilesetTexture': '',
    'tileMapImage': '',
    'tileHeight': 32,
    'tileWidth': 32,
    'tileMapWidth': 20,
    'tileMapHeight': 15,
    'useRenderGrid': false,
    'renderGrid': undefined,
    'renderView': undefined,
    // ^ This option may be useless in the future ^

    'root':{},
    'use_root': false,
    'debug_draw': false
}

Scene.LoadTexture = function LoadTexture(sprite_id, sprite_path) {
    Scene.textures_array.push(
        {
            id: sprite_id,
            path: sprite_path
        }
    )
}

Scene.LoadMap = function LoadMap(map, id) {
    if (map != undefined) {
        Scene.map = map
    }
    else {
        Scene.map = [];
    }
    Scene.mapid = id;
}

/*
    w, : number of columns
    h, : number of rows
    cell_w, : pixel width of each cell
    cell_h, : pixel height of each cell
*/
Scene.CreateRenderGrid = function CreateRenderGrid(w, h, cell_w, cell_h) {
    Scene.useRenderGrid = true;
    Scene.renderGrid = {
        "w": w,
        "h": h,
        "cell_w": cell_w,
        "cell_h": cell_h,
        "tw": w * cell_w,
        "th": h * cell_h
    };

    Scene.grid = [];

    for (var y = 0; y < h; y++) {
        Scene.grid[y] = Array(w);
        for (var x = 0; x < w; x++) {
            Scene.grid[y][x] = [];
        }
    }
}

Scene.CreteRenderView = function CreteRenderView(x, y, w, h) {
    if (Scene.useRenderGrid == true) {
        Scene.renderView = {
            "x": x,
            "y": y,
            "w": w,
            "h": h
        }
    }
}

Scene.ScaleRenderView = function ScaleRenderView(w, h) {
    if (Scene.useRenderGrid == true) {
        // debugger;
        var rv = Scene.renderView;
        if (rv.w + w < Scene.renderGrid.tw &&
            rv.h + h < Scene.renderGrid.th) {
            Scene.renderView.w += w;
            Scene.renderView.h += h;
        }

        // rv.y -= h;
        //Also center the view
        if (rv.x - w > 0) {
            rv.x -= w;
        }
        else {
            rv.x = 0;
        }

        if (rv.y - h > 0) {
            rv.y -= h;
        }
        else {
            rv.y = 0;
        }
    }
}

Scene.moveRenderView = function moveRenderView(x, y, abs = false) {
    if (abs == false) {
        var next_pos = { "x": Scene.renderView.x + x, "y": Scene.renderView.y + y };
        var vr = { "h": Scene.renderView.h, "w": Scene.renderView.w };
        if (next_pos.y >= 0 && next_pos.y <= Scene.renderGrid.th &&
            next_pos.y + vr.h <= Scene.renderGrid.th) {
            if (next_pos.x >= 0 && next_pos.x <= Scene.renderGrid.tw &&
                next_pos.x + vr.w <= Scene.renderGrid.tw) {
                Scene.renderView.x += x;
                Scene.renderView.y += y;
            }
        }
    }
    else {
        //Center the view at the given pos, is it exit the grid , push it inside
        var vx = Scene.renderView.x
        var vy = Scene.renderView.y

        vx = Math.round(x - (Scene.renderView.w / 2));
        vy = Math.round(y - (Scene.renderView.h / 2));

        if (vx + Scene.renderView.w > Scene.renderGrid.tw) {
            vx -= Scene.renderView.w - Scene.renderGrid.tw;
        }

        if (vy + Scene.renderView.h > Scene.renderGrid.th) {
            vy -= Scene.renderView.h - Scene.renderGrid.th;
        }

        if (vx < 0) {
            vx = 0;
        }

        if (vy < 0) {
            vy = 0;
        }

        Scene.renderView.x = vx;
        Scene.renderView.y = vy;
    }
}

Scene.fillRenderGrid = function fillRenderGrid() {
    if (Scene.useRenderGrid == true) {
        Scene.entities.forEach(function (e, i) {
            var cell_x = Math.floor(e.x / Scene.renderGrid.cell_w);
            var cell_y = Math.floor(e.y / Scene.renderGrid.cell_h);
            // debugger;
            if (cell_x <= Scene.renderGrid.w - 1 && cell_y <= Scene.renderGrid.h) {
                Scene.grid[cell_y][cell_x].push(e);
            }
        });
    }
}

Scene.totalTiles = function totalTiles() {
    var tileset = this.textures[this.tilesetTexture];
    if (tileset) {
        var numCellTileW = this.tileset.width / this.tileHeight;
        var numCellTileH = this.tileset.height / this.tileWidth;
        return numCellTileW * numCellTileH;
    }
    else {
        return 0;
    }
}

Scene.setTileImage = function setTileImage(imagePath, tileWidth = 32, tileHeight = 32, col = 20, row = 15) {
    Scene.tilesetTexture = imagePath;
    Scene.tileWidth = tileWidth;
    Scene.tileHeight = tileHeight;
    Scene.tileMapWidth = col;
    Scene.tileMapHeight = row;
}

Scene.getSpriteType = function getSpriteType(image) {
    var type = "color";
    if (image == "" || image == undefined) {
        type = "image";
    }
    return type;
}

Scene.CreateSprite = function CreateSprite(image, color, x, y, width, height, layer = 0, type, scale) {
    //Sprite with an image or a color
    if (!type || type === "") {
        var type = "image";
        if (image == "" || image == undefined) {
            type = "color";
        }
    }

    var rect = {
        "imageFile": image,
        "color": color,
        "x": x,
        "y": y,
        "width": width,
        "height": height,
        "layer": layer,
        "type": type,
        "scale": scale
    }

    return rect;
};

//TODO add real docs
// the columns and rows start from 1
Scene.CreateAnimatedSprite = function CreateAnimatedSprite(images, timeForFrame, x, y, width, height, layer = 0, isSpriteSheet = false, col = 0, row = 0,
    t_width = 32, t_height = 32) {
    var no_animation = timeForFrame == -1;
    var type = "tileImage";
    if (no_animation == true) {
        type = "tileImage";
    }
    else {
        type = "animated";
    }

    var rect = {
        "imageFile": images,
        "x": x,
        "y": y,
        "width": width,
        "height": height,
        "layer": layer,
        "frameTime": timeForFrame,
        "sFrameTime": timeForFrame,
        "lastFrame": 0,
        "isSpriteSheet": isSpriteSheet,
        "col": col,
        "row": row,
        "t_width": t_width,
        "t_height": t_height,
        "no_animation": no_animation,
        "type": type
    }

    return rect;
};

Scene.CreateSound = function CreateSound(id, path, options) {
    var s = {
        "id": id,
        "soundFile": path,
        "soundEle": null,
        "options": options
    }
    Scene.sounds[id] = s;
    return s;
}

Scene.CreateEntity = function CreateEntity(id, Sprite, handleUpdate, active = true, visible = true) {
    var entity = Object.assign({}, Sprite, {
        'id': id,
        'handleUpdate': handleUpdate,
        'active': active,
        'visible': visible,
        'setActive': function setActive(status) {
            this.visible = status;
            this.active = status;
            if (this.sFrameTime && status == false) {
                this.lastFrame = 0;
                this.frameTime = this.sFrameTime;
            }
        }
    });

    var uid = (Scene.entities.push(entity) - 1);
    entity.uid = uid;
    return entity;
}

Scene.RemoveAllEntity = function RemoveAllEntity(id) {

    Scene.entities = Scene.entities.filter(function (e) {
        if (id == "all") {
            return false;
        }
        else {
            return (e.id != id);
        }
    })
}

Scene.RemoveEntity = function RemoveEntity(id) {
    var entity = Scene.GetEntity(id);
    if (entity != undefined) {
        // for(var i=0; i< Scene.entities.length; i++) {
        // 	if()
        // }
        var start = Scene.entities.indexOf(entity);
        Scene.entities.splice(start, 1);
    }
}

Scene.SetActive = function SetActive(id, activeS = true) {
    var entity = Scene.GetEntity(id);
    if (entity != undefined) {
        entity.visible = activeS;
        entity.active = activeS;
    }
}

Scene.GetEntity = function GetEntity(id) {
    for (var i = 0; i < Scene.entities.length; i++) {
        if (Scene.entities[i].id == id) {
            return Scene.entities[i];
        }
    }
    return null;
}

Scene.GetEntityByUid = function GetEntity(uid) {
    return Scene.entities[uid];
}

Scene.RemoveEntityByUid = function RemoveEntityByUid(uid) {
    return Scene.entities.splice(uid, 1);
}

Scene.PlaySound = function PlaySound(id,volume) {
    //This should be the howler
    if (volume > 0)
    {
        Scene.sounds[id].soundEle.volume(volume)
    }

    Scene.sounds[id].soundEle.play();
    return;
}

Scene.SoundSetLoop = function SoundSetLoop(id, loop) {
    Scene.sounds[id].soundEle.loop = loop;
    return;
}

Scene.StopSound = function StopSound(id) {
    Scene.sounds[id].soundEle.stop();
    return;
}

Scene.SetMute = function SetMute(isMute) {
    Scene.isMute = isMute;
    if(isMute == true)
    {
        for (var key in Object.keys(Scene.sounds)) {
            Scene.sounds[key].soundEle.pause();
        }
    }
}

Scene.FindEntity = function FindEntity(func) {
    for (var i = 0; i < Scene.entities.length; i++) {
        if (func(Scene.entities[i], i) == true) {
            return {
                ent: Scene.entities[i],
                i: i
            };
        }
    }
}

Scene.GetEntities = function GetEntities(id) {
    var list = [];
    for (var i = 0; i < Scene.entities.length; i++) {
        if (Scene.entities[i].id == id) {
            list.push(Scene.entities[i]);
            // return Scene.entities[i];
        }
    }

    return list;
}

Scene.CountActiveEntitiesById = function CountActiveEntitiesById(id) {
    var totalActiveEntity = 0;
    for (var i = 0; i < Scene.entities.length; i++) {
        var e = Scene.entities[i];
        if (e.id == id && e.active == true) {
            totalActiveEntity++;
        }
    }
    return totalActiveEntity;
}

Scene.UI = {
    DrawText: function DrawText(text, x, y, color, size = '48', align = 'center') {
        var uiText = {
            'id': Scene.uiEntities.length,
            'text': text,
            'x': x,
            'y': y,
            'color': color,
            'size': size,
            'align': align,
            'visible': true,
            'setActive': function setActive(status) {
                this.visible = status;
            }
        }

        Scene.uiEntities.push(uiText);
        return uiText;
    },
    RemoveEntity: function RemoveEntity(id) {
        var iToR = -1;
        for (var i = 0; i < Scene.uiEntities.length; i++) {
            if (Scene.uiEntities[i].id == id) {
                iToR = i;
                break;
            }
        }

        Scene.uiEntities.splice(iToR, 1);
    }
}



//Scene V2 - Tree rendering and sorting

Scene.CreateRootNode = function()
{
    let node_opt = {
        id:"root",
        path:"",
        data:{
            x:0, 
            y:0,
            hidden:false
        },
        render_func: () => undefined
    }
    Scene.root = Scene.CreateNode(node_opt);
}

Scene.PrintAllNodes = function()
{
    console.log("All nodes - ", Scene.root);
    Scene.PrintNode(Scene.root, 0);
}

Scene.PrintNode = function (node, indent)
{
    console.log(" ".repeat(indent), "| Node id : " + node.id);

    if(node.child == null || node.child.length == 0)
    {
        return;
    }

    indent++;

    for(let i in node.child)
    {
        Scene.PrintNode(node.child[i], indent);
    }
}

Scene.CreateNode = function(options)
{
    return Object.assign({
        //questo è il tile o uno sprite
        id:"",
        tmp:false,
        child: [],
        order: 0,
        data: null,
        add_child: Scene.AddNode,
        get_path: Scene.GetPath,
        render_func: undefined,
    },options);
}

//Test example node_id_A/node_id_B/node_id_C
Scene.GetNode = function(path, start_node) {
    //TODO get the correct node from the path

    //The input path should be 
    //and the first / is the root, omitted
    //the other indicate child nodes
    let root = start_node || Scene.root;
    let node;

    let nodes = path.split("/");
    /*
        [node_id_A, node_id_B, node_id_C]

    */

    // console.log("root:", root);
    // console.log("nodes:", nodes);

    if(nodes.length > 0)
    {
        //get the first node
        let first_id = nodes.splice(0, 1);

        // console.log("first_id:", first_id[0]);

        let first_child = root.child.find((node) => {
            return node.id === first_id[0];
        });

        // console.log("first_child:", first_child);

        if(nodes.length > 0)
        {
            node = Scene.GetNode(nodes.join("/"), first_child);
            return node;
        }
        else if (first_child)
        {
            return first_child; //No other nodes left, a child found
        }

        return null; //Node not found in the root child;
    }
}

Scene.RemoveNode = (path) => {
    //Get the node
    let to_delete = Scene.GetNode(path);

    //Do not delete the root
    if(to_delete && to_delete.id === "root")
    {
        return;
    }

    let father = to_delete.father;
    if(father && father.child)
    {
        let index = father.child.findIndex((node) => node.id === to_delete.id)
        father.child.splice(index, 1);
    }
}

//Add a node to a node as a child
Scene.AddNode = function(node) {
    if(node)
    {
        // console.log("AddNode this", this);
        let node_default_order = this.child.length || 0;
        let node_default_id = "Node_" + node_default_order;

        node.order = node.order || node_default_order;
        node.id = node.id || node_default_id;
        node.father = this;

        if(node.father) {
            node.path = node.father.path + "/" + node.id;
        }

        this.child.push(node);
    }
}

Scene.GetPath = function ()
{
    return this.path;   
}

//Add a sprite to draw from a texture
//Required the texture, if no node is specified add to root
Scene.AddSprite = function(options, path)
{
    // console.log("Scene.AddSprite path:", path);
    let options_passed = Object.assign({
        texture:null,
        x:0, 
        y:0, 
        w:0, 
        h:0,
        node_path: path || "/",
        tmp:false,
        scale: null
    }, options);

    if(options_passed.texture === null) {
        //Can't add sprite with no texture
        console.log("No texture specified");
        return;
    }

    // console.log("Scene.AddSprite options_passed.node_path:", options_passed.node_path);

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawSprite);
}
//Add a sprite to draw from a texture, with region
Scene.AddSpriteExt = function(options, path)
{
    let options_passed = Object.assign({
        texture:null,
        x:0, 
        y:0, 
        w:0, 
        h:0,
        sx:0, sy:0, sw:0, sh:0,
        node_path: path || "/",
        tmp:false,
        scale: null
    }, options);

    if(options_passed.texture === null) {
        //Can't add sprite with no texture
        console.log("No texture specified");
        return;
    }

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawSpriteExt);
}

//Add a sprite to draw from a texture, with region
Scene.AddSpriteAnimated = function(options, path)
{
    /*
        
        "frameTime": timeForFrame,
        "sFrameTime": timeForFrame,
        "lastFrame": 0,
        "isSpriteSheet": isSpriteSheet,
        "col": col,
        "row": row,
        "t_width": t_width,
        "t_height": t_height,
        "no_animation": no_animation,
        "type": type
    */
    let options_passed = Object.assign({
        texture:null,
        x:0, 
        y:0, 
        w:0, 
        h:0,
        sx:0, sy:0, sw:0, sh:0,
        frameTime: 0,
        sFrameTime: 0,
        lastFrame: 0,
        isSpriteSheet: 0,
        col: col,
        row: row,
        t_width: t_width,
        t_height: t_height,
        node_path: path || "/",
        tmp:false,
        scale: null
    }, options);

    if(options_passed.texture === null) {
        //Can't add sprite with no texture
        console.log("No texture specified");
        return;
    }

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawSpriteAnimated);
}

//Add a colored Rectangle
Scene.AddRect = function(options, path)
{
    let options_passed = Object.assign({
        texture:null,
        x:0, 
        y:0, 
        w:0, 
        h:0,
        sx:0, sy:0, sw:0, sh:0,
        node_path: path || "/",
        tmp:false,
        scale: null,
        color:"white"
    }, options);

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawRect);
}

//Add a Text
Scene.AddText = function(options, path)
{
    let options_passed = Object.assign({
        x:0, 
        y:0,
        node_path: path || "/",
        tmp:false,
        text : "",
        color : "white",
        align : "left",
        font : "serif",
        size : "12"
    }, options);

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawText);
}

function AddSpriteNodeInternal(options, rendering_func)
{
    let father = null;
    // console.log("Scene.AddSprite final options_passed:", options);    

    if(options.node_path !== "/")
    {
        father = Scene.GetNode(options.node_path);
    }
    // console.log("Scene.AddSprite final father:", father);

    //default hidden false
    options.hidden = options.hidden || false;
    //Create the Node, made it a Sprite, add to the Root or node passed
    //Need to fix this
    let node_options = {
        id:options.id,
        tmp:options.tmp,
        order:options.order,
        data: options,
        render_func: rendering_func
    }

    let node = Scene.CreateNode(node_options);
    // console.log("Scene.AddSprite final node:", node);

    if(father !== null)
    {
        // node.father = father;
        
        father.add_child(node);
    }
    else
    {
        // console.log("Scene.AddSprite add to root");
        // node.father = Scene.root;
        
        Scene.root.add_child(node);
    }

    return node;
}

//Draw a tmp sprite or spriteExt
Scene.DrawSprite = function(options)
{
    //Is the same as a normal spite, but is tmp
    let options_passed = Object.assign({
        tmp:true,
    }, options);
    
    Scene.AddSprite(options_passed);
}

//Draw a tmp sprite or spriteExt
Scene.DrawSpriteExt = function(options)
{
    //Is the same as a normal spite, but is tmp
    let options_passed = Object.assign({
        tmp:true,
    }, options);
    
    Scene.AddSpriteExt(options_passed);
}

Scene.DrawRect = function(options, path)
{
    let options_passed = Object.assign({
        x:0, 
        y:0, 
        w:0, 
        h:0,
        node_path: path || "/",
        color:"white",
        tmp:true,
        scale: null
    }, options);

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawRect);
}

Scene.DrawText = function(options)
{
    let options_passed = Object.assign({
        x:0, 
        y:0,
        node_path: "/",
        color:"white",
        tmp:true,
        align:"left",
        font: "Serif",
        size: "12"
    }, options);

    let father = null;

    if(options_passed.node_path !== "/")
    {
        father = Scene.GetNode(options_passed.node_path);
    }

    //Create the Node, made it a Sprite, add to the Root or node passed
    //Need to fix this
    let node_options = {
        id:options_passed.id,
        tmp:options_passed.tmp,
        order:options_passed.order,
        data: {
            text: options_passed.text,
            x:options_passed.x, 
            y:options_passed.y,
            color:options_passed.color,
            align: options_passed.align,
            font : options_passed.font,
            size : options_passed.size,
        },
        render_func: RenderingFunctions.DrawText
    }

    let node = Scene.CreateNode(node_options);
    // console.log("Scene.AddSprite final node:", node);

    if(father !== null)
    {
        // console.log("Scene.AddSprite add to father");
        father.add_child(node);
        //set path 
        //TODO 
    }
    else
    {
        // console.log("Scene.AddSprite add to root");
        Scene.root.add_child(node);
    }
}

// Scene.AddLine = function(options)
Scene.DrawLine = function(options, path)
{
    let options_passed = Object.assign({
        x_start:0, 
        y_start:0,
        x_end:0, 
        y_end:0,
        node_path: path || "/",
        color:"white",
        tmp:true,
    }, options);

    return AddSpriteNodeInternal(options_passed, RenderingFunctions.DrawLine);
}

Scene.useRoot = function(val)
{
    Scene.use_root = val || false;
}

export { Scene };
