// var rightPressed;
// var leftPressed;
// var downPressed;
// var upPressed;

import {GSEngine} from './main.js';

var Input = {
  //keycodes mapping to init status and check keys
  keyCodes : {
    'ArrowUp':"up",
    "ArrowDown":"down",
    "ArrowLeft":"left",
    "ArrowRight": "right",
    'KeyW':'w',
    "KeyS":'s',
    'KeyA':'a',
    'KeyD':'d',
    'Enter':'enter',
    'Escape':'escape',
    'KeyR':'r',
    'KeyP':'p',
    'KeyE':'e',
    'KeyQ':'q',
    'KeyC':'c',
    'Space':'space',
    'KeyH':'h',
    'KeyF':'f',
    'KeyT':'t',
    'KeyL':'l',
    'KeyK':'k',
    'KeyI':'i',
    'KeyG':'g',
    'KeyZ':'z',
    'KeyX':'x',
    'KeyO':'o',
    'KeyU':'u',
    'KeyN':'n',
    'KeyJ':'j',
    'KeyV':'v',
    'KeyM':'m',
    'KeyB':'b',
    'KeyTab':'Tab',
    'ctrlKeyZ' : 'ctrlkeyz'
  },

  init: () => {
    //Create all the key status from the map
    let props = Object.getOwnPropertyNames(Input.keyCodes);
    console.log("Input init")
    props.forEach(
      function (val, idx, array) {
        // console.log("Processed key:", val)

        Input.keyStatus[Input.keyCodes[val]] = {
          pressed: false,
          prev_press: false,
          num:0
        }
      }
    );
  },

  keyStatus: {},
  lastRawKey: "",
  mouse: {
    screenX: 0,
    screenY: 0,
    clientX: 0,
    clientY: 0,
    leftPressed: {
      pressed: false,
      prev_press: false
    },
    rightPressed: {
      pressed: false,
      prev_press: false
    },
    wheel: false
  },
  touch: {
    screenX: 0,
    screenY: 0,
    clientX: 0,
    clientY: 0,
    statePressed: {
      pressed: false,
      prev_press: false
    }
  },
  IsKeyDown: function IsKeyDown(key) {
    var status = false;
    if (this.keyStatus[key]) {
      status = this.keyStatus[key].pressed == true
    }
    return status;
  },

  IsKeyPressed: function IsKeyPressed(key) //Is key pressed and then released
  {
    if(key == undefined)
    {
      return;
    }
    var keyl = key.toLowerCase();
    var res = false;
    if (this.keyStatus[keyl]) {
      if (this.keyStatus[keyl].prev_press == true && this.keyStatus[keyl].pressed == false) {
        res = true;
      }
    }
    return res;
  },

  IsMouseLeftPressed: function IsMouseLeftPressed() {
    var val = false;

    if (this.mouse.leftPressed.pressed == false && this.mouse.leftPressed.prev_press == true) {
      val = true;      
    }
    this.mouse.leftPressed.prev_press = false;
    return val;
  },

  IsMouseLeftDown: function IsMouseLeftDown() {
    return this.mouse.leftPressed.pressed;
  },

  IsTouchDown: function IsTouchDown() {
    return this.touch.statePressed.pressed;
  },

  IsTouchPressed: function IsTouchPressed()
  {
    var val = false;

    if (this.touch.statePressed.pressed == false && this.touch.statePressed.prev_press == true) {
      val = true;      
    }
    this.touch.statePressed.prev_press = false;
    return val;
  },

  IsMouseRightPressed: function IsMouseRightPressed() {
    var val = false;

    if (this.mouse.rightPressed.pressed == false && this.mouse.rightPressed.prev_press == true) {
      val = true;
    }
    this.mouse.rightPressed.prev_press = false;
    return val;
  },

  IsMouseRightDown: function IsMouseRightDown() {
    return this.mouse.rightPressed.pressed;
  },

  //type of event is -1 for Holding, +1 for release
  ChangeKeyStatus: function ChangeKeyStatus(key, val, typeOfEvent) 
  {
    // if(typeOfEvent == 1) {
    //   console.log("ChangeKeyStatus key:", key);
    // }
    let keyStatus = Input.keyStatus[key];
    if(keyStatus) {
      keyStatus.prev_press = keyStatus.pressed;
      keyStatus.pressed = val;
      
      if(typeOfEvent == 1 || typeOfEvent == -1)
      {
        keyStatus.num += typeOfEvent
      }
    }
  },

  clearInputPrevPressed() {
    var props = Object.getOwnPropertyNames(Input.keyStatus);
    props.forEach(
      function (key, idx, array) {
        if (Input.keyStatus[key].pressed == false) {
          Input.ChangeKeyStatus(key, false);
        }
        // console.log(val + ' -> ' + Input.keyStatus[key].pressed);
        // console.log(val + ' -> ' + Input.keyStatus[key].prev_press);
      }
    );

    Input.lastRawKey = "";
    Input.has_pasted = false;

    if(Input.mouse.leftPressed.pressed == false)
    {
      Input.mouse.leftPressed.prev_press = Input.mouse.leftPressed.pressed;
      Input.mouse.leftPressed.pressed = false;
    }

    if(Input.touch.statePressed.pressed == false)
    {
      Input.touch.statePressed.prev_press = Input.touch.statePressed.pressed;
      Input.touch.statePressed.pressed = false;
    }
  },

  stopInputStuck() {
    var props = Object.getOwnPropertyNames(Input.keyStatus);
    props.forEach(
      function (key, idx, array) {        
        Input.ChangeKeyStatus(key, false);
      }
    );

    Input.lastRawKey = "";
    Input.has_pasted = false;

    
      Input.mouse.leftPressed.prev_press = Input.mouse.leftPressed.pressed;
      Input.mouse.leftPressed.pressed = false;

    
      Input.touch.statePressed.prev_press = Input.touch.statePressed.pressed;
      Input.touch.statePressed.pressed = false;
  },

  SwitchKeyCode: function SwitchKeyCode(code, val, typeOfEvent) 
  {
    // if(typeOfEvent == 1) {
    //   console.log("SwitchKeyCode code:", code);
    // }
    this.ChangeKeyStatus(Input.keyCodes[code], val, typeOfEvent);
  },

  mouseMoveHandler: function mouseMoveHandler(event) {
    var canvas = document.getElementById(GSEngine.Window.canvasId);
    var rect = canvas.getBoundingClientRect();

    Input.mouse.screenX = event.screenX;
    Input.mouse.screenY = event.screenY;

    Input.mouse.clientX = event.clientX - rect.left;
    Input.mouse.clientY = event.clientY - rect.top;

    if (GSEngine.Scene.useRenderGrid == true) {
      Input.mouse.clientX += GSEngine.Scene.renderView.x;
      Input.mouse.clientY += GSEngine.Scene.renderView.y;
    }

    // console.log("screenX:", Input.mouse.clientX, "-screenY:", Input.mouse.clientY);

  },

  mouseUp: function mouseUp(event) {
    // console.log("mouseUp");
    if (event.button == 0) {
      Input.mouse.leftPressed.prev_press = Input.mouse.leftPressed.pressed;
      Input.mouse.leftPressed.pressed = false;
    }
    if (event.button == 2) {
      Input.mouse.rightPressed.prev_press = Input.mouse.rightPressed.pressed;
      Input.mouse.rightPressed.pressed = false;
    }
    // console.log("leftPressed:", Input.mouse.leftPressed);
  },

  mouseDown: function mouseDown(event) {
    // console.log("mouseDown x:", GSEngine.Input.mouse.clientX, "y:", GSEngine.Input.mouse.clientY);
    if (event.button == 0) {
      Input.mouse.leftPressed.prev_press = Input.mouse.leftPressed.pressed;
      Input.mouse.leftPressed.pressed = true;
    }
    if (event.button == 2) {
      Input.mouse.rightPressed.prev_press = Input.mouse.rightPressed.pressed;
      Input.mouse.rightPressed.pressed = true;
    }
    // Input.mouse.leftPressed.prev_press = Input.mouse.leftPressed.pressed;


  },

  touchStart: function touchStart(ev)
  {
    var canvas = document.getElementById(GSEngine.Window.canvasId);
    var rect = canvas.getBoundingClientRect();

    //Get 1 touch
    if(ev.targetTouches.length <= 0)
    {
      return
    }

    let touch = ev.targetTouches[0];

    // console.log("touchStart x:", touch.clientX, "y:", touch.clientY);

    Input.touch.clientX = touch.clientX - rect.left;
    Input.touch.clientY = touch.clientY - rect.top;

    Input.touch.screenX = touch.screenX,
    Input.touch.screenY = touch.screenY,

    Input.touch.statePressed.prev_press = Input.touch.statePressed.pressed;
    Input.touch.statePressed.pressed = true;

    // console.log("touch start:", Input.touch.statePressed.pressed);
  },

  touchMove: function touchMove(ev)
  {
    var canvas = document.getElementById(GSEngine.Window.canvasId);
    var rect = canvas.getBoundingClientRect();

    //Get 1 touch
    if(ev.targetTouches.length <= 0)
    {
      return
    }

    let touch = ev.targetTouches[0];

    Input.touch.screenX = touch.screenX;
    Input.touch.screenY = touch.screenY;

    Input.touch.clientX = touch.clientX - rect.left;
    Input.touch.clientY = touch.clientY - rect.top;

    if (GSEngine.Scene.useRenderGrid == true) {
      Input.touch.clientX += GSEngine.Scene.renderView.x;
      Input.touch.clientY += GSEngine.Scene.renderView.y;
    }
  },

  touchEnd: function touchEnd(ev)
  {
    // console.log("touchEnd ");

    Input.touch.statePressed.prev_press = Input.touch.statePressed.pressed;
    Input.touch.statePressed.pressed = false;
  },

  touchCancel: function touchCancel(ev)
  {
    // console.log("touchCancel ");
    Input.touch.statePressed.prev_press = Input.touch.statePressed.pressed;
    Input.touch.statePressed.pressed = false;
  },

  getRawKey: function getRawKey() {
    return this.lastRawKey;
  },

  handlePaste: function handlePaste(e) {
    var clipboardData, pastedData;

    // Stop data actually being pasted into div
    e.stopPropagation();
    e.preventDefault();

    // Get pasted data via clipboard API
    clipboardData = e.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text');

    // console.log("pastedData:", pastedData);
    // Do whatever with pasteddata
    this.pastedData = pastedData;
    this.has_pasted = true;
  }
}

Input.keyDownHandler = function keyDownHandler(event) {
  if (event.preventDefaulted) {
    return; // Do nothing if event already handled
  }

  var val = true;
  // console.log("ctrlKey:",event.ctrlKey );
  // console.log("metaKey:",event.metaKey );

  // console.log("metaKey:",event.getModifierState("Alt"));
  if (!event.altKey && !event.ctrlKey && !event.metaKey) {
    Input.lastRawKey = event.key;
  }

  let code = event.ctrlKey ? 'ctrl' + event.code : event.code;
  // console.log("pressed code", code);

  Input.SwitchKeyCode(code, true, +1);

  // Consume the event so it doesn't get handled twice
  if(event.code != "F12")
  {
    event.preventDefault();
  }

};

Input.keyUpHandler = function keyUpHandler(event) {
  if (event.preventDefaulted) {
    return; // Do nothing if event already handled
  }

  let code = event.ctrlKey ? 'ctrl' + event.code : event.code;

  Input.SwitchKeyCode(code, false, -1);

  // Consume the event so it doesn't get handled twice
  event.preventDefault();

  // console.log("key Up Handler:", event.key, Input.keyStatus[event.key]);
};

Input.logStatus = function logStatus() {
  Input.keyStatus;
  var props = Object.getOwnPropertyNames(Input.keyStatus);
  props.forEach(
    function (val, idx, array) {
      console.log(val + ' -> ' + Input.keyStatus[val].pressed);
        console.log(val + ' -> ' + Input.keyStatus[val].prev_press);
      }
  );

  return props;
}

export { Input };
