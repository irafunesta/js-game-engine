//Entry point of the program

import {Input} from './Input.js';
import {Scene} from './Scene.js';
import {Physics} from './physics.js';
import {StateManager} from './State.js';
import {Net} from './Network.js';
import { saveAs } from 'file-saver';
import {Howl, Howler} from './Howler.js';
import Stats from './Stats.js';

//Polyfill
if (Number.EPSILON === undefined){
    Number.EPSILON  =  Math.pow(2, -52);
}

// var screenWidth = 640;
// var screenHeight = 480;

function FillBackground(renderingContext, color, borderWidth, borderColor, alpha = 1)
{
  var screenWidth = Window.screenWidth;
  var screenHeight = Window.screenHeight;

  renderingContext.globalAlpha = alpha;
  // renderingContext.strokeStyle = 'red ';
  // console.log("borderColor :" , borderColor);
  if(borderWidth > 0 && borderColor !== undefined) {
    color = borderColor
    renderingContext.fillStyle = color;
    renderingContext.fillRect(0, 0, screenWidth, screenHeight);
    renderingContext.clearRect(borderWidth, borderWidth,  screenWidth-borderWidth * 2, screenHeight-borderWidth * 2);
  }
  else {
    renderingContext.fillStyle = color;
    renderingContext.fillRect(0, 0, screenWidth, screenHeight);
  }

  // renderingContext.globalAlpha = 1;
}

function ClearCanvas(renderingContext)
{
  var screenWidth = Window.screenWidth;
  var screenHeight = Window.screenHeight;

  // renderingContext.globalAlpha = alpha;
  // renderingContext.strokeStyle = 'red ';
  // console.log("borderColor :" , borderColor);

  renderingContext.clearRect(0, 0, renderingContext.canvas.width, renderingContext.canvas.height);
  // renderingContext.globalAlpha = 1;
}

function LoadImage(path, entity_index) {
  var img = new Image();
  img.src = path;
  img.onload = function() {
    console.log(path + 'Img Ready');
    Scene.entities[entity_index].image = img;
  }

  // return img;
}

function LoadSingleTexture(path, texture_index) {
  var img = new Image();
  
  img.onload = function() {
    console.log(path + 'Img Ready');
    Scene.textures[texture_index] = img;
  }
  img.src = path;
  // return img;
}

function LoadSound(path, isloop) {
  return new Howl({
    src: [path],
    loop : isloop
    // onload:function(){
    //   console.log(path + 'sound Ready');
    // }
  });
}

// function DrawImage(rContext, imagePath, x, y, width, height)
// {
//   var img = new Image();
//   img.src = imagePath;
//   img.onload = function() {
//     if(width != null && height != null) {
//       rContext.drawImage(img, x, y, width, height);
//     }
//     else {
//       rContext.drawImage(img, x, y);
//     }
//   }
// }

/*
* Starting with the semicolon is in case whatever line of code above this example
* relied on automatic semicolon insertion (ASI). The browser could accidentally
* think this whole example continues from the previous line. The leading semicolon
* marks the beginning of our new line if the previous one was not empty or terminated.
*
* Let us also assume that MyGame is previously defined.
*
* MyGame.lastRender keeps track of the last provided requestAnimationFrame timestamp.
* MyGame.lastTick keeps track of the last update time. Always increments by tickLength.
* MyGame.tickLength is how frequently the game state updates. It is 20 Hz (50ms) here.
*
* timeSinceTick is the time between requestAnimationFrame callback and last update.
* numTicks is how many updates should have happened between these two rendered frames.
*
* render() is passed tFrame because it is assumed that the render method will calculate
*          how long it has been since the most recently passed update tick for
*          extrapolation (purely cosmetic for fast devices). It draws the scene.
*
* update() calculates the game state as of a given point in time. It should always
*          increment by tickLength. It is the authority for game state. It is passed
*          the DOMHighResTimeStamp for the time it represents (which, again, is always
*          last update + MyGame.tickLength unless a pause feature is added, etc.)
*
* setInitialState() Performs whatever tasks are leftover before the mainloop must run.
*                   It is just a generic example function that you might have added.
*/

var MyGame = {
  paused: false,
  PauseGame: function PauseGame()
  {
    MyGame.paused = !MyGame.paused;
  }
};

function CreateSprite(entity, entity_index)
{
  
  if(entity.imageFile && typeof (entity.imageFile) != 'string' && entity.imageFile.length > 0)
  {
    entity.image = [];
    for (var i = 0; i < entity.imageFile.length; i++) {
      var sprite = entity.imageFile[i];
      entity.image.push(LoadImageArray(sprite, entity_index));
    }
  }
  else
  {
    // Scene.textures[entity.texture_name] = LoadImage(entity.imageFile);
    entity.image = LoadImage(entity.imageFile, entity_index);
  }
  entity.startingX = entity.x;
  entity.startingY = entity.y;
  return entity;
}

function CreateSound(s, isloop = false)
{
  if(s && s.soundFile)
  {
    if(s.options)
    {
      isloop = s.options.loop;
    }
    
    s.soundEle = LoadSound(s.soundFile, isloop);
  }
  return s;
}

function LoadTexture(textures_array)
{
  var texture_images = [];
  for(let i = 0; i< textures_array.length; i++)
  {
    var image = LoadSingleTexture(textures_array[i].path, textures_array[i].id);
  }
}

function LoadEntitiesFromScene(scene)
{
  var sprites = [];
  for(let i = 0; i< scene.entities.length; i++)
  {
    var s = CreateSprite(scene.entities[i], i);
    sprites[i] = s;
  }
  return sprites;
}

function LoadSoundsFromScene(Scene)
{
  var sounds = {};
  // console.log("Scene.sounds:", Scene.sounds);
  // console.log("Scene.sounds keys:", Object.keys(Scene.sounds));

  //Need an howler for each sound
  

  for(let i of Object.keys(Scene.sounds))
  {    
    // console.log("LoadSoundsFromScene i:", i)
    // console.log("LoadSoundsFromScene s:", Scene.sounds[i]);
    
    var s = CreateSound(Scene.sounds[i]);
    sounds[i] = s;
  }
  return sounds;
}

function LoadTileset(path, targetScene)
{
  var img = new Image();
  img.src = path;
  img.onload = function() {
    // console.log(path + 'Tile Ready');
    targetScene.tileMapImage = img;
  }

  return img;
}

function ResizeCanvas(canvasId)
{
  // console.log("Resize");
  var canvas = document.getElementById(canvasId);

  var devicePixelRatio = window.devicePixelRatio || 1,
      backingStoreRatio = Window.ctx2.webkitBackingStorePixelRatio ||
                            Window.ctx2.mozBackingStorePixelRatio ||
                            Window.ctx2.msBackingStorePixelRatio ||
                            Window.ctx2.oBackingStorePixelRatio ||
                            Window.ctx2.backingStorePixelRatio || 1,

      ratio = devicePixelRatio / backingStoreRatio,
      auto;

      backingStoreRatio = 2;
      // console.log(backingStoreRatio);

    // ensure we have a value set for auto.
    // If auto is set to false then we
    // will simply not upscale the canvas
    // and the default behaviour will be maintained
    if (typeof auto === 'undefined') {
        auto = true;
    }

    // upscale the canvas if the two ratios don't match
    if (auto && devicePixelRatio !== backingStoreRatio) {

        var oldWidth = Window.viewWidth;
        var oldHeight = Window.viewHeight;

        var newGameWidth = oldWidth * ratio;
        var newGameHeight = oldHeight * ratio;

        canvas.setAttribute("width", newGameWidth);
        canvas.setAttribute("height", newGameHeight);

        if(Scene.useRenderGrid == false)
        {
          Window.screenWidth = newGameWidth;
          Window.screenHeight = newGameHeight;
        }

        Window.viewWidth = newGameWidth;
        Window.viewHeight = newGameHeight;

        // canvas.width = Window.viewWidth;
        // canvas.height = Window.viewHeight;

        // canvas.style.width = oldWidth + 'px';
        // canvas.style.height = oldHeight + 'px';

        // now scale the context to counter
        // the fact that we've manually scaled
        // our canvas element
        canvas.getContext('2d').scale(ratio, ratio);

        Window.ctx.scale(ratio, ratio);
        Window.ctx2.scale(ratio, ratio);
        Window.ctxUi.scale(ratio, ratio);
    }
}

function setInitialState(canvasId, initCallback) {
  console.log("Initialize");

  Window.canvasId = canvasId;
  var canvas = document.getElementById(Window.canvasId);
  canvas.setAttribute("width", Window.viewWidth);
  canvas.setAttribute("height", Window.viewHeight);

  var canvas2 = document.createElement('canvas');
  canvas2.id     = "canvas2";
  canvas2.width  = Window.screenWidth;
  canvas2.height = Window.screenHeight;

  var Uican = document.createElement('canvas');
  Uican.id     = "Uicanvas";
  Uican.width  = Window.screenWidth;
  Uican.height = Window.screenHeight;

  if (canvas.getContext)
  {
    Window.ctx = canvas2.getContext('2d');
    Window.ctx2 = canvas.getContext('2d');
    Window.ctxUi = Uican.getContext('2d');

    //Create the Scene root node
    Scene.CreateRootNode();

    //Init input
    Input.init();

    if(initCallback != null)
    {
      initCallback(Window.ctx);
    }

    LoadTexture(Scene.textures_array);
    // Scene.entities = LoadEntitiesFromScene(Scene);
    Scene.sounds = LoadSoundsFromScene(Scene);
    if(Scene.tilesetTexture != '') 
    {
      // LoadTileset(Scene.tilesetTexture, Scene);
    }

    if(Scene.useRenderGrid)
    {
      Scene.fillRenderGrid();
    }
  } 
  else 
  {
    // canvas-unsupported code here
    console.log("Error context not supported");
    return 1;
  }

  //Initialize Input
  document.addEventListener('keydown', Input.keyDownHandler, false);
  document.addEventListener('keyup', Input.keyUpHandler, false);
  document.addEventListener('mousemove', Input.mouseMoveHandler, false);
  document.addEventListener('mousedown', Input.mouseDown, false);
  document.addEventListener('mouseup', Input.mouseUp, false);
  
  window.addEventListener('focus', (event) => {
    console.log("window focused out");
  },false);
  
  window.addEventListener('blur', (event) => {
    console.log("window blur out");
    Input.stopInputStuck()
  },false);

  // Register touch event handlers
  document.addEventListener('touchstart', Input.touchStart, false);
  document.addEventListener('touchmove', Input.touchMove, false);
  document.addEventListener('touchcancel', Input.touchCancel, false);
  document.addEventListener('touchend', Input.touchEnd, false);

  //disable context menu
  // document.addEventListener('contextmenu', event => event.preventDefault());

  // console.log(StateManager);

  canvas.focus();

  return 0;
}

function CheckCollision(rect1, rect2)
{
  // if(rect1.active == null)
  // {
  // 	rect1.active = true;
  // }
  // if(rect2.active == null)
  // {
  // 	rect2.active = true;
  // }
  // console.log("rect 1 active", rect1.active);

  if(rect1 && rect2)
  {
    if (rect1.x < rect2.x + rect2.w &&
       rect1.x + rect1.w > rect2.x &&
       rect1.y < rect2.y + rect2.h &&
       rect1.h + rect1.y > rect2.y) {
        // collision detected!
      return true;
    }
  }
  return false;
}

function Every(array, callback) //My Every version because the buildin doesn't work
{
  for(var i= 0; i< array.length; i++)
  {
    var element = array[i];
    callback(element);
  }
}

function DrawRect(rect, color)
{
  Window.ctx.fillStyle = color;
  Window.ctx.fillRect(rect.x, rect.y, rect.width, rect.height);
}

function SortAllSprites(sprites)
{
  sprites.sort(
    function(a, b)
    {
      //-1 inferiore, 1 maggiore 0 uguale
      if(a.layer >  b.layer)
        return -1;
      if(a.layer <  b.layer)
        return 1;
      return 0;
    }
  );
}

function DrawAllSprites(sprites, timeFromLastRender)
{
  SortAllSprites(sprites);
  Every(sprites, function(sp) {
    if(sp.active == true && sp.visible == true)
    {
      //sp.image is not an imag object anymore, just a string or array of strings
      switch(sp.type)
      {
        case "tileImage":
          if(sp.imageFile != undefined)
          {
            var sourcePos = {};
            var texture = Scene.textures[sp.imageFile];
            sourcePos.x = (sp.col - 1) * sp.t_width;
            sourcePos.y = (sp.row - 1) * sp.t_height;
            if (texture)
            {
              Window.ctx.drawImage(texture, sourcePos.x, sourcePos.y, sp.t_width, sp.t_height, sp.x, sp.y, sp.width, sp.height);
            }
          }
          break;
        case "animated":
          if(sp.imageFile != undefined)
          {
            var sourcePos = GSEngine.GetXY(sp.lastFrame, sp.col, sp.row, sp.t_width, sp.t_height);
            //sourcePos.x = (sp.row - 1) * sp.t_width;
            //sourcePos.y = (sp.col - 1) * sp.t_height;
            var texture = Scene.textures[sp.imageFile];
            if (texture)
            {
              Window.ctx.drawImage(texture, sourcePos.x, sourcePos.y, sp.t_width, sp.t_height, sp.x, sp.y, sp.width, sp.height);

              if(sp.no_animation == false)
              {
                sp.frameTime -= timeFromLastRender;
                if(sp.frameTime <= 0)
                {
                  sp.lastFrame++
                  sp.lastFrame %= sp.row * sp.col;
                  sp.frameTime = sp.sFrameTime;
                }
              }
            }
          }
          break;
        case "image":
          if(sp.imageFile != undefined)
          {
            var texture = Scene.textures[sp.imageFile];
            let x = sp.x;
            let y = sp.y;
            //Adapt for scale
            if(sp.scale){
              if(sp.scale.x < 0)
              {
                x *= -1;
                x -= sp.width 
              }
              if(sp.scale.y < 0)
              {
                y *= -1;
                y -= sp.height;
              }
              Window.ctx.save();
              Window.ctx.scale(sp.scale.x, sp.scale.y);
            }
            
            if (texture)
            {
              Window.ctx.drawImage(texture, x, y, sp.width, sp.height);
            }
            // Window.ctx.scale(1, 1);
            Window.ctx.restore();
          }
          break;
        case "color":
            Window.ctx.fillStyle = sp.color;
            Window.ctx.fillRect(sp.x, sp.y, sp.width, sp.height);
          break;
        case "textbox":
            Window.ctx.fillStyle = sp.color;
            Window.ctx.fillRect(sp.x, sp.y, sp.width, sp.height);
            if(sp.text)
            {
              let text_size = sp.size || "12";
              //TODO use a Text name space ?
              RawDrawText(sp.text, sp.x, sp.y + Number(text_size), sp.textColor || "white", text_size, sp.align || 'left', true);
            }
          break;
      }

      // if(sp.imageFile == null || sp.imageFile == undefined)
      // {
      // 	Window.ctx.fillStyle = sp.color;
      // 	Window.ctx.fillRect(sp.x, sp.y, sp.width, sp.height);
      // 	return;
      // }

      // if(sp.imageFile != "" && typeof sp.imageFile != 'string' && sp.imageFile.length > 0)
      // {
      // 	Window.ctx.drawImage(sp.image[sp.lastFrame], sp.x, sp.y, sp.width, sp.height);
      // 	sp.frameTime -= timeFromLastRender;
      // 	if(sp.frameTime <= 0 && sp.frameTime <= -99)
      // 	{
      // 		sp.lastFrame++
      // 		sp.lastFrame %= sp.imageFile.length;
      // 		sp.frameTime = sp.sFrameTime;
      // 	}
      // }
      // else if (sp.isSpriteSheet == true && sp.imageFile != "" && sp.imageFile.length > 0 && sp.image != undefined)
      // {
      // 	var sourcePos = {};
      // 	sourcePos.x = (sp.row - 1) * sp.t_width;
      // 	sourcePos.y = (sp.col - 1) * sp.t_height;
      // 	Window.ctx.drawImage(sp.image, sourcePos.x, sourcePos.y, sp.t_width, sp.t_height, sp.x, sp.y, sp.width, sp.height);
      // 	if(sp.no_animation == false)
      // 	{
      // 		sp.frameTime -= timeFromLastRender;
      // 		if(sp.frameTime <= 0 && sp.frameTime <= -99)
      // 		{
      // 			sp.lastFrame++
      // 			sp.lastFrame %= sp.row * sp.col;
      // 			sp.frameTime = sp.sFrameTime;
      // 		}
      // 	}
      // }
      // else if(sp.imageFile != "" && sp.image)
      // {
      //
      // }
      // else {
      //
      // }

      if(GSEngine.drawBounds == true) {
        Window.ctx.strokeStyle = 'green';
        Window.ctx.strokeRect(sp.x, sp.y, sp.width, sp.height);
      }

      // if(sp.color == 'blue')
      // {
      // 	Window.ctx.fillStyle = 'yellow';
      // 	Window.ctx.fillRect(sp.nextX, sp.nextY, 5, 5);
      // }
    }
    // sp.Render(MyGame.ctx);
  });
}

function DrawMouse()
{
  // var canvas = document.getElementById(Window.canvasId);
  // var xMargin = canvas.getAttribute("margin-left");
  // var yMargin = canvas.getAttribute("margin-top");
  //
  // var padding = canvas.getAttribute("padding");
  //
  // console.log("xMargin =", xMargin);
  // console.log("yMargin =", yMargin);
  // console.log("padding =", padding);

  Window.ctx.strokeStyle = 'green';
  var txt = Math.round(Input.mouse.clientX) + ',' + Math.round(Input.mouse.clientY);

  GSEngine.rawDrawText(txt, Input.mouse.clientX, Input.mouse.clientY, 'white', '10');

  //Horrizontal
  Window.ctx.beginPath();
  Window.ctx.moveTo(0, Input.mouse.clientY);
  Window.ctx.lineTo(Window.screenWidth, Input.mouse.clientY);
  Window.ctx.moveTo(Input.mouse.clientX, 0);
  Window.ctx.lineTo(Input.mouse.clientX, Window.screenHeight);
  Window.ctx.stroke();
}

function muteSounds(mute)
{
  Howler.mute(mute);
}

function RawDrawText(text, x, y, color, size = '48', align = 'center', ui = false, fontFamily = "serif") {
  var fontSize = size + "px " + fontFamily;
  var text = text || "";
  if(ui == false)
  {
    Window.ctx.font = fontSize;
    Window.ctx.fillStyle = color;
    Window.ctx.textAlign = align;
    Window.ctx.fillText(text.toString(), x, y);
  }
  else
  {
    Window.ctxUi.font = fontSize;
    Window.ctxUi.fillStyle = color;
    Window.ctxUi.textAlign = align;
    Window.ctxUi.fillText(text.toString(), x, y);
  }
}

/*
Cycle all the letter of the string and draw each one
alternating the color, in the same order of colors
*/
function RawDrawRainbowText(text, x, y, colors, size = '48', align = 'center', ui = false, fontFamily = "serif", letter_space = 3) 
{
  let fontSize = size + "px " + fontFamily;
  text = text || "";
  let color_id = GSEngine.rainbow.starting_color || 0;
  let next_x = 0;
  let start_x = x;
  let context = Window.ctx;

  if(ui == true)
  {
    context = Window.ctxUi;
  }

  context.font = fontSize;
  context.textAlign = align;

  if(align == "center")
  {
    start_x -= Math.round(text.length / 2) * letter_space;
  }
 
  for(let i =0; i<text.length; i++)
  {
    context.fillStyle = colors[color_id];
    context.fillText(text[i].toString(), start_x + next_x, y);
    color_id++
    color_id %= colors.length;
    next_x += letter_space;
  }
}

function DrawSprite(sprite_id, color, x, y, w, h, ui = false, source_x, source_y, source_w, source_h)
{
  var texture = Scene.textures[sprite_id];
  if (texture)
  {
    if(ui == true)
    {
      Window.ctxUi.drawImage(texture, source_x, source_y, source_w, source_h, x, y, w, h);
    }
    else
    {
      Window.ctx.drawImage(texture, source_x, source_y, source_w, source_h, x, y, w, h);
    }
  }
  else //Draw color sprite
  {
    if(ui == true)
    {
      Window.ctxUi.fillStyle = color;
      Window.ctxUi.fillRect(x, y, w, h);
    }
    else
    {
      Window.ctx.fillStyle = color;
      Window.ctx.fillRect(x, y, w, h);
    }
    
  }
}

// function RawDrawTile(tileImage, x, y, color, size = '48', align = 'center', ui = false) {
//   var fontSize = size + 'px serif';
//   if(ui == false)
//   {
//     Window.ctx.font = fontSize;
//     Window.ctx.fillStyle = color;
//     Window.ctx.textAlign = align;
//     Window.ctx.fillText(text.toString(), x, y);
//   }
//   else
//   {
//     Window.ctxUi.font = fontSize;
//     Window.ctxUi.fillStyle = color;
//     Window.ctxUi.textAlign = align;
//     Window.ctxUi.fillText(text.toString(), x, y);
//   }
// }

function DrawText(uiE)
{
  var fontSize = uiE.size + 'px serif';
  Window.ctx.font = fontSize;
  Window.ctx.fillStyle = uiE.color;
  Window.ctx.textAlign = uiE.align;
  Window.ctx.fillText(uiE.text.toString(), uiE.x, uiE.y);
}

function DrawUI(uiEntities)
{
  Every(uiEntities, function Draw(uiE) {
    if(uiE.visible == true)
    {
      DrawText(uiE);
    }
  });
}

// var map = [
// 	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,
// 	0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
// 	1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1
// ];

function DrawGrid(cellWidth, cellHeight, color, showNum, mapWidth, mapHeight)
{
  Window.ctx.strokeStyle = color || 'black';
  Window.ctx.fillStyle = color || 'black';
  Window.ctx.textAlign = "center";
  var fontSize = '14' + 'px serif';
  Window.ctx.font = fontSize;

  var numCellWidth = (mapWidth || 640) / cellWidth;
  var numCellHeight = (mapHeight || 320) / cellHeight;

  var midCellx = cellWidth/ 2;
  var midCelly = cellHeight/ 2;

  for(var y=0; y< numCellHeight; y++ ) {
    for(var x=0; x< numCellWidth; x++ ) {
      Window.ctx.strokeRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
      if(showNum) {
        Window.ctx.fillText((x + y * numCellWidth).toString(), (x * cellWidth) + midCellx, (y * cellHeight) + midCelly);
      }
      // if(map[x + y * numCellWidth] === 1)	{
      // 	Window.ctx.fillRect(x * cellWidth, y * cellHeight, cellWidth, cellHeight);
      // }
    }
  }
}

function DrawTiles(tilesetTexture, tileMap, cellWidth, cellHeight)
{
  var tileset = Scene.textures[tilesetTexture];
  if(tileset)
  {
    var numCellTileW = tileset.width / cellWidth;
    var numCellTileH = tileset.height / cellHeight;

    for(var i=0; i< tileMap.length; i++ ) 
    {
      if(tileMap[i][2] > -1 && tileMap[i][2] <= (numCellTileH * numCellTileW) &&
        tileMap[i][3] > -1 && tileMap[i][3] <= (numCellTileH * numCellTileW))
      {
        // var c = tileMap[i][2];
        var sy = tileMap[i][3];
        var sx = tileMap[i][2];

        var dy = tileMap[i][1]
        var dx = tileMap[i][0];

        Window.ctx.drawImage(tileset, sx * cellWidth, sy * cellHeight, cellWidth, cellHeight,
          dx * cellWidth, dy * cellHeight, cellWidth, cellHeight);
      }
    }
  }
  // console.log(ignoredTiles);
}

//Draw the tileset palette (all the images in the tileset)
function DrawTilesPalette(tilesetTexture, tileMap, cellWidth, cellHeight, pageNum, size)
{
  var tileset = Scene.textures[tilesetTexture];
  if(tileset)
  {
    //Blue background
    GSE.rawDrawRect(0, 0, tileset.width, tileset.height, "pink", 3, "black");

    var target_width = Window.screenWidth;
    var target_height = Window.screenHeight;

    if(Scene.useRenderGrid == true)
    {
      target_width = Scene.renderView.w;
      target_height = Scene.renderView.h;
    }

    Window.ctxUi.drawImage(tileset, paletteView.x, paletteView.y,tileset.width, tileset.height,
      0,0,tileset.width, tileset.height);
  }
}

function DrawTile(tilesetTexture, tileIndex, x, y, cellWidth, cellHeight)
{
  var tileset = Scene.textures[tilesetTexture];
  if(tileset)
  {
    var numCellTileW = tileset.width / cellWidth;

    var c = tileIndex;
    var sy = Math.floor(c / numCellTileW);
    var sx = c - (numCellTileW * sy);

    Window.ctx.drawImage(tileset, sx * cellWidth, sy * cellHeight, cellWidth, cellHeight,
      x, y, cellWidth, cellHeight);
  }
}

var TileIndex = 0;
var palettePage = 0; //Tileset palette page
var paletteSize = 30;
var currentPalette = [];
var showPalette = false;
var can_place_block = true;

//TODO fix palette size
var paletteView = {
  x: 0,
  y: 0,
  w: 640,
  h: 480
}

function Swap()
{
  // draw sprites on ctx2,
  // draw ui on ctx2
  if(Scene.useRenderGrid == false)
  {
    Window.ctx2.drawImage(Window.ctx.canvas, 0,0, Window.viewWidth, Window.viewHeight);
  }
  else
  {
    // Window.ctx2.drawImage(Window.ctx.canvas, 0,0);
    Window.ctx2.drawImage(Window.ctx.canvas, Scene.renderView.x, Scene.renderView.y, Scene.renderView.w, Scene.renderView.h,
      0, 0, Window.viewWidth, Window.viewHeight);
  }

  Window.ctx2.drawImage(Window.ctxUi.canvas, 0, 0, Window.viewWidth, Window.viewHeight);
}

function GetSpritesFromGrid()
{
  var sp = [];
  if(Scene.useRenderGrid == true)
  {
    //Render only the object on the region
    var view = Scene.renderView;
    var cell_w = Scene.renderGrid.cell_w;
    var cell_h = Scene.renderGrid.cell_h;

    var first_x = Math.floor(view.x / cell_w);
    var first_y = Math.floor(view.y / cell_h);

    var cells_w = Math.min(Math.round((Scene.renderGrid.tw - view.x) / cell_w), Math.round(view.w / cell_w));
    var cells_h = Math.min(Math.round((Scene.renderGrid.th - view.y) / cell_h), Math.round(view.h / cell_h));

    for(var x = 0; x <= cells_w; x++)
    {
      for(var y = 0; y <= cells_h; y++)
      {
        var ent = Scene.grid[first_y + y][first_x + x];
        if(ent)
        {
          ent.forEach(function (v) {
            if(v.active != true)
            {
              v.setActive(true);
            }
            sp.push(v);
          });
        }
      }
    }
  }
  return sp;
}

function Render(tFrame, callbackRender)
{
  //Clear the ctx canvas, no back, draw the sprites on it
  //Clear the ui no back, draw the ui on it.
  //Clear the ctx2 with background, draw on it the ctx, draw on it the ui.
  ClearCanvas(Window.ctx);
  ClearCanvas(Window.ctx2);
  ClearCanvas(Window.ctxUi);

  FillBackground(Window.ctx2, Window.backColor, 0);

  if(GSEngine.renderGrid && GSEngine.editorMode == true) {
    // DrawGrid(GSE.Scene.tileWidth,GSE.Scene.tileHeight, 'blue', false);

  }

  if(Scene.useRoot == false)
  {
    if(Scene.useRenderGrid == true)
    {
      DrawAllSprites(GetSpritesFromGrid(), tFrame);
    }
    else
    {
      DrawAllSprites(Scene.entities, tFrame);
    }

    if(Scene.tilesetTexture !== '' && Scene.map != undefined)
    {
      DrawTiles(Scene.tilesetTexture, Scene.map, Scene.tileWidth, Scene.tileHeight);
    }

    var currentState = StateManager.currentState();
    if(currentState != undefined && typeof(currentState.render) == 'function') {
      currentState.render(tFrame);
    }

    if(callbackRender != null)
    {
      callbackRender(tFrame);
    }

    DrawUI(Scene.uiEntities);
  }
  else
  {
    //Render the new Scene v2
    //Traverse the tree, order the childs
    //Traverse the tree from the root, and call draw
    //PS. Rember then to move all the rendering callback before this

    var currentState = StateManager.currentState();
    if(currentState != undefined && typeof(currentState.render) == 'function') {
      currentState.render(tFrame);
    }

    if(callbackRender != null)
    {
      callbackRender(tFrame);
    }

    OrderSceneTree(Scene.root);
    DrawSceneTree(Scene.root, Window.ctx);
    CleanSceneTree(Scene.root);
  }

  

  /*
      This is a little tile editor, but not all games use this, or use tiles at all
      so maybe remove this from the Engine
  */
  if(GSEngine.editorMode == true) 
  {
    // DrawMouse();
    var mX = Input.mouse.clientX;
    var mY = Input.mouse.clientY;

    //Calculate the mouse pos based on the view and snapped to the tile grid
    var curPos = GetCursorPosition(mX, mY);

    var mx_snapped = Math.floor(curPos.x / Scene.tileWidth) * Scene.tileWidth;
    var my_snapped = Math.floor(curPos.y / Scene.tileHeight) * Scene.tileHeight;

    //Draw tile at is position
    if(Scene.tilesetTexture !== '' && Scene.map != undefined)
    {
      DrawTile(Scene.tilesetTexture, TileIndex, mx_snapped, my_snapped, Scene.tileWidth, Scene.tileHeight);
    }

    // if(Scene.useRenderGrid == true)
    // {
      // DrawGrid(Scene.renderGrid.cell_w, Scene.renderGrid.cell_h, "blue", true);
      DrawGrid(Scene.tileWidth, Scene.tileHeight, "green", false);

    // }

    //Show palette
    if(showPalette)
    {
      
      DrawTilesPalette(Scene.tilesetTexture, Scene.map, Scene.tileWidth, Scene.tileHeight, palettePage, paletteSize);
      DrawTileNumber(mX, mY, GSE.Scene.tileHeight, GSE.Scene.tileWidth);
    }

    if (GSEngine.logInput == true)
    {
      LogInputStatus();
    }

    DrawCursor(curPos.x, curPos.y, mx_snapped, my_snapped);
  }

  //Draw fps and numtick
  // if(GSEngine.timeSinceTick)
  // {
  //   GSE.rawDrawText((GSE.timeSinceTick / 1000).toFixed(3), 20, GSE.Window.viewHeight - 24, 'white', '24', 'left', true);
  //   GSE.rawDrawText(GSE.numTicks, 20, GSE.Window.viewHeight - 4, 'white', '24', 'left', true);
  // }
}

//The higher is above
function compareNodes(nodeA, nodeB)
{
	let numA = nodeA.order;
	let numB = nodeB.order;

	if (numA < numB) {
	  return -1;
	}
	if (numA > numB) {
	  return 1;
	}
	// a must be equal to b
	return 0;
}

function OrderSceneTree(node)
{
  //Need to be recursive
  if(node.child == null || node.child.length == 0)
  {
    //No child return
    return;
  }

  node.child.sort(compareNodes);
  for(let i in node.child)
  {
    OrderSceneTree(node.child[i]);
  }
}

//Remove all the tmp nodes

//TODO clean all the node with a filter, and then recur on the childs
function CleanSceneTree(node)
{
  //Need to be recursive
  if(node.child == null || node.child.length == 0)
  {
    return;
  }

  node.child = node.child.filter((node) => {
    return node.tmp === false; //return not tmp nodes
  })

  for(let i in node.child)
  {
    CleanSceneTree(node.child[i]);
  }
}

//Need to be drawn reversed, 
//first father then child, or the child will be under the father
function DrawSceneTree(node, context, indent, count, father_offset, offset_x, offset_y)
{
  indent = indent || 0;
  count = count || 0;
  offset_x = offset_x || 0;
  offset_y = offset_y || 0;
  father_offset = father_offset || {x:0, y:0};

  //also draw a debug tree on view
  //for now text
  if(Scene.debug_draw) {
    let x = 128 + (indent * 16);
    let y = 10 + (count * 16);

    GSE.rawDrawText(node.path + "-id: " + node.id + "- order:" + node.order, x, y, 'white', '12', 'left',  true);
  }

  //add accumulated offset from father ??
  if(typeof node.render_func === "function")
  {
    context.father_offset = {x:offset_x, y:offset_y};
    node.render_func(context);
  }
  // console.log("DrawSceneTree node:", node);
  //Need to be recursive
  if(node.child == null || node.child.length == 0)
  {
    //No child, draw this node if it can, return
    //Draw leaf
    // console.log(" ---- Render node: ", node, " ----  ");
    // node.render_func(context);
    return count;
  }

  indent++;
  //It does not change for every 
  offset_x += node.data.x
  offset_y += node.data.y
  // father_offset.x += node.data.x; 
  // father_offset.y += node.data.y;

  // debugger;

  if(node.data && node.data.hidden && node.data.hidden === true)
  {
    return count;
  }

  //Disable a node whit no graphics
  // if(!node.active)
  // {
  //   return count;
  // }
 
  
  for(let i in node.child)
  { 
    count++;

    count = DrawSceneTree(node.child[i], context, indent, count, father_offset, offset_x, offset_y);
  }

  return count;
  
}

function DrawTileNumber(mX, mY, tileHeight, tileWidth)
{
  if(Scene.useRenderGrid == true)
  {
    mX -= Scene.renderView.x;
    mY -= Scene.renderView.y;
  }

  //Compensate for the palette view
  mX += paletteView.x;
  mY += paletteView.y;

  var tile_x = Math.floor(mX / tileWidth);
  var tile_y = Math.floor(mY / tileHeight);

 tile_x++;
 tile_y++;

  // GSE.rawDrawRectUi(paletteView.x + 32, paletteView.y + 64, 128, 64, "black");
  GSE.rawDrawText("tile :" + tile_x.toFixed(1).toString() + "," + tile_y.toFixed(1).toString(), 32, 64, 'white', '12', 'left',  true);
}

function GetCursorPosition(mX, mY)
{
  var curX = mX;
  var curY = mY;

  if(Scene.useRenderGrid == true)
  {
    curX -= Scene.renderView.x;
    curY -= Scene.renderView.y;
  }
  else
  {
    curX = (curX * Window.screenWidth ) / Window.viewWidth;
    curY = (curY * Window.screenWidth ) / Window.viewWidth;
  }
    /*Window.screenWidth = screenWidth;
    Window.screenHeight = screenHeight

    Window.viewWidth = viewWidth;
    Window.viewHeight = viewHeight;*/
  //The x and y are based on the View, we need to find the one based on the screen
  // mX : viewWidth = ? : screenWidth
  //mX * screenWidth / viewWidth
  

  var cur_x_snapped = Math.floor(curX / Scene.tileWidth) * Scene.tileWidth;
  var cur_y_snapped = Math.floor(curY / Scene.tileHeight) * Scene.tileHeight;

  return {x: cur_x_snapped, y: cur_y_snapped};
}

//Draw the cursor user to place the tiles 
function DrawCursor(mX, mY, globalX, globalY)
{
  GSE.rawDrawRectLine(mX , mY , Scene.tileWidth, Scene.tileHeight, "yellow", 3);

  var tile_x = Math.floor(globalX / Scene.tileWidth);
  var tile_y = Math.floor(globalY / Scene.tileHeight);

  var pos_x = 32;
  var pos_y = 32;

  if(Scene.useRenderGrid == true)
  {
    pos_x = Scene.renderView.x + 32;
    pos_y = Scene.renderView.y + 32;
  }

  GSE.rawDrawText(tile_x.toFixed(1).toString() + "," + tile_y.toFixed(1).toString(), pos_x, pos_y, 'white', '12', 'left',  true);
  GSE.rawDrawText(mX.toString() + "," + mY.toString(), pos_x, pos_y + 12, 'white', '12', 'left',  true);

  // GSE.rawDrawText(tile_y.toFixed(1).toString(), Scene.renderView.x + 32, Scene.renderView.y + 32, 'white', '12', true);
}

function LogInputStatus()
{
  // var props = GSE.Input.logStatus()

  var mX = Input.mouse.clientX;
  var mY = Input.mouse.clientY;

  var mx_snapped = Math.floor(mX / Scene.tileWidth) * Scene.tileWidth;
  var my_snapped = Math.floor(mY / Scene.tileHeight) * Scene.tileHeight;
  
  GSE.rawDrawText("mouseX:" + mX.toFixed(1).toString(), Scene.renderView.x + 32, Scene.renderView.y + 32, 'white', '12', true);
  GSE.rawDrawText("mouseY:" + mY.toFixed(1).toString(), Scene.renderView.x + 32, Scene.renderView.y + 52, 'white', '12', true);

  GSE.rawDrawText("snappedX:" + mx_snapped.toFixed(1).toString(), Scene.renderView.x + 32, Scene.renderView.y + 72, 'white', '12', true);
  GSE.rawDrawText("snappedY:" + my_snapped.toFixed(1).toString(), Scene.renderView.x + 32, Scene.renderView.y + 92, 'white', '12', true);

  // props.forEach(
  //   function (val, idx, array) {
  //     GSE.rawDrawText(val.toString() +".pressed =" + GSE.Input.keyStatus[val].pressed.toString(), 48, (32 * idx) + 12, 'white', '12', true);
  //     GSE.rawDrawText(val.toString() +".prev_press ="+ GSE.Input.keyStatus[val].prev_press.toString(), 48, (32 * idx) + 24, 'white', '12', true);
  //   }
  // );
}

function SelectTileSetFromPalette(m_x, m_y, tileHeight, tileWidth)
{
  var res = 0;
  var tileset = GSE.Scene.textures[GSE.Scene.tilesetTexture];
  if(tileset)
  {
    //conpensate for the grid
    if(Scene.useRenderGrid == true)
    {
      m_x -= Scene.renderView.x;
      m_y -= Scene.renderView.y;
    } //Compensate for a different view size
    else
    {
      m_x = (m_x * Window.screenWidth ) / Window.viewWidth;
      m_y = (m_y * Window.screenWidth ) / Window.viewWidth;
    }

    //Compensate for the palette view
    //TODO set the palette view dynamically
    m_x += paletteView.x;
    m_y += paletteView.y;

    var total_map_width = tileset.width;
    var total_map_height = tileset.height;

    var tileMapWidth = Math.floor(total_map_width / tileWidth);

    if(m_x > 0 && m_x < total_map_width && m_y > 0 && m_y < total_map_height)
    {
      //Clicked inside the palette
      //get the x, y of the map
      var tile_x = Math.floor(m_x / tileWidth);
      var tile_y = Math.floor(m_y / tileHeight);

      console.log("tile_x:", tile_x);
      console.log("tile_y:", tile_y);

      var selectedTile = (tile_y * tileMapWidth) + tile_x;
      console.log("selectedTileNew:", selectedTile);
      res = selectedTile;
    }
  }
  return res;
}

//TODO add docs
//min and max included
function RandomRange(min, max) {
  return Math.random() * (max - min) + min;
}

function UpdateTextBox(sp) {
  //Special case for textboxes, 
  //Handle the textbox focus/input
  if(sp.focused)
  {
    let raw_input = GSE.Input.getRawKey();
    if(raw_input === "Backspace")
    {
      sp.text = sp.text.substr(0, sp.text.length - 1);
      // console.log("text:", sp.text);
      return;
    }
    if(raw_input.length == 1) {
      sp.text += raw_input;
    }
    if(Input.has_pasted)
    {
      sp.text += Input.pastedData
    }
    // console.log("text:", sp.text);
  }

  //Focus
  if(GSE.Input.IsMouseLeftDown())
  {
    if (checkBounds(sp.x, sp.y, sp.width, sp.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
    {
      sp.focused = true;
      console.log("focused:", sp.id);
    }
    else if(sp.focused)
    {
      sp.focused = false;
      console.log("focus out:", sp.id);
    }
  }
}

function HandleUpdates(sprites, delta)
{
  Every(sprites, function (sp)
  {
    if(sp.active == true)
    {
      if(sp.handleUpdate !== null)
      {
        sp.handleUpdate(sp, delta);
      }

      if(sp.type === "textbox")
      {
        UpdateTextBox(sp);
      }
    }
  });
}

function SaveMap(filename, mapData)
{
  try {
    var isFileSaverSupported = !!new Blob;
    var blob = new Blob(mapData, {type: "text/plain;charset=utf-8"});
    console.log("blob:", blob);
    saveAs(blob, filename);
  } 
  catch (e) 
  {
    console.log("exception:", e);
    console.log("manual save:", map_file);
  }

  // console.log(map_file);
}

function Instantiate(sp, x, y)
{
  //return a copy
  //for test
  var copy = Object.assign({}, sp, {"x":x, "y":y, "color":"red"});
  return copy;
}

// var start = 3;
// var end = 10;

// var l = start;
// var time = 6; //time in tick
var t = 0;

function Lerp(start, end, time)
{
  var result = start;
  if(t == 0) {
    t = 100 / time;
  }
  var distance = end - start;
  var l = start;

  l = Math.round(start + distance * (t / 100));

  if(end < start)
  {
    result = end;
  }
  else if(l < end)
  {
    t += 100 / time;
    result = l;
    // console.log(l);
  }

  return l;
}

function checkBoundsRects(rect1, rect2) 
{
  return checkBounds(rect1.x,rect1.y, rect1.w, rect1.h, rect2.x,rect2.y, rect2.w, rect2.h);
}

function checkBounds(x1,y1, w1, h1, x2,y2,w2,h2) {
  if (x1 < x2 + w2 &&
     x1 + w1 > x2 &&
     y1 <y2 + h2 &&
     h1 + y1 >y2) {
      // collision detected!
    return true;
  }

  return false;
}

//TODO make this function for more than one tile
function MovePaletteView(x, y, abs = false)
{
  var tileset = GSE.Scene.textures[GSE.Scene.tilesetTexture];

  if(tileset)
  {
    var th = tileset.height;
    var tw = tileset.width;

    if(abs == false)
    {
        var next_pos = {"x": paletteView.x + x, "y": paletteView.y + y};
        var vr = {"h": paletteView.h, "w": paletteView.w};

        if(next_pos.y >= 0 && next_pos.y <= th &&
              next_pos.y + vr.h <= th)
        {
            if (next_pos.x >= 0 && next_pos.x <= tw &&
                  next_pos.x + vr.w <= tw)
            {
                paletteView.x += x;
                paletteView.y += y;
            }
        }
    }
    else
    {
        //Center the view at the given pos, is it exit the grid , push it inside
        var vx = paletteView.x
        var vy = paletteView.y

        vx = Math.round(x - (paletteView.w / 2));
        vy = Math.round(y - (paletteView.h / 2));

        if(vx + paletteView.w > tw)
        {
            vx -= paletteView.w - tw;
        }

        if(vy + paletteView.h > th)
        {
            vy -= paletteView.h - th;
        }

        if(vx < 0 )
        {
            vx = 0;
        }

        if(vy < 0 )
        {
            vy = 0;
        }

        paletteView.x = vx;
        paletteView.y = vy;
    }
  }
}

function Update(lastTick, timeSinceTick, callbackUpdate) //This is called 1 or more time each frame, depending on the passed time
{
  let delta = timeSinceTick / 1000;
  GSEngine.timeSinceTick = timeSinceTick;
  GSEngine.lastTick = lastTick;

  // if(GSE.Input.IsKeyPressed('p'))
  // {
  //   GSE.pause = !GSE.pause || false;
  // }

  if(GSE.pause)
  {
    Input.clearInputPrevPressed();
    return;
  }

  HandleUpdates(Scene.entities, delta); // also the delta in ms
  if(callbackUpdate != null)
  {
    callbackUpdate(lastTick, delta);
  }

  var currentState = StateManager.currentState();
  if(currentState != undefined && typeof(currentState.update) == 'function') {
    currentState.update(lastTick, delta); //Delta time in sec
  }

  if(GSEngine.editorMode == true)
  {
    if(GSE.Input.IsKeyPressed('s'))
    {
      console.log("save map");
      SaveMap(GSE.Scene);
    }

    if(GSE.Input.IsKeyPressed('i'))
    {
      console.log("save map");
      GSEngine.logInput = !GSEngine.logInput
    }

    if(GSE.Input.IsKeyPressed('l'))
    {
      // TileIndex++;
      // TileIndex %= Scene.totalTiles();
      palettePage++;
      palettePage %= Math.round(Scene.totalTiles() / paletteSize);
      console.log("Next page ", palettePage);
    }

    if(GSE.Input.IsKeyPressed('k'))
    {
      if(palettePage > 0)
      {
        palettePage--;
      }
      else
      {
        palettePage = Math.round(Scene.totalTiles() / paletteSize);
      }

      palettePage %= Math.round(Scene.totalTiles() / paletteSize);
      console.log("prev page ", palettePage);
    }

    // if(Input.IsKeyPressed('q'))
    // {
    //   showPalette = !showPalette;
    //   console.log("showPalette:", showPalette);
    // }

    // if(Input.IsKeyDown('g'))
    // {
    //   //Move the palette view to the right
    //   MovePaletteView(32, 0)
    // }

    // if(Input.IsKeyDown('d'))
    // {
    //   //Move the palette view to the left
    //   MovePaletteView(-32, 0)
    // }

    // if(Input.IsKeyDown('r'))
    // {
    //   //Move the palette view to the right
    //   MovePaletteView(0, -32)
    // }

    // if(Input.IsKeyDown('f'))
    // {
    //   //Move the palette view to the right
    //   MovePaletteView(0, 32)
    // }

    if(Input.IsMouseLeftDown())
    {
      var mX = Input.mouse.clientX; //Are the exact value in pixel on the canvas ctx, are not affected by the view
      var mY = Input.mouse.clientY;

      if(showPalette)
      {
        can_place_block = false;
        
        console.log("tileIndex:", TileIndex);
        TileIndex = SelectTileSetFromPalette(mX, mY, GSE.Scene.tileHeight, GSE.Scene.tileWidth);
        showPalette = false;
        setTimeout(function(){can_place_block = true}, 500);
      }
      else if(can_place_block == true)
      {
        console.log("Left click");
        //Do shit to add tiles
        //Get mouse pos

        mX = (mX * Window.screenWidth ) / Window.viewWidth;
        mY = (mY * Window.screenWidth ) / Window.viewWidth;

        var mapX = Math.floor(mX / GSE.Scene.tileWidth);
        var mapY = Math.floor(mY / GSE.Scene.tileHeight);

        var tileset = GSE.Scene.textures[GSE.Scene.tilesetTexture];

        if(tileset) //Add the selected tile from the palette to the map
        {
          var numCellTileW = tileset.width / GSE.Scene.tileHeight;
          // var numCellTileH = GSE.Scene.tileMapImage.height / GSE.Scene.tileWidth;

          var c = TileIndex;
          var sy = Math.floor(c / numCellTileW);
          var sx = c - (numCellTileW * sy);
        
          // var mapIndex = mapX + mapY * GSE.Scene.tileMapWidth;

          if(Scene.map != undefined)
          {
            if(mX >= 0 && mX < Window.screenWidth &&
              mY >= 0 && mY < Window.screenHeight)
            {
              // Scene.map[mapIndex] = TileIndex;
              var selectedTileIndex = -1;
              //Search to see if the cliccked tile is already present
              var selectedTile = Scene.map.find(function(t, i)
              {
                if( t[0] == mapX && t[1] == mapY)
                {
                  selectedTileIndex = i;
                  return true;
                }
                return false;
              });

              if(selectedTile && selectedTileIndex != -1)
              {
                //Tile already filled change and don't add
                Scene.map[selectedTileIndex] = [mapX, mapY, sx, sy];
              }
              else
              {
                Scene.map.push([mapX, mapY, sx, sy]);
              }
            }
            else
            {
              console.log("Out of the map");
            }
          }
        }
      }
    }
  }

  if(Input.IsKeyPressed('e') && GSEngine.enableEditor)
  {
    GSEngine.editorMode = !GSEngine.editorMode;
    GSEngine.renderGrid = true;
    showPalette = false;
    console.log("showPalette:", showPalette);
  }

  

  //TODO does this works ?
  Input.clearInputPrevPressed(); //At the end of the update for the frame clear all the input
  // Scene.fillRenderGrid();

  //State saved for rainbow text
  // GSEngine.rainbow.starting_color = 0;

  if(GSEngine.rainbow.color_th >= GSEngine.rainbow.color_change_time)
  {
    GSEngine.rainbow.starting_color++
    GSEngine.rainbow.starting_color %= 7; //TODO make this a confing or fixed
    GSEngine.rainbow.color_th = 0;
  }
  else
  {
    GSEngine.rainbow.color_th += 1 * delta;
  }
}

// ;(function () {
//
// })();

var Window = {
  screenWidth: 640,
  screenHeight : 480,
  // leftWall : leftWall,
  // rightWall : rightWall,
  // upWall : upWall,
  // downWall : downWall
  CreateRect : function CreateRect(xx, yy, wwidth, hheight)
  {
    return Object.assign({}, {
      x:xx, y:yy, width:wwidth, height:hheight
    });
  }
};

var Collisions = {
  CheckCollision: CheckCollision
};

var RenderingFunctions = {
  'DrawSprite': function DrawSprite(ctx)
  {
    let node = this;
    if(node.data != null && node.data.texture != null)
    {
      let x_offset = 0;
      let y_offset = 0;

      if(node.data.hidden === true) {
        return;
      }

      /*
        This is only taking care of direct father
        I want it to be recursive
        cicling the fact that i am traversing father -> child, 
        i can keep a cumulative offet, that will be passed down, when i move to the 
        next child
        this way i also remove an if
      */
      x_offset = ctx.father_offset.x;
      y_offset = ctx.father_offset.y;
      // if(node.father && node.father.data)
      // {
      //     x_offset = node.father.data.x;
      //     y_offset = node.father.data.y;
      // }

      let sp = node.data;
      var texture = Scene.textures[sp.texture];
      let x = x_offset + sp.x;
      let y = y_offset + sp.y;
      let width = sp.w;
      let height = sp.h;
      //Adapt for scale
      if(sp.scale){
        if(sp.scale.x < 0)
        {
          x *= -1;
          x -= sp.width 
        }
        if(sp.scale.y < 0)
        {
          y *= -1;
          y -= sp.height;
        }
        ctx.save();
        ctx.scale(sp.scale.x, sp.scale.y);
      }
      
      if (texture)
      {
        ctx.drawImage(texture, x, y, width, height);

        if(Scene.debug_draw) {
          // console.log("x, y", x, y);
          RawDrawText(node.data.id, x+1 ,y+12, "red", "12", "left", false);
          RawDrawText(node.data.id, x+2,y+12, "black", "12", "left", false);
        }
      }

      ctx.restore();
    }
  },
  "DrawSpriteExt": function DrawSpriteExt(ctx)
  {
    let node = this;
    if(node.data != null && node.data.texture != null)
    {
      let x_offset = 0;
      let y_offset = 0;

      if(node.data.hidden === true) {
        return;
      }

      x_offset = ctx.father_offset.x;
      y_offset = ctx.father_offset.y;

      let sp = node.data;
      var texture = Scene.textures[sp.texture];
      let x = x_offset + sp.x;
      let y = y_offset + sp.y;
      let width = sp.w;
      let height = sp.h;
      let source_x = sp.sx;
      let source_y = sp.sy;
      let source_w = sp.sw;
      let source_h = sp.sh;

      //Adapt for scale
      if(sp.scale){
        if(sp.scale.x < 0)
        {
          x *= -1;
          x -= sp.width 
        }
        if(sp.scale.y < 0)
        {
          y *= -1;
          y -= sp.height;
        }
        ctx.save();
        ctx.scale(sp.scale.x, sp.scale.y);
      }
      
      if (texture)
      {
        // console.log("draw source_h:", source_h)
        ctx.drawImage(
          texture, 
          source_x, 
          source_y, 
          source_w, 
          source_h, 
          x, 
          y, 
          width, 
          height);
      }

      
      // ctx.scale(1, 1);
      ctx.restore();
    }
  },
  "DrawRect": function DrawRect(ctx)
  {
    let node = this;
    
    if(node.data != null)
    {

      if(node.data.hidden === true) {
        return;
      }

      let x_offset = 0;
      let y_offset = 0;

      x_offset = ctx.father_offset.x;
      y_offset = ctx.father_offset.y;

      let border = node.data.border;
      let borderColor = node.data.borderColor;
      let x = x_offset + node.data.x 
      let y = y_offset + node.data.y
      let width = node.data.w
      let height = node.data.h
      let gradient = node.data.gradient
      let color = node.data.color
      let alpha = node.data.alpha

      if(node.data.scale){
        if(node.data.scale.x < 0)
        {
          x *= -1;
          x -= node.data.w 
        }
        if(node.data.scale.y < 0)
        {
          y *= -1;
          y -= node.data.h;
        }
        ctx.save();
        ctx.scale(node.data.scale.x, node.data.scale.y);
        x /= node.data.scale.x;
        y /= node.data.scale.y;
      }

      if(border > 0 && borderColor !== undefined) {
        // color = borderColor
        ctx.fillStyle = borderColor;
        ctx.fillRect(x, y, width, height);

        var fillPosX = x + border;
        var fillPosY = y + border;
        var fillWidth = width - border * 2;

        var grad = ctx.createRadialGradient(x + (width / 2), (fillPosY + height) / 2, 10, (fillPosX + width) / 2, (fillPosY + height) / 2, width);
        grad.addColorStop(0, '#0cd3c6');
        // grad.addColorStop(0.9, '#019F62');
        grad.addColorStop(0.7, '#0c7dd3');

        if(gradient == true)
        {
          ctx.fillStyle = grad;
        }
        else {
          ctx.fillStyle = color;
        }

        if(alpha)
        {
          ctx.globalAlpha = alpha;
          ctx.fillRect(fillPosX, fillPosY, width - border * 2, height - border * 2);
          ctx.globalAlpha = 1;
        }
        else
        {
          ctx.fillRect(fillPosX, fillPosY, width - border * 2, height - border * 2);
        }
      }
      else {
        ctx.fillStyle = color;
        
        if(alpha)
        {
          ctx.globalAlpha = alpha;
          ctx.fillRect(x, y, width, height);
          ctx.globalAlpha = 1;
        }
        else
        {
          ctx.fillRect(x, y, width, height);
        }
      }

      ctx.restore();
    }
  },
  "DrawText": function DrawText(ctx)
  {
    //Now the x, and y are offeset from the father if present
    //root will have x and y = 0;
    let node = this;
    let x_offset = 0;
    let y_offset = 0;

      x_offset = ctx.father_offset.x;
      y_offset = ctx.father_offset.y;

    if(node.data != null)
    {
      if(node.data.hidden === true) {
        return;
      }

      let text = node.data.text;
      let x = x_offset + node.data.x;
      let y = y_offset + node.data.y;
      let color = node.data.color;
      let align = node.data.align;
      let font = node.data.font;
      let size = node.data.size;

      RawDrawText(text, x, y, color, size, align, false, font);
    }    
  },
  "DrawLine": function DrawLine(ctx)
  {
    //Now the x, and y are offeset from the father if present
    //root will have x and y = 0;
    let node = this;
    let x_offset = 0;
    let y_offset = 0;

      x_offset = ctx.father_offset.x;
      y_offset = ctx.father_offset.y;

    if(node.data != null)
    {
      if(node.data.hidden === true) {
        return;
      }
      
      let x_start = x_offset + node.data.x_start;
      let y_start = y_offset + node.data.y_start;
      let x_end = x_offset + node.data.x_end;
      let y_end = y_offset + node.data.y_end;
      let color = node.data.color;
      let size = node.data.size;

      ctx.strokeStyle = color;
      ctx.lineWidth = size;

      ctx.beginPath();
      ctx.moveTo(x_start, y_start);
      ctx.lineTo(x_end, y_end);      
      ctx.stroke();
    }    
  },
  "DrawSpriteAnimated": function DrawSpriteAnimated(ctx)
  {
    let node = this;
    if(node.data != null && node.data.texture != null)
    {
      let x_offset = 0;
      let y_offset = 0;

      if(node.data.hidden === true) {
        return;
      }

      x_offset = ctx.father_offset.x;
      y_offset = ctx.father_offset.y;

      let sp = node.data;
      var texture = Scene.textures[sp.texture];
      let x = x_offset + sp.x;
      let y = y_offset + sp.y;
      let width = sp.w;
      let height = sp.h;
      let source_x = sp.sx;
      let source_y = sp.sy;
      let source_w = sp.sw;
      let source_h = sp.sh;

      //Adapt for scale
      if(sp.scale){
        if(sp.scale.x < 0)
        {
          x *= -1;
          x -= sp.width 
        }
        if(sp.scale.y < 0)
        {
          y *= -1;
          y -= sp.height;
        }
        ctx.save();
        ctx.scale(sp.scale.x, sp.scale.y);
      }
      
      if (texture)
      {
        // console.log("draw source_h:", source_h)
        ctx.drawImage(
          texture, 
          source_x, 
          source_y, 
          source_w, 
          source_h, 
          x, 
          y, 
          width, 
          height);
      }

      
      // ctx.scale(1, 1);
      ctx.restore();
    }
  },
  

}

function Timer(time, callback)
{
	this.counter = 0
	this.threshold = time
	this.callback= callback
  
	this.tick= (delta) => {
		if(this.counter <= this.threshold)
		{
			this.counter += 1 * delta
			return;
		}

		//Do the action and reset
		if(this.callback && typeof(this.callback) == "function")
		{
			this.counter = 0;
			this.callback()
		}
	}

  this.restart = () => {
    this.counter = 0;
  }
}

var GSEngine = {
  Init: function Init(canvasId, screenWidth, screenHeight, viewWidth, viewHeight, autoresize, initCallback, updateCallback, renderCallback, backColor = "black")
  {
    var stats = new Stats();
    stats.showPanel( 0 ); // 0: fps, 1: ms, 2: mb, 3+: custom
    stats.domElement.style.cssText = 'position:absolute;top:500px;left:0px;';
    document.body.appendChild( stats.dom );

    var stats2 = new Stats();
    stats2.showPanel( 1 ); // 0: fps, 1: ms, 2: mb, 3+: custom
    stats2.domElement.style.cssText = 'position:absolute;top:500px;left:80px;';
    document.body.appendChild( stats2.dom );
    
    function main( tFrame ) 
    {
	    stats.begin();
      stats2.begin();

      MyGame.stopMain = window.requestAnimationFrame( main );
      var nextTick = MyGame.lastTick + MyGame.tickLength;
      var numTicks = 0;

      //If tFrame < nextTick then 0 ticks need to be updated (0 is default for numTicks).
      //If tFrame = nextTick then 1 tick needs to be updated (and so forth).
      //Note: As we mention in summary, you should keep track of how large numTicks is.
      //If it is large, then either your game was asleep, or the machine cannot keep up.
      // var frameTDiff = tFrame - MyGame.lastTick;
      var timeSinceTick;
      if (tFrame > nextTick) {
        timeSinceTick = tFrame - MyGame.lastTick;
        numTicks = Math.floor( timeSinceTick / MyGame.tickLength );
        // GSEngine.numTicks = numTicks;
      }

      queueUpdates( numTicks, timeSinceTick, updateCallback);
      var delta = tFrame - MyGame.lastRender;

      if(numTicks > 0)
      {
        Render( delta , renderCallback);
        Swap();
      }

      stats.end();
      stats2.end();

      MyGame.lastRender = tFrame;
      GSEngine.numTicks = numTicks;
      Window.fps = numTicks;
    }

    function queueUpdates( numTicks, timeSinceTick, updateCallback) {
      // if (numTicks > 4) { numTicks = 3};
      for(var i=0; i < numTicks; i++) {
        MyGame.lastTick = MyGame.lastTick + MyGame.tickLength; //Now lastTick is this tick.
        Update( MyGame.lastTick, timeSinceTick, updateCallback);
      }
    }

    MyGame.lastTick = performance.now();
    MyGame.lastRender = MyGame.lastTick; //Pretend the first draw was on first update.
    MyGame.tickLength = (1000/50); //This sets your simulation to run at 20Hz (60ms)

    Window.screenWidth = screenWidth;
    Window.screenHeight = screenHeight

    Window.viewWidth = viewWidth;
    Window.viewHeight = viewHeight;
    Window.backColor = backColor;

    // window.onresize = ResizeCanvas(canvasId);
    if(autoresize == true)
    {
      window.onresize = function ()
      {
        ResizeCanvas(canvasId);
      }
    }

    if(setInitialState(canvasId, initCallback) == 0) {
      console.log('Loop Start');
      main(performance.now()); // Start the cycle
    }
    else {
      console.log('Error Initialization not ok');
    }
  },
  "rawDrawText": RawDrawText,
  "rawDrawRainbowText":RawDrawRainbowText,
  "randomRange": RandomRange,
  "rawDrawRect": function rawDrawRect(x, y, width, height, color, border = 0, borderColor, gradient, alpha) {
    if(border > 0 && borderColor !== undefined) {
      // color = borderColor
      Window.ctx.fillStyle = borderColor;
      Window.ctx.fillRect(x, y, width, height);

      var fillPosX = x + border;
      var fillPosY = y + border;
      var fillWidth = width - border * 2;

      var grad = Window.ctx.createRadialGradient(x + (width / 2), (fillPosY + height) / 2, 10, (fillPosX + width) / 2, (fillPosY + height) / 2, width);
      grad.addColorStop(0, '#0cd3c6');
       // grad.addColorStop(0.9, '#019F62');
       grad.addColorStop(0.7, '#0c7dd3');

      if(gradient == true)
      {
        Window.ctx.fillStyle = grad;
      }
      else {
        Window.ctx.fillStyle = color;
      }

      if(alpha)
      {
        Window.ctx.globalAlpha = alpha;
        Window.ctx.fillRect(fillPosX, fillPosY, width - border * 2, height - border * 2);
        Window.ctx.globalAlpha = 1;
      }
      else
      {
        Window.ctx.fillRect(fillPosX, fillPosY, width - border * 2, height - border * 2);
      }

      // Window.ctx.clearRect(borderWidth, borderWidth,  screenWidth-borderWidth * 2, screenHeight-borderWidth * 2);
    }
    else {
      Window.ctx.fillStyle = color;
      
      if(alpha)
      {
        Window.ctx.globalAlpha = alpha;
        Window.ctx.fillRect(x, y, width, height);
        Window.ctx.globalAlpha = 1;
      }
      else
      {
        Window.ctx.fillRect(x, y, width, height);
      }
    }
  },
  "rawDrawRectUi": function rawDrawRect(x, y, width, height, color, border = 0, borderColor, gradient) {
    if(border > 0 && borderColor !== undefined) {
      // color = borderColor
      Window.ctxUi.fillStyle = borderColor;
      Window.ctxUi.fillRect(x, y, width, height);

      var fillPosX = x + border;
      var fillPosY = y + border;
      var fillWidth = width - border * 2;

      var grad = Window.ctxUi.createRadialGradient(x + (width / 2), (fillPosY + height) / 2, 10, (fillPosX + width) / 2, (fillPosY + height) / 2, width);
      grad.addColorStop(0, '#0cd3c6');
       // grad.addColorStop(0.9, '#019F62');
       grad.addColorStop(0.7, '#0c7dd3');

      if(gradient == true)
      {
        Window.ctxUi.fillStyle = grad;
      }
      else {
        Window.ctxUi.fillStyle = color;
      }

      Window.ctxUi.fillRect(fillPosX, fillPosY, width - border * 2, height - border * 2);

      // Window.ctxUi.clearRect(borderWidth, borderWidth,  screenWidth-borderWidth * 2, screenHeight-borderWidth * 2);
    }
    else {
      Window.ctxUi.fillStyle = color;
      Window.ctxUi.fillRect(x, y, width, height);
    }
  },
  "rawDrawRectLine": function rawDrawRectLine(x, y, width, height, color, border = 2) 
  {
    // Window.ctx.fillStyle = color;
    // Window.ctx.fillRect(x, y, width, height);
    //Clear the center
    // Window.ctxUi.fillStyle = color;d
    // Window.ctxUi.clearRect(x + border, y + border, width - border * 2, height - border * 2);
    // Window.ctxUi.drawImage(Window.ctxUi.canvas, x + border, y + border, width, height, x + border, y + border, width, height);
    Window.ctxUi.strokeStyle = color;

    Window.ctxUi.beginPath();
    Window.ctxUi.moveTo(x, y);
    Window.ctxUi.lineTo(x + width, y);

    Window.ctxUi.moveTo(x + width, y);
    Window.ctxUi.lineTo(x + width, y + height);
    
    Window.ctxUi.moveTo(x + width, y + height);
    Window.ctxUi.lineTo(x, y + height);
    
    Window.ctxUi.moveTo(x, y + height);
    Window.ctxUi.lineTo(x, y);
    
    Window.ctxUi.stroke();
  },
  "drawSprite": DrawSprite,
  renderGrid: false,
  editorMode: false,
  logInput: false,

  "Window" : Window,
  "Scene" : Scene,
  "Input" : Input,
  "Collisions" : Collisions,
  "Physics" : Physics,
  "StateManager": StateManager,
  "Net":Net,
  "GetXY": function GetXY(i, col, row, w, h)
  {
    var x = 0;
    var y = 0;

    x = i % col
    x *= w;
    y = Math.floor(i / col);
    y *= h;

    // console.log("x:", x,"y:", y);
    return {"x":x, "y":y};
  },
  "showPalette":function()
  {
    return showPalette;
  },
  "drawGrid": DrawGrid,
  "rainbow" : {
    starting_color : 0,
    color_th : 0,
    color_change_time: 1
  },
  "muteSounds":muteSounds,
  "checkBounds":checkBounds,
  "checkBoundsRect":checkBoundsRects,
  "SaveMap": SaveMap,
  "Timer": Timer
}

//Add my global engine to the document ?
;(function () {
  global.GSE = GSEngine;
  
  //Require.js export
  try {
    define({ e:GSEngine, w:Window});
  }
  catch (err)
  {
    console.error("define Error:", err);
  }
})();

export {Window, Collisions, GSEngine, RenderingFunctions};
