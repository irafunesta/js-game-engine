//
// var State = {
// 	"update": function update() {
//
// 	},
// 	"render": function render() {
//
// 	},
// }

var StateManager = {
    "states":[],
    "stateQueue": [],
    "currentState": function currentState() {
        return this.stateQueue[this.stateQueue.length - 1];
    },
    "prevState": function prevState(name) {
        if(name != undefined)
        {
            var lastStateIndex = 0;
            for(var i= 0; i< this.stateQueue.length; i++) {
                var f = this.stateQueue[i];
                if(name == f.name)
                {
                    lastStateIndex = i;
                }
            }

            this.stateQueue.splice(lastStateIndex + 1); //remove all the element added after the status
        }
        else {
            this.stateQueue.pop();
        }

        console.log("stateQueue", this.stateQueue);
    },
    "pushState": function pushState(state) {
        this.stateQueue.push(state);
    },
    "createState": function CreateState(name, updateF, renderF, createF) {
        var s = Object.assign({}, {
            "name": name,
            "update": updateF,
            "render": renderF,
            "create": createF
        });

        this.states.push(s);
    },
    "nextState": function nextState(state) {
        if(typeof(state) == 'string') //Passed the name of the state
        {
            var s = null;
            for(var i= 0; i< this.states.length; i++) {
                var f = this.states[i];
                if(state == f.name)
                {
                    s = f;
                    break;
                }
            }

            this.pushState(s);
        }
        else if(typeof(state) == 'object' && state.name != undefined) { //Passed a object state TODO Not working now
            this.pushState(state);
        }

        console.log(this.stateQueue);
    },
    "setCurrentState": function setCurrentState(stateId = "") {
        for(var i= 0; i< this.states.length; i++)
        {
            var state = this.states[i];
            if(state.name == stateId)
            {
                //TODO fix this queue thing
                if(state.create != null && typeof(state.create) == 'function')
                {
                    state.create();
                }

                this.stateQueue = [];
                this.stateQueue.push (state);
                // console.log("stateQueue", this.stateQueue);
                return;
            }
        }
        console.log("State Id", stateId, "Not found");
    }
}

export {StateManager};
