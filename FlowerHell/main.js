MG = {
	mapW : 320, //4
	mapH : 480 //3
};

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, 320, 480, false, InitGame, Update, null, "black");
})();

function InitGame(ctx)
{
	var grid_size = 10;
	var col = 8;
	var row = 8;
	MG.score = 0;
	MG.inGame = true;
	// GSE.Scene.CreateRenderGrid(10, 10, MG.mapW / 10, MG.mapH / 10);
	// GSE.Scene.CreteRenderView(0,0,1024,768);

	GSE.Scene.LoadTexture("back", "./images/back.jpg");
	GSE.Scene.LoadTexture("gems", "./images/gem_set.png");
	GSE.Scene.LoadTexture("b_Restart", "./images/b_Restart.png");
	GSE.Scene.LoadTexture("b_Pause3", "./images/b_Pause3.png");
	

	var planetSpr = [];

	var backSpr = GSE.Scene.CreateSprite("back",
		null,
		0, //x
		0, //y
		800, //w
		600, //h
		2 //layer
	);

	planetSpr[0] = GSE.Scene.CreateAnimatedSprite("gems", -1, 0, 0, 32, 32, 1, true, 1, 1, 32, 32)
	
	var timeBar = GSE.Scene.CreateSprite("",
		'#999966',
		15, //x
		402, //y
		(320 - 32), //w
		16, //h
		2 //layer
	);

	var timeBarBack = GSE.Scene.CreateSprite("",
		'black',
		15, //x
		402, //y
		(320 - 32), //w
		16, //h
		2 //layer
	);

	//TestPlanets();
	//Made a grid of enemies
	// GenPlanets(planets, planetSpr, false);
	MG.color_gem = [];

	MG.color_gem[0] = {"col":1, "row":1, "color_id": 0}
	MG.color_gem[1] = {"col":3, "row":2, "color_id": 1}
	MG.color_gem[2] = {"col":5, "row":3, "color_id": 2}
	MG.color_gem[3] = {"col":1, "row":5, "color_id": 3}
	MG.color_gem[4] = {"col":1, "row":8, "color_id": 4}
	MG.color_gem[5] = {"col":6, "row":9, "color_id": 5}


	MG.grid = [];
	var ids = 0;

	// for(var y= 0; y< grid_size; y++)
	// {
	// 	// var column = [];
	// 	for(var x= 0; x< grid_size; x++)
	// 	{
	// 		var backCell = GSE.Scene.CreateEntity("backcell", backCellSpr, UpdateBackCell);
	// 		backCell.x = 200 + (x*32);
	// 		backCell.y = 32 + (y*32);
	// 	}
	// }

	GSE.Scene.CreateEntity("back", backSpr, null);
	GSE.Scene.CreateEntity("timeBarBack", timeBarBack, null);
	var tb = GSE.Scene.CreateEntity("timeBar", timeBar, timeBarUpdate);
	tb.totalWidth = tb.width;

  	MG.timeBonus = GSE.Scene.UI.DrawText("0", 158, 391,"white", "24", "center");
	MG.timeBonus.setActive(false);




	for(var y= 0; y< row; y++)
	{
		// var column = [];
		for(var x= 0; x< col; x++)
		{
			var spr = Math.round(GSE.randomRange(0, MG.color_gem.length - 1));

			var cell = GSE.Scene.CreateEntity(ids.toString(), planetSpr[0], UpdateCell);
			cell.x = 32 + (x*32);
			cell.y = 96 + (y*32);
			cell.color_id = spr;
			cell.col = MG.color_gem[spr].col;
			cell.row = MG.color_gem[spr].row;
			cell.startingX = cell.x;
			cell.startingY = cell.y;
			// cell.color_id = MG.color_gem[ci].color_id;


			ids++;

			MG.grid.push(cell);
		}
		// grid.push(column);
	}

	MG.particles = [];

	//Create gem effects
	for(var y= 0; y< 20; y++)
	{
		var spr = Math.round(GSE.randomRange(0, MG.color_gem.length - 1));

		var cell = GSE.Scene.CreateEntity("particle", planetSpr[0], UpdateParticle);
		cell.x = 0;
		cell.y = 0;
		//cell.color_id = spr;
		//cell.col = MG.color_gem[spr].col;
		//cell.row = MG.color_gem[spr].row;
		cell.setActive(false);

		MG.particles.push(cell);
	}

	// var testAnim = GSE.Scene.CreateEntity("test", testAnim, null);

	var mapState = GSE.StateManager.createState("Game", function mapUpdate()
	{
		if(GSE.Input.IsKeyPressed('Space'))
		{
			MG.inGame = false;
			GSE.StateManager.setCurrentState("Pause");
		}

	}, function Render(ctx) {
		// GSE.rawDrawText(GSE.Scene.renderView.x.toString(), 20, 20, 'white', '24', 'left', true);
		GSE.rawDrawText("HI", 70, 44, 'white', '20', 'left', true);
		GSE.rawDrawText(MG.bestScore.toString(), 158, 44, 'white', '24', 'center', true);
		GSE.rawDrawText(MG.score.toString(), 158, 64, 'white', '24', 'center', true);
		// GSE.rawDrawText(Math.round(MG.time).toString(), 20, 80, 'white', '24', 'left', true);
		// GSE.rawDrawText(Math.round(MG.scoreInterval).toString(), 20, 100, 'blue', '24', 'left', true);
		if(GSE.Window.fps && false)
		{
			GSE.rawDrawText(GSE.Window.fps.toString(), 20, 60, 'white', '24', 'left', true);
		}
		// GSE.rawDrawText(MG.highscore.toString(), GSE.Window.screenWidth / 2 + 128 + 64, 60, 'white', '20');
	});

	var gameOverState = GSE.StateManager.createState("GameOver", function mapUpdate()
	{
		// if(GSE.Input.IsKeyPressed('Space'))
		// {
		// 	MG.restartGame();
		// 	MG.RestartButton.setActive(false);
		// 	MG.RestartButtonBack.setActive(false);
		// }
	},
	function Render(ctx)
	{
		// GSE.rawDrawRect(GSE.Window.screenWidth / 2 - 158, GSE.Window.screenHeight / 2 - 120, 316, 200, '#333300');
		GSE.rawDrawText("GAME OVER", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 60, 'white', '42');
		// GSE.rawDrawText("SPACE TO RESTART", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2, 'white', '20');
		GSE.rawDrawText("SCORE", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 26, 'white', '20');
		GSE.rawDrawText(MG.score.toString(), GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 8, 'white', '20');
	});

	GSE.StateManager.createState("Pause", function mapUpdate()
	{
		// if(GSE.Input.IsKeyPressed('Space'))
		// {
		// 	MG.inGame = true;
		// 	GSE.StateManager.setCurrentState("Game");
		// }
	},
	function Render(ctx)
	{
		// GSE.rawDrawRect(GSE.Window.screenWidth / 2 - 158, GSE.Window.screenHeight / 2 - 120, 316, 200, '#333300');
		GSE.rawDrawText("PAUSE", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 48, 'white', '42');
		// GSE.rawDrawText("SPACE TO RESUME", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2, 'white', '20');
		// GSE.rawDrawText("SCORE", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 + 48, 'white', '20');
		// GSE.rawDrawText(MG.score.toString(), GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 + 68, 'white', '20');
		GSE.rawDrawText(MG.score.toString(), 158, 44, 'white', '24', 'center', true);
	});

	GSE.StateManager.setCurrentState("Game");

	// MG.highscore = localStorage.getItem("strInvHS") || 0;

	// GSE.Scene.setTileImage("./tilesets/Dungeon_A4.png");

	// GSE.Scene.map = [[0, 0, 1],[2, 3, 0],[10, 10, 9]];

	var restartBackSpr = GSE.Scene.CreateSprite("",
		'#333300',
		0, //x
		120, //y
		320, //w
		200, //h
		-2 //layer
	);

	MG.RestartButtonBack = GSE.Scene.CreateEntity("restart", restartBackSpr, null);
	MG.RestartButtonBack.setActive(false);

	var restartSpr = GSE.Scene.CreateSprite("b_Restart",
		null,
		GSE.Window.screenWidth / 2 - 16, //x
		248, //y
		32, //w
		32, //h
		-2 //layer
	);
	MG.RestartButton = GSE.Scene.CreateEntity("restart", restartSpr, clickRestart);
	MG.RestartButton.setActive(false);

	var pauseBackSpr = GSE.Scene.CreateSprite("",
		'#333300',
		0, //x
		120, //y
		320, //w
		200, //h
		-2 //layer
	);

	MG.pauseButtonBack = GSE.Scene.CreateEntity("restart", pauseBackSpr, null);
	MG.pauseButtonBack.setActive(false);

	var pauseSpr = GSE.Scene.CreateSprite("b_Pause3",
		'',
		278, //x
		17, //y
		32, //w
		32, //h
		-2 //layer
	);
	MG.pauseButton = GSE.Scene.CreateEntity("restart", pauseSpr, clickPause);
	// MG.pauseButton.setActive(false);

	// var resumeSpr = GSE.Scene.CreateSprite("./images/b_Restart.png",
	// 	'',
	// 	GSE.Window.screenWidth / 2 - 16, //x
	// 	218, //y
	// 	32, //w
	// 	32, //h
	// 	2 //layer
	// );
	MG.resumeButton = GSE.Scene.CreateEntity("resume", restartSpr, clickResume);
	MG.resumeButton.setActive(false);

	MG.seq = [];
	MG.seq_start = false;
	MG.particleUpSpeed = 3;
	MG.timeDecrease = 0.02;
	MG.oldScore = MG.score;
	MG.scoreInterval = 5000;
	MG.startingTime = 100;
	// MG.startingTime = 2;
	MG.time = MG.startingTime;
	MG.bestScore = localStorage.getItem("Flhs") || 0;
}

function clickRestart(self)
{
	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		self.setActive(false);
		MG.RestartButtonBack.setActive(false);
		MG.restartGame();
	}
}

function clickPause(self)
{
	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true &&
		MG.inGame == true)
	{
		self.setActive(false);
		MG.pauseButtonBack.setActive(true);
		MG.resumeButton.setActive(true);
		MG.inGame = false;
		GSE.StateManager.setCurrentState("Pause");
	}
}

function clickResume(self)
{
	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		self.setActive(false);
		MG.pauseButtonBack.setActive(false);
		MG.pauseButton.setActive(true);
		// MG.resumeButton.setActive(false);
		GSE.StateManager.setCurrentState("Game");
		MG.inGame = true;
	}
}

function timeBarUpdate(self)
{
	if(MG.inGame == false)
	{
		return;
	}

	self.width = (self.totalWidth / MG.startingTime) * MG.time;
}

function checkBounds(x1,y1, w1, h1, x2,y2,w2,h2) {
	if (x1 < x2 + w2 &&
		 x1 + w1 > x2 &&
		 y1 <y2 + h2 &&
		 h1 + y1 >y2) {
			// collision detected!
		return true;
	}

	return false;
}

function distance(x1, y1, x2, y2) {
	var dx = Math.pow((x2-x1),2);
	var dy = Math.pow((y2-y1),2);

	// console.log("dx:", dx, " dy:", dy);

	var dist = Math.sqrt(dx + dy);
	return Math.round(dist);
}

function HighlightGem(gem)
{
	gem.width = 40;
	gem.height = 40;
	gem.layer = 1;

	console.log("gem startingX:", gem.startingX);
	console.log("gem startingY:", gem.startingY);

	gem.x = gem.startingX - 6;
	gem.y = gem.startingY - 6;

	gem.in_seq = true;
}

function DeHighlightGem(gem)
{
	gem.width = 32;
	gem.height = 32;
	gem.layer = 1;
	gem.x = gem.startingX;
	gem.y = gem.startingY;
	gem.in_seq = false;
}

function UpdateBackCell(self) {
	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		self.color = "yellow";
	}
	else
	{
		self.color = "white";
	}
}

function UpdateCell(self) {

	if(MG.inGame == false)
	{
		return;
	}

	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		if(self.in_seq == true)
		{
			return;
		}

		if(MG.seq_start == false)
		{
			MG.seq_start = true;
			MG.seq.push(self);

			HighlightGem(self);
		}
		else
		{
			// debugger;
			if(MG.seq.length >= 1)
			{
				var checkDuplicate = MG.seq.find(function (gem){
					return gem.id == self.id;
				});

				var lastGem = MG.seq[MG.seq.length - 1]

				if(lastGem.color_id == self.color_id &&
					checkDuplicate == undefined &&
					distance(self.x, self.y, lastGem.startingX, lastGem.startingY) <= 49)
				{
					//Selezionato una gemma dello stesso colore
					console.log("Gemma stesso colore")
					MG.seq.push(self);
					HighlightGem(self);
				}
			}
		}
	}
}

MG.Wait = function Wait(code, delay) {
	var w = delay || 1000;
	setTimeout(code, w);
};

MG.restartGame = function ()
{
	MG.score = 0;
	MG.time = MG.startingTime;
	MG.timeDecrease = 0.02;
	MG.scoreInterval = 5000;
	// MG.scoreInterval = 5000;
	// MG.timeBeforeMove = 2;
	// MG.enemyDirection = 2; //0 - left, 1 - down, 2 - right
	// MG.prevEnemyDirection = 0; //0 - left, 1 - down, 2 - right

	// MG.mScount = 1;

	for (var r = 0; r < MG.particles.length; r++)
  {
 		var eshoot = MG.particles[r];
  	eshoot.setActive(false);
  }

	//Scrumble the board
	for (var r = 0; r < MG.grid.length; r++)
  {
 		var gem = MG.grid[r];
		var ci = Math.round(GSE.randomRange(0, MG.color_gem.length - 1));
		// var ci_color_id = MG.color_gem[ci].color_id;
		gem.col = MG.color_gem[ci].col;
		gem.row = MG.color_gem[ci].row;
		gem.color_id = MG.color_gem[ci].color_id;
  	// eshoot.setActive(true);
  }

	GSE.StateManager.setCurrentState("Game");
	MG.inGame = true;
}

MG.gameOver = function ()
{
	GSE.Scene.PlaySound('explosion');
	MG.inGame = false;
	if(MG.score > MG.highscore)
	{
		MG.highscore = MG.score
		localStorage.setItem("strInvHS", MG.score);
	}

	var motherShip = GSE.Scene.GetEntity('motherShip');
	if(motherShip.active == true)
	{
		motherShip.setActive(false);
		MG.mScount++;
	}

	var enemies = GSE.Scene.GetEntities('enemy');

	if(enemies && enemies.length > 0)
	{
		//Bullet Collisions
		for(var i = 0; i< enemies.length; i++)
		{
			var e = enemies[i];
			e.setActive(false);
		}
	}

	GSE.StateManager.setCurrentState("GameOver");
}

function Update(lastTick, timeSinceTick)
{
	var addScore = false;
	if(MG.inGame == false)
	{
		return;
	}

	// MG.oldScore = MG.score;

	if(MG.time < 0)
	{
		MG.inGame = false;
		GSE.StateManager.setCurrentState("GameOver");

		if(MG.score > MG.bestScore)
		{
			MG.bestScore = MG.score;
			localStorage.setItem("FLhs", MG.score);
		}

		MG.RestartButton.setActive(true);
		MG.RestartButtonBack.setActive(true);
	}

	if(GSE.Input.IsMouseLeftDown() != true)
	{
		//Sequence end
		MG.seq_start = false;

		MG.seq.forEach(function (gem){
			console.log("color:", gem.color_id);

			DeHighlightGem(gem);
			//Change gem color
			// debugger;
			if(MG.seq.length >= 3)
			{
				//Create a particle for each gem
				var p = MG.particles.find(function (p){
					return p.active == false;
				});

				if(p != undefined)
				{
					p.setActive(true);
					p.x = gem.x;
					p.y = gem.y;
					p.col = gem.col;
					p.row = gem.row;
					p.color_id = gem.color_id;
					p.animY = gem.y;
					MG.particleUpSpeed = 3;
				}

				var ci = Math.round(GSE.randomRange(0, MG.color_gem.length - 1));
				// var ci_color_id = MG.color_gem[ci].color_id;
				gem.col = MG.color_gem[ci].col;
				gem.row = MG.color_gem[ci].row;
				gem.color_id = MG.color_gem[ci].color_id;
			}
		});

		if(MG.seq.length >= 3)
		{
			var timeB = (0.5 * MG.seq.length) + Math.round(MG.seq.length / 3) * 0.1;
			// MG.oldScore = MG.score;
			MG.score += 100 * MG.seq.length;
			MG.time += timeB;
			MG.scoreInterval -= 100 * MG.seq.length;

			MG.timeBonus.setActive(true);
			MG.timeBonus.text = "+" + Math.round(timeB.toString());

			setTimeout(function(){
				MG.timeBonus.setActive(false);
			}, 500);
		}

		MG.seq.splice(0,MG.seq.length);
	}

	MG.time -= MG.timeDecrease;

	if(MG.scoreInterval <= 0)
	{
		MG.timeDecrease += 0.015;
		// MG.oldScore = MG.score;
		MG.scoreInterval = 5000;
		MG.time = MG.startingTime;
	}
}

function UpdateParticle(self)
{
	self.y -= MG.particleUpSpeed;

	if(self.y < 0)
	{
		self.setActive(false);
	}
	if(Math.abs(self.y - self.animY) >= 60)
	{
		MG.particleUpSpeed += 0.25;
	}
}
