MG = {
	mapW: 640,
	mapH: 480,
	maps: [],
	current_level : 0,
	current_room : 0,
	current_level_node: null,
	current_tile: 0,
	tile_w: 32,
	tile_h: 32,
	rooms: []
};

//Alias
const Scene = GSE.Scene;


function Room() {
	return {
		'id':0,
		'name':'room_0',
		'exits':{
			'u':-1,
			'd':-1,
			'l':-1,
			'r':-1
		},
		entities:[]
	}
}

let player = {
	ref: null,
	lives : 4,
	attack_time: 0.5,
	state : "move",
	last_direction : {x:0, y:1},
	anim :{
		frames : 3,
		current_frame : 0,
		frame_speed : 20,
		frame_time : 0,
		anim_run : false
	}
}

//Mapping
//Mapping of id to Entity or sprite
const MapEntity = {
	"0": (map_data, father) => { //Player
		//Create the player
		player.ref = Scene.AddRect({
			texture: "pl_right",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:0
		}, father.get_path());
	},
	"1": (map_data, father) => { //Walls
		//Create the wall
		walls.push(Scene.AddRect({
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			color: "grey",
			tile_id:1
		}, father.get_path()));
	},
	"2": (map_data, father) => { //Holes
		//Create the wall

		let hole = Hole(
			Scene.AddRect({
				x: MG.tile_w * map_data.pos.x,
				y: MG.tile_h * map_data.pos.y,
				w: MG.tile_w,
				h: MG.tile_h,
				color: "brown",
				tile_id:2
			}, father.get_path()),
			5
		)

		holes.push(hole);
	},
	"3": (map_data, father) => { //Player target
		//Create the player target
		player_target = Scene.AddSprite({
			texture: "player_target",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:3,
		}, father.get_path());

		player_target.order = -1;
	},
	"4": (map_data, father) => { //Boxes target
		//Create the box target
		let tmp = Scene.AddSprite({
			texture: "box_target",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:4
		}, father.get_path());
		tmp.order = -1;
		box_targets.push(tmp);
	},
	"5": (map_data, father) => { //Gate
		//Create a gate
		let tmp = Scene.AddSprite({
			texture: "gate",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:5
		}, father.get_path());
		gates.push(tmp);
	},
	"6": (map_data, father) => { //Keys
		//Create a key
		let tmp = Scene.AddSprite({
			texture: "key",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:6
		}, father.get_path());
		keys.push(tmp);
	},
}

const MapLoadEntity = {	
	"1": (entity, father) => { //Walls
		//Create the wall
		walls.push(Scene.AddRect(entity.ref.data, father.get_path()));
	},
	"2": (entity, father) => { //Holes
		//Create the wall

		let hole = Hole(
			Scene.AddRect(entity.ref.data, father.get_path()),
			entity.max_life
		)

		hole.max_life = entity.max_life;
		hole.life = entity.life;

		holes.push(hole);
	},
	"3": (map_data, father) => { //Player target
		//Create the player target
		player_target = Scene.AddSprite({
			texture: "player_target",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:3,
		}, father.get_path());

		player_target.order = -1;
	},
	"4": (map_data, father) => { //Boxes target
		//Create the box target
		let tmp = Scene.AddSprite({
			texture: "box_target",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:4
		}, father.get_path());
		tmp.order = -1;
		box_targets.push(tmp);
	},
	"5": (map_data, father) => { //Gate
		//Create a gate
		let tmp = Scene.AddSprite({
			texture: "gate",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:5
		}, father.get_path());
		gates.push(tmp);
	},
	"6": (map_data, father) => { //Keys
		//Create a key
		let tmp = Scene.AddSprite({
			texture: "key",
			x: MG.tile_w * map_data.pos.x,
			y: MG.tile_h * map_data.pos.y,
			w: MG.tile_w,
			h: MG.tile_h,
			tile_id:6
		}, father.get_path());
		keys.push(tmp);
	},
}

const MapEditorAdd = {
	"0": "Player",
	"1": "Wall",
	"2": "Holes",
	"3": "Target_Player",
	"4": "Target_Box",
	"5": "Gate",
	"6": "Key"
}

const MapEditorRemove = {
	"0": () => {},
	"1": (x, y) => { //Wall
		let id = walls.findIndex((wall) => {
			return wall.data.x === x && wall.data.y === y;
		})

		if(id >= 0) {
			walls.splice(id, 1);
		}
	},
	"2": (x, y) => { //Holes
		let id = holes.findIndex((box) => {
			return box.data.x === x && box.data.y === y;
		})

		if(id >= 0) {
			holes.splice(id, 1);
		}
	},
	"3": () => {},
	"4": (x, y) => { //Targets_boxes
		let id = box_targets.findIndex((target) => {
			return target.data.x === x && target.data.y === y;
		})

		if(id >= 0) {
			box_targets.splice(id, 1);
		}
	},
	"5": (x, y) => { //Gates
		let id = gates.findIndex((gate) => {
			return gate.data.x === x && gate.data.y === y;
		})

		if(id >= 0) {
			gates.splice(id, 1);
		}
	},
	"6": (x, y) => { //Gates
		let id = keys.findIndex((key) => {
			return key.data.x === x && key.data.y === y;
		})

		if(id >= 0) {
			keys.splice(id, 1);
		}
	},
}

const MapEditorDraw = {
	"0": (x, y) => { //player
		Scene.DrawRect({
			texture: "pl_right",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h,
			color:"white"
		})
	},
	"1": (x, y) => { //"Wall",
		Scene.DrawRect({
			color: "grey",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	"2":  (x, y) => { //"Holes",
		Scene.DrawRect({
			color: "brown",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	"3": (x, y) => { //"Target Player",
		Scene.DrawSprite({
			texture: "player_target",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	}, 
	"4": (x, y) => { //"Target Box",
		Scene.DrawSprite({
			texture: "box_target",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	"5": (x, y) => { //"Gate",
		Scene.DrawSprite({
			texture: "gate",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
	"6": (x, y) => { //"Key",
		Scene.DrawSprite({
			texture: "key",
			x: x,
			y: y,
			w: MG.tile_w,
			h: MG.tile_h
		})
	},
}


let wall, spike, sword;
let player_speed = 128;
let player_lives = 4;

let x_diff = 0;
let y_diff = 0;

let walls = [];
let holes = [];
let enemies = [];
let skip_input = false;
let timer = 0;

function Hole(sprite, life)
{
	return {
		'ref': sprite,
		'life': life,
		'max_life': life
	}
}

// frames = 3;
// current_frame = 0;
// frame_speed = 20;
// frame_time = 0;
// anim_run = false;

function RenderGame(delta)
{
	//animate player ?
	if(player.anim.anim_run) {
		if(player.anim.frame_time > player.anim.frame_speed)
		{
			player.anim.current_frame += 1;
			player.anim.current_frame %= player.anim.frames;
			player.anim.frame_time = 0;
		}

		player.anim.frame_time += 1*delta;

		player.ref.data.sx = player.anim.current_frame * MG.tile_w;
	}
	else
	{
		player.ref.data.sx = 0;
	}

	Scene.DrawText({
		x:16, 
		y:20,
		color:"violet",
		align:"left",
		font: "Serif",
		size: "20",
		text: player.anim.current_frame.toString()
	});

	// for(let i =0; i< game_commands.length; i++) {
	// 	Scene.DrawText({
	// 		x:16, 
	// 		y:40 + ((16 + 8) *i),
	// 		color:"violet",
	// 		align:"left",
	// 		font: "Serif",
	// 		size: "20",
	// 		text: game_commands[i] 
	// 	});
	// }
}

function InitGame(ctx)
{
	Scene.useRoot(true);

	//Fills maps
	MG.maps = Object.keys(levels);

	//Create level from map

	GSE.Scene.LoadTexture("player_walk", "./tilesets/player_walk.png");

	//TODO manage states
	GSE.StateManager.createState("Game", UpdateGame, RenderGame);
	GSE.StateManager.createState("Editor", UpdateEditor, RenderEditor);

	GSE.StateManager.setCurrentState("Game");

	//Ci sarà poi la Scritta e uscita solo a destra
	let start_room = new Room();
	start_room.exits.r = 1;
	start_room.name = "start";

	// start_room.entities.push({
	// 	"id":0,
	// 	"pos":{
	// 		"x":8,
	// 		"y":5
	// 	}
	// })

	MG.rooms.push(start_room);

	GenerateRooms();
	console.log(MG.rooms);

	player.ref = Scene.AddSpriteExt({
		texture: "player_walk",
		x: MG.tile_w * 10,
		y: MG.tile_h * 5,
		w: MG.tile_w,
		h: MG.tile_h,
		sx:0, sy:0, sw:32, sh:32,
		tile_id:0,
		id:"player"
	});

	LoadRoom(MG.current_room);
}

function ReverseExit(exit)
{
	switch(exit)
	{
		case 'r': return 'l';
		case 'u': return 'd';
		case 'd': return 'u';
		case 'l': return 'r';
	}
}

function MapDirection(dir)
{
	switch(dir)
	{
		case 0: return 'u';
		case 1: return 'd';
		case 2: return 'l';
		case 3: return 'r';
	}
}

function GenerateRooms() {

	let max_rooms = 4;
	let last_exit = 'r';
	//Inizia dalla stanza 1 e crea room collegate nelle 4 direzioni
	for(let i =1; i< max_rooms; i++)
	{
		let room = new Room();
		room.id = i;
		room.name = 'room_' + i;
		if(i == 1) //Prima stanza, deve essere collegata a start room
		{
			room.exits[ReverseExit(last_exit)] = 0;
		}
		else
		{
			
			room.exits[ReverseExit(last_exit)] = i-1;
		}

		//aggiungi un numero random di holes
		let num_holes = Math.round(GSE.randomRange(0, 3));
		for(let h=0; h<num_holes; h++)
		{
			let x = Math.round(GSE.randomRange(0, (MG.mapW / MG.tile_w)))
			let y = Math.round(GSE.randomRange(0, (MG.mapH / MG.tile_h)))
			//TODO correctly add an entity;
			room.entities.push({
				"id":2,
				"pos":{
					"x":x,
					"y":y
				}
			});
		}

		//seleziona una direzione


		let direction = Math.round(GSE.randomRange(0, 3));
		while(room.exits[MapDirection(direction)] != -1)
		{
			direction = Math.round(GSE.randomRange(0, 3));
		}
		
		last_exit = MapDirection(direction);
		if((i+1) < max_rooms)
		{
			room.exits[last_exit] = i+1;
		}		
		MG.rooms.push(room);
	}
}

const game_commands = [
	"Frecce/WASD = Routa personaggio",
	"Spazio = Spara",
	"R = Ricarica livello"
]

function AdjustPLayer(dir)
{
	let x = 0;
	let y = 0;
	switch(dir)
	{
		case 'd':  //Down
			x = player.ref.data.x;
			y = 1 * MG.tile_h;
			break;
		case 'u': 
			x = player.ref.data.x;
			y = ((MG.mapH / MG.tile_h) - 1) * MG.tile_h;
			break;

		case 'r':  //Right
			x = 1 * MG.tile_w;
			y = player.ref.data.y;
			break;
		case 'l': //Left
			x = ((MG.mapH / MG.tile_w) - 1) * MG.tile_w;
			y = player.ref.data.y;
			break;
		// case 'd': return 'u';
		// case 'l': return 'r';
	}
	return {
		x: x,
		y: y
	}
}

let room_state = {}

function SavePrevState()
{
	//Save prev state
	let current_room = MG.rooms[MG.current_room];
	room_state[current_room.name] = {
		"walls" : walls,
		"holes" : holes
	}
}

function LoadRoom(id, current_level_node, dir)
{
	let map_to_load = MG.rooms[id];

	console.log("Load room:", map_to_load.id);

	//Manage player position
	if(player.ref && dir) {
		let pos = AdjustPLayer(dir);
		player.ref.data.x = pos.x;
		player.ref.data.y = pos.y;
	}

	//Clear all arrays
	walls = [];
	holes = [];
	box_targets = [];
	gates = [];
	keys = [];

	//Restore previous state, if present
	let prev_room_state = room_state[map_to_load.name];
	if(prev_room_state)
	{
		console.log("Restore prev state of room:", map_to_load.name);
		// walls = prev_room_state.walls;
		// holes = prev_room_state.holes;

		//Now i need to recreate the Scene Node father and add , all the refernce whit the data
		if(current_level_node) {
			//clean current level
			Scene.RemoveNode(current_level_node.get_path());
		}
	
		MG.current_level_node = Scene.CreateNode({ id: map_to_load.name });
		Scene.root.add_child(MG.current_level_node);

		//Restore walls
		for(let wall of prev_room_state.walls)
		{
			/*
				ref : 
				life:
				max_life
			*/
			if(MapLoadEntity[wall.ref.data.tile_id])
			{
				MapLoadEntity[wall.ref.data.tile_id](wall, MG.current_level_node);
			}

		}
		for(let hole of prev_room_state.holes)
		{
			/*
				ref : 
				life:
				max_life
			*/
			if(MapLoadEntity[hole.ref.data.tile_id])
			{
				MapLoadEntity[hole.ref.data.tile_id](hole, MG.current_level_node);
			}

		}

	}
	else
	{
		//LOAD NEW
		if(current_level_node) {
			//clean current level
			Scene.RemoveNode(current_level_node.get_path());
		}

		MG.current_level_node = Scene.CreateNode({ id: map_to_load.name });
		Scene.root.add_child(MG.current_level_node);

		for (let entity of map_to_load.entities) {
			//call the correct creation fun
			if (MapEntity[entity.id]) {
				MapEntity[entity.id](entity, MG.current_level_node);
			}
		}
	}
}

function LoadLevel(id, current_level_node)
{
	let map_to_load = MG.maps[id];

	console.log("Load map:", map_to_load);

	//Clear all arrays
	walls = [];
	holes = [];
	box_targets = [];
	gates = [];
	keys = [];

	player.dir = {x:1, y:0};

	if(current_level_node) {
		//clean current level
		Scene.RemoveNode(current_level_node.get_path());
	}

	MG.current_level_node = Scene.CreateNode({ id: map_to_load });
	Scene.root.add_child(MG.current_level_node);

	let level = levels[map_to_load];
	for (let entity of level.entities) {
		//call the correct creation fun
		if (MapEntity[entity.id]) {
			MapEntity[entity.id](entity, MG.current_level_node);
		}
	}
}

function Update(lastTick, delta)
{
	
}

function UpdateGame(lastTick, delta)
{
	//Switch to editor
	if (GSE.Input.IsKeyPressed("p")) {
		console.log("Change state to Editor");
		//Reload the level to fix the walls and boxes array
		// LoadLevel(MG.current_level, MG.current_level_node);
		GSE.StateManager.setCurrentState("Editor");
	}

	let pl_direction = {x:0, y:0};
	
	if(GSE.Input.IsKeyDown("d") || GSE.Input.IsKeyDown("right")) {
		pl_direction.x = 1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
	}
	else if(GSE.Input.IsKeyDown("a") || GSE.Input.IsKeyDown("left")) {
		pl_direction.x = -1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
	}
	else if(GSE.Input.IsKeyDown("w") || GSE.Input.IsKeyDown("up")) {
		pl_direction.y = -1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
	}
	else if(GSE.Input.IsKeyDown("s") || GSE.Input.IsKeyDown("down")) {
		pl_direction.y = 1;
		player.last_direction = pl_direction;
		player.anim.anim_run = true;
	}
	else
	{
		player.anim.anim_run = false;
	}
	
	if(GSE.Input.IsKeyDown("enter") || GSE.Input.IsKeyDown("space")) 
	{
		player.state = "digging"
	}
	else
	{
		player.state = "move"
	}

	if (player.state === "move") {
		player.ref.data.x += pl_direction.x * player_speed * delta;
		player.ref.data.y += pl_direction.y * player_speed * delta;

		let exit = '';
		//In case out of screen, move to a new room
		//exit R
		if(player.ref.data.x + player.ref.data.w > MG.mapW)
		{
			exit = 'r';
		}
		else if(player.ref.data.x < 0) //exit l
		{
			exit = 'l';
		}
		else if(player.ref.data.y + player.ref.data.h > MG.mapH)
		{
			exit = 'd';
		}
		else if(player.ref.data.y < 0)
		{
			exit = 'u';
		}

		if(exit)
		{
			let room = MG.rooms[MG.current_room];
			let exit_id = room.exits[exit];
			if(exit_id > -1) {
				console.log("load room at exit:", exit, " id:", exit_id);
				player.state = "load"
				SavePrevState();
				LoadRoom(exit_id, MG.current_level_node, exit);
				MG.current_room = exit_id;
			}
			return;
		}
	}

	//move player out of wall ?
	for(let wall of walls) {
		if(GSE.Collisions.CheckCollision(player.ref.data, wall.data)) {
			let bounce = BounceFromWall(player.ref, wall);

			player.ref.data.x += bounce.x;
			player.ref.data.y += bounce.y;
		}
	}

	//Hit a spike
	// if(GSE.Collisions.CheckCollision(spike.data, player.ref.data)) {
	// 	let bounce = BounceFromWall(player.ref, spike, 32);
	// 	player.ref.data.x += bounce.x;
	// 	player.ref.data.y += bounce.y;
	// }
	// shadow.data.hidden = true;

	// if(sword.data.hidden == false)
	// {
	// 	if(timer >= player.attack_time) {
	// 		timer = 0;
	// 		sword.data.hidden = true;
	// 		player.state = "move"
	// 	}
	// 	timer += 1* delta;
	// }

	//Enemies will track my sword
	for(let hole of holes) {

		//Scale dirt based on life
		let scale =  (hole.life / hole.max_life);
		// console.log("scale:", scale);		
		hole.ref.data.h = MG.tile_h * scale;


		if(GSE.Collisions.CheckCollision(player.ref.data, hole.ref.data)) {
			//console.log("collision player if digging, dig");		
			// let bounce = BounceFromWall(player.ref, enemy, 20);
			// player.ref.data.x += bounce.x;
			// player.ref.data.y += bounce.y;
			if(player.state === "digging")
			{
				hole.life -= 1 * delta;

				if(hole.life <= 1)
				{
					hole.life = 0;
					hole.ref.data.hidden = true;
				}
			}
			break;
		}
	}
}

function UpdateEditor(delta)
{
	if (GSE.Input.IsKeyPressed("p")) {
		console.log("Change state to Game");
		//Reload the level to fix the walls and boxes array
		// LoadLevel(MG.current_level, MG.current_level_node);
		GSE.StateManager.setCurrentState("Game");
	}

	if (GSE.Input.IsKeyPressed("n")) {
		console.log("Load next level");
		MG.current_level += 1;
		MG.current_level %= MG.maps.length;

		console.log("MG.current_level:", MG.current_level);
		console.log("level name:", MG.maps[MG.current_level]);
		
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	if(GSE.Input.IsMouseLeftPressed())
	{
		let x, y;
		x = Math.floor(GSE.Input.mouse.clientX / MG.tile_w);
		y = Math.floor(GSE.Input.mouse.clientY / MG.tile_h);

		if(x >= 0 && x <= MG.mapW / MG.tile_w && y >= 0 && y <= MG.mapH / MG.tile_h )
		{
			
			if(!MG.open_palette)
			{
				//Place the selected item
				let is_cell_full = levels[MG.maps[MG.current_level]].entities.findIndex((ent) => {
					return x === ent.pos.x && y === ent.pos.y;
				})

				console.log("is_cell_full:", is_cell_full);

				if(is_cell_full < 0) {
					console.log("place a box")
					//Place a Box in the level
					let entity = {
						"id":MG.current_tile,
						"pos":{
							"x":x,
							"y":y
						}
					}
					console.log("entity:", entity);

					levels[MG.maps[MG.current_level]].entities.push(entity);
					MapEntity[entity.id](entity, MG.current_level_node);
				}
			}
			else
			{
				//Select the item
				MG.current_tile = MG.palette_sel_tile;
				//Close the palette ?
				MG.open_palette = false;
			}
		}
	}

	if(GSE.Input.IsKeyPressed("q"))
	{
		MG.open_palette = !MG.open_palette;
	}

	if(GSE.Input.IsKeyPressed("d"))
	{
		let x, y;
		x = Math.floor(GSE.Input.mouse.clientX / MG.tile_w);
		y = Math.floor(GSE.Input.mouse.clientY / MG.tile_h);

		if(x >= 0 && x <= MG.mapW / MG.tile_w && y >= 0 && y <= MG.mapH / MG.tile_h )
		{
			console.log("remove a tile")
			console.log("MG.current_level_node:", MG.current_level_node);

			//Find the node
			let node_selected = MG.current_level_node.child.find((node) => {

				let full_x = x*32, full_y = y * 32;

				return full_x === node.data.x && full_y === node.data.y;
			});

			if(node_selected) 
			{
				console.log("Tile found:", node_selected.data);
				let index = levels[MG.maps[MG.current_level]].entities.findIndex((ent) => {

					return x === ent.pos.x && y === ent.pos.y;
				});

				console.log("index found:", index);	

				//remove node on level
				if(index >= 0) {

					//If it is a box, a wall or a box_target also remove it from the relative list
					
					MapEditorRemove[node_selected.data.tile_id](node_selected.data.x, node_selected.data.y);
					levels[MG.maps[MG.current_level]].entities.splice(index, 1);
					Scene.RemoveNode(node_selected.get_path());
				}
			}
		}
	}

	//Create a new level
	if(GSE.Input.IsKeyPressed("u"))
	{
		let new_name = prompt("Nome del nuovo livello ?", "nuovo");
		let new_id = MG.maps.push(new_name);
		
		levels[new_name] = {
			"entities":[]
		}
		//TODO this doesn't load the new level
		LoadLevel(new_id-1, MG.current_level_node);
	}

	//Save the map
	if(GSE.Input.IsKeyPressed("s"))
	{
		console.log("save map ?");
		let mapData = ["const levels =" + JSON.stringify(levels) + ";"];
		GSE.SaveMap("levels.js", mapData);
	}

	//Reload the map
	if(GSE.Input.IsKeyPressed("r"))
	{
		console.log("reload map");
		console.log("MG.current_level:", MG.current_level);
		
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Slide all the map up
	if(GSE.Input.IsKeyPressed("up"))
	{
		console.log("transalte up");
		console.log("MG.current_level:", MG.current_level);

		SlideLevel({x:0, y:-1});
		LoadLevel(MG.current_level, MG.current_level_node);
	}
	
	//Slide all the map right
	if(GSE.Input.IsKeyPressed("right"))
	{
		console.log("transalte right");

		SlideLevel({x:1, y:0});
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Slide all the map left
	if(GSE.Input.IsKeyPressed("left"))
	{
		console.log("transalte left");

		SlideLevel({x:-1, y:0});
		LoadLevel(MG.current_level, MG.current_level_node);
	}

	//Slide all the map down
	if(GSE.Input.IsKeyPressed("down"))
	{
		SlideLevel({x:0, y:1});	
		LoadLevel(MG.current_level, MG.current_level_node);
	}
}

function BounceFromWall(player, wall, multiplier = 1)
{
	x_diff = 0;
	y_diff = 0;

	//fix X 
	//Case 1 if Px < Wx
	if(player.data.x < wall.data.x) {
		x_diff = (player.data.x + player.data.w) - wall.data.x;
		x_diff *= -1;
	}
	else if(player.data.x > wall.data.x) {
		x_diff = (wall.data.x + wall.data.w) - player.data.x;
	}
	//fix Y
	//Player over
	if(player.data.y < wall.data.y) {
		y_diff = (player.data.y + player.data.w) - wall.data.y;
		y_diff *= -1;		
	}
	else if(player.data.y > wall.data.y) {
		y_diff = (wall.data.y + wall.data.w) - player.data.y;		
	}

	let x_diff_abs = Math.abs(x_diff);
	let y_diff_abs = Math.abs(y_diff);

	let apply_x = 0;
	let apply_y = 0;

	let amount = 0;

	//extract x_diff
	if(x_diff_abs > 0 && y_diff_abs > 0) { //Both not 0 , use the lower
		if( x_diff_abs < y_diff_abs) {
			apply_x = 1;
			amount = x_diff;
		}
		else {
			apply_y = 1;
			amount = y_diff;
		}
	}
	else if(x_diff_abs > 0 && y_diff_abs == 0) { //Apply X
		apply_x = 1;
		amount = x_diff;
	}
	else if(y_diff_abs > 0 && x_diff_abs == 0) { //Apply Y
		apply_y = 1;
		amount = y_diff;
	}

	console.log("collision amount:", amount);
	console.log("collision multiplier:", multiplier);

	//I want a consistent bounce 
	// The overlap need to be different, 

	//Align to rect
	// player.data.x += amount * apply_x
	// player.data.y += amount * apply_y

	//Bounce the amount of pixel
	// if(multiplier > 1) {
	// 	player.data.x += multiplier * Math.sign(amount) * apply_x;
	// 	player.data.y += multiplier * Math.sign(amount) * apply_y;
	// }

	let x_val = amount * apply_x * multiplier;
	let y_val = amount * apply_y * multiplier;

	if(multiplier > 1) {
		x_val += multiplier * Math.sign(amount) * apply_x
		y_val += multiplier * Math.sign(amount) * apply_y
	}

	return {
		x: x_val,
		y: y_val
	}
}

const editor_commands = [
	"p:Exit",
	"u:New level",
	"n:cycle level",
	"q:Palette",
	"r:reset",
	"s:save all"
]

function RenderEditor(delta)
{
	GSE.drawGrid(MG.tile_w, MG.tile_h, "green", false)

	let x, y;
		x = Math.floor(GSE.Input.mouse.clientX / MG.tile_w);
		y = Math.floor(GSE.Input.mouse.clientY / MG.tile_h);
	let string = x + ", " + y;
	let string2 = GSE.Input.mouse.clientX + ", " + GSE.Input.mouse.clientY;

	let mouse_rect = Rect(GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, MG.tile_w, MG.tile_h);

	Scene.DrawText({
        x:GSE.Input.mouse.clientX, 
        y:GSE.Input.mouse.clientY,
        color:"white",
        align:"left",
        font: "Serif",
        size: "12",
		text: string,
		id:"Coords"
    });

	//Draw level name
	Scene.DrawText({
		x:MG.mapW / 2, 
		y:32,
		color:"white",
		align:"center",
		font: "Serif",
		size: "16",
		text: MG.maps[MG.current_level]
	});

	Scene.DrawText({
		x:16, 
		y:16,
		color:"white",
		align:"left",
		font: "Serif",
		size: "16",
		text: "Commands:"
	});

	for(let i =0; i< editor_commands.length; i++) {
		Scene.DrawText({
			x:16, 
			y:32 + (16*i),
			color:"white",
			align:"left",
			font: "Serif",
			size: "16",
			text: editor_commands[i] 
		});
	}
	
	// Scene.DrawText({
    //     x:GSE.Input.mouse.clientX, 
    //     y:GSE.Input.mouse.clientY + 16,
    //     color:"white",
    //     align:"left",
    //     font: "Serif",
    //     size: "12",
	// 	text: MapEditorAdd[MG.current_tile]
    // });

	if(MG.open_palette) 
	{
		let start_x = 64 + 32, start_y = 320, i=0;

		//Palette background
		Scene.DrawRect({
			color: "Black",
			x: start_x - 32,
			y: start_y - 32,
			w: MG.tile_w * 15,
			h: MG.tile_h * 3
		})

		for(let key of Object.keys(MapEditorAdd)) 
		{
			let object_rect = Rect(start_x + (i*64), start_y, MG.tile_w, MG.tile_h);
			if(GSE.Collisions.CheckCollision(mouse_rect, object_rect))
			{
				Scene.DrawRect({
					color: "yellow",
					x: (start_x + (i*64)) - 4,
					y: start_y - 4,
					w: MG.tile_w + 8,
					h: MG.tile_h + 8
				})

				MG.palette_sel_tile = Number(key);
			}

			MapEditorDraw[key](start_x + (i*64),start_y);
			i++;
		}
	}
}

function Render(delta)
{
	// Scene.DrawText({
	// 	x: 10, 
	// 	y: 10,
	// 	color:"white",
	// 	text: player.ref.data.x
	// })

	// Scene.DrawText({
	// 	x: 10, 
	// 	y: 64,
	// 	color:"red",
	// 	size: "32",
	// 	text: player_lives
	// })

	// Scene.DrawText({
	// 	x: 10, 
	// 	y: 84,
	// 	color:"white",
	// 	size: "20",
	// 	text: x_diff.toString(3)
	// })

	// Scene.DrawText({
	// 	x: 10, 
	// 	y: 104,
	// 	color:"white",
	// 	size: "20",
	// 	text: y_diff.toString(3)
	// })

	for(let enemy of enemies) {
		Scene.DrawText({
			x: enemy.ref.data.x, 
			y: enemy.ref.data.y,
			color:"white",
			size: "32",
			text: enemy.life + "/" + enemy.max_life
		})
	}

	Scene.DrawText({
		x: 32, 
		y: 32,
		color:"white",
		size: "20",
		text: MG.rooms[MG.current_room].id.toString()
	});
}

function SlideLevel(dir) {
	let map_to_load = MG.maps[MG.current_level];

	console.log("Load map:", map_to_load);

	let level = levels[map_to_load];
	for (let entity of level.entities) {
		//Move everything up
		entity.pos.x += dir.x;
		entity.pos.y += dir.y;
	}
}

function Rect(x, y, w, h)
{
	return {
		x:x, y:y, w:w, h:h}
}

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "#073319");
})();