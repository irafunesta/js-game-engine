MG = {};

;(function () {
	GSE.Init("myCanvas", 800, 600, 800, 600, false, InitGame, Update);
})();

function handleUpdatePlayer(self)
{
	// console.log(MG.text); //Cause MG is global ?
	if(MG.inGame == false)
	{
		return;
	}

	var currentState = GSE.StateManager.currentState();
	if(currentState.name != "Game") {
		return;
	}

	if(GSE.Input.IsKeyDown('left') == true && self.x > 0)
	{
		self.x -= MG.tankSpeed;
	}

	if(GSE.Input.IsKeyDown('right') == true && self.x < GSE.Window.screenWidth - 32)
	{
		self.x += MG.tankSpeed;
	}

	if(GSE.Input.IsKeyPressed('space') == true)
	{
		var aShoot = GSE.Scene.GetEntity('shoot');
		if(aShoot.active == false)
		{
			aShoot.x = self.x + 16;
			aShoot.y = self.y + 6;
			GSE.Scene.SetActive('shoot', true);
			GSE.Scene.PlaySound('laser');
		}
	}
}

function shootBehavior(self)
{
	//Check out of bounds
	if(self.y < 0)
	{
		//Out of bounds
		// GSE.Scene.SetActive(self.id, false);
		self.setActive(false);
	}
	else {
		self.y -= MG.shoot_speed;
	}
}

function eShootBehavior(self)
{
	//Check out of bounds
	if(self.y > GSE.Window.screenHeight)
	{
		//Out of bounds
		// GSE.Scene.SetActive(self.id, false);
		self.setActive(false);
	}
	else {
		self.y += MG.e_shoot_speed;
	}
}

function EnemyMove(self)
{
	if(MG.inGame == false)
	{
		return;
	}

	var currentState = GSE.StateManager.currentState();
	if(currentState.name != "Game") {
		return;
	}

	if(self.active != true)
	{
		self.totMove = 0;
	//	return;
	}

	if(self.totMove != undefined)
	{
		self.totMove += 1
	}
	else {
		self.totMove = 0;
	}

	if(self.totMove >= MG.timeBeforeMove)
	{
		//Go in the current direction
		switch(MG.enemyDirection)
		{
			case 0:
			{ //left
				if(self.x - MG.enemyMovement >= 0)
				{
					self.x -= MG.enemyMovement;
				}

				self.totMove = 0;
				break;
			}
			case 2:
			{ //Right
				if(self.x + MG.enemyMovement <= GSE.Window.screenWidth)
				{
					self.x += MG.enemyMovement;
				}

				self.totMove = 0;
				break;
			}
		}
	}

	if(self.shoot_step == undefined)
	{
		self.shoot_step = GSE.randomRange(MG.enemy_min_perc,MG.enemy_max_perc);
	}

	if(self.shoot_step > 0)
	{
		self.shoot_step--;
	}
	else
	{
		var can_shoot = MG.enemy_kill > MG.col || MG.totalEnemies < 5;
		can_shoot = true;
		if(GSE.randomRange(0,20) > MG.enemy_can_shoot && can_shoot)
		{
			// var eShoot = GSE.Scene.GetEntity("eShoot");
			// var tank = GSE.Scene.GetEntity("tank");
			for (var r = 0; r < MG.shootPool.length; r++)
			{
				var eShoot = MG.shootPool[r];
				if(eShoot.active == false)
				{
					//shoot
					eShoot.x = self.x + 16;
					eShoot.y = self.y + 16;
					eShoot.setActive(true);
					MG.enemy_kill = 5;
					break;
				}
			}
		}
		self.shoot_step = GSE.randomRange(MG.enemy_min_perc,MG.enemy_max_perc);
	}
}

function motherShipUpdate (self)
{
	if(MG.inGame == false)
	{
		return;
	}

	var currentState = GSE.StateManager.currentState();
	if(currentState.name != "Game") {
		return;
	}

	if(self.count == undefined)
	{
		self.count = 1
	}

	if(self.x < GSE.Window.screenWidth)
	{
		self.x += MG.motherShipSpeed;
	}
	else
	{
		self.x = 0
		//self.y = 92
		MG.mScount++;
		self.setActive(false);
		GSE.Scene.StopSound('mother');
	}
}

function InitGame(ctx)
{
	MG.enemyDirection = 2; //0 - left, 1 - down, 2 - right
	MG.prevEnemyDirection = 0; //0 - left, 1 - down, 2 - right
	MG.row = 5;
	MG.col = 11;
	MG.tankSpeed = 8;
	MG.shoot_speed = 12;
	MG.e_shoot_speed = 12;
	MG.inGame = true;
	MG.score = 0;
	MG.timeBeforeMove = 32;
	MG.enemyMovement = 8;
	MG.enemyDownMovement = 32;
	MG.totalEnemies = MG.col * MG.row;
	MG.speedIncrement = MG.timeBeforeMove / (MG.totalEnemies);
	MG.enemyCanMoveDown = false;
	MG.totalTankLives = 3;
	MG.tankLives = MG.totalTankLives;
	MG.enemy_can_shoot = 15;
	MG.enemy_min_perc = 220; //Minimum time before shooting
	MG.enemy_max_perc = 420; //Max time before shoot
	MG.enemy_kill = 0;
	MG.active_enemies = MG.totalEnemies;
	MG.mScount = 1
	MG.motherShipSpeed = 3;
	MG.motherShipScore = 1000;
	MG.motherShipScoreAdj = 500;
	MG.highscore = 0;
	MG.maxShootPool = 2;
	MG.shootPool = [];

	// GSE.Net.createRequest("http://localhost:5000/jsonTest");

	GSE.Scene.LoadTexture("tank", "./images/tank.png");
	GSE.Scene.LoadTexture("alien1", "./images/alien1.png");
	GSE.Scene.LoadTexture("alien2", "./images/alien2.png");
	GSE.Scene.LoadTexture("alien3", "./images/alien3.png");
	GSE.Scene.LoadTexture("mothership", "./images/mothership.png");
	GSE.Scene.LoadTexture("tank_explode", "./images/tank_explode.png");

	// var tank = "./images/tank.png";
	// var laser_sound =

	GSE.Scene.CreateSound('laser', "./sound/laser01.wav");
	GSE.Scene.CreateSound('hit', './sound/hurt00.wav');
	GSE.Scene.CreateSound('mother', './sound/powerup05.wav', true);
	GSE.Scene.CreateSound('explosion', './sound/explosion00.wav', true);
	//Sound from Pack: 8Bit Video Games by sharesynth
	//A way to create the Scene, and add object to it for drawing on the window
	// var id = "Player";

	var tankSpr = GSE.Scene.CreateSprite("tank",
		null,
		32, //x
		GSE.Window.screenHeight - 64, //y
		32, //w
		32, //h
		1 //layer
	);

	var shootSpr = GSE.Scene.CreateSprite("",
		'white',
		48,
		GSE.Window.screenHeight - 92,
		4,
		16,
		2
	);

	var shootEneSpr = GSE.Scene.CreateSprite("",
		'blue',
		48,
		GSE.Window.screenHeight - 92,
		4,
		16,
		2
	);

	MG.enemiesSpr = [];

	MG.enemiesSpr[0] = GSE.Scene.CreateSprite("alien1",
		"",
		100,
		100,
		32,
		32,
		-1
	);

	MG.enemiesSpr[1] = GSE.Scene.CreateSprite("alien2",
		'white',
		100,
		100,
		32,
		32,
		-1
	);
	MG.enemiesSpr[2] = GSE.Scene.CreateSprite("alien3",
		'white',
		100,
		100,
		32,
		32,
		-1
	);

	var motherShipSpr = GSE.Scene.CreateSprite("mothership",
		'red',
		100,
		100,
		32,
		32,
		-1
	)

	var tankExplosionSpr = GSE.Scene.CreateAnimatedSprite("tank_explode",
		100,
		32,
		100,
		32,
		32,
		-1,
		true,
		3,
		1
	);

	GSE.Scene.CreateEntity('tank_explosion', tankExplosionSpr, null, false);

	var tank = GSE.Scene.CreateEntity('tank', tankSpr, handleUpdatePlayer);
	var shoot = GSE.Scene.CreateEntity('shoot', shootSpr, shootBehavior);

	var motherShip = GSE.Scene.CreateEntity("motherShip", motherShipSpr, motherShipUpdate);
	shoot.setActive(false);

	motherShip.count = 1;
	motherShip.y = 96;
	motherShip.setActive(false);

	//Made a grid of enemies
	for (var r = 0; r < MG.row; r++)
	{
		for(var c = 0; c < MG.col; c++)
		{
			var enemySpr = 0;
			switch (r)
			{
				case 0:
					enemySpr = 2;
					break;
				case 1:
				case 2:
					enemySpr = 1;
					break;
				case 3:
				case 4:
					enemySpr = 0;
					break;
			}

			var enemy = GSE.Scene.CreateEntity('enemy', MG.enemiesSpr[enemySpr], EnemyMove);
			enemy.x = 64 + c * ( 32 + 32);
			enemy.y = 98 + r * ( 32 + 16);
			enemy.startingX = enemy.x;
			enemy.startingY = enemy.y;
			enemy.setActive(false);
		}
	}

	MG.tankLivesA = [];

	//Tank lifes
	for (var r = 0; r < MG.totalTankLives; r++)
	{
		var tankLive = GSE.Scene.CreateEntity('tankLive', tankSpr, null);
		tankLive.x = 64 + r * (64);
		tankLive.y = 32;
		tankLive.setActive(true);
		MG.tankLivesA[r] = tankLive;
	}

	//Tank lifes
	for (var r = 0; r < MG.maxShootPool; r++)
	{
		var enemyShoot = GSE.Scene.CreateEntity('eShoot', shootEneSpr, eShootBehavior);
		enemyShoot.setActive(false);
		MG.shootPool[r] = enemyShoot;
	}

	var menuState = GSE.StateManager.createState("Menu", function mapUpdate()
	{
		if(GSE.Input.IsKeyPressed('space'))
		{
			MG.restartGame();
		}

		if(GSE.Input.IsKeyPressed("h"))
		{
			GSE.Net.send(function ok(e)
			{
				if (GSE.Net.req.readyState == 4 && GSE.Net.req.status == 200)
				{
        			var response = JSON.parse(GSE.Net.req.responseText);
        			alert(response.ip);
    			}
			});
		}
	},
	function Render(ctx)
	{
		GSE.rawDrawText("STAR INVADER", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 92, 'white', '52');
		GSE.rawDrawText("SPACE TO START", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2, 'white', '42');

		GSE.rawDrawText(MG.score.toString(), GSE.Window.screenWidth / 2, 60, 'white', '24');
		GSE.rawDrawText("HI", GSE.Window.screenWidth / 2 + 128, 60, 'white', '20');
		GSE.rawDrawText(MG.highscore.toString(), GSE.Window.screenWidth / 2 + 128 + 64, 60, 'white', '20');

		GSE.rawDrawText("by Guidi Simone, sounds: 8Bit Video Games by sharesynth", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 + 42, 'white', '18');

	});

	var mapState = GSE.StateManager.createState("Game", function mapUpdate() {
		if(GSE.Input.IsKeyPressed('c'))
		{
			MG.gameOver();
		}

		// for (var r = 0; r < MG.tankLives; r++)
		// {
		// 	var tankLive = MG.tankLivesA[r];
		// 	tankLive.setActive(true);
		// }

	}, function Render(ctx) {
		GSE.rawDrawText(MG.score.toString(), GSE.Window.screenWidth / 2, 60, 'white', '24');
		GSE.rawDrawText("HI", GSE.Window.screenWidth / 2 + 128, 60, 'white', '20');
		GSE.rawDrawText(MG.highscore.toString(), GSE.Window.screenWidth / 2 + 128 + 64, 60, 'white', '20');
	});

	var gameOverState = GSE.StateManager.createState("GameOver", function mapUpdate()
	{
		if(GSE.Input.IsKeyPressed('Space'))
		{
			MG.restartGame();
		}
	},
	function Render(ctx)
	{
		GSE.rawDrawText("GAME OVER", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 - 48, 'white', '42');
		GSE.rawDrawText("SPACE TO RESTART", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2, 'white', '20');
		GSE.rawDrawText("SCORE", GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 + 48, 'white', '20');
		GSE.rawDrawText(MG.score.toString(), GSE.Window.screenWidth / 2, GSE.Window.screenHeight / 2 + 68, 'white', '20');
	});

	GSE.StateManager.setCurrentState("Menu");

	MG.highscore = localStorage.getItem("strInvHS") || 0;
}

MG.Wait = function Wait(code, delay) {
	var w = delay || 1000;
	setTimeout(code, w);
};

MG.restartGame = function ()
{
	MG.score = 0;
	MG.timeBeforeMove = 32;
	MG.enemyDirection = 2; //0 - left, 1 - down, 2 - right
	MG.prevEnemyDirection = 0; //0 - left, 1 - down, 2 - right
	MG.tankLives = MG.totalTankLives;
	MG.mScount = 1;

	for (var r = 0; r < MG.tankLives; r++)
    {
   		var tankLive = MG.tankLivesA[r];
    	tankLive.setActive(true);
    }

	for (var r = 0; r < MG.shootPool.length; r++)
    {
   		var eshoot = MG.shootPool[r];
    	eshoot.setActive(false);
    }

	GSE.StateManager.setCurrentState("Game");

	var aShoot = GSE.Scene.GetEntity('shoot');
	var enemies = GSE.Scene.GetEntities('enemy');

	aShoot.setActive(false);

	if(enemies && enemies.length > 0)
	{
		//Bullet Collisions
		for(var i = 0; i< enemies.length; i++)
		{
			var e = enemies[i];
			e.x = e.startingX;
			e.y = e.startingY;
			e.totMove = 0;
			e.setActive(true);
		}
	}

	MG.inGame = true;
}

MG.gameOver = function ()
{
	GSE.Scene.PlaySound('explosion');
	MG.inGame = false;
	if(MG.score > MG.highscore)
	{
		MG.highscore = MG.score
		localStorage.setItem("strInvHS", MG.score);
	}

	var motherShip = GSE.Scene.GetEntity('motherShip');
	if(motherShip.active == true)
	{
		motherShip.setActive(false);
		MG.mScount++;
	}

	var enemies = GSE.Scene.GetEntities('enemy');

	if(enemies && enemies.length > 0)
	{
		//Bullet Collisions
		for(var i = 0; i< enemies.length; i++)
		{
			var e = enemies[i];
			e.setActive(false);
		}
	}
	//wait two seconds before the game over screen
	setTimeout(function() {
		GSE.StateManager.setCurrentState("GameOver");
	}, 1000);
}

MG.nextLevel = function ()
{
	MG.inGame = false; //Set all alien before restart
	MG.score += 50;
	MG.timeBeforeMove = 32;
	MG.enemyDirection = 2; //0 - left, 1 - down, 2 - right
	MG.prevEnemyDirection = 0; //0 - left, 1 - down, 2 - right
	MG.enemy_kill = 0;

	//GSE.StateManager.setCurrentState("Game");

	var aShoot = GSE.Scene.GetEntity('shoot');
	var enemies = GSE.Scene.GetEntities('enemy');
	var motherShip = GSE.Scene.GetEntity('motherShip');

	aShoot.setActive(false);
	if(motherShip.active == true)
	{
		motherShip.setActive(false);
		MG.mScount++;
	}

	if(enemies && enemies.length > 0)
	{
		for(var i = 0; i< enemies.length; i++)
		{
			var e = enemies[i];
			e.x = e.startingX;
			e.y = e.startingY;
			e.totMove = 0;
			e.setActive(true);
		}
	}

	MG.inGame = true;
}

MG.loseLife = function loseLife()
{
	GSE.Scene.PlaySound('explosion');
	MG.inGame = false; //Set all alien before restart

	if(MG.tankLives > 0)
	{
		var aShoot = GSE.Scene.GetEntity('shoot');
		var tank = GSE.Scene.GetEntity('tank');
		var explosion = GSE.Scene.GetEntity('tank_explosion');

		MG.tankLives--;
		MG.tankLivesA[MG.tankLives].setActive(false);

		explosion.x = tank.x;
		explosion.y = tank.y;

		// tank.x = tank.startingX;
		// tank.y = tank.startingY;

		aShoot.setActive(false);
		tank.setActive(false);
		explosion.setActive(true);

		setTimeout(MG.TankDeathAnim, 2000);
	}
	else
	{
		MG.gameOver();
	}
}

MG.TankDeathAnim = function TankDeathAnim()
{
	MG.inGame = true;
	var tank = GSE.Scene.GetEntity('tank');
	var explosion = GSE.Scene.GetEntity('tank_explosion');

	tank.setActive(true);
	explosion.setActive(false);
}

function Update(lastTick, timeSinceTick)
{
	if(MG.inGame == false)
	{
		return;
	}

	var aShoot = GSE.Scene.GetEntity('shoot');
	var player = GSE.Scene.GetEntity('tank');
	var enemies = GSE.Scene.GetEntities('enemy');
	var enemy_shoots = GSE.Scene.GetEntities('eShoot');
	var activeEnemies = GSE.Scene.CountActiveEntitiesById('enemy');
	var motherShip = GSE.Scene.GetEntity('motherShip');
	var currentState = GSE.StateManager.currentState();

	var mScore = (MG.mScount * 1000) + (MG.motherShipScoreAdj * MG.mScount -1);
	if(MG.score >= mScore && motherShip.active == false)
	{
		GSE.Scene.PlaySound('mother');
		motherShip.x = 0;
		//motherShip.y = 92
		motherShip.setActive(true);
		console.log("Active motherShip");
	}

	MG.active_enemies = activeEnemies;

	for(var i = 0; i< enemy_shoots.length; i++)
	{
		var enemy_shoot = enemy_shoots[i];
		if(GSE.Collisions.CheckCollision(player, enemy_shoot) == true)
		{
			MG.loseLife();
		}
	}

	if(motherShip && motherShip.active == true)
	{
		if(GSE.Collisions.CheckCollision(aShoot, motherShip) == true)
		{
			GSE.Scene.PlaySound('hit');
			aShoot.setActive(false);
			motherShip.setActive(false);
			MG.mScount++;
			MG.score += 300;
		}
	}

	if(enemies && enemies.length > 0 && activeEnemies > 0)
	{
		//Bullet Collisions
		for(var i = 0; i< enemies.length; i++)
		{
			var e = enemies[i];
			if(e.active == false)
			{
				continue;
			}

			if(GSE.Collisions.CheckCollision(aShoot, e) == true)
			{
				GSE.Scene.PlaySound('hit');
				GSE.Scene.SetActive(aShoot.id, false);
				// GSE.Scene.SetActive(e.id, false);
				e.setActive(false);
				MG.score += 100;
				MG.enemy_kill++;
				//Icrement enemy speed
				MG.timeBeforeMove = Math.max(1, MG.timeBeforeMove - MG.speedIncrement);
			}

			//Player collision - game over
			if(GSE.Collisions.CheckCollision(player, e) == true)
			{
				MG.gameOver();
				//Lose lifes before game over
				//MG.loseLife();
			}
		}

		//Determine enemy direction
		switch(MG.enemyDirection)
		{
			case 0:
			{	//left
				// self.x += 32;
				for(var i = 0; i< enemies.length; i++)
				{
					var e = enemies[i];

					if(e.active == false)
					{
						continue;
					}

					if(e.x - MG.enemyMovement < MG.enemyMovement)
					{
						//One of the enemy will go out of the screen
						MG.prevEnemyDirection = MG.enemyDirection;
						MG.enemyDirection = 1;
						return;
					}
				}
				break;
			}
			case 1:
			{	//Down
				var allEnemyReadyToMove = 0;
				for(var i = 0; i< enemies.length; i++)
				{
					var e = enemies[i];

					if(e.active == false)
					{
						continue;
					}
					if(e.totMove >= MG.timeBeforeMove)
					{
						//One of the enemy go out of the screen
						allEnemyReadyToMove++;
					}
				}

				if(allEnemyReadyToMove >= activeEnemies)
				{
					//Move down all enemies
					// debugger;
					for(var i = 0; i< enemies.length; i++)
					{
						var e = enemies[i];
						if(e.active == false)
						{
							continue;
						}
						e.y += MG.enemyDownMovement;
						e.totMove = 0;
					}

					if(MG.prevEnemyDirection == 0)
					{
						MG.enemyDirection = 2;
					}
					else if(MG.prevEnemyDirection == 2)
					{
						MG.enemyDirection = 0;
					}
				}

				break;
			}
			case 2: //Right
			{
				for(var i = 0; i< enemies.length; i++)
				{
					var e = enemies[i];

					if(e.active == false)
					{
						continue;
					}

					if(e.x + MG.enemyMovement + e.width >= GSE.Window.screenWidth)
					{
						//One of the enemy go out of the screen
						MG.prevEnemyDirection = MG.enemyDirection;
						MG.enemyDirection = 1;
						return;
					}
				}
				break;
			}
		}
	}
	else
	{
		//Next Level
		if(currentState.name == "Game")
		{
			MG.nextLevel();
		}
	}
}
