function BounceFromWall(player, wall, multiplier = 1)
{
	x_diff = 0;
	y_diff = 0;

	//fix X 
	//Case 1 if Px < Wx
	if(player.data.x < wall.data.x) {
		x_diff = (player.data.x + player.data.w) - wall.data.x;
		x_diff *= -1;
	}
	else if(player.data.x > wall.data.x) {
		x_diff = (wall.data.x + wall.data.w) - player.data.x;
	}
	//fix Y
	//Player over
	if(player.data.y < wall.data.y) {
		y_diff = (player.data.y + player.data.w) - wall.data.y;
		y_diff *= -1;		
	}
	else if(player.data.y > wall.data.y) {
		y_diff = (wall.data.y + wall.data.w) - player.data.y;		
	}

	let x_diff_abs = Math.abs(x_diff);
	let y_diff_abs = Math.abs(y_diff);

	let apply_x = 0;
	let apply_y = 0;

	let amount = 0;

	//extract x_diff
	if(x_diff_abs > 0 && y_diff_abs > 0) { //Both not 0 , use the lower
		if( x_diff_abs < y_diff_abs) {
			apply_x = 1;
			amount = x_diff;
		}
		else {
			apply_y = 1;
			amount = y_diff;
		}
	}
	else if(x_diff_abs > 0 && y_diff_abs == 0) { //Apply X
		apply_x = 1;
		amount = x_diff;
	}
	else if(y_diff_abs > 0 && x_diff_abs == 0) { //Apply Y
		apply_y = 1;
		amount = y_diff;
	}

	// console.log("collision amount:", amount);
	// console.log("collision multiplier:", multiplier);

	//I want a consistent bounce 
	// The overlap need to be different, 

	//Align to rect
	// player.data.x += amount * apply_x
	// player.data.y += amount * apply_y

	//Bounce the amount of pixel
	// if(multiplier > 1) {
	// 	player.data.x += multiplier * Math.sign(amount) * apply_x;
	// 	player.data.y += multiplier * Math.sign(amount) * apply_y;
	// }

	let x_val = amount * apply_x * multiplier;
	let y_val = amount * apply_y * multiplier;

	if(multiplier > 1) {
		x_val += multiplier * Math.sign(amount) * apply_x
		y_val += multiplier * Math.sign(amount) * apply_y
	}

	return {
		x: x_val,
		y: y_val
	}
}