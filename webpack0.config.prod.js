const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const PROD = false;

module.exports = {
	entry: './core/main.js',
	devtool: 'inline-source-map',
	output:{
		filename: 'bundle.server.js',
		path: path.resolve(__dirname, 'ServerSC', "public", "dist")
	}
};

if(PROD == true) {
	module.exports.plugins = [new UglifyJsPlugin()]
}
