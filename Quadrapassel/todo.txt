Menu
    - DONE New Game
    - DONE Highscores
        Saves 10 scores
    - Credits

DONE - Background animation
DONE - Score for dropping pieces
DONE - Exit Pause with keyboard
DONE - Line clear animation (stop a few frames before clearing rows)
DONE - Rotate Left
NOPE - Key remap

Bugfix:
Dropping pieces is way faster and it will go over to the next piece
immediatly

Credits :
Tiles : antony999k OpenGameArt.org
blocks2 : GameArtForge
Music : Melody Loop mix 130 bpm by ▂▃▄▅▆▇█▓▒░DAVEJF░▒▓█▇▆▅▄▃▂
SFX : 
    Blip_C_03 by cabled_mess, 
    Perc Bip by SpiceProgram
    menuSel by RunnerPack