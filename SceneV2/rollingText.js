//init
MG.rollingText = {
    x : 0,
    y : 32,
    screen_w : 200,
    scroll_speed : 10000,
    scroll_w : 256,
    scroll_th : 1,
    scroll_time : 0,
    text: "1234",
    char_x : [],
    char_w : 20
}

console.log("MG.rollingText.char_x", MG.rollingText.char_x)

//Adjust the char_x
for(let char_i in MG.rollingText.text)
{
    MG.rollingText.char_x.push(0 + MG.rollingText.x + char_i * MG.rollingText.char_w);
}

console.log("MG.rollingText.char_x", MG.rollingText.char_x)

//Update
if(swing_active) {
    swing += swing_speed * delta;
    swing %= 360;
}


if(MG.rollingText.scroll_time > MG.rollingText.scroll_th)
{
    MG.rollingText.scroll_time = 0;
    
    for(let i in MG.rollingText.char_x)
    {
        MG.rollingText.char_x[i] += MG.rollingText.scroll_w * delta;
        MG.rollingText.char_x[i] %= MG.rollingText.screen_w;
    }
}
else
{
    MG.rollingText.scroll_time += MG.rollingText.scroll_speed * delta;
}


//Render

let y = 10;
	let x_start = 10;
	let y_start = 10;

	let x_swing = 0;
	if(swing_active) {
		x_swing = Math.sin(swing) * swing_diff;
	}

    for(let char_i in MG.rollingText.text)
	{
		let y_swing = 0;
		if(swing_active) {
			y_swing = Math.sin(swing + (char_i * MG.rollingText.char_w) ) * swing_diff;
		}

		let scroll_text = {
			x: MG.rollingText.char_x[char_i],
			y: MG.rollingText.y + y_swing,
			text: MG.rollingText.text[char_i],
			align: "left",
			color: "white",
			size: "46",
		}

		Scene.DrawText(scroll_text);
	}

	for(let char_i in MG.rollingText.text)
	{
		text1.text = Math.round(MG.rollingText.char_x[char_i]);
		text1.y = 64 + (32 * char_i);

		Scene.DrawText(text1);
	}