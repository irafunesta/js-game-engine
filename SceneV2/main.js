
MG = {
	mapW: 640,
	mapH: 480
};

const swing_active = true;
let swing = 0;
const swing_speed = 8;
const swing_diff = 4;
//Alias
const Scene = GSE.Scene;

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();

function InitGame(ctx)
{
	console.log("InitGame");

	GSE.Scene.useRoot(true);
	GSE.Scene.debug_draw = true;

	GSE.Scene.LoadTexture("simple_spr", "./sprites/goblin.png");
	GSE.Scene.LoadTexture("spr2", "./sprites/b_Pause3.png");
	GSE.Scene.LoadTexture("spr3", "./sprites/b_Restart.png");

	// let node = {
	// 	a:"prova",
	// 	b:19,
	// 	id:"tile"
	// }
	// let render_node = GSE.Scene.CreateNode(node);
	// console.log("render_node", render_node);

	let nodeA = {
		texture: "simple_spr",
		h:300,
		w:300,
		id: "nodeA"
	}

	GSE.Scene.AddSprite(nodeA);

	let node_ret = GSE.Scene.GetNode("nodeA");
	console.log("Node ret:", node_ret);

	let nodeB = {
		texture: "spr2",
		h:121,
		w:96,
		x:100,
		id: "nodeB"
	}

	GSE.Scene.AddSprite(nodeB, "nodeA");
	
	let sprite_ext = {
		texture: "spr3",
		h:127,
		w:132,
		order:-1,
		x:120,
		y:100,
		id:"asd"
	}
	GSE.Scene.AddSprite(sprite_ext);

	//Test AddRect
	let rectA = {
		texture: "simple_spr",
		h:32,
		w:32,
		x: 150,
		y: 160,
		id: "rectA",
		order: 3
	}
	GSE.Scene.AddRect(rectA);

	//Test AddText
	let textA = {
		x:180, 
        y:100,
        tmp:false,
        text : "Pippo",
        color : "white",
        align : "left",
        font : "serif",
        size : "24",
		id: "textA",
	}
	GSE.Scene.AddText(textA, "nodeA");

	GSE.Scene.PrintAllNodes();

	// GSE.Scene.AddSprite(sprite);

	// let nodeC = {
	// 	texture: "simple_spr",
	// 	h:300,
	// 	w:300,
	// 	id: "nodeC"
	// }
	// GSE.Scene.AddSprite(sprite);

	// let pause = {
	// 	texture: "spr2",
	// 	x:150,
	// 	y:150,
	// 	h:64,
	// 	w:64,
	// 	order:2
	// }
	// GSE.Scene.AddSprite(pause);

	// let restart = {
	// 	texture: "spr3",
	// 	x:160,
	// 	y:150,
	// 	h:64,
	// 	w:64,
	// 	order:-1
	// }
	// GSE.Scene.AddSprite(restart);


	

	
}

function Update(lastTick, delta)
{
	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);

	
}

function Render(delta)
{
	// console.log("Render delta:", delta);
	// console.log("swing:", swing);
	
	//DrawTile(x_start + (x*cell_size) + x_swing, y_start + (y*cell_size), cell_size, tile_id);

	// let sprite_ext_tmp = {
	// 	texture: "simple_spr",
	// 	h:150,
	// 	w:150,
	// 	sx:100, sy:100,
	// 	sw:150,sh:150,
	// 	order:-10,
	// 	x:x_start + x_swing,
	// 	y:y_start,
	// 	id:"tmp node"
	// }
	// GSE.Scene.DrawSpriteExt(sprite_ext_tmp);

	// GSE.rawDrawRect(x_start + x_swing, 350, 16, 16, "green", 0, null);
	// let rect_tmp = {
	// 	color: "green",
	// 	h:MG.mapH,
	// 	w:32,
	// 	x:MG.rollingText.screen_w,
	// 	y:0,
	// 	id:"tmp node",
	// 	order:2
	// }

	// let rect_border = Object.assign({},rect_tmp, {
	// 	border: 3,
	// 	borderColor:"black",
	// 	x:x_start + 10 + x_swing,
	// });

	// let rect_gradient = Object.assign({},rect_tmp, {
	// 	gradient: true,
	// 	color:null,
	// 	x:x_start + 20 + x_swing,
	// });

	// let rect_aplha = Object.assign({},rect_tmp, {
	// 	alpha: 0.4,
	// 	x:x_start + 30 + x_swing,
	// 	order:2,
	// 	color:"violet"
	// });

	// Scene.DrawRect(rect_tmp);
	// Scene.DrawRect(rect_border);
	// Scene.DrawRect(rect_gradient);
	// Scene.DrawRect(rect_aplha);

	// let text = {
	// 	x: 10,
	// 	y: 360,
	// 	text: "TEXT",
	// 	align: "left",
	// 	color: "green",
	// 	size: "23",
	// 	order: 5
	// }

	// let text1 = {
	// 	x: 10,
	// 	y: 64,
	// 	text: MG.rollingText.x.toString(),
	// 	align: "left",
	// 	color: "black",
	// 	size: "42"
	// }

	// Scene.DrawText(text);
	// Scene.DrawText(text1);

	// let scroll_text = {
	// 	x: MG.rollingText.x,
	// 	y: MG.rollingText.y,
	// 	text: MG.rollingText.text,
	// 	align: "left",
	// 	color: "black",
	// 	size: "23",
	// }

}