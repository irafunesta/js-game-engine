MG = {
	mapW: 640,
	mapH: 480,
	tile_w: 32,
	tile_h: 32,
};

//Alias
const Scene = GSE.Scene;
const Input = GSE.Input;

let padding = 10;

//map
let map = {
	widht : 9,
	height : 9,
	bombnum : 8
}

let number_map = [];
let reveal_map = [];

let final_map = [];

function toMap(x, y)
{
	//Out of the map
	if( (x < 0 || y < 0) || (x >= map.widht || y >= map.height)) {
		return -1; //to make the map return undefined
	}
	return (y * map.widht) + x;
}

// let current_cell = {x:0, y:0}
// let previous_cell = {x:0, y:0}
// let next_cell = {x:0, y:0}

let check_direction = [{x:0, y:-1},{x:1, y:0}, {x:0, y:+1}, {x:-1, y:0} ]
let current_frame = 0;
let end = 0;

/*
{
		prev_cell : undefined,
		current_cell : {x:0, y:0},
		current_direction : 0,
		next_cell : {x:0, y:0}
	}
*/
let stack_frames = [];

let visited_cells = [];

let step_timer;

let is_win = false;
let is_game_over = false;
let last_bomb = {x:0, y:0}
let gametime = 0;

let field_offset = {x: MG.tile_w * 5, y:MG.tile_h * 2};

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();

function GenerateMap()
{
	final_map = [];
	visited_cells = [];

	for(let x=0; x< map.widht; x++) {
		for(let y=0; y< map.height; y++) {
			final_map.push({
				number : 0,
				bomb : false,
				flag : false,
				visible : false
			})
		}
	}

	for(let i=0; i< map.bombnum; i++) {
		pos = Math.floor(GSE.randomRange(0,map.widht * map.height));
		final_map[pos].bomb = true;
	}
	
	//Pass all the map, and change the number on the map based on how many bombs are around
	for(let x=0; x< map.widht; x++) {
		for(let y=0; y< map.height; y++) {
			//count bomb

			if(final_map[toMap(x,y)].bomb)
			{
				continue;
			}

			let left_up, center_up, right_up;
			let left, right;
			let left_down, center_down, right_down;
			
			left_up = final_map[toMap(x - 1, y - 1)]?.bomb ? 1 : 0;
			center_up = final_map[toMap(x, y - 1)]?.bomb ? 1 : 0;
			right_up = final_map[toMap(x + 1, y - 1)]?.bomb ? 1 : 0;

			left = final_map[toMap(x - 1, y)]?.bomb ? 1 : 0;
			right = final_map[toMap(x + 1, y)]?.bomb ? 1 : 0;

			left_down = final_map[toMap(x - 1, y + 1)]?.bomb ? 1 : 0;
			center_down = final_map[toMap(x, y + 1)]?.bomb ? 1 : 0;
			right_down = final_map[toMap(x + 1, y + 1)]?.bomb ? 1 : 0;

			final_map[toMap(x, y)].number = left_up + center_up + right_up + left + right + left_down + center_down + right_down;

		}
	}
}

function InitGame(ctx)
{
	//generate empty map
	GenerateMap();

	step_timer = new GSE.Timer(0.5, () => 
	{
		let frame = stack_frames[stack_frames.length-1]
		if(!frame) {
			return;
		}

		makeConnectedCellVisibleStep(frame.next_cell.x, frame.next_cell.y);
	});

}

/*
	for each cell connected to the on clicked, make them, visible if
	are not bomb.

	Do a recursive function passing all the neightbour of a cell connected
*/
function makeConnectedCellVisible(x, y)
{
	//The cell can also wrap around the map so x and y need to remain in boud
	if( (x < 0 || y < 0) || (x >= map.widht || y >= map.height))
	{
		return;
	}

	if(visited_cells.indexOf(toMap(x, y)) != -1)
	{
		return;
	}

	let cell = final_map[toMap(x, y)];

	if(cell == undefined || cell.visible || cell.bomb)
	{
		return;
	}

	final_map[toMap(x, y)].visible = true;
	visited_cells.push(toMap(x, y));

	//Return when u encounter a number cell, but not the firs one
	if(cell.number != 0 && visited_cells.length > 0)
	{
		return ;
	}

	//check up
	makeConnectedCellVisible(x, y - 1);
	//check left
	makeConnectedCellVisible(x - 1, y);
	//check right
	makeConnectedCellVisible(x + 1, y);
	//check down
	makeConnectedCellVisible(x, y + 1);

	return;
}

function returnFromCell()
{
	let frame = stack_frames[stack_frames.length-1];
	if(frame)
	{
		frame.current_direction++;
		if(frame.current_direction < check_direction.length)
		{
			let dir = check_direction[frame.current_direction];
			frame.next_cell = {
				x: frame.current_cell.x + dir.x,
				y: frame.current_cell.y + dir.y
			};
		}
		else
		{
			//if all direction are closed move to previous frame
			stack_frames.pop();
		}
	}
	console.log("frame stack:", stack_frames);
}

function makeConnectedCellVisibleStep(x, y)
{
	//The cell can also wrap around the map so x and y need to remain in boud
	if( (x < 0 || y < 0) || (x >= map.widht || y >= map.height))
	{
		//Check next direction
		returnFromCell()
		return;
	}

	if(visited_cells.indexOf(toMap(x, y)) != -1)
	{
		returnFromCell()
		return;
	}

	let cell = final_map[toMap(x, y)];

	if(cell == undefined || cell.visible || cell.bomb)
	{
		returnFromCell();
		return;
	}

	final_map[toMap(x, y)].visible = true;

	//Return when u encounter a number cell, but not the firs one
	if(cell.number != 0 && stack_frames.length > 1)
	{
		returnFromCell();
		return;
	}

	let frame = {
		prev_cell : stack_frames[stack_frames.length-1]?.current_cell,
		current_cell : {x:x, y:y},
		current_direction : 0
	}

	let dir = check_direction[frame.current_direction];
	frame.next_cell = {
		x: x + dir.x,
		y: y + dir.y
	};

	stack_frames.push(frame)
	visited_cells.push(toMap(x, y))

	console.log("frame stack:", stack_frames);
}

function Reset()
{
	is_game_over = false;
	is_win = false;
	last_bomb = {};
	gametime = 0;
	GenerateMap();
}

function Update(lastTick, delta)
{
	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);
	if(is_game_over)
	{
		if(Input.IsKeyPressed(Input.keyCodes.KeyR))
		{
			Reset();
		}
		return;
	}

	if(is_win)
	{
		if(Input.IsKeyPressed(Input.keyCodes.KeyR))
		{
			Reset();
		}
		return;
	}

	gametime += 1 * delta;

	if(Input.IsMouseLeftPressed())
	{
		let mouseX = Input.mouse.clientX - padding;
		let mouseY = Input.mouse.clientY - padding;

		console.log("normal pos:", mouseX, mouseY);

		//Round muose position
		mouseX = Math.floor((mouseX - field_offset.x) / MG.tile_w);
		mouseY = Math.floor((mouseY - field_offset.y) / MG.tile_h);

		console.log(mouseX, mouseY);
		let cell = final_map[toMap(mouseX, mouseY)];
		console.log("map", cell);

		if(!cell) 
		{
			return;
		}

		if(cell.bomb) {
			//Game Over
			// Show all the bombs and the one clicked in red
			last_bomb = {x:mouseX, y:mouseY};
			is_game_over = true;
			return;
		}
		makeConnectedCellVisible(mouseX, mouseY);

		is_win = true;
		//Pass all the map and if the only not visibile cell are bombs, then w win
		for(let i=0; i< map.widht * map.height; i++) {
			let cell = final_map[i];
			
			//Exit at the first cell that isn't a bomb and is not visible
			if(!cell.visible && !cell.bomb) {
				is_win = false;
				break;
			}
		}

	}

	if(stack_frames.length > 0)
	{
		step_timer.tick(delta);
	}
}

function Render(delta)
{
	for(let x=0; x< map.widht; x++) {
		for(let y=0; y< map.height; y++) 
		{
			let cell = final_map[toMap(x, y)];
			let cell_color = "grey";

			let frame = stack_frames[stack_frames.length-1]
			if(frame)
			{
				if(frame.current_cell.x == x && frame.current_cell.y == y)
				{
					cell_color = "green";
				}

				if(frame.prev_cell?.x == x && frame.prev_cell?.y == y)
				{
					cell_color = "red";
				}

				if(frame.next_cell?.x == x && frame.next_cell?.y == y)
				{										
					cell_color = "blue";
				}
			}

			if(cell.visible && !cell.bomb) {
				Scene.DrawRect({
					x: x * 32 + field_offset.x ,
					y: y * 32 + field_offset.y,
					h:32,
					w:32,
					color : "grey"
				})

				if(cell.number === 0)
				{
					continue;
				}

				//Draw number
				Scene.DrawText({
					x: x * 32 + 16 + field_offset.x,
					y: y * 32 + 16 + field_offset.y,
					h:32,
					w:32,
					color : "white",
					text : cell.number.toString(),
					align : "center",
					size : "14"
				})
			}
			else
			{
				//if we hit a bomb then show them, 
				if((is_game_over || is_win) && cell.bomb)
				{
					cell_color = "black";
					if(last_bomb.x === x && last_bomb.y === y)
					{
						cell_color = "red";
					}
				}

				Scene.DrawRect({
					x: x * 32 + field_offset.x,
					y: y * 32 + field_offset.y,
					h:32,
					w:32,
					color : cell_color
				})

				// GSE.rawDrawRectLine( (x * 32) + 2, (y * 32) + 2 , 32, 32, "white", 2);
				Scene.DrawRect({
					x: x * 32 + field_offset.x,
					y: y * 32 + field_offset.y,
					h:4,
					w:32,
					color : "white"
				})
				Scene.DrawRect({
					x: x * 32 + field_offset.x,
					y: y * 32 + field_offset.y,
					h:32,
					w:4,
					color : "white"
				})
			}
		

			GSE.rawDrawRectLine( x * 32 + field_offset.x, y * 32 + field_offset.y, 32, 32, "black", 1);
		}
	}

	//Debug draw
	for(let x=0; x< map.widht; x++) {
		for(let y=0; y< map.height; y++) {
			
			let cell = final_map[toMap(x, y)];
			let cell_color = "grey";

			let frame = stack_frames[stack_frames.length-1]
			if(frame)
			{
				if(frame.current_cell.x == x && frame.current_cell.y == y)
				{
					cell_color = "green";
				}

				if(frame.prev_cell?.x == x && frame.prev_cell?.y == y)
				{
					cell_color = "red";
				}

				if(frame.next_cell?.x == x && frame.next_cell?.y == y)
				{								
							
					cell_color = "yellow";
				}

				Scene.DrawRect({
					y: y * 32 + field_offset.x,
					y: y * 32 + field_offset.y,
					h:32,
					w:32,
					color : cell_color
				})
			}
		}
	}

	if(is_win)
	{
		Scene.DrawText({
			x: MG.mapW / 2 - 32,
			y: MG.mapH / 2,
			h:32,
			w:32,
			color : "black",
			text : "YOU WIN",
			align : "center",
			size : "25"
		})

		Scene.DrawText({
			x: MG.mapW / 2 - 32 +1,
			y: MG.mapH / 2 + 1,
			h:32,
			w:32,
			color : "white",
			text : "YOU WIN",
			align : "center",
			size : "25"
		})

		Scene.DrawText({
			x: MG.mapW / 2 - 32,
			y: MG.mapH / 2 + 20,
			h:32,
			w:32,
			color : "black",
			text : "Press R to Restart",
			align : "center",
			size : "20"
		})
	}
	
	if(is_game_over)
	{
		Scene.DrawText({
			x: MG.mapW / 2 - 32,
			y: MG.mapH / 2,
			h:32,
			w:32,
			color : "black",
			text : "GAME OVER",
			align : "center",
			size : "25"
		})

		Scene.DrawText({
			x: MG.mapW / 2 - 32 +1,
			y: MG.mapH / 2 + 1,
			h:32,
			w:32,
			color : "white",
			text : "GAME OVER",
			align : "center",
			size : "25"
		})

		Scene.DrawText({
			x: MG.mapW / 2 - 32,
			y: MG.mapH / 2 + 20,
			h:32,
			w:32,
			color : "black",
			text : "Press R to Restart",
			align : "center",
			size : "20"
		})

		
	}


	//Game time
	Scene.DrawText({
		x: MG.mapW / 2 - 32,
		y: MG.tile_h,
		h:32,
		w:32,
		color : "white",
		text : "Time: " + Math.round(gametime).toString(),
		align : "center",
		size : "20"
	})
}