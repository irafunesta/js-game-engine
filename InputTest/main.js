MG = {
	mapW: 640,
	mapH: 480,
	tile_w : 32,
	tile_h : 32
};

//Alias
const Scene = GSE.Scene;
const Input = GSE.Input;

let inputs = {
	[Input.keyCodes.ArrowUp] : false,
	[Input.keyCodes.ArrowRight] : false
}

let updateTimer = (timer, delta) => {
	if(timer.counter <= timer.threshold)
	{
		console.log("count")
		timer.counter += 1 * delta
		return;
	}

	//Do the action and reset
	if(timer.callback && typeof(timer.callback) == "function")
	{
		console.log("do action")
		timer.counter = 0;
		timer.callback()
	}
}

let testTimer;

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "grey");
})();

function InitGame(ctx)
{
	testTimer = new GSE.Timer(3, () => {
		console.log("test");
	})

	GSE.Scene.LoadTexture("objects", "./imgs/objects.png");

	Scene.AddSpriteExt(
		{
			texture: "objects",
			x: MG.tile_w * 10,
			y: MG.tile_h * 5,
			w: MG.tile_w,
			h: MG.tile_h,
			sx:0, sy:0, sw:32, sh:32,
			tile_id:0,
			scale:{x:1.5, y:1.5},
			id:"player"
		}
	)

	Scene.AddSpriteExt(
		{
			texture: "objects",
			x: MG.tile_w * 8,
			y: MG.tile_h * 5,
			w: MG.tile_w,
			h: MG.tile_h,
			sx:0, sy:0, sw:32, sh:32,
			tile_id:0,
			scale:{x:1, y:1},
			id:"player2"
		}
	)
}

function Update(lastTick, delta)
{

	if(Input.IsKeyDown(Input.keyCodes.ArrowUp))
	{
		inputs[Input.keyCodes.ArrowUp] = true;
	}
	else
	{
		inputs[Input.keyCodes.ArrowUp] = false;
	}

	//Switch on off
	if(Input.IsKeyPressed(Input.keyCodes.ArrowRight))
	{
		inputs[Input.keyCodes.ArrowRight] = !inputs[Input.keyCodes.ArrowRight]
	}
}

function Render(delta)
{
	Scene.DrawText({
		text:"Premuto up " + inputs[Input.keyCodes.ArrowUp],
		x : 100,
		y:100,
		color: "black",
		size: '20',		
	})

	Scene.DrawText({
		text:"Premuto right " + inputs[Input.keyCodes.ArrowRight],
		x : 100,
		y:140,
		color: "black",
		size: '20',		
	})
}