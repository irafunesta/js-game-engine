MG = {
	mapW: 640,
	mapH: 480
};

//Alias
const Scene = GSE.Scene;

const googol_array = [];
let current_num = 0;
let stop = false;
let speed = 10;

let timer = [];

; (function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();

function InitGame(ctx) {
	for (let i = 0; i < 101; i++) {
		googol_array[i] = 0;
	}

	for (let i = 0; i < 6; i++) {
		timer[i] = 0; //singel number divided by 1/60/60*60 ecc
	}

	googol_array[0] = 0;
	// googol_array[googol_array.length] = 2;
}

function Update(lastTick, delta) {
	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);
	if (stop == true) {
		return;
	}

	let index = googol_array.length - countZeroes(speed);
	googol_array[index] += speed * delta;
	if (googol_array[index] > 9) {

		googol_array[index] = 0;
		current_num = index - 1;

		while (googol_array[current_num] >= 9) {
			googol_array[current_num] = 0;
			current_num -= 1;
		}
		//reset
		googol_array[current_num]++;
	}

	if (GSE.Input.IsKeyPressed("w") || GSE.Input.IsKeyPressed("up")) {
		speed *= 10;
	}
	if (GSE.Input.IsKeyDown("d") || GSE.Input.IsKeyDown("down")) {
		speed -= 10;
	}

}

function countZeroes(num)
{
	let count = 0;
	if(num > 0)
	{
		if(num % 10 == 0)
		{
			count++;
		}

		count += countZeroes(num / 10);
	}
	return count;
}

function Render(delta) {
	let start_x = 132;
	let start_y = 62;
	let gap = 8;
	let cell_size = 32;

	Scene.DrawText({
		x: start_x - 32,
		y: start_y,
		color: "green",
		align: "center",
		font: "Serif",
		size: "24",
		text: speed.toString(),
		id: "Coords"
	});

	for (let y = 0; y < 10; y++) {
		for (let x = 0; x < 11; x++) {
			if (x + (y * 10) > googol_array.length - 1) {
				return;
			}
			let val = googol_array[x + (y * 10)] || 0;
			if (val) {
				val = Math.floor(val);
			}


			Scene.DrawText({
				x: start_x + (x * 32),
				y: start_y + (y * 32),
				color: "white",
				align: "center",
				font: "Serif",
				size: "24",
				text: val.toString(),
				id: "Coords"
			});
		}
	}

}