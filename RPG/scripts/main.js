define(["mapTest", "sub/sub", "../dist/bundle.js"], function(mapTest, sub, GSEngine)
{
    MG = {
        mapW : 640, //4
        mapH : 480, //3
        view_w : 640, // not ready for a screen scaling, set wiew and map width/height to the same
        view_h : 480,
        view : undefined,
        debug : false,
    };

    console.log("mapTest ", mapTest);
    console.log("Sub thing, ", sub);
    console.log("GSEngine, ", GSEngine);

    let GSE = GSEngine.e;

    GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.view_w, MG.view_h, false, InitGame, Update, Render, "black");
    
    var Scene;

    function GetEnemyInRange(grid, x, y, range = 1)
    {
        /*  TODO
            Change the range in to a type of range, 
            with a list of possible cell starting from a x, y as the center
        */
        let enemies = [];

        //just 4 square next to the player
        let range_1 = [[1, 0], [0, 1], [-1, 0], [0, -1]]

        console.log("range_1:", range_1);

        for (let i = 0; i < range_1.length; i++)
        {
            let cell = range_1[i];
            // console.log("cell:", cell);
            let x_c = x + cell [0];
            let y_c = y + cell [1];
            console.log("x:", x_c);
            console.log("y:", y_c);
            let enemy_uid = grid [y_c][x_c];

            console.log("enemy_uid:", enemy_uid);
            if(enemy_uid != -1)
            {
                let enemy = GSE.Scene.GetEntityByUid(enemy_uid);
                if(enemy.tag != undefined && enemy.tag.indexOf("enemy") != -1)
                {
                    enemies.push(enemy);
                }
            }
        }

        console.log("enemies in range", enemies);
        return enemies;
    }

    MG.FindClosestChar = function FindClosestChar(curr_enemy)
    {
        let closest_char = null;
        let min_distance = 999;

        MG.player_party.forEach(char => {
            let dist = distance(char.x, char.y, curr_enemy.x, curr_enemy.y);
            if(dist < min_distance)
            {
                closest_char = char;
                min_distance = dist;
            }
        });

        return closest_char;
    }

    MG.MoveEnemy = function MoveEnemy(curr_enemy, tileWidth, obj_grid, delta = 1)
    {
        var target_pos = {
            "x":curr_enemy.m_row * tileWidth,
            "y":curr_enemy.m_col * tileWidth,
        };

        if(curr_enemy.x != target_pos.x || curr_enemy.y != target_pos.y)
        {
            curr_enemy.x += 10 * curr_enemy.dir.x * delta;
            curr_enemy.y += 10 * curr_enemy.dir.y * delta;
        }
        else
        {
            curr_enemy.state = "move_end";
        }
    }

    MG.SelectCellToMove = function SelectCellToMove(curr_enemy, close_char, obj_grid)
    {
        var next_pos = {
            "row":curr_enemy.m_row, 
            "col":curr_enemy.m_col, 
            "x":curr_enemy.x, 
            "y":curr_enemy.y
        };

        var dir = {
            "x":0,
            "y":0
        }

        if(close_char.x > curr_enemy.x)
        {
            //move right
            next_pos.col += 1;
            dir.x = 1;
        }
        else if(close_char.x < curr_enemy.x)
        {
            //move left
            next_pos.col -= 1;
            dir.x = -1;
        }
        else if(close_char.y > curr_enemy.y)
        {
            //move down
            next_pos.row += 1;
            dir.y = 1;
        }
        else if(close_char.y < curr_enemy.y)
        {
            //move up
            next_pos.row -= 1;
            dir.y = -1;
        }

        if(obj_grid[next_pos.row][next_pos.col] == -1)
        {
            obj_grid[curr_enemy.m_row][curr_enemy.m_col] = -1;
            obj_grid[next_pos.row][next_pos.col] = curr_enemy.uid;
            
            curr_enemy.m_col = next_pos.col;
            curr_enemy.m_row = next_pos.row;

            curr_enemy.dir = dir;

            curr_enemy.state = "moving";
        }
    }

    function InitGame(ctx)
    {
        Scene = GSE.Scene;

        var grid_size = 10;
        var col = 20;
        var row = 15;
        MG.score = 0;
        MG.inGame = true;

        GSE.Scene.LoadTexture("spr", "./tilesets/project_utumno_full.png");
        GSE.Scene.LoadTexture("tank_explode", "./images/tank_explode.png");
        GSE.Scene.LoadTexture("cursor", "./images/cursor.png");
        // GSE.Scene.LoadTexture("tileset", "./tilesets/project_utumno_full.png");

        GSE.Scene.CreateSound('hit', './sound/hurt00.wav');

        var sprites = GSE.Scene.CreateAnimatedSprite("spr", -1, 0, 0, 32, 32, 1, true, 5, 80, 32, 32);
        var p2 = GSE.Scene.CreateAnimatedSprite("spr", -1, 0, 0, 32, 32, 1, true, 6, 80, 32, 32);
        var p3 = GSE.Scene.CreateAnimatedSprite("spr", -1, 0, 0, 32, 32, 1, true, 7, 80, 32, 32);
        var enemy = GSE.Scene.CreateAnimatedSprite("spr", -1, 0, 0, 32, 32, 1, true, 16, 60, 32, 32); //16, 60 

        var char_sprites = [sprites, p2, p3];
        MG.player_party = [];

        MG.obj_grid = [];

        for(let r = 0; r < row; r++)
        {
            let row = [];

            for(let c = 0; c < col; c++)
            {
                row.push(-1);
            }

            MG.obj_grid.push(row);
        }

        for(var i = 0; i < 3; i++)
        {
            var char = GSE.Scene.CreateEntity("char" + i.toString(), char_sprites[i], null);
            
            let col = (9 + i);
            let row = 7;

            char.x = 32 * col;
            char.y = 32 * row;
            char.speed = 5;
            char.steps = 0;
            char.action_point = 2;
            char.action_taken = 0;
            char.hp = 200 + i;

            MG.player_party.push(char);
            MG.obj_grid[row][col] = char.uid;
        }

        let enemy_pos = [
            [8,5],
            [9,4],
            [10,5]
        ]

        MG.enemy_party = [];

        for(let i = 0; i < 1; i++)
        {
            let e = GSE.Scene.CreateEntity("enemy", enemy, null);
            let x_c = enemy_pos[i][0];
            let y_c = enemy_pos[i][1]

            e.x =  32 * x_c;
            e.y = 32 * y_c;

            e.speed = 5;
            e.steps = 0;
            e.tag = "enemy";
            e.hp = 200;
            e.action_point = 2;
            e.action_taken = 0;
            e.m_col = x_c;
            e.m_row = y_c;
            e.state = "idle";

            MG.obj_grid[y_c][x_c] = e.uid;
            MG.enemy_party.push(e);
        }        

        MG.selected_player_character = 0;

        MG.turns_list = ["player", "enemy"];
        MG.current_turn = 0;

        MG.steps = [];
        MG.enemy_in_range = [];
        MG.enemy_selected = 0;
        
        MG.enemy_to_act = 0;

        var mapState = GSE.StateManager.createState("Battle", function battle(delta)
        {
            if(MG.turns_list[MG.current_turn] == "player")
            {
                /* 
                    Is the player party turn
                    Select which character is acting 
                        Select action (attack magic items) or movement
                        Select the other option or pass
                        if all characters have acted, pass the turn
                        else select next character
                */
                if(GSE.Input.IsKeyPressed('right'))
                {                    
                    for (let index = 0; index < MG.player_party.length; index++)
                    {
                        MG.selected_player_character++;
                        MG.selected_player_character = MG.selected_player_character % MG.player_party.length;
                        
                        var next_char = MG.player_party[MG.selected_player_character];
                        var next_char_avaiable_action = next_char.action_point - next_char.action_taken;
                        
                        if(next_char_avaiable_action > 0)
                        {
                            //Found the character
                            return;
                        }
                    }

                    //No char found
                    MG.current_turn = 1 //Enemy turn
                }

                if(GSE.Input.IsKeyPressed('left'))
                {
                    for (let index = 0; index < MG.player_party.length; index++)
                    {
                        if (MG.selected_player_character > 0)
                        {
                            MG.selected_player_character--;
                        }
                        else
                        {
                            MG.selected_player_character = MG.player_party.length - 1;
                        }
                        
                        MG.selected_player_character = MG.selected_player_character % MG.player_party.length;
                        
                        var next_char = MG.player_party[MG.selected_player_character];
                        var next_char_avaiable_action = next_char.action_point - next_char.action_taken;
                        
                        if(next_char_avaiable_action > 0)
                        {
                            //Found the character
                            return;
                        }
                    }

                    //No char found
                    MG.current_turn = 1 //Enemy turn

                }

                if(GSE.Input.IsKeyPressed('x'))
                {
                    GSE.StateManager.setCurrentState("battle_menu");
                }
            }
            else if (MG.turns_list[MG.current_turn] == "enemy")
            {
                console.log("Enemy turn");
                
                /*Get the first enemy with action point left.
                Get the nearest char, as the target
                Find:
                if the target is next to me 
                    i have action left
                        Attack
                    else
                        Pass
                else
                    Find a direction to the target
                    If i still have movements 
                        move in that direction
                        GOTO find
                    else
                        Pass


                */

                //Select enemy with AP
                for (let i = 0; i < MG.enemy_party.length; i++)
                {
                    let curr_enemy = MG.enemy_party[i];

                    if(curr_enemy.action_point - curr_enemy.action_taken > 0)
                    {
                        MG.enemy_to_act = i;
                        break;
                    }
                }
                
                let curr_enemy = MG.enemy_party[MG.enemy_to_act];
                let close_char = MG.FindClosestChar(curr_enemy);
                console.log("enemy state:", curr_enemy.state);
                switch(curr_enemy.state)
                {
                    case "idle":
                        // Get player target, and move to it
                        if(close_char != null)
                        {
                            curr_enemy.state = "select_cell";
                            curr_enemy.target = close_char;
                        }
                    break;
                    case "select_cell":
                        MG.SelectCellToMove(curr_enemy, close_char, obj_grid);
                        break;
                    case "moving":
                        // move to the target and when steps ends or target reached attack or pass
                        MG.MoveEnemy(curr_enemy, GSE.Scene.tileWidth, obj_grid, delta)
                        break;
                    case "move_end":
                        // for now pass the turn
                        MG.current_turn = 0 //Player turn
                        //Reset player action
                        for (let index = 0; index < MG.player_party.length; index++)
                        {
                            MG.player_party[index].action_taken = 0;
                            MG.selected_player_character = 0;
                        }
                        //TODO inc steps, select_cell or netx action
                        break;
                }



                // for (let i = 0; i < MG.enemy_party.length; i++)
                // {
                //     let curr_enemy = MG.enemy_party[i];
                //     let close_char = MG.FindClosestChar(curr_enemy);

                //     if(close_char != null)
                //     {
                //         if(curr_enemy.action_point - curr_enemy.action_taken > 0)
                //         {
                //             MG.MoveEnemy(curr_enemy, close_char, MG.obj_grid);
                //         }
                //     }
                // }

                
            }
            
        }, function Render(ctx) {

            GSE.rawDrawText(MG.turns_list[MG.current_turn], 64, 64, 'white', '10', 'left', true);

            GSE.rawDrawText( GSE.Input.mouse.clientX.toString() + "," + GSE.Input.mouse.clientY.toString(), 74, 74, 'white', '10', 'left', true);
            // GSE.Input.mouse.clientX;
        
            
            if(MG.turns_list[MG.current_turn] == "player")
            {
                var sel_obj = MG.player_party[MG.selected_player_character];
                GSE.rawDrawRectLine(sel_obj.x , sel_obj.y , GSE.Scene.tileWidth, GSE.Scene.tileHeight, "yellow", 3);
                GSE.rawDrawRect(sel_obj.x, sel_obj.y, GSE.Scene.tileWidth, GSE.Scene.tileHeight, "dodgerblue", 0, null, null, 0.5);
            }

        }, function Enter(){
            console.log("Enter Battle State");
        });

        MG.battle_menu_option = ["Move", "Attack", "Pass", "Turn"];
        MG.cur_battle_option = 0;

        GSE.StateManager.createState("battle_menu", function mapUpdate(delta)
        {
            var sel_obj = MG.player_party[MG.selected_player_character];

            if(MG.turns_list[MG.current_turn] == "player")
            {
                /* 
                    Cancel action
                */
                if(GSE.Input.IsKeyPressed('z'))
                {
                    MG.cur_battle_option = 0;
                    GSE.StateManager.setCurrentState("Battle");
                }

                if(GSE.Input.IsKeyPressed('down'))
                {
                    MG.cur_battle_option++;
                    MG.cur_battle_option = MG.cur_battle_option % MG.battle_menu_option.length;
                }

                if(GSE.Input.IsKeyPressed('up'))
                {
                    if (MG.cur_battle_option > 0)
                    {
                        MG.cur_battle_option--;
                        MG.cur_battle_option = MG.cur_battle_option % MG.battle_menu_option.length;
                    }
                    else
                    {
                        MG.cur_battle_option = MG.battle_menu_option.length - 1;
                    }
                }

                //Confirm menu selection
                if(GSE.Input.IsKeyPressed('x'))
                {
                    var possible_action = sel_obj.action_point - sel_obj.action_taken;
                    var selected_option = MG.battle_menu_option[MG.cur_battle_option];

                    if(selected_option == "Move" && possible_action > 0)
                    {
                        MG.start_char_pos_x = MG.player_party[MG.selected_player_character].x;
                        MG.start_char_pos_y = MG.player_party[MG.selected_player_character].y;
                        GSE.StateManager.setCurrentState("battle_move");
                    }
                    else if (selected_option == "Attack" && possible_action > 0)
                    {
                        //TODO attack
                        GSE.StateManager.setCurrentState("battle_attack");
                    }
                    else if (selected_option == "Pass")
                    {
                        //TODO pass the turn , going to the next avaiable character with action point
                        var loop = 0;

                        while(loop < MG.player_party.length)
                        {
                            MG.selected_player_character++;
                            MG.selected_player_character = MG.selected_player_character % MG.player_party.length;
                            
                            var next_char = MG.player_party[MG.selected_player_character];
                            var next_char_avaiable_action = next_char.action_point - next_char.action_taken;
                            
                            if(next_char_avaiable_action > 0)
                            {
                                //Found the character
                                break;
                            }
                            loop++;
                        }

                        //No char found
                        if (loop >= MG.player_party.length)
                        {
                            MG.current_turn = 1 //Enemy turn
                        }

                        MG.cur_battle_option = 0;
                        GSE.StateManager.setCurrentState("Battle");
                    }
                    else if (selected_option == "Turn")
                    {
                        //Debug for enemy AI
                        MG.current_turn = 1;
                        GSE.StateManager.setCurrentState("Battle");
                    }
                    else
                    {
                        //TODO error sound
                        GSE.Scene.PlaySound('hit');
                    }
                }
            }
            
        }, function Render(ctx) {
            var sel_obj = MG.player_party[MG.selected_player_character];
            //Cursor selected char
            GSE.rawDrawRectLine(sel_obj.x , sel_obj.y , GSE.Scene.tileWidth, GSE.Scene.tileHeight, "blue", 3);

            //Menu background
            GSE.rawDrawRect(16 , GSE.Window.screenHeight - 128, 128, 128, "blue", 3, "white");

            for(var i =0 ; i< MG.battle_menu_option.length; i++)
            {
                var menu_x = 16 + 20;
                var menu_y = (GSE.Window.screenHeight - 128) + 16 + (16 * i);

                if(MG.cur_battle_option == i) //This option is selected
                {
                    // console.log("menu x:", menu_x, " menu y:", menu_y);
                    // GSE.rawDrawRect(menu_x , menu_y - 10, 128, 16, "black", 2, "white");
                    GSE.drawSprite("cursor", menu_x - 16, menu_y - 10, 16, 16, true);
                }
                // console.log("menu w x:", menu_x, " menu w y:", menu_y);
                GSE.rawDrawText(MG.battle_menu_option[i], menu_x, menu_y, 'white', '10', 'left', true);
            }

            var possible_action = sel_obj.action_point - sel_obj.action_taken;
            GSE.rawDrawText("Action left:" + possible_action.toString(), menu_x, menu_y + 16, 'white', '10', 'left', true);
        }, function CreateEnter(){
            MG.cur_battle_option = 0;
        });

        GSE.StateManager.createState("battle_move", function(delta)
        {
            if(MG.turns_list[MG.current_turn] == "player")
            {
                /* 
                    Move the current selected player
                    The player can move for a set amount of squares
                */
                var sel_obj = MG.player_party[MG.selected_player_character];
                var last_step = MG.steps[MG.steps.length-1];

                var sel_obj_col = Math.floor(sel_obj.x / Scene.tileWidth); // x
                var sel_obj_row = Math.floor(sel_obj.y / Scene.tileHeight); // y

                if(GSE.Input.IsKeyPressed('z'))
                {
                    //Cancel movement
                    MG.player_party[MG.selected_player_character].x = MG.start_char_pos_x;
                    MG.player_party[MG.selected_player_character].y = MG.start_char_pos_y;
                    MG.steps = [];
                    sel_obj.steps = 0;

                    //Clear current cell
                    MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                    //restore last cell
                    var sel_obj_col_old = Math.floor(MG.start_char_pos_x / Scene.tileWidth); // x
                    var sel_obj_row_old = Math.floor(MG.start_char_pos_y / Scene.tileHeight); // y

                    MG.obj_grid[sel_obj_row_old][sel_obj_col_old] = sel_obj.uid;

                    GSE.StateManager.setCurrentState("battle_menu");
                }

                if(GSE.Input.IsKeyPressed('down'))
                {
                    

                    var next_cell = MG.obj_grid[sel_obj_row + 1][sel_obj_col];
                    //TODO check if the player can move here
                    if(sel_obj.y < GSE.Window.screenHeight - GSE.Scene.tileHeight)
                    {
                        if(last_step == "up")
                        {
                            MG.steps.pop();
                            sel_obj.steps--;
                            sel_obj.y += GSE.Scene.tileHeight;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row + 1][sel_obj_col] = sel_obj.uid;
                        }
                        else if(sel_obj.steps < sel_obj.speed && next_cell == -1)
                        {
                            MG.steps.push("down");
                            sel_obj.steps++;
                            sel_obj.y += GSE.Scene.tileHeight;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row + 1][sel_obj_col] = sel_obj.uid;
                        }
                        else
                        {
                            GSE.Scene.PlaySound('hit');
                        }
                    }
                    else
                    {
                        GSE.Scene.PlaySound('hit');
                    }

                }

                if(GSE.Input.IsKeyPressed('up'))
                {
                    var sel_obj_col = Math.floor(sel_obj.x / Scene.tileWidth); // x
                    var sel_obj_row = Math.floor(sel_obj.y / Scene.tileHeight); // y

                    var next_cell = MG.obj_grid[sel_obj_row - 1][sel_obj_col];
                    //TODO check if the player can move here
                    if(sel_obj.y >= GSE.Scene.tileHeight)
                    {
                        if(last_step == "down")
                        {
                            MG.steps.pop();
                            sel_obj.steps--;
                            sel_obj.y -= GSE.Scene.tileHeight;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row - 1][sel_obj_col] = sel_obj.uid;
                        }
                        else if(sel_obj.steps < sel_obj.speed && next_cell == -1)
                        {
                            MG.steps.push("up");
                            sel_obj.steps++;
                            sel_obj.y -= GSE.Scene.tileHeight;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row - 1][sel_obj_col] = sel_obj.uid;
                        }
                        else
                        {
                            GSE.Scene.PlaySound('hit');
                        }
                    }
                    else
                    {
                        //TODO play sound
                        GSE.Scene.PlaySound('hit');
                    }
                }

                if(GSE.Input.IsKeyPressed('right'))
                {
                    var sel_obj_col = Math.floor(sel_obj.x / Scene.tileWidth); // x
                    var sel_obj_row = Math.floor(sel_obj.y / Scene.tileHeight); // y

                    var next_cell = MG.obj_grid[sel_obj_row][sel_obj_col + 1];
                    //TODO check if the player can move here
                    if(sel_obj.x < GSE.Window.screenWidth - GSE.Scene.tileWidth)
                    {
                        // sel_obj.x += GSE.Scene.tileWidth;
                        if(last_step == "left")
                        {
                            MG.steps.pop();
                            sel_obj.steps--;
                            sel_obj.x += GSE.Scene.tileWidth;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row][sel_obj_col + 1] = sel_obj.uid;
                        }
                        else if(sel_obj.steps < sel_obj.speed && next_cell == -1)
                        {
                            MG.steps.push("right");
                            sel_obj.steps++;
                            sel_obj.x += GSE.Scene.tileWidth;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row][sel_obj_col + 1] = sel_obj.uid;
                        }
                        else
                        {
                            GSE.Scene.PlaySound('hit');
                        }
                    }
                    else
                    {
                        //TODO play sound
                        GSE.Scene.PlaySound('hit');
                    }
                }

                if(GSE.Input.IsKeyPressed('left'))
                {
                    var sel_obj_col = Math.floor(sel_obj.x / Scene.tileWidth); // x
                    var sel_obj_row = Math.floor(sel_obj.y / Scene.tileHeight); // y

                    var next_cell = MG.obj_grid[sel_obj_row][sel_obj_col - 1];

                    if(sel_obj.x >= GSE.Scene.tileWidth)
                    {
                        // sel_obj.x -= GSE.Scene.tileWidth;

                        if(last_step == "right")
                        {
                            MG.steps.pop();
                            sel_obj.steps--;
                            sel_obj.x -= GSE.Scene.tileWidth;
                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row][sel_obj_col - 1] = sel_obj.uid;
                        }
                        else if(sel_obj.steps < sel_obj.speed && next_cell == -1)
                        {
                            MG.steps.push("left");
                            sel_obj.steps++;
                            sel_obj.x -= GSE.Scene.tileWidth;

                            //Clear current cell
                            MG.obj_grid[sel_obj_row][sel_obj_col] = -1;
                            //Update next cell
                            MG.obj_grid[sel_obj_row][sel_obj_col - 1] = sel_obj.uid;
                        }
                        else
                        {
                            GSE.Scene.PlaySound('hit');
                        }
                    }
                    else
                    {
                        //TODO play sound
                        GSE.Scene.PlaySound('hit');
                    }
                }

                //Confirm movement and TODO use an action point
                if(GSE.Input.IsKeyPressed('x'))
                {
                    if(sel_obj.steps > 0) //decrease action point only if it actually moved
                    {
                        sel_obj.action_taken++;
                    }
                    MG.steps = [];
                    sel_obj.steps = 0;
                    GSE.StateManager.setCurrentState("battle_menu");
                }
            }
            
        }, function Render(ctx) {
            var sel_obj = MG.player_party[MG.selected_player_character];
            GSE.rawDrawRectLine(sel_obj.x , sel_obj.y , GSE.Scene.tileWidth, GSE.Scene.tileHeight, "blue", 3);       

            sel_obj.steps < sel_obj.speed
            GSE.rawDrawText("steps:" + sel_obj.steps.toString() + "/" + sel_obj.speed.toString(), 16, 16, 'white', '12', 'left', true);
            GSE.rawDrawText("lastStep:" + MG.steps.toString(), 16, 32, 'white', '12', 'left', true);
        });

        GSE.StateManager.createState("battle_attack", function(delta)
        {
            if(MG.turns_list[MG.current_turn] == "player")
            {
                /* 
                    Select the enemy to attack
                    The enemy need to be next to the char

                */

                var sel_obj = MG.player_party[MG.selected_player_character];

                var sel_obj_col = Math.floor(sel_obj.x / Scene.tileWidth); // x
                var sel_obj_row = Math.floor(sel_obj.y / Scene.tileHeight); // y

                if(GSE.Input.IsKeyPressed('z')) // Back
                {
                    GSE.StateManager.setCurrentState("battle_menu");
                }

                //Confirm attack and use an action point
                if(GSE.Input.IsKeyPressed('x')) // Ok
                {
                    var sel_enemy = MG.enemy_in_range[MG.enemy_selected];
                    var possible_action = sel_obj.action_point - sel_obj.action_taken;

                    if(sel_enemy != null && possible_action > 0)
                    {
                        //TODO calculate the correct damage
                        sel_enemy.hp -= 15;
                        sel_obj.action_taken++;
                        GSE.StateManager.setCurrentState("battle_menu");
                    }
                }

                //Cycle through the enemies
                if(GSE.Input.IsKeyPressed('left'))
                {
                    if(MG.enemy_in_range.length > 0)
                    {
                        if (MG.enemy_selected > 0)
                        {
                            MG.enemy_selected--;
                        }
                        else
                        {
                            MG.enemy_selected = MG.enemy_in_range.length - 1;
                        }
                        MG.enemy_selected = MG.enemy_selected % MG.enemy_in_range.length;
                    }
                }

                if(GSE.Input.IsKeyPressed('right'))
                {
                    if(MG.enemy_in_range.length > 0)
                    {
                        MG.enemy_selected++;
                        MG.enemy_selected = MG.enemy_selected % MG.enemy_in_range.length;
                    }
                }
            }
            
        }, function Render(ctx) {
            var sel_obj = MG.player_party[MG.selected_player_character];
            
            
            GSE.rawDrawRectLine(sel_obj.x , sel_obj.y , GSE.Scene.tileWidth, GSE.Scene.tileHeight, "blue", 3);       

            if(MG.enemy_in_range.length > 0)
            {
                var sel_enemy = MG.enemy_in_range[MG.enemy_selected];
                if(sel_enemy != null)
                {
                    GSE.rawDrawRect(sel_enemy.x, sel_enemy.y, GSE.Scene.tileWidth, GSE.Scene.tileHeight, "red", 0, null, null, 0.5);

                    //Draw enemy status
                    let x = 250;
                    let w = 128;
                    let h = 128;
                    GSE.rawDrawRect(x , GSE.Window.screenHeight - h, w, h, "blue", 3, "white");
                    GSE.rawDrawText("HP:" + sel_enemy.hp.toString() , x + 5, GSE.Window.screenHeight - (h - 12), 'red', '12', 'left', true);
                }
            }

            GSE.rawDrawRectLine(sel_obj.x , sel_obj.y + GSE.Scene.tileHeight + 3, GSE.Scene.tileWidth, GSE.Scene.tileHeight, "red", 3); // up
            GSE.rawDrawRectLine(sel_obj.x , sel_obj.y -  GSE.Scene.tileHeight - 3, GSE.Scene.tileWidth, GSE.Scene.tileHeight, "red", 3); // down
            GSE.rawDrawRectLine(sel_obj.x + GSE.Scene.tileWidth + 3, sel_obj.y , GSE.Scene.tileWidth, GSE.Scene.tileHeight, "red", 3);  // right
            GSE.rawDrawRectLine(sel_obj.x - GSE.Scene.tileWidth - 3, sel_obj.y , GSE.Scene.tileWidth, GSE.Scene.tileHeight, "red", 3);  // left
        
            // GSE.rawDrawText("steps:" + sel_obj.steps.toString() + "/" + sel_obj.speed.toString(), 16, 16, 'white', '12', 'left', true);
            // GSE.rawDrawText("lastStep:" + MG.steps.toString(), 16, 32, 'white', '12', 'left', true);

            
            
        }, function Create(){
            console.log("Create battle_attack State");
            let sel_obj = MG.player_party[MG.selected_player_character];

            let sel_obj_col = Math.floor(sel_obj.x / Scene.tileWidth); // x
            let sel_obj_row = Math.floor(sel_obj.y / Scene.tileHeight); // y
            MG.enemy_in_range = GetEnemyInRange(MG.obj_grid, sel_obj_col, sel_obj_row);
        });

        console.log("state queue:" , GSE.StateManager.stateQueue);

        GSE.StateManager.setCurrentState("Battle");

        GSE.Scene.setTileImage("spr", 32, 32);

        GSE.Scene.LoadMap(mapTest.map_01, "map_01");
    }

    function checkBounds(x1,y1, w1, h1, x2,y2,w2,h2) {
        if (x1 < x2 + w2 &&
             x1 + w1 > x2 &&
             y1 <y2 + h2 &&
             h1 + y1 >y2) {
                // collision detected!
            return true;
        }
    
        return false;
    }
    
    function distance(x1, y1, x2, y2) {
        var dx = Math.pow((x2-x1),2);
        var dy = Math.pow((y2-y1),2);
    
        // console.log("dx:", dx, " dy:", dy);
    
        var dist = Math.sqrt(dx + dy);
        return Math.round(dist);
    }
    
    function DrawObjectGrid(obj_grid, row, col)
    {
        if(MG.debug == false)
        {
            return;
        }

        for(let r = 0; r < row; r++)
        {
            for(let c = 0; c < col; c++)
            {
                let x = c * Scene.tileWidth;
                let y = r * Scene.tileHeight;
                GSE.rawDrawRect(x + 10, y + 6 , 16, 16, "white", 3);
                GSE.rawDrawText(obj_grid[r][c].toString(), x + 16, y + 16, 'black', '12', 'left', true);
            }
        }
    }
    
    function Update(lastTick, timeSinceTick)
    {
        // var addScore = false;
        if(MG.inGame == false)
        {
            return;
        }

        if(GSE.Input.IsKeyPressed('p'))
        {
            MG.debug = !MG.debug;
        }
    
    }

    function Render(delta)
    {
        // GSE.rawDrawText("Custom general render callback", 170, 170, 'white', '12', 'left', true);
        // GSE.rawDrawRectLine(250 , 180, 64, 64, "red", 3);

        DrawObjectGrid(MG.obj_grid, 15, 20);

        //Draw the current select char stats and health
        var sel_obj = MG.player_party[MG.selected_player_character];
        if(sel_obj != null)
        {
            //Draw player hp
            GSE.rawDrawRect(130 , GSE.Window.screenHeight - 128, 128, 128, "blue", 3, "white");
            GSE.rawDrawText("HP:" + sel_obj.hp.toString() , 135, GSE.Window.screenHeight - 116, 'white', '12', 'left', true);
        }
    }

    return MG;
});