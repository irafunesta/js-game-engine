const path = require('path');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const PROD = true;

module.exports = {
	entry: './core/main.js',
	devtool: 'inline-source-map',
	output:{
		filename: 'bundle.prod.js',
		path: path.resolve(__dirname, 'dist')
	}
};

if(PROD == true) {
	module.exports.plugins = [new UglifyJsPlugin()]
}
