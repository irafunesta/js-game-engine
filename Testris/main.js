MG = {
	mapW : 640, //4
	mapH : 896, //3

	getLineThresold: function (level) {
		let num_line = 10;
		num_line = (10*level) + 3;
		return num_line;
	},

	isMuted: false
};

let highscores = [];
let credits = [
	"Graphics:",
	"OpenGameArt.org",
	"Tiles: from antony999k", 
	"blocks22: from GameArtForge", 
	"",
	"Music:",
	"Melody Loop mix by DAVEJF", 
	"",
	"SFX :",	
	"Blip_C_03 by cabled_mess",
    "Perc Bip by SpiceProgram",
	"menuSel by RunnerPack"
];
let max_saved_scores = 10;
let start_color_id = 0;
let color_change_time = 30; //secs
let color_th = 0;

const cell_size = 32;
const border_offset = 32;
const font_name = "Courier";
const grid_border_offset = 16;
const colors = ["black", "red", "blue", "pink", "white", "yellow", "green", "aqua"];

const swing_active = false;
let swing = 0;
const swing_speed = 8;
const swing_diff = 4;

var back_anim = {
	active : true,
	swing: 0,
	swing_speed: 0.1,
	swing_diff: 4,

	cur_line : 0,
	line_speed : 0.01,
	line_thr: 1,
	dir:1
}

timePressed = 0;
pressMax = 100;
pressMaxStart = 100;
pressMaxMin = 20;
maxSpeed = 1;

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "black");
})();

function CreatePieces() {
	var long1 = [
		0,1,0,0,
		0,1,0,0,
		0,1,0,0,
		0,1,0,0
	]

	var long = [
		0,0,0,0,
		1,1,1,1,
		0,0,0,0,
		0,0,0,0
	]
	
	var t = [
		1,1,1,0,
		0,1,0,0,
		0,0,0,0,
		0,0,0,0
	]

	var t1 = [
		0,0,1,0,
		0,1,1,0,
		0,0,1,0,
		0,0,0,0
	]

	var t2 = [
		0,0,0,0,
		0,1,0,0,
		1,1,1,0,
		0,0,0,0
	]

	var t3 = [
		1,0,0,0,
		1,1,0,0,
		1,0,0,0,
		0,0,0,0
	]



	var lL = [
		0,0,0,0,
		1,1,1,0,
		0,0,1,0,
		0,0,0,0
	]

	var lL1 = [
		0,1,0,0,
		0,1,0,0,
		1,1,0,0,
		0,0,0,0
	]

	var lL2 = [
		1,0,0,0,
		1,1,1,0,
		0,0,0,0,
		0,0,0,0
	]

	var lL3 = [
		0,1,1,0,
		0,1,0,0,
		0,1,0,0,
		0,0,0,0
	]





	var rL = [
		1,1,0,0,
		0,1,0,0,
		0,1,0,0,
		0,0,0,0
	]

	var rL1 = [
		0,0,1,0,
		1,1,1,0,
		0,0,0,0,
		0,0,0,0
	]

	var rL2 = [
		0,1,0,0,
		0,1,0,0,
		0,1,1,0,
		0,0,0,0
	]

	var rL3 = [
		0,0,0,0,
		1,1,1,0,
		1,0,0,0,
		0,0,0,0
	]

	var lS = [
		0,1,1,0,
		1,1,0,0,
		0,0,0,0,
		0,0,0,0
	]

	var lS1 = [
		1,0,0,0,
		1,1,0,0,
		0,1,0,0,
		0,0,0,0
	]

	var rS = [
		1,1,0,0,
		0,1,1,0,
		0,0,0,0,
		0,0,0,0
	]

	var rS1 = [
		0,0,1,0,
		0,1,1,0,
		0,1,0,0,
		0,0,0,0
	]

	var square = [
		0,1,1,0,
		0,1,1,0,
		0,0,0,0,
		0,0,0,0
	]

	rot0 = [long,  t,  lL,  rL,  lS,  rS,  square]
	rot1 = [long1, t1, lL1, rL1, lS1, rS1, square]
	rot2 = [long,  t2, lL2, rL2, lS,  rS,  square]
	rot3 = [long1, t3, lL3, rL3, lS1, rS1, square]

	return [rot0, rot1, rot2, rot3];
}

function InitGame(ctx)
{
	var col = 10;
	var row = 20;

	MG.col = col;
	MG.row = row;
	MG.score = 0;
	MG.inGame = false;
	MG.line_to_fill = row;
	MG.level = 0;
	MG.lines_cleared = 0;
	MG.drop_score = 0;

	MG.menu_options = ["START", "HIGHSCORES", "CREDITS"];
	MG.selected_option = 0;

	MG.clear_anim_time_theshold = 0.3;
	MG.clear_anim_time = MG.clear_anim_time_theshold;
	MG.clear_anim_flip = 0;
	MG.clear_anim_frame_count = 0;

	GSE.Scene.LoadTexture("tiles", "./images/tiles_v2.png");
	GSE.Scene.LoadTexture("tiles2", "./images/tiles_2.png");
	GSE.Scene.LoadTexture("b_Restart", "./images/b_Restart.png");
	GSE.Scene.LoadTexture("b_Pause3", "./images/b_Pause3.png");

	GSE.Scene.CreateSound("blip", "./sound/blip.wav");
	GSE.Scene.CreateSound("music", "./sound/music.mp3", {loop:true});
	GSE.Scene.CreateSound("clear_line", "./sound/menusel.wav");
	GSE.Scene.CreateSound("place_piece", "./sound/place_piece.wav");
			
	MG.grid = [];
	MG.fall_threshold = 1;
	MG.fall_time = MG.fall_threshold;
	MG.time_decrease = 1;

	MG.pieces = CreatePieces();

	MG.movingPiece = {
		x : 4,
		y : 0,
		piece : Math.round(GSE.randomRange(0, 6)),
		rot: 0,
		nextPiece : Math.round(GSE.randomRange(0, 6))
	}

	// MG.movingPiece.piece = 0;
	// TestGrid();
	ClearGrid();

	var mapState = GSE.StateManager.createState("Game", function mapUpdate(delta)
	{
		if(GSE.Input.IsKeyPressed('p'))
		{
			MG.inGame = false;
			GSE.StateManager.setCurrentState("Pause");
		}

		if(GSE.Input.IsKeyPressed('escape'))
		{
			GSE.StateManager.setCurrentState("Menu");
			GSE.Scene.StopSound("music");
		}

		//Pieces is falling
		if(MG.fall_time <= 0 )
		{
			MG.movingPiece.y++;
			MG.fall_time = MG.fall_threshold;

			//if the piece is at the end of the map, 
			//copy it to the grid and start from above
			//Check if the grid can hold the piece (check for collision)
			for(var y= 0; y< 4; y++)
			{
				for(var x= 0; x< 4; x++)
				{					
					var px, py, id;
					px = MG.movingPiece.x;
					py = MG.movingPiece.y;
					id = MG.movingPiece.piece;
					rot = MG.movingPiece.rot;
					//debugger;

					var map_y = py + y;
					var map_x = px + x;

					var grid = MG.grid[map_y * col + map_x];
					var piece = MG.pieces[rot][id][y*4 + x];

					//End of map
					let out = piece == 1 && map_y >= MG.row;

					if((piece == 1 && grid >= 1) || out)
					{
						//sum of 0 + 1 or 1 + 0
						//collision
						console.log("Play sound place_piece");
						GSE.Scene.PlaySound("place_piece");
						MG.movingPiece.y--;
						//copy to the grid

						//Add drop score
						MG.score += Math.floor(MG.drop_score);
						MG.drop_score = 0;

						for(let y= 0; y< 4; y++)
						{
							for(let x= 0; x< 4; x++)
							{
								var px, py, id;
								px = MG.movingPiece.x;
								py = MG.movingPiece.y;
								id = MG.movingPiece.piece;
								let rot = MG.movingPiece.rot;

								let grid = MG.grid[(py + y)*col + (px + x)];
								let piece = MG.pieces[rot][id][y*4 + x];

								//Insert a color
								if(piece == 1)
								{
									MG.grid[(py + y)*col + (px + x)] = id+1;
								}
							}
						}
					

						//Clear full lines
						let lines_to_clear = GetFullLines();
						if(lines_to_clear.total > 0) 
						{
							//Go into state lines to clear;
							MG.lines_to_clear = lines_to_clear;
							GSE.StateManager.setCurrentState("ClearLines");

							ScoreLines(lines_to_clear.total);
						}

						//Next piece
						MG.movingPiece.y = 0;
						MG.movingPiece.x = 4;
						MG.movingPiece.piece = MG.movingPiece.nextPiece;
						MG.movingPiece.nextPiece = Math.round(GSE.randomRange(0, 6))
						MG.movingPiece.rot = 0;
						
						//Cheeck for GameOver
						if(IsCollidingWithGrid()) {
							//The new piece is colliding
							GSE.StateManager.setCurrentState("FillBoard");
						}

						break;
					}
				}
			}

		}
		else {
			MG.fall_time -= MG.time_decrease * delta;
		}

		//Swing, or angle
		if(swing_active) {
			swing += swing_speed * delta;
			swing %= 360;
		}
	}, 
	function Render(ctx) 
	{
		/*if(GSE.Window.fps && false)
		{
			GSE.rawDrawText(GSE.Window.fps.toString(), 20, 60, 'white', '24', 'left', true);
		}	*/	
	});

	var gameOverState = GSE.StateManager.createState("GameOver", function mapUpdate()
	{
		if(GSE.Input.IsKeyDown("r"))
		{
			MG.restartGame();
		}
	},
	function Render(ctx)
	{
		
	});

	GSE.StateManager.createState("Pause", null,	null);

	GSE.StateManager.createState("FillBoard", FillBoardUpdate, null);
	GSE.StateManager.createState("ClearLines", UpdateClearLines, null);

	GSE.StateManager.createState("Menu", UpdateMenu, null);
	GSE.StateManager.createState("Highscore", UpdateHighscores, null);
	GSE.StateManager.createState("Credits", UpdateCredits, null);

	GSE.StateManager.setCurrentState("Menu");
	// GSE.StateManager.setCurrentState("Highscore");

	saved_score = localStorage.getItem("Tshs") || "";
	
	if(saved_score && saved_score !== "")
	{	
		let arr = saved_score.split("|");
		for(score of arr)
		{
			let num = score.split(";");

			if(num && num.length > 0)
			{
				let score_obj = {
					name: num[0],
					score: Number(num[1])
				}

				highscores.push(score_obj);
			}
		}
		
		// if(highscores.length == 0)
		// {
		// 	highscores.push(0);
		// }
	}
	GSE.rainbow.color_change_time = 0.2;

	//Muted state
	let saved_muted = localStorage.getItem("Tshs-mute");
	MG.isMuted = saved_muted == "true" ? true : false;
	GSE.muteSounds(MG.isMuted);
}

function DrawScores()
{
	let scoreboard_x = 64 + 32*12 + border_offset + 8;
	let scoreboard_y = 32*8;

	GSE.rawDrawRect(64 + 32*12, scoreboard_y - 32, cell_size * 4, cell_size * 11, "black", 0, null)

	GSE.rawDrawText("HI", scoreboard_x + 10, scoreboard_y, 'white', '26', 'left', false, font_name);

	let score_top = 0;
	if(highscores[0])
	{		
		score_top = highscores[0].score || 0;
	}

	GSE.rawDrawText(score_top.toString(), scoreboard_x+ 23, scoreboard_y + 40, 'white', '26', 'center', true, font_name);
	GSE.rawDrawText("SCORE", scoreboard_x - 15, scoreboard_y + 90, 'white', '26', 'left', true, font_name);

	GSE.rawDrawText(MG.score.toString(), scoreboard_x + 23 , scoreboard_y + 130, 'white', '26', 'center', true, font_name);
}

function DrawLevels()
{
	const level_board_x = 64 + 32*12 + border_offset + 12; 
	const level_board_y = 32*14;
	const color = "white";

	GSE.rawDrawText("LEVEL", level_board_x + 20, level_board_y, color, '26', 'center', true, font_name);
	GSE.rawDrawText(MG.level.toString(), level_board_x + 20, level_board_y + 30, color, '26', 'center', true, font_name);
	
	GSE.rawDrawText("LINES", level_board_x + 20, level_board_y + 70, color, '26', 'center', true, font_name);
	GSE.rawDrawText(MG.lines_cleared.toString(), level_board_x + 20, level_board_y + 100, color, '26', 'center', true, font_name);	
}

function IsInState(state_list, reverse)
{
	let check = state_list.indexOf(GSE.StateManager.currentState().name) != -1
	if(reverse) {
		check = !check;
	}
	return check;
}

function IsLineFull(line)
{
	let isFull = true;
	for(var x=0; x < MG.col; x++)
	{
		if(MG.grid[line * MG.col + x] == 0) {
			//Hole
			isFull = false;
			break;
		}
	}
	return isFull;
}

MG.theme = 0;

function DrawTile(x, y, size, id)
{
	const tile_size = 64;
	let tile = "tiles2";

	if([1, 2].indexOf(MG.theme) != -1)
	{
		//skip Eye
		MG.theme = 3;
	}
	

	//id from 1 to 7, cause 0 is black
	if(id == 0)
	{
		GSE.rawDrawRect(x, y, size, size, colors[id], 0, null);
	}
	else
	{		
		id = MG.theme * 7 + id;
		id -= 1;

		GSE.drawSprite("tiles2", null, x, y, size, size, false, id*tile_size, 0, tile_size, tile_size);
	}
}

function Render(delta)
{
	//Draw the game grid
	var x_start = 32 + border_offset + 16;
	var y_start = 64 + 16;

	var color = "pink";
	var border = 1;
	var border_color = 1;

	DrawBackground(0, 0, cell_size, back_anim);

	if(IsInState(["Menu", "Highscore", "Credits"], true)) //Every state except Menu, DrawGrid
	{	
		DrawControls(cell_size * 3, cell_size * 24, "game");
		DrawGridBorder(x_start, y_start);
		DrawGameGrid(x_start, y_start, color, border, border_color);
	}

	if(IsInState(["Game", "Pause", "ClearLines"])) 
	{
		//Draw the moving piece
		for(var y= 0; y< 4; y++)
		{
			//Just need a grid of 1 or 0
			for(var x= 0; x< 4; x++)
			{
				var sx, sy, id;
				sx = x_start + MG.movingPiece.x * cell_size;
				sy = y_start + MG.movingPiece.y * cell_size;
				id = MG.movingPiece.piece;
				var rot = MG.movingPiece.rot;

				if(MG.pieces[rot][id][y*4 + x] == 1) {

					let tile_id = id+1;
					// color = colors[id+1];
					// border = 0;
					// border_color = "white";

					// GSE.rawDrawRect(sx + (x*cell_size), sy + (y*cell_size), cell_size, cell_size, color, border, border_color);

					DrawTile(sx + (x*cell_size), sy + (y*cell_size), cell_size, tile_id);
				}			
			}
		}


	}

	if(IsInState(["Menu", "Highscore", "Credits"], true)) //Every state except Menu, DrawGrid
	{
		//Draw next piece
		let next_x = x_start + 32*10 + border_offset + grid_border_offset;
		let next_y = y_start - grid_border_offset;
		for(var y= 0; y< 4; y++)
		{
			for(var x= 0; x< 4; x++)
			{
				var sx, sy, id;
				sx = next_x ;
				sy = next_y ;
				id = MG.movingPiece.nextPiece;
				var rot = 0;
				let tile_id = 0;

				if(MG.pieces[rot][id][y*4 + x] == 1) {
					tile_id = id+1;
				}
				
				DrawTile(sx + (x*cell_size), sy + (y*cell_size), cell_size, tile_id);
			}
		}

		DrawScores();
		DrawLevels();
	}

	if(GSE.StateManager.currentState().name == "ClearLines")
	{
		RenderClearLinesAnimation(delta, x_start, y_start);
	}
	
	if(GSE.StateManager.currentState().name == "FillBoard")
	{
		FillBoardRender(x_start, y_start);
	}
	

	if(GSE.StateManager.currentState().name == "GameOver")
	{		
		let color = "white"
		let color_shadow = "black"
		let shadow_off = 3;
		GSE.rawDrawText("GAME OVER", x_start + 32 + shadow_off, y_start + 228 + shadow_off, color_shadow, '42', true);
		GSE.rawDrawText("GAME OVER", x_start + 32, y_start + 228, color, '42', true);

		let restart_x = x_start + 100;
		GSE.rawDrawText("R", restart_x + shadow_off, GSE.Window.screenHeight / 2 - 60 + shadow_off, color_shadow, '42', true);
		GSE.rawDrawText("R", restart_x, GSE.Window.screenHeight / 2 - 60, 'red', '42', true);

		GSE.rawDrawText("estart", restart_x + 27 + shadow_off, GSE.Window.screenHeight / 2 - 60 + shadow_off, color_shadow, '42', true);
		GSE.rawDrawText("estart", restart_x + 27, GSE.Window.screenHeight / 2 - 60, color, '42', true);
		
		//Restart button
		// GSE.drawSprite("b_Restart", null, x_start + 168, 248, 32, 32, false, 0, 0, 132, 127);
	}

	if(IsInState(["Menu"]))
	{
		DrawControls(cell_size * 3, cell_size * 16, "menu");
		DrawMenu();
	}

	if(IsInState(["Highscore"]))
	{
		DrawControls(cell_size * 3, cell_size * 22, "highscore");
		DrawHighscore(start_color_id);

		if(color_th >= color_change_time)
		{
			start_color_id++
			start_color_id %= 2;
			color_th = 0;
		}
		else
		{
			color_th += 1 * delta;
		}
	}

	if(IsInState(["Credits"]))
	{
		DrawControls(cell_size * 3, cell_size * 22, "credits");
		DrawCredits();
	}
}

function DrawGridBorder(x_grid, y_grid)
{
	var x_start = x_grid - grid_border_offset;
	var y_start = y_grid - grid_border_offset;

	const border = 0;
	const border_color = "";
	const color = "violet";

	//Black background
	GSE.rawDrawRect(x_start, y_start, cell_size * MG.col, cell_size* MG.row, "black", 0, null);

	for(var y= 0; y< MG.row + 1; y++)
	{
		if(y == 0 || y == MG.row) {
			for(var x= 0; x< MG.col + 1; x++)
			{	
				//first and last line
				GSE.rawDrawRect(x_start + (x*cell_size), y_start + (y*cell_size), cell_size, cell_size, color, 0, border_color);
			}
		}
		else
		{
			//left
			GSE.rawDrawRect(x_start, y_start + (y*cell_size), cell_size, cell_size, color, 0, border_color);

			//right
			GSE.rawDrawRect(x_start + (MG.col*cell_size), y_start + (y*cell_size), cell_size, cell_size, color, 0, border_color);
		}
	}
}

function clickRestart(self)
{
	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		MG.restartGame();
		self.setActive(false);
		MG.RestartButtonBack.setActive(false);
		
	}
}

function clickPause(self)
{
	if(GSE.Input.IsMouseLeftPressed() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		// self.setActive(false);
		TogglePause();
	}
}

function TogglePause()
{
	MG.inGame = !MG.inGame;

	if(GSE.StateManager.currentState().name == "Game") {
		GSE.StateManager.setCurrentState("Pause");
	}
	else if(GSE.StateManager.currentState().name == "Pause") {
		GSE.StateManager.setCurrentState("Game");
	}
}

function clickResume(self)
{
	if(GSE.Input.IsMouseLeftDown() &&
	  checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true)
	{
		self.setActive(false);
		MG.pauseButton.setActive(true);
		// MG.resumeButton.setActive(false);
		GSE.StateManager.setCurrentState("Game");
		MG.inGame = true;
	}
}

function checkBounds(x1,y1, w1, h1, x2,y2,w2,h2) {
	if (x1 < x2 + w2 &&
		 x1 + w1 > x2 &&
		 y1 <y2 + h2 &&
		 h1 + y1 >y2) {
			// collision detected!
		return true;
	}

	return false;
}

function distance(x1, y1, x2, y2) {
	var dx = Math.pow((x2-x1),2);
	var dy = Math.pow((y2-y1),2);

	var dist = Math.sqrt(dx + dy);
	return Math.round(dist);
}

function compare(a, b) {

	let numA = a.score;
	let numB = b.score; 

	if (numA < numB) {
	  return 1;
	}
	if (numA > numB) {
	  return -1;
	}
	// a must be equal to b
	return 0;
  }

MG.restartGame = function ()
{	
	MG.score = 0;
	MG.fall_threshold = 1;
	MG.fall_time = MG.fall_threshold;
	MG.time_decrease = 1;
	MG.line_to_fill = MG.row;
	MG.lines_cleared = 0;
	MG.level = 0;

	//Clear the grid
	ClearGrid();

	//New piece
	MG.movingPiece = {
		x : 4,
		y : 0,
		piece : Math.round(GSE.randomRange(0, 6)),
		rot: 0,
		nextPiece: Math.round(GSE.randomRange(0, 6))
	}
	
	GSE.StateManager.setCurrentState("Game");
	MG.inGame = true;

	GSE.Scene.StopSound("music");
	GSE.Scene.PlaySound("music");
}

function TestGrid() {
	for(var y= 0; y< MG.row; y++)
	{
		//Just need a grid of 1 or 0
		for(var x= 0; x< MG.col; x++)
		{
			let val = 0;
			if(y > 15 && x != 9)
			{
				val = 1;
				if(y == 16 && x == 3)
				{
					val = 0;
				}
			}
			MG.grid[y * MG.col + x] = val;
		}
	}
}

function ClearGrid() {
	for(var y= 0; y< MG.row; y++)
	{
		//Just need a grid of 1 or 0
		for(var x= 0; x< MG.col; x++)
		{
			MG.grid[y * MG.col + x] = 0;
		}
	}
}

function DrawBackground(x_start, y_start, cell_size, back_anim)
{
	for(var y= 0; y< MG.mapH / 32; y++)
	{
		//Just need a grid of 1 or 0
		for(var x= 0; x< MG.mapW / 32; x++)
		{	
			let cell_val = 1;
			let tile_id = cell_val;
			let displace = 0;

			scale_swing = 0;

			scale_swing = Math.abs(y - back_anim.cur_line) * -1;

			displace = scale_swing / 2;
			
			DrawTile(x_start + (x*cell_size) - displace, y_start + (y*cell_size) - displace, cell_size + scale_swing, tile_id);
		}
	}
}

function Update(lastTick, delta)
{
	var addScore = false;
	if(MG.inGame == false && GSE.StateManager.currentState().name == "Pause")
	{
		//Unpause
		if(GSE.Input.IsKeyPressed("Space")) {
			TogglePause();
		}
		return;
	}
	
	if(GSE.Input.IsKeyDown("right")) 
	{		
		if(timePressed == 0 && pressMax == pressMaxStart) 
		{
			//Press one time
			MoveHorrizontal(1);
		}

		timePressed += 1 * delta;
		if(timePressed >= pressMax) {
			MoveHorrizontal(1);
			timePressed = 0;
			pressMax -= 5;
			pressMax = Math.max(pressMaxMin, pressMax);
		}
	}
	else if(GSE.Input.IsKeyDown("left")) 
	{
		if(timePressed == 0 && pressMax == pressMaxStart) 
		{
			//Press one time
			MoveHorrizontal(-1);
		}

		timePressed += 1 * delta;
		if(timePressed >= pressMax) {
			MoveHorrizontal(-1);
			timePressed = 0;
			pressMax -= 5;
			pressMax = Math.max(pressMaxMin, pressMax);
		}
	}
	else { //Release button
		timePressed = 0;
		pressMax = pressMaxStart;
	}

	if(GSE.Input.IsKeyDown("s") || GSE.Input.IsKeyDown("down")) {
		MG.fall_time = 0;
		MG.drop_score += 1 * delta;
	}

	if(GSE.Input.IsKeyPressed("Space") || GSE.Input.IsKeyPressed("d")) {
		// GSE.Scene.PlaySound("blip");
		RotatePiece(1) //rotate right
	}

	if(GSE.Input.IsKeyPressed("a")) {
		// GSE.Scene.PlaySound("blip");
		RotatePiece(-1) //rotate left
	}

	if(back_anim.active) {

		back_anim.line_thr -= back_anim.line_speed * delta;
		if(back_anim.line_thr <= 0)
		{
			if(back_anim.cur_line < 0 || back_anim.cur_line > 28)
			{
				back_anim.dir *= -1;
			}

			back_anim.cur_line += back_anim.dir;
			back_anim.line_thr = 1;
		}
	}

	if(IsInState(["Game", "Pause", "GameOver"]))
	{
		if(GSE.Input.IsKeyPressed('escape'))
		{
			GSE.StateManager.setCurrentState("Menu");
			GSE.Scene.StopSound("music");
		}
	}

	if(IsInState(["Highscore", "Credits"]))
	{
		if(GSE.Input.IsKeyPressed('escape'))
		{
			GSE.StateManager.setCurrentState("Menu");
			GSE.Scene.StopSound("music");
		}
	}

	if(GSE.Input.IsKeyPressed("m"))
	{
		MG.isMuted = !MG.isMuted;
		localStorage.setItem("Tshs-mute", MG.isMuted);
		console.log("Muting game:", MG.isMuted);
		GSE.muteSounds(MG.isMuted);
	}
}

function RotatePiece(direction)
{
	MG.movingPiece.rot += direction;

	if(MG.movingPiece.rot < 0)
	{
		MG.movingPiece.rot = 3;
	}
	else
	{
		MG.movingPiece.rot %= 4;
	}

	if( !IsCollidingWithGrid()) 
	{
		if(IsOutOfWalls("left")) {
			while(IsOutOfWalls("left")) {
				MG.movingPiece.x++; 
			}
		}
		else if(IsOutOfWalls("right")) {
			while(IsOutOfWalls("right")) {
				MG.movingPiece.x--;
			}
		}
	}
	else
	{
		//prevent rotation 
		MG.movingPiece.rot -= direction;
		MG.movingPiece.rot %= 4;	
	}
}

function MoveHorrizontal(dir)
{
	MG.movingPiece.x += dir;

	if(dir > 0) {
		if(IsOutOfWalls("right") || IsCollidingWithGrid()) {
			MG.movingPiece.x -= dir;
		}
	}
	else {
		if(IsOutOfWalls("left") || IsCollidingWithGrid()) {
			MG.movingPiece.x -= dir;
		}
	}
}

//type left or right
function IsOutOfWalls(type)
{
	for(var y= 0; y< 4; y++)
	{
		//check if a 1 is out of bound, 
		for(var x= 0; x< 4; x++)
		{
			var id;
			id = MG.movingPiece.piece;

			let out = x + MG.movingPiece.x >= MG.col;
			var rot = MG.movingPiece.rot;

			switch(type)
			{
				case "left": 
					out = x + MG.movingPiece.x < 0;
					break;
				case "right": 
					out = x + MG.movingPiece.x >= MG.col;
					break;
				default:
					break;
			}			

			if(MG.pieces[rot][id][y*4 + x] == 1 && out) {
				//move it back
				return true;
			}			
		}
	}
}

function IsCollidingWithGrid()
{
	for(var y= 0; y< 4; y++)
	{
		for(var x= 0; x< 4; x++)
		{
			var px, py, id;
			px = MG.movingPiece.x;
			py = MG.movingPiece.y;
			id = MG.movingPiece.piece;
			rot = MG.movingPiece.rot;
			//debugger;

			var map_y = py + y;
			var map_x = px + x;

			var grid = MG.grid[map_y * MG.col + map_x];
			var piece = MG.pieces[rot][id][y*4 + x];

			//End of map
			let out = piece == 1 && map_y >= MG.row;

			if(piece == 1 && grid >=1 || out)
			{
				return true;
			}
		}
	}
}

function GetFullLines()
{
	let total_line_cleared = 0;
	let lines = [];
	for(var y=0; y < MG.row; y++)
	{
		let isFull = true;
		for(var x=0; x < MG.col; x++)
		{
			if(MG.grid[y * MG.col + x] == 0) {
				//Hole
				isFull = false;
				break;
			}
		}

		if(isFull)
		{
			total_line_cleared++;
			lines.push(y);
		}
	}
	return {
		"total": total_line_cleared,
		"lines": lines
	}
}

function ClearLines() 
{
	for(var y=0; y < MG.row; y++)
	{
		let isFull = true;
		for(var x=0; x < MG.col; x++)
		{
			if(MG.grid[y * MG.col + x] == 0) {
				//Hole
				isFull = false;
				break;
			}
		}

		if(isFull)
		{			
			for(let x=0; x < MG.col; x++)
			{
				MG.grid[y * MG.col + x] = 0;
			}

			//Move down the upper lines
			for(var i = y-1; i > 0; i--)
			{
				//copy the i line to i+1
				for(let x=0; x < MG.col; x++)
				{
					MG.grid[(i+1) * MG.col + x] = MG.grid[i * MG.col + x];
				}
			}
		}
	}
}

function ScoreLines(num_lines)
{
	GSE.Scene.PlaySound("clear_line");
	//Score based on lines_cleared
	if(num_lines > 0) 
	{
		MG.lines_cleared += num_lines;
		let score = 0;
		switch (num_lines) {
			case 4:
				score = 12000;
				break;
			case 3:
				score = 6000;
				break;
			case 2:
				score = 2000;
				break;
			case 1:
				score = 1000;
				break;
			default:
				score = 0;
		}
		MG.score += (score * MG.level || 1);

		if(MG.score > MG.bestScore) {
			MG.bestScore = MG.score;
			localStorage.setItem("Tshs", MG.score);
		}

		if(MG.lines_cleared > MG.getLineThresold(MG.level)) {
			MG.level++;
			MG.theme = MG.level% 6; 
			MG.time_decrease += 1;
		}
	}
}

function UpdateClearLines (delta)
{
	//Goback to normal state
	if(MG.clear_anim_time <= 0)
	{
		ClearLines();
		GSE.StateManager.setCurrentState("Game");
		MG.lines_to_clear = {total:0, lines:[]};
		MG.clear_anim_time = MG.clear_anim_time_theshold;
		MG.clear_anim_flip = 0;
		MG.clear_anim_frame_count = 0;
	}

	MG.clear_anim_time -= 1 * delta;
	MG.clear_anim_frame_count++;
	if(MG.clear_anim_frame_count >= 4)
	{
		MG.clear_anim_flip++;
		MG.clear_anim_flip %= 2;
		MG.clear_anim_frame_count = 0;
	}
}

function RenderClearLinesAnimation(delta, x_start, y_start)
{
	let lcolor = ["white", "black"];

	//Draw the lines in a special way
	for(y of MG.lines_to_clear.lines)
	{
		for(var x= 0; x< MG.col; x++)
		{
			let color = lcolor[MG.clear_anim_flip];

			GSE.rawDrawRect(x_start + (x*cell_size), y_start + (y*cell_size), cell_size, cell_size, color, 0, "black");
		}
	}
}

function FillBoardUpdate()
{
	if(MG.line_to_fill < 0)
	{
		GSE.StateManager.setCurrentState("GameOver");

		//Save the highscores here
		SaveHighscore();
	}

	//Fill a line in the grid
	for(var x= 0; x< MG.col; x++)
	{	
		let tile_id = (x + MG.line_to_fill)%6;
		
		if(MG.line_to_fill > 5 && MG.line_to_fill < 12)
		{
			tile_id = 3;
		}

		MG.grid[((MG.line_to_fill - 1) * MG.col) + x] = tile_id + 1;
	}

	MG.line_to_fill--;
}

function FillBoardRender(x_start, y_start)
{
	//Draw the lines in a special way
	for(var x= 0; x< MG.col; x++)
	{
		GSE.rawDrawRect(x_start + (x*cell_size), y_start + (MG.line_to_fill *cell_size), cell_size, cell_size, "red", 1, "white");
	}
}

function DrawGameGrid(x_start, y_start, color, border, border_color)
{
	for(var y= 0; y< MG.row; y++)
	{
		//Just need a grid of 1 or 0
		for(var x= 0; x< MG.col; x++)
		{				
			let cell_val = MG.grid[y* MG.col + x];
			let tile_id = cell_val;

			x_swing = 0;
			if(swing_active) {
				x_swing = Math.sin(swing + y) * swing_diff;
			}
			
			DrawTile(x_start + (x*cell_size) + x_swing, y_start + (y*cell_size), cell_size, tile_id);
		}
	}
}

function UpdateMenu(delta)
{
	if(GSE.Input.IsKeyPressed('up'))
	{
		MG.selected_option--;
		if(MG.selected_option < 0)
		{
			MG.selected_option = MG.menu_options.length - 1;
		}
		else
		{
			MG.selected_option %= MG.menu_options.length;
		}
	}

	if(GSE.Input.IsKeyPressed('down'))
	{
		MG.selected_option++;
		MG.selected_option %= MG.menu_options.length;		
	}

	if(GSE.Input.IsKeyPressed('enter'))
	{
		//Do stuff
		switch(MG.selected_option)
		{
			case 0: 
			{
				MG.restartGame();
				break;
			}
			case 1: 
			{
				GSE.StateManager.setCurrentState("Highscore");
				break;
			}			
			default:
				GSE.StateManager.setCurrentState("Credits");
				break;
		}
	}
}

function DrawMenu()
{
	let menu_x = 32 + border_offset;
	let menu_y = (MG.mapH / 2) - 32 * MG.menu_options.length;

	let color = "white";

	//Background
	GSE.rawDrawRect(menu_x + cell_size, menu_y - cell_size, cell_size * 14, cell_size * 4, "black", 0);

	GSE.rawDrawText("TETRAS", (menu_x + MG.mapW / 2) - 60, menu_y, 'white', '46', 'center', false, font_name);

	menu_y += 32;

	for(option in MG.menu_options) {

		if(option == MG.selected_option) {
			//Option Highlight
			GSE.rawDrawRect(menu_x + 64, menu_y + (option * 30) - 25, 32*12, 32, "violet", 2, "black");
		}
		GSE.rawDrawText(MG.menu_options[option], (menu_x + MG.mapW / 2) - 60, menu_y + option * 30, color, '26', 'center', false, font_name);
	}
}

function UpdateHighscores(delta)
{
	//Swing, or angle
	//if(swing_active) {
		swing += swing_speed * delta;
		swing %= 360;
	//}

	if(GSE.Input.IsKeyPressed("enter"))
	{
		GSE.StateManager.setCurrentState("Menu");
	}
}

function DrawHighscore(start_color_id) 
{
	let menu_x = 32 + border_offset;
	let menu_y = 8 * cell_size;
	let color = "white";

	//BG
	GSE.rawDrawRect(cell_size * 3, menu_y - cell_size, cell_size * 14, cell_size * 13, "black", 0);

	GSE.rawDrawText("HIGHSCORES", (menu_x + MG.mapW / 2) - 60, menu_y, color, '36', 'center', false, font_name);
	
	//Colors https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#color_keywords
	//
	let offset_y = 36;
	let y = 36;
	let num = 1;

	let alt_color = ["pink", "red"];
	let c_id = start_color_id;
	let rainbow_colors = ["red", "orange", "yellow", "green", "dodgerblue", "darkmagenta", "deeppink"];

	for(i in highscores) {

		let score_obj = highscores[i];
		let x = menu_x + cell_size * 5;
		// let color = alt_color[c_id];		
		let x_swing = 0;

		x_swing = Math.sin(swing + y) * swing_diff;

		GSE.rawDrawText(num.toString() + ".", x + x_swing, menu_y + y, color, '26', 'center', false, font_name);

		let name_space = cell_size * 4;

		GSE.rawDrawRainbowText( score_obj.name + ":" +score_obj.score.toString(), x + x_swing + name_space, menu_y + y, rainbow_colors, '26', 'center', false, font_name, 16, start_color_id);
		
		y += offset_y;
		num++;
		c_id++
		c_id %= 2;
	}
}

function DrawControls(x, y, type)
{
	//Draw a black bg

	GSE.rawDrawRect(x, y, cell_size * 14, cell_size * 3, "black", 0);

	let color = "white";
	let x_offset = 32;
	let y_offset = 32;
	
	switch(type)
	{
		case "menu":
			{
				GSE.rawDrawText("Move: Arrow keys", x + x_offset, y + y_offset, color, '26', 'left', false, font_name);

				y_offset += y_offset * 1.5;

				GSE.rawDrawText("Action: Enter   Mute: M", x + x_offset, y + y_offset, color, '26', 'left', false, font_name);
				break;
			}
		case "game":
			{
				GSE.rawDrawText("Move: Arrow keys", x + x_offset, y + y_offset, color, '26', 'left', false, font_name);

				y_offset += y_offset * 1.5;

				GSE.rawDrawText("Rotate: A, D   Esc: Menu", x + x_offset, y + y_offset, color, '26', 'left', false, font_name);
				break;
			}
		case "credits":
		case "highscore":
			{
				GSE.rawDrawText("Back: Enter or Esc", x + x_offset, y + y_offset, color, '26', 'left', false, font_name);
				break;
			}
		default:
			{
				break;
			}
	}
}

function UpdateCredits(delta)
{
	//Swing, or angle
	//if(swing_active) {
		swing += swing_speed * delta;
		swing %= 360;
	//}

	if(GSE.Input.IsKeyPressed("enter") || GSE.Input.IsKeyPressed("escape"))
	{
		GSE.StateManager.setCurrentState("Menu");
	}
}

function DrawCredits()
{
	let menu_x = 32 + border_offset;
	let menu_y = 4 * cell_size;
	let color = "white";

	//BG
	GSE.rawDrawRect(cell_size * 3, menu_y - cell_size, cell_size * 14, cell_size * 17, "black", 0);

	GSE.rawDrawText("CREDITS", (menu_x + MG.mapW / 2) - 60, menu_y, color, '36', 'center', false, font_name);
	
	//Colors https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#color_keywords
	//
	let offset_y = 36;
	let y = 36;

	for(i in credits) {

		let credit = credits[i];
		let x = menu_x + 8 * cell_size;
		let x_swing = 0;

		x_swing = Math.sin(swing + y) * swing_diff;

		GSE.rawDrawText(credit.toString(), x + x_swing, menu_y + y, color, '26', 'center', false, font_name);
		
		y += offset_y;
	}
}

function SaveHighscore()
{
	let name = "";

	console.log("highscores.length", highscores.length);
	if(highscores.length == 0 || highscores.length < 10)
	{
		name = prompt("Inserisci nome (max 5 char)");
		name = name.substring(0, 5);
	}
	else
	{
		//If score is an highscore, ask for a name
		for(score_obj of highscores)
		{
			if(MG.score > score_obj.score)
			{
				name = prompt("New Highscore (max 5 char)");
				name = name.substring(0, 5);
				break;
			}
		}
	}

	//Add score
	let score_to_add = {
		name : name,
		score: MG.score
	}
	
	highscores.push(score_to_add);
	highscores.sort(compare);
	highscores = highscores.splice(0,10);

	let save_scores = [];
	for(score_obj of highscores)
	{
		save_scores.push(score_obj.name + ";" + score_obj.score.toString());
	}
	localStorage.setItem("Tshs", save_scores.join("|"));
}

