//todo order this
let data = [
	"#00a4e6",
"#F0F8FF",
"#FFA07A",
"#FAEBD7",
"#20B2AA",
"#00FFFF",
"#87CEFA",
"#7FFFD4",
"#778899",
"#F0FFFF",
"#B0C4DE",
"#F5F5DC",
"#FFFFE0",
"#FFE4C4",
"#00FF00",
"#000000",
"#32CD32",
"#FFEBCD",
"#FAF0E6",
"#0000FF",
"#FF00FF",
"#8A2BE2",
"#800000",
"#A52A2A",
"#66CDAA",
"#DEB887",
"#0000CD",
"#5F9EA0",
"#BA55D3",
"#7FFF00",
"#9370DB",
"#D2691E",
"#3CB371",
"#FF7F50",
"#7B68EE",
"#6495ED",
"#00FA9A",
"#FFF8DC",
"#48D1CC",
"#DC143C",
"#C71585",
"#00FFFF",
"#191970",
"#00008B",
"#F5FFFA",
"#008B8B",
"#FFE4E1",
"#B8860B",
"#FFE4B5",
"#A9A9A9",
"#FFDEAD",
"#006400",
"#000080",
"#BDB76B",
"#FDF5E6",
"#8B008B",
"#808000",
"#556B2F",
"#6B8E23",
"#FF8C00",
"#FFA500",
"#9932CC",
"#FF4500",
"#8B0000",
"#DA70D6",
"#E9967A",
"#FFFF00",
"#8B0000",
"#FF0000"
]


function initColors() {
	let r = 0;
	let g = 0;
	let b = 0;

	let colors = []

	let step = Math.floor(255/10);

	for(let r=0; r<255; r+=step)
	{
		rHex = r.toString(16).padStart(2, "0");
		gHex = g.toString(16).padStart(2, "0");
		bHex = b.toString(16).padStart(2, "0");
		colors.push("#" + rHex + gHex + bHex);
	}

	r = 0;

	for(let g=0; g<255; g+=step)
	{
		rHex = r.toString(16).padStart(2, "0");
		gHex = g.toString(16).padStart(2, "0");
		bHex = b.toString(16).padStart(2, "0");
		colors.push("#" + rHex + gHex + bHex);
	}

	g = 0;

	for(let b=0; b<255; b+=step)
	{
		rHex = r.toString(16).padStart(2, "0");
		gHex = g.toString(16).padStart(2, "0");
		bHex = b.toString(16).padStart(2, "0");
		colors.push("#" + rHex + gHex + bHex);
	}

	return colors;
}

function initColors2() {
	let r = 0;
	let g = 0;
	let b = 0;

	let colors = []

	let step = Math.floor(255/2);

	for(let r=0; r<255; r+=step)
	{
		let rHex = r.toString(16).padStart(2, "0");
		for(let g=0; g<255; g+=step)
		{
			let gHex = g.toString(16).padStart(2, "0");
			for(let b=0; b<255; b+=step)
			{
				let bHex = b.toString(16).padStart(2, "0");
				colors.push("#" + rHex + gHex + bHex);
			}
		}
	}	

	return colors;
}