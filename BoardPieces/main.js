MG = {
	mapW: 640,
	mapH: 480,
	tile_width : 32,
	tile_height : 32,
	paletteH : 64
};

//Alias
const Scene = GSE.Scene;
const Input = GSE.Input;

//grid
let grid_width = Math.floor(MG.mapW / MG.tile_width);
let grid_height = Math.floor(MG.mapH / MG.tile_height);
let grid = [grid_width][grid_height];

let cell_x = 0;
let cell_y = 0;
let mouse_rect = {};

let token_map = {}
let palette_map = {}; //Hold the color and node for the palette

let token_is_selected = false;
let selected_token = undefined;
let prev_pos = {x:0,y:0};

//Palette
let palette_is_open = false;
let selected_palette = ""; //Index on the array of possibile tokens

//Padding for the window, this is in the css, but it still is part of the cavas so the actual screen start at 10, 10
let padding = 10; 

//System to undo an action
/*
	Add a token
		token position
	Remove a token
		token position
		and color
	Move a token
		starting token position
		current token position

*/
function Action(type, data) {
	this.type = type
	this.data = data
}

let ActionType = {
	Add : 'Add',
	Remove : 'Rem',
	Move : 'Move'
}

//Ever growing stack of actions ??
let actions = [];
// let max_actions = 3;
// let last_action = -1;

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH + MG.paletteH, MG.mapW, MG.mapH + MG.paletteH, false, InitGame, Update, Render, "grey");
})();

function hash(x, y)
{
	return x + "-" + y;
}

function Rect(x, y, w, h)
{
	return {
		x:x, y:y, w:w, h:h}
}

function InitGame(ctx)
{
	
	GSE.StateManager.createState("Map", UpdateMap, undefined);
	//Try witht he palette always visible, yes but i still need to be in 2 different cases
	GSE.StateManager.createState("Palette", UpdatePalette, RenderPalette);

	GSE.StateManager.setCurrentState("Map");

	// data = initColors2();
}

function Update(lastTick, delta)
{
	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);

	cell_x = Math.floor((GSE.Input.mouse.clientX - padding) / MG.tile_width);
	cell_y = Math.floor((GSE.Input.mouse.clientY - padding) / MG.tile_height);

	mouse_rect = Rect(GSE.Input.mouse.clientX - padding, GSE.Input.mouse.clientY - padding, 1, 1);

	if(Input.IsKeyPressed(Input.keyCodes.KeyB))
	{
		console.log("actions:", actions);
	}

	if(Input.IsKeyPressed(Input.keyCodes.ctrlKeyZ))
	{
		console.log("actions:", actions);		
		invertAction(); 
		//Will i need to generate the invers action
	}
}

function Render(delta)
{
	GSE.drawGrid(MG.tile_width, MG.tile_height, "black", false, MG.mapW, MG.mapH);
	
	let string = cell_x + ", " + cell_y;

	Scene.DrawText({
        x:GSE.Input.mouse.clientX, 
        y:GSE.Input.mouse.clientY,
        color:"white",
        align:"left",
        font: "Serif",
        size: "12",
		text: string,
		id:"Coords"
    });

	//Actual palette
	let start_pos = {x:16, y:MG.mapH + 8};
	let spacing_width = 0;
	let spacing_height = 0;
	
	let width = MG.tile_width / 2;
	let height = MG.tile_height / 2;

	let columns = 32;
	let rows = 3;

	for(let y = 0; y < rows; y++)
	{
		for(let x = 0; x < columns; x++)
		{
			let color = data[(y*columns) + x]
			
			let token = Scene.DrawRect(
				{
					x: start_pos.x + (x*spacing_width) + (x*MG.tile_width / 2), 
					y: start_pos.y + (y*spacing_height) + (y*MG.tile_height / 2),
					w: width,
					h: height,
					color: color,
					hidden: false
				}
			)

			if(selected_palette == color)
			{
				Scene.DrawRect(
					{
						x: start_pos.x + (x*spacing_width) + (x*MG.tile_width / 2),
						y: start_pos.y + (y*spacing_height) + (y*MG.tile_height / 2),
						w: 8, 
						h: 8, 
						color: "black",
						hidden: false
					}
				)
			}

			palette_map[hash(x,y)] = token;
		}
	}
}

//Move tokens around
function UpdateMap()
{
	if(GSE.Input.IsMouseLeftPressed())
	{
		let x, y;
		x = cell_x;
		y = cell_y;

		if(x >= 0 && x <= MG.mapW / MG.tile_width && y >= 0 && y <= MG.mapH / MG.tile_height )
		{
			// console.log("Inside the grid")
			if(token_is_selected == false) {
				//no token selected
				//select the token
				
				if(token_map[hash(x,y)])
				{
					console.log("Select the token")
					token_is_selected = true;
					selected_token = token_map[hash(x,y)];
					prev_pos.x = x;
					prev_pos.y = y;

					console.log("Selected token:", token_is_selected);
				}
			}
			else
			{
				//i have a selected token, chek if the space is empty
				if(!token_map[hash(x,y)] || (x == prev_pos.x && y == prev_pos.y))
				{
					//drop here the token
					selected_token.data.x = 0 + (x*MG.tile_width), 
					selected_token.data.y = 0 + (y*MG.tile_height),

					console.log("moved token at :", selected_token.data.x, ",", selected_token.data.y);
					
					//the new map pos need to be filled
					token_map[hash(x,y)] = selected_token;
					console.log("Fill map:", hash(x,y));
					console.log("Fill map token is :", selected_token);

					pushAction (new Action(ActionType.Move, {
						token : selected_token,
						lastpos : {x: x, y: y}, //actually current
						currentpos : {x: prev_pos.x, y : prev_pos.y}
					}))

					//the prev pos need to be freed and then saved again
					//is it's not the same position
					if(x != prev_pos.x || y != prev_pos.y)
					{
						console.log("clear prev pos:", prev_pos);
						token_map[hash(prev_pos.x, prev_pos.y)] = undefined;
					}
					console.log("Map prev pos:", token_map[hash(prev_pos.x, prev_pos.y)]);
					prev_pos.x = x;
					prev_pos.y = y;
					console.log("new prev:", prev_pos);

					//no token is selected anymore
					selected_token = undefined;
					token_is_selected = false;
					console.log("Selected token:", selected_token);
				}
			}
		}
	}

	//Delete enemy
	if(GSE.Input.IsKeyPressed(Input.keyCodes.Escape))
	{
		// deleteToken(cell_x, cell_y);
		console.log("enter palette")
		GSE.StateManager.setCurrentState("Palette");
	}

	if(token_is_selected)
	{
		selected_token.data.x = cell_x * MG.tile_width 
		selected_token.data.y = cell_y * MG.tile_height
	}
}

function deleteToken(cell_x, cell_y)
{
	let token = token_map[hash(cell_x,cell_y)];
	if(token)
	{
		//remove an enemy
		Scene.RemoveNode(token.get_path())
		token_map[hash(cell_x, cell_y)] = undefined;
	}
	return token;
}

function pushAction(action)
{
	actions.push(action);

	console.log("pushAction :", action);
	console.log("actions :", actions);
}

function popAction()
{
	return actions.pop();
}

function invertAction()
{
	let action = popAction();
	console.log("invertAction :", action);

	if(!action) 
	{
		return;
	}

	switch (action.type)
	{
		case ActionType.Add:
			{
				//Get the placed token and delete it
				let pos = action.data.position
				console.log("Delete token at pos", pos);
				deleteToken(pos.x, pos.y);
				break;
			}
		case ActionType.Remove:
			{
				//Add the token back, at the position of deletetion
				let pos = action.data.position;
				let selected_palette = action.data.token.data.color;
				console.log("Add back a token at pos:", pos, " with color:", selected_palette);
				addToken(pos.x, pos.y, selected_palette);
				break;
			}
		case ActionType.Move:
			{
				//Remove the token from the last positin
				//Add it to the current position
				let lastpos = action.data.lastpos;
				let currentpos = action.data.currentpos;
				let token = action.data.token;

				deleteToken(lastpos.x, lastpos.y);
				addToken(currentpos.x, currentpos.y, token.data.color);

				break;
			}
	}
}

function addToken(cell_x, cell_y, selected_palette)
{
	let token = Scene.AddRect(
		{
			x: (cell_x * MG.tile_width), 
			y: (cell_y * MG.tile_height),
			w: MG.tile_width, 
			h: MG.tile_height, 
			color: selected_palette,
			hidden: false
		}
	)
	token_map[hash(cell_x, cell_y)] = token;
	return token;
}

//Render palette
function UpdatePalette()
{
	if(GSE.Input.IsKeyPressed(Input.keyCodes.Escape))
	{		
		selected_palette = "";
		GSE.StateManager.setCurrentState("Map");
	}

	/*
	Now u will be able to select a color and press a key
	to add a letter of that color to the map, 
	*/
	if(Input.IsMouseLeftPressed())
	{

		//if i have a valid selection, and i am in the map, and is an empty cell, i add a token to the field
		if(selected_palette != "")
		{
			let map_rect = Rect(0, 0, MG.mapW, MG.mapH);
			if(GSE.Collisions.CheckCollision(mouse_rect, map_rect))
			{
				if(!token_map[hash(cell_x,cell_y)])
				{
					//add a token
					let token = addToken(cell_x, cell_y, selected_palette);
					
					//Generate an action
					pushAction (new Action(ActionType.Add, {
						token : token,
						position : {x: cell_x, y: cell_y}
					}))
				}
			}
		}

		let props = Object.getOwnPropertyNames(palette_map);
		
		props.forEach(
		  function (val, idx, array) {
			// console.log("palette_map", val) //is the key
	
			//Check if u pressed one of this colors
			let token = palette_map[val];

			let object_rect = Rect(token.data.x, token.data.y, MG.tile_width / 2, MG.tile_height / 2);
			if(GSE.Collisions.CheckCollision(mouse_rect, object_rect))
			{
				console.log("clicked on ", token.data.color);
				selected_palette = token.data.color;
				return;
			}
		  }
		);

		console.log("Map:", token_map);
	}

	if(Input.IsKeyPressed(Input.keyCodes.KeyD))
	{
		let token = deleteToken(cell_x, cell_y);
		
		//Generate an action
		pushAction (new Action(ActionType.Remove, {
			token : token,
			position : {x: cell_x, y: cell_y}
		}))
	}
}

function RenderPalette()
{
	Scene.DrawText({
        x:16, 
        y:16,
        color:"black",
        align:"left",
        font: "Serif",
        size: "12",
		text: "Palette",
		id:"Coords"
    });
}