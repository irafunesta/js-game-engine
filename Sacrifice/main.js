MG = {
	mapW: 1188, //4
	mapH: 905, //3,
	viewW: window.innerWidth,
	viewH: window.innerHeight
};

var cardWidth = 750 / 9
var cardHeght = 1050 / 9
var fullcardWidth = 750 / 2.5
var fullcardHeight = 1050 / 2.5

LAYERS = {
	back: 90,
	zones: 80,
	cards: 70,
	preview: 60
}

cardEntities = [];
cardIstances = [];

var ZONES = [];

var zone_size = {
	w: 84,
	h: 98
}

var ZONES_NAMES = {
	COMMANDO: "Commando",
	DECK: "deck",
	HAND: "hand",
	DISCARD: "scarti"
}

PLAYER1 = new PlayerFactory("player1");
PLAYER2 = new PlayerFactory("player2");

; (function () {
	GSE.Init("myCanvas", MG.mapW, MG.viewH, MG.mapW, MG.viewH, false, InitGame, null, undefined, "black");
})();


function LoadCards() {
	for (var i in cardsDb) {
		var card = cardsDb[i];

		GSE.Scene.LoadTexture(card.id + "-std_img", card.img);
		GSE.Scene.LoadTexture(card.id + "-tap_img", card.img_tap);

		if (card.img_back) {
			GSE.Scene.LoadTexture(card.id + "-back_img", card.img_back);
		}
		else
		{
			card.back_image = "default_back_carte";
		}

		//Saves in Entities just the base card;
		//Useless, this have the same data as the card inside cardsDb, only needed to laod textures
		// cardEntities.push(card); //i have loaded all the images, and have the base card
	}
}

function ZoneFactory(name, entity, drawF, updateF) {
	return {
		name: name,
		order: [],
		entity: entity,
		limit: -1,
		player_ref:null,
		use_limit: false,
		addTop: function addTop(id) {
			if (this.use_limit) {
				if (this.order.length < this.limit) {
					this.order = [id].concat(this.order);
					return true;
				}
				return false;
			}
			else {
				this.order = [id].concat(this.order);
				return true;
			}
		},
		addEnd: function addEnd(id) {

			if (this.use_limit) {
				if (this.order.length < this.limit) {
					this.order.push(id);
					return true;
				}
				return false;
			}
			else {
				this.order.push(id);
				return true;
			}
		},
		shuffle: function shuffle() {
			// this.order.push(id);
		},
		draw: function () {
			drawF(this.order, this.entity, this.name);
		},
		update: function () {
			if (updateF) {
				updateF(this);
			}
		},
		getTop: function getTop() {
			var cardId = this.order.splice(0, 1);
			//Find the instance cards, 
			return cardIstances.find((card) => card.id == cardId);
		},
		removeId: function removeId(id) {
			console.log("removeId order:", this.order)
			var index = this.order.findIndex((card) => card == id);
			console.log("removeId index:", index)
			this.order.splice(index, 1);
			console.log("removeId order:", this.order)
		}
	}
}

function Counter(name, text, background, start = 0, updateF, drawF) {
	return {
		name: name,
		counter: start,
		update: () => {

			updateF(this);
		},
		draw: () => {

			drawF(this);
		}
	}
}

function GetCardById(id) {
	//cardEntities is not needed anymore

	return cardIstances.find((card) => card.id == id);
}

function GetCardDataById(id) {
	return cardsDb.find((card) => card.id == id);
}

function CreateFrontZone(player, position, color, name, mirror) {

	for (var i = 0; i < 4; i++) {

		var front_zone_spr = GSE.Scene.CreateSprite(null,
			color,
			position.x + (i * 100), //x 
			position.y, //y
			zone_size.w, //w
			zone_size.h, //h
			LAYERS.zones, //layer
			null, 
			mirror
		);

		var front_zone1_e = GSE.Scene.CreateEntity(name + i, front_zone_spr, null);

		var frontZone1 = new ZoneFactory(name + i, front_zone1_e, (cards, entity, name) => {
			for (var i in cards) {
				var card = GetCardById(cards[i]);
				if (card && dragged_card == null) {
					card.x = entity.x + (i * card.width);
					card.y = entity.y;
					card.setActive(true);
					card.scale = mirror;
				}
			}
		}, null);

		frontZone1.use_limit = true;
		frontZone1.limit = 1;

		// MG.front_zone1 = frontZone1;
		frontZone1.player_ref = player;
		player.front_row[i] = frontZone1;

		ZONES.push(frontZone1);
	}
}

function CreateBackZone(player, position, color, name, mirror) {

	for (var i = 0; i < 4; i++) {
		var front_zone_spr = GSE.Scene.CreateSprite(null,
			color,
			position.x + (i * 100), //x 
			position.y, //y
			zone_size.w, //w
			zone_size.h, //h
			LAYERS.zones, //layer
			null, 
			mirror
		);

		var front_zone1_e = GSE.Scene.CreateEntity(name + i, front_zone_spr, null);

		var backZone1 = new ZoneFactory(name + i, front_zone1_e, (cards, entity, name) => {
			for (var i in cards) {
				var card = GetCardById(cards[i]);
				if (card && dragged_card == null) {
					card.x = entity.x + (i * card.width);
					card.y = entity.y;
					card.setActive(true);
					card.scale = mirror
				}
			}
		}, null);

		backZone1.use_limit = true;
		backZone1.limit = 1;
		backZone1.player_ref = player;
		player.back_row[i] = backZone1;

		// MG.front_zone1 = backZone1;

		ZONES.push(backZone1);
	}
}

function CreateScartiZone(player, position, color, name, mirror) {

	let front_zone_spr = GSE.Scene.CreateSprite(null,
		color,
		position.x, //x 
		position.y, //y
		zone_size.w, //w
		zone_size.h, //h
		LAYERS.zones, //layer
		null,
		mirror
	);

	let front_zone1_e = GSE.Scene.CreateEntity(name, front_zone_spr, null);

	let scartiZone = new ZoneFactory(name, front_zone1_e, (cards, entity, name) => {
		for (var i in cards)
			// {
			// 	var card = GetCardById(cards[i]);
			// 	if(card && dragged_card == null){
			// 		card.x = entity.x + (i * card.width);
			// 		card.y = entity.y;
			// 		card.setActive(true);
			// 	}
			// }

			//draw only the last card
			if (cards.length > 0 && dragged_card == null) {
				var card = GetCardById(cards[0]);
				card.x = entity.x;
				card.y = entity.y;
				card.setActive(true);
				card.scale = mirror;

				//disable the other cards ? 
				for (var i = 1; i < cards.length; i++) {
					var card = GetCardById(cards[i]);
					if (card) {
						card.setActive(false);
					}
				}
			}

	}, null);

	scartiZone.use_limit = false;

	// MG.scarti_zone = scartiZone;

	scartiZone.player_ref = player;
	player.discard_zone = scartiZone;
	ZONES.push(scartiZone);
}

function CreateCommanderZone(player) {

	let offset = {x:284, y:478};
	let scale = {x:1, y:1}
	let color = "yellow"
	let name = "p1_commander_zone"

	if(player.name == "player2") {
		offset.x = 795.5;
		offset.y = 179;

		scale.x = -1;
		scale.y = -1;

		color = "LemonChiffon";
		name = "p2_commander_zone"
	}

	var front_zone_spr = GSE.Scene.CreateSprite(null,
		color,
		offset.x, //x 
		offset.y, //y
		zone_size.w, //w
		zone_size.h, //h
		LAYERS.zones, //layer,
		null, //type
		scale //scale
	);

	let front_zone1_e = GSE.Scene.CreateEntity("p1_commander_zone", front_zone_spr, null);

	let commandZone = new ZoneFactory(name, front_zone1_e, (cards, entity, name) => {
		for (var i in cards) {
			var card = GetCardById(cards[i]);
			if (card && dragged_card == null) {				
				card.x = entity.x + (i * card.width);
				card.y = entity.y;
				card.setActive(true);
				card.scale = scale;
			}
		}
	}, null);

	commandZone.use_limit = true;
	commandZone.limit = 1;
	commandZone.player_ref = player;

	// MG.command_zone = commandZone;

	ZONES.push(commandZone);

	return commandZone;
}

function CreateHandZone(player, position, color, name, mirror)
{
	let hand_zone_spr = GSE.Scene.CreateSprite(null,
		color,
		position.x, //x 
		position.y, //y
		zone_size.w * 10, //w
		zone_size.h, //h
		LAYERS.zones, //layer,
		null,
		mirror
	);

	let hand_zone_e = GSE.Scene.CreateEntity("p1_hand_zone", hand_zone_spr, null);

	let handZone = new ZoneFactory(name, hand_zone_e, function (cards, entity, name) {
		for (var i in cards) {
			var card = GetCardById(cards[i]);
							
				if (card && dragged_card == null) {
					card.x = entity.x + (i * card.width);
					card.y = entity.y;
					card.setActive(true);
					card.scale = mirror;

					if(player.name == PLAYER1.name) {
						//Use front image
						card.imageFile = card.std_img;
					}
					else
					{
						//Player2
						//Use the back image
						card.imageFile = card.back_image;
						
					}
				}
		}
	}, null);

	//Set player ref
	handZone.player_ref = player;
	//Add to player
	player.hand_zone = handZone;
	//Add to zones
	ZONES.push(handZone);
}

/*The sum of both decks are all the cards that are in game
//so in cardsEntities i just need the base card once, 
//and in cardsInstances i need all the instance of cards present
//both connected by the same ids ?
no , Entities has the cardId TD01/001IT
	 Instances has a new id and a reference to Entities

  Entities doesn't need to be a Entity in the scene, 
  Istances should be stored based on the player, so
  the PC acts on the player1 and the AI on player2

*/

function PlayerFactory(name, deck_zone = null, hand_zone = null) {
	return {
		name: name,
		command_zone: null, //Id of the command card
		deck_zone: deck_zone, //delegate to the deck zone
		hand_zone: hand_zone,
		discard_zone: null,
		front_row: [],
		back_row: []
	}
}

function CreateCardIstance(id, zone, counter = 0) {
	// let counter = counter || 0;
	console.log("CreateCardIstance id:", id);
	let card_data = GetCardDataById(id);

	let cardSpr = GSE.Scene.CreateSprite(card_data.id + "-std_img",
		null,
		MG.mapW, //x
		MG.mapH, //y
		cardWidth, //w
		cardHeght, //h
		LAYERS.cards //layer
	);

	let cardEntity = GSE.Scene.CreateEntity(card_data.id, cardSpr, UpdateCard, false, false);

	if (card_data.img_back) {
		cardEntity.can_flip = true;
		cardEntity.can_tap = false;
		cardEntity.back_image = card_data.id + "-back_img";
	}
	else {
		cardEntity.can_tap = true;
	}

	cardEntity.std_img = card_data.id + "-std_img";
	cardEntity.tapped_img = card_data.id + "-tap_img";
	cardEntity.tapped = false;
	cardEntity.flipped = false;
	cardEntity.zone = zone;

	//Extend card entity
	let ext_card = Object.assign({}, card_data, cardEntity, { id: id + "_" + counter.toString() });
	cardEntity = Object.assign(cardEntity, ext_card);

	return cardEntity;
}

function LoadDeck(deck, player) {
	//  Command Zone|Deck|Other ...
	// "TD01/021IT|1xTD01/001IT;2xTD01/027IT"

	let zones = deck.split("|");

	if (!zones || zones.length < 0) {
		console.error("Error on loading deck:", deck);
		return;
	}

	let command_card = zones[0];
	let deck_cards = zones[1];

	if (command_card) {
		let instace = CreateCardIstance(command_card, player.command_zone);
		let uid = cardIstances.push(instace);
		instace.id = instace.id + "_" + uid.toString();
		// player.command = instace.id; //Fill the command_zone zone for the player1

		//This could be a solution
		player.command_zone.addEnd(instace.id);
		// MG.command_zone.addEnd(instace.id);
	}

	if (deck_cards) {
		let cards_ids = deck_cards.split(";");
		if (!cards_ids || cards_ids.length < 0) {
			console.error("Can't read deck section");
			return;
		}

		for (let idNum in cards_ids) {
			let cardAndQ = cards_ids[idNum];
			let quantity_id = cardAndQ.split("x");
			if (!quantity_id || quantity_id.length < 0) {
				console.error("Can't read single card");
				return;
			}

			let num = quantity_id[0];
			let id = quantity_id[1];

			for (let i = 0; i < num; i++) {
				let instace = CreateCardIstance(id, player.deck_zone, i);
				let uid = cardIstances.push(instace);
				instace.id = instace.id + "_" + uid.toString();

				player.deck_zone.addEnd(instace.id);
				// MG.deck_zone.addEnd(instace.id);
			}
		}
	}
}

function InitGame(ctx) {
	GSE.Scene.LoadTexture("default_back_carte", './images/cards/retro_per_beta.jpg');

	LoadCards();

	// var timeBar = GSE.Scene.CreateSprite("",
	// 	'#999966',
	// 	15, //x
	// 	402, //y
	// 	(320 - 32), //w
	// 	48, //h
	// 	2, //layer
	// 	"textbox" //type
	// );

	//Textbox
	// var tb = GSE.Scene.CreateEntity("timeBar", timeBar, timeBarUpdate);
	// tb.totalWidth = tb.width;
	// tb.setActive(false);
	// MG.textbox = tb;
	// MG.textbox.focused = false;
	// MG.textbox.text = "";
	// MG.textbox.size = "24"

	
	GSE.Scene.LoadTexture("back_field", "./images/back_field.jpg");
	GSE.Scene.LoadTexture("back_field_flip", "./images/back_field_flip.jpg");

	var back_card = GSE.Scene.CreateSprite("default_back_carte",
		null,
		110, //x
		100, //y
		cardWidth, //w
		cardHeght, //h
		LAYERS.cards //layer
	);

	var back_card_flipped = GSE.Scene.CreateSprite("default_back_carte",
		null,
		110, //x
		100, //y
		cardWidth, //w
		cardHeght, //h
		LAYERS.cards, //layer,
		null,
		{ x: -1, y: -1 }
	);

	var field_size = { w: 1417 / 2.3, h: 709 / 2.3 }
	var leftBorder = (MG.mapW / 2) - (field_size.w / 2) - 10;
	var topBoder = 75;
	var offset = 300;
	var offset_x = 0;

	var back_field2 = GSE.Scene.CreateSprite("back_field",
		null,
		leftBorder, //x
		topBoder, //y
		field_size.w, //w
		field_size.h, //h
		LAYERS.back, //layer,
		null,
		{ x: -1, y: -1 }
	);

	var back_field = GSE.Scene.CreateSprite("back_field",
		null,
		leftBorder + offset_x, //x
		topBoder + offset, //y
		field_size.w, //w
		field_size.h, //h
		LAYERS.back //layer
	);

	var deck_zone_spr = GSE.Scene.CreateSprite(null,
		"red",
		800, //x 
		388, //y
		zone_size.w, //w
		zone_size.h, //h
		LAYERS.zones //layer
	);

	var p2_deck_zone_spr = GSE.Scene.CreateSprite(null,
		"red",
		285, //x 
		251, //y
		zone_size.w, //w
		zone_size.h, //h
		LAYERS.zones, //layer,
		null,//type,
		{x:-1, y:-1} //scale
	);

	// var hand_zone_spr = GSE.Scene.CreateSprite(null,
	// 	"blue",
	// 	200, //x 
	// 	663, //y
	// 	zone_size.w * 10, //w
	// 	zone_size.h, //h
	// 	LAYERS.zones //layer
	// );

	var fullCard = GSE.Scene.CreateSprite(null,
		null,
		10, //x
		105, //y
		fullcardWidth, //w
		fullcardHeight, //h
		LAYERS.preview, //layer,
		"image"
	);

	MG.full_card = GSE.Scene.CreateEntity("full_card", fullCard, null);

	GSE.Scene.CreateEntity("back_field", back_field, null);
	GSE.Scene.CreateEntity("back_field2", back_field2, null);
	var deck_zone_e = GSE.Scene.CreateEntity("p1_deck_zone", deck_zone_spr, null);
	var p2_deck_zone_e = GSE.Scene.CreateEntity("p2_deck_zone", p2_deck_zone_spr, null);
	
	// var hand_zone_e = GSE.Scene.CreateEntity("p1_hand_zone", hand_zone_spr, null);

	MG.p1_deck_size = GSE.Scene.UI.DrawText("0", deck_zone_e.x + 90, deck_zone_e.y + 50, "White", '22', 'center');
	MG.p2_deck_size = GSE.Scene.UI.DrawText("0", p2_deck_zone_e.x + 90, p2_deck_zone_e.y + 50, "White", '22', 'center');

	// var front_zone1_e = GSE.Scene.CreateEntity("p1_front_zone1", front_zone1_spr, null);


	MG.backDeck = GSE.Scene.CreateEntity("p1_back_deck", back_card, null);
	MG.p2_backDeck = GSE.Scene.CreateEntity("p2_back_deck", back_card_flipped, null);
	MG.p2_backDeck.setActive(true);
	MG.backDeck.setActive(false);

	var p1_deckZone = new ZoneFactory(ZONES_NAMES.DECK, deck_zone_e, function (cards, entity) {
			if (cards.length > 0) {
				MG.backDeck.setActive(true);
				MG.p1_deck_size.setActive(true);
				MG.backDeck.x = entity.x;
				MG.backDeck.y = entity.y;

				MG.p1_deck_size.text = cards.length.toString();
			}
			else {
				MG.backDeck.setActive(false);
				MG.p1_deck_size.setActive(false);
			}
		},
		function (zone) {
			if (checkBounds(zone.entity.x, zone.entity.y, zone.entity.width, zone.entity.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) &&
				GSE.Input.IsMouseLeftPressed() && dragged_card == null) {
				//The first card goes to the hand zone
				var topCard = zone.getTop();
				if(topCard){
					topCard.zone = PLAYER1.hand_zone;
					PLAYER1.hand_zone.addTop(topCard.id);
					console.log("Added card:", topCard.id, "zone:", PLAYER1.hand_zone.name);
				}
			}
		});
	
	var p2_deckZone = new ZoneFactory(ZONES_NAMES.DECK, p2_deck_zone_e, function (cards, entity) {
			if (cards.length > 0) {
				MG.p2_backDeck.setActive(true);
				MG.p2_deck_size.setActive(true);
				MG.p2_backDeck.x = entity.x;
				MG.p2_backDeck.y = entity.y;

				MG.p2_deck_size.text = cards.length.toString();
			}
			else {
				MG.p2_backDeck.setActive(false);
				MG.p2_deck_size.setActive(false);
			}
		},
		function (zone) {

			//The p2 deck is controlled by the AI
			if(GSE.Input.IsKeyPressed("w"))
			{
				//Mock ai draw
				let topCard = zone.getTop();
				if(topCard){
					topCard.zone = PLAYER2.hand_zone;
					PLAYER2.hand_zone.addTop(topCard.id);
					console.log("Added card:", topCard.id, "zone:", PLAYER2.hand_zone.name);
				}

			}
		});

	// var handZone = new ZoneFactory(ZONES_NAMES.HAND, hand_zone_e, function (cards, entity, name) {
	// 	for (var i in cards) {
	// 		var card = GetCardById(cards[i]);
	// 		if (card && dragged_card == null) {
	// 			card.x = entity.x + (i * card.width);
	// 			card.y = entity.y;
	// 			card.setActive(true);
	// 		}
	// 	}
	// }, null);

	// var frontZone1 = new ZoneFactory("front_zone1", front_zone1_e, (cards, entity, name) =>{
	// 	for(var i in cards)
	// 	{
	// 		var card = GetCardById(cards[i]);
	// 		if(card && dragged_card == null){
	// 			card.x = entity.x + (i * card.width);
	// 			card.y = entity.y;
	// 			card.setActive(true);
	// 		}
	// 	}
	// }, null);

	// frontZone1.use_limit = true;
	// frontZone1.limit = 1;

	MG.deck_zone = p1_deckZone;
	// MG.hand_zone = handZone;
	// MG.front_zone1 = frontZone1;

	ZONES.push(p1_deckZone);
	ZONES.push(p2_deckZone);

	// ZONES.push(handZone);

	// handZone.player_ref = PLAYER1;
	CreateHandZone(PLAYER1, {x:200, y:663}, "blue", "p1_hand_zone", {x:1, y:1});
	CreateHandZone(PLAYER2, {x:191, y:5}, "cyan", "p2_hand_zone", {x:-1, y:-1});

	// CreateFrontZoneP1();
	CreateFrontZone(PLAYER1, {x:386, y:406}, "pink", "p1_front_zone", {x:1, y:1});
	CreateFrontZone(PLAYER2, {x:403, y:251}, "hotpink", "p2_front_zone", {x:-1, y:-1});

	CreateBackZone(PLAYER1, {x:386, y:546}, "green", "p1_back_zone", {x:1, y:1});
	CreateBackZone(PLAYER2, {x:403, y:104}, "greenyellow", "p2_back_zone", {x:-1, y:-1});

	CreateScartiZone(PLAYER1, {x:797, y:554}, "grey", "p1_discard_zone", {x:1, y:1});
	CreateScartiZone(PLAYER2, {x:283.5, y:102}, "cadetblue", "p2_discard_zone", {x:-1, y:-1});


	//Read player1 deck
	// var player1Deck = "TD01/021IT|1xTD01/001IT;2xTD01/027IT";

	PLAYER1.deck_zone = p1_deckZone;
	// PLAYER1.hand_zone = handZone;
	PLAYER1.command_zone = CreateCommanderZone(PLAYER1);

	PLAYER2.deck_zone = p2_deckZone;
	PLAYER2.command_zone = CreateCommanderZone(PLAYER2);

	LoadDeck(SBlack1, PLAYER1);

	LoadDeck(SBlack1, PLAYER2);

	console.log("P1 cards:", PLAYER1.cards);

	// var testAnim = GSE.Scene.CreateEntity("test", testAnim, null);

	var mapState = GSE.StateManager.createState("Game", function mapUpdate() {
		if (GSE.Input.IsKeyPressed('Space')) {
			MG.inGame = false;
			GSE.StateManager.setCurrentState("Pause");
		}

		//Check the input ?
		if (MG.peerInput) {
			console.log("peer input:", MG.peerInput);
			//Connect and then clear or ask again
			connectToPeer(MG.peerInput);
			MG.peerInput = undefined;
		}

		if (GSE.Input.IsMouseLeftDown() && dragged_card != null) {
			dragged_card.x = GSE.Input.mouse.clientX - dragged_offset.x;
			dragged_card.y = GSE.Input.mouse.clientY - dragged_offset.y;
		}

		//Draw all zones
		for (var i in ZONES) {
			var zone = ZONES[i];
			zone.draw();
			zone.update();
			// console.log("zone ", zone.name, "- order:", zone.order);
		}

		if (!GSE.Input.IsMouseLeftDown() && dragged_card != null) {
			//Stop dragging
			dragged_card.layer = LAYERS.cards;
			console.log("x:", dragged_card.x, "y:", dragged_card.y);

			//I have dropped the card, if it is in a zone do something
			for (var i in ZONES) {
				var zone = ZONES[i];
				var entity = zone.entity;
				// console.log("check bounds:", entity.x, entity.y, entity.width, zone.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2);
				if (checkBounds(entity.x, entity.y, entity.width, entity.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true) {
					//The card has fall in a zone
					console.log("card fall in zone:", zone.name);
					//remove the card from the old zone
					if (zone.addTop(dragged_card.id)) {
						dragged_card.zone.removeId(dragged_card.id); //Remove the card and return the id

						dragged_card.zone = zone;
					}


				}
			}

			dragged_card = null;
		}

		//Updatefullcard
		var cardActive = cardIstances.filter((card) => card.active);
		MG.full_card.setActive(false);
		for (var i in cardActive) {
			var card = cardActive[i];
			// console.log("spam:", card);
			// console.log("check for fullcard:", card.x, card.y, card.width, card.height)
			if (checkBounds(card.x, card.y, card.width, card.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true) {
				// debugger;
				MG.full_card.setActive(true);
				MG.full_card.imageFile = card.imageFile;
				if (card.tapped) {
					MG.full_card.imageFile = card.std_img;
				}
				break;
			}
		}



		//Draw cards in hand
		// var cards_in_hand = cardEntities.filter((card) => card.zone == "hand");
		// console.log("cards_in_hand:", cards_in_hand)
		// for(var i in cards_in_hand)
		// {
		// 	// console.log("draw card in hand")
		// 	var card = cards_in_hand[i];
		// 	card.x = MG.hand_zone.x + (i * card.width);
		// 	card.y = MG.hand_zone.y;
		// 	card.setActive(true);
		// }

	}, function Render(ctx) {

		// if(MG.peerId)
		// 	GSE.rawDrawText(MG.peerId, 158, 64, 'white', '24', 'center', true);

		// GSE.rawDrawText(Math.round(MG.time).toString(), 20, 80, 'white', '24', 'left', true);
		// GSE.rawDrawText(Math.round(MG.scoreInterval).toString(), 20, 100, 'blue', '24', 'left', true);
		if (GSE.Window.fps && false) {
			GSE.rawDrawText(GSE.Window.fps.toString(), 20, 60, 'white', '24', 'left', true);
		}
		// GSE.rawDrawText(MG.highscore.toString(), GSE.Window.screenWidth / 2 + 128 + 64, 60, 'white', '20');
	});

	GSE.StateManager.setCurrentState("Game");

	// MG.highscore = localStorage.getItem("strInvHS") || 0;

	// GSE.Scene.setTileImage("./tilesets/Dungeon_A4.png");

	// GSE.Scene.map = [[0, 0, 1],[2, 3, 0],[10, 10, 9]];
}

// function GetCardInDeck()
// {
// 	return cardEntities.filter((card) => card.zone == "deck");
// }

var dragged_card = null;
var dragged_offset = null;

function UpdateCard(self) {

	//His a player card ?
	let isMineCard = self.zone.player_ref && self.zone.player_ref.name == PLAYER1.name;
	//Zoom on mouse over
	if (checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true) {
		if (GSE.Input.IsMouseLeftDown() && dragged_card == null && isMineCard) {
			dragged_card = self;
			dragged_offset = {
				x: GSE.Input.mouse.clientX - self.x,
				y: GSE.Input.mouse.clientY - self.y
			}
			self.layer = LAYERS.cards - 1;
		}

		if (GSE.Input.IsKeyPressed("t") && isMineCard) {
			if (self.tapped != undefined && self.can_tap && self.zone.name != PLAYER1.hand_zone.name) {
				self.tapped = !self.tapped;

				var tmp = self.width;
				self.width = self.height;
				self.height = tmp;
			}
		}

		if (GSE.Input.IsKeyPressed("f") && isMineCard) {
			if (self.flipped != undefined && self.can_flip) {
				self.flipped = !self.flipped;
			}
		}

		//Discard card, to the owner discard pile
		if (GSE.Input.IsKeyPressed("d") && isMineCard) {			
			//Rimuovi la carta da dove era prima
			self.zone.removeId(self.id);
			//mettila nella pila degli scarti del giocatore a cui apprtiene
			// console.log("self zone:", )
			self.zone.player_ref.discard_zone.addTop(self.id);
			//aggiorna la carta
			self.zone = self.zone.player_ref.discard_zone;
		}

		//Show full card
		// MG.full_card.setActive(true);
		// MG.full_card.imageFile = self.imageFile;
	}
	// else
	// {
	// 	if(MG.full_card.imageFile == self.imageFile)
	// 	{
	// 		MG.full_card.setActive(false);
	// 	}
	// }

	if (self.can_tap) {
		if (self.tapped) {
			self.imageFile = self.tapped_img
		}
		else {
			self.imageFile = self.std_img
		}
	}

	if (self.can_flip) {
		if (self.flipped) {
			self.imageFile = self.back_image
		}
		else {
			self.imageFile = self.std_img
		}
	}

	//Manage zone visibility
	switch (self.zone.name) {
		case ZONES_NAMES.DECK:
			self.setActive(false);
			break;
		case ZONES_NAMES.HAND:
			self.setActive(true);
			break;
		default:
			self.setActive(true);
			break;
	}
}

function checkBounds(x1, y1, w1, h1, x2, y2, w2, h2) {
	if (x1 < x2 + w2 &&
		x1 + w1 > x2 &&
		y1 < y2 + h2 &&
		h1 + y1 > y2) {
		// collision detected!
		return true;
	}

	return false;
}

function distance(x1, y1, x2, y2) {
	var dx = Math.pow((x2 - x1), 2);
	var dy = Math.pow((y2 - y1), 2);

	// console.log("dx:", dx, " dy:", dy);

	var dist = Math.sqrt(dx + dy);
	return Math.round(dist);
}

function UpdateBackCell(self) {
	if (GSE.Input.IsMouseLeftDown() &&
		checkBounds(self.x, self.y, self.width, self.height, GSE.Input.mouse.clientX, GSE.Input.mouse.clientY, 2, 2) == true) {
		self.color = "yellow";
	}
	else {
		self.color = "white";
	}
}

MG.Wait = function Wait(code, delay) {
	var w = delay || 1000;
	setTimeout(code, w);
};

function UpdateParticle(self) {
	self.y -= MG.particleUpSpeed;

	if (self.y < 0) {
		self.setActive(false);
	}
	if (Math.abs(self.y - self.animY) >= 60) {
		MG.particleUpSpeed += 0.25;
	}
}
