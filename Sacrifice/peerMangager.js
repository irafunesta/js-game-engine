var peer = new Peer();

	peer.on('open', function(id) {
		console.log('My peer ID is: ' + id);
		MG.peerId = id;

		// MG.peerInput = window.prompt("Inserisci peerId a cui connetersi");
	});	

	peer.on('error', function(err) {
		console.warn('Peer error: ' + err.type);
		if(err.type == "peer-unavailable") {
			alert("Giocatore non trovato");
			MG.peerInput = window.prompt("Inserisci peerId a cui connetersi");
		}
	});	

	peer.on('connection', function(connection) {
		console.log("peer is connected ");

		connection.on('data', function(data) {
			console.log('Received', data);
		});

		// Send messages
		connection.send('Hello!');

		MG.connection = connection;
    });
    
    MG.peer = peer;

    
function connectToPeer(peerId) {
	var connection = MG.peer.connect(peerId);
	console.log(connection);

	connection.on('open', function() {
		console.log("Connection open");
		// Receive messages
		connection.on('data', function(data) {
		  console.log('Received', data);
		});
	  
		// Send messages
		connection.send('Hello!');
	});

	connection.on('error', function(err) {
		console.warn('connection error:', err);
	});

	MG.connection = connection;	
}

function sendData(data)
{
	MG.connection.send(data);
}