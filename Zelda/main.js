MG = {
	mapW: 640,
	mapH: 480
};

//Alias
const Scene = GSE.Scene;

let wall, spike, sword;
let player_speed = 64;
let player_lives = 4;

let player = {
	ref: null,
	lives : 4,
	attack_time: 0.5,
	state : "move",
	last_direction : {x:0, y:1}
}

let x_diff = 0;
let y_diff = 0;

let walls = [];
let enemies = [];
let skip_input = false;
let timer = 0;

function InitGame(ctx)
{
	player.ref = Scene.AddRect({
		x: 32 * 5,y: 32 * 7,w:32, h:32, color:"green"
	});

	walls.push(Scene.AddRect({
		x: 32 * 10,y: 32*5,w:32, h:32, color:"grey"
	}))

	walls.push(Scene.AddRect({
		x: 32 * 13,y: 32*9,w:64, h:64, color:"grey"
	}))

	walls.push(Scene.AddRect({
		x: 32 * 11,y: 32*5,w:32, h:32, color:"grey"
	}))

	spike = Scene.AddRect({
		x: 32 * 5,y: 32 * 5,w:32, h:32, color:"red"
	});

	sword = Scene.AddRect({
		x: 32 * 5,y: 32 * 5,w:16, h:16, color:"silver", hidden:true
	}, player.ref.get_path());

	enemies.push(
		Scene.AddRect({
			x: 32 * 10,y: 32 * 1,w:32, h:32, color:"violet"
		}),
		Scene.AddRect({
			x: 32 * 10,y: 32 * 3,w:32, h:32, color:"violet"
		}),
	)
}

function Update(lastTick, delta)
{
	// console.log("Update delta:", delta);
	//Swing, or angle
	// console.log("delta:", delta);

	let pl_direction = {x:0, y:0};
	
	if(GSE.Input.IsKeyDown("d") || GSE.Input.IsKeyDown("right")) {
		pl_direction.x = 1;
		player.last_direction = pl_direction;
	}
	else if(GSE.Input.IsKeyDown("a") || GSE.Input.IsKeyDown("left")) {
		pl_direction.x = -1;
		player.last_direction = pl_direction;
	}
	else if(GSE.Input.IsKeyDown("w") || GSE.Input.IsKeyDown("up")) {
		pl_direction.y = -1;
		player.last_direction = pl_direction;
	}
	else if(GSE.Input.IsKeyDown("s") || GSE.Input.IsKeyDown("down")) {
		pl_direction.y = 1;
		player.last_direction = pl_direction;
	}
	
	if(GSE.Input.IsKeyDown("enter") || GSE.Input.IsKeyDown("space")) {

		//show sword
		sword.data.hidden = false;
		//align sword
		sword.data.x = player.ref.data.w / 2 + player.last_direction.x * 16;
		sword.data.y = player.ref.data.h / 2 + player.last_direction.y * 16;

		if(player.last_direction.x < 0)
		{
			sword.data.x += player.last_direction.x * 16;
			sword.data.y += player.last_direction.y * 16 - sword.data.h / 2;
		}
		if(player.last_direction.x > 0)
		{
			sword.data.y += player.last_direction.y * 16 - sword.data.h / 2;
		}

		if(player.last_direction.y < 0)
		{
			sword.data.x += player.last_direction.x * 16 - sword.data.w / 2;
			sword.data.y += player.last_direction.y * 16;
		}
		if(player.last_direction.y > 0)
		{
			sword.data.x += player.last_direction.x * 16 - sword.data.w / 2;
		}

		player.state = "attack"
	}

	if (player.state === "move") {
		player.ref.data.x += pl_direction.x * player_speed * delta;
		player.ref.data.y += pl_direction.y * player_speed * delta;
	}

	//move player out of wall ?
	for(let wall of walls) {
		if(GSE.Collisions.CheckCollision(player.ref.data, wall.data)) {
			let bounce = BounceFromWall(player.ref, wall);

			player.ref.data.x += bounce.x;
			player.ref.data.y += bounce.y;
		}
	}

	//Hit a spike
	if(GSE.Collisions.CheckCollision(spike.data, player.ref.data)) {
		let bounce = BounceFromWall(player.ref, spike, 32);
		player.ref.data.x += bounce.x;
		player.ref.data.y += bounce.y;
	}
	// shadow.data.hidden = true;

	if(sword.data.hidden == false)
	{
		if(timer >= player.attack_time) {
			timer = 0;
			sword.data.hidden = true;
			player.state = "move"
		}
		timer += 1* delta;
	}

	//Enemies will track my sword
	for(let enemy of enemies) {

		//The actual hitbox of the sword need to be added to the plauer
		sword_box = {
			x : player.ref.data.x + sword.data.x,
			y : player.ref.data.y + sword.data.y,
			w : 16, h:16
		} 

		if(GSE.Collisions.CheckCollision(sword_box, enemy.data)) {
			console.log("collision");
			if(sword.data.hidden === true) {
				return;
			}
			// Colliding whit my sword kills them
			enemy.data.hidden = true; //good for now //need an active thing
			enemy.data.x = -32
			enemy.data.y = -32

		}

		if(GSE.Collisions.CheckCollision(player.ref.data, enemy.data)) {
			console.log("collision player");		
			let bounce = BounceFromWall(player.ref, enemy, 20);
			player.ref.data.x += bounce.x;
			player.ref.data.y += bounce.y;
		}
	}
}

function BounceFromWall(player, wall, multiplier = 1)
{
	x_diff = 0;
	y_diff = 0;

	//fix X 
	//Case 1 if Px < Wx
	if(player.data.x < wall.data.x) {
		x_diff = (player.data.x + player.data.w) - wall.data.x;
		x_diff *= -1;
	}
	else if(player.data.x > wall.data.x) {
		x_diff = (wall.data.x + wall.data.w) - player.data.x;
	}
	//fix Y
	//Player over
	if(player.data.y < wall.data.y) {
		y_diff = (player.data.y + player.data.w) - wall.data.y;
		y_diff *= -1;		
	}
	else if(player.data.y > wall.data.y) {
		y_diff = (wall.data.y + wall.data.w) - player.data.y;		
	}

	let x_diff_abs = Math.abs(x_diff);
	let y_diff_abs = Math.abs(y_diff);

	let apply_x = 0;
	let apply_y = 0;

	let amount = 0;

	//extract x_diff
	if(x_diff_abs > 0 && y_diff_abs > 0) { //Both not 0 , use the lower
		if( x_diff_abs < y_diff_abs) {
			apply_x = 1;
			amount = x_diff;
		}
		else {
			apply_y = 1;
			amount = y_diff;
		}
	}
	else if(x_diff_abs > 0 && y_diff_abs == 0) { //Apply X
		apply_x = 1;
		amount = x_diff;
	}
	else if(y_diff_abs > 0 && x_diff_abs == 0) { //Apply Y
		apply_y = 1;
		amount = y_diff;
	}

	console.log("collision amount:", amount);
	console.log("collision multiplier:", multiplier);

	//I want a consistent bounce 
	// The overlap need to be different, 

	//Align to rect
	// player.data.x += amount * apply_x
	// player.data.y += amount * apply_y

	//Bounce the amount of pixel
	// if(multiplier > 1) {
	// 	player.data.x += multiplier * Math.sign(amount) * apply_x;
	// 	player.data.y += multiplier * Math.sign(amount) * apply_y;
	// }

	let x_val = amount * apply_x * multiplier;
	let y_val = amount * apply_y * multiplier;

	if(multiplier > 1) {
		x_val += multiplier * Math.sign(amount) * apply_x
		y_val += multiplier * Math.sign(amount) * apply_y
	}

	return {
		x: x_val,
		y: y_val
	}
}

function Render(delta)
{
	Scene.DrawText({
		x: 10, 
		y: 10,
		color:"white",
		text: player.ref.data.x
	})

	Scene.DrawText({
		x: 10, 
		y: 64,
		color:"red",
		size: "32",
		text: player_lives
	})

	// Scene.DrawText({
	// 	x: 10, 
	// 	y: 84,
	// 	color:"white",
	// 	size: "20",
	// 	text: x_diff.toString(3)
	// })

	// Scene.DrawText({
	// 	x: 10, 
	// 	y: 104,
	// 	color:"white",
	// 	size: "20",
	// 	text: y_diff.toString(3)
	// })
}

;(function () {
	GSE.Init("myCanvas", MG.mapW, MG.mapH, MG.mapW, MG.mapH, false, InitGame, Update, Render, "blue");
})();